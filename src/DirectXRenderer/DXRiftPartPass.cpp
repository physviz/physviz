#include "DXRiftPartPass.h"
#include "DXRenderData.h"
#include "DXRenderPassData.h"
#include "occulus_texture.h"
#include "DXRiftPassData.h"

//used by rift controller
EyeHack g_hack;

extern ovrSession g_session;
extern ovrGraphicsLuid g_gluid;

void DXRiftPartPass::InitPass(RenderPassData * _data)
{
	DXRenderPassData* DXRPD = (DXRenderPassData*)_data;
	m_eye = DXRPD->m_this_screen;

	m_RPD = DXRiftPassData::Get();

	DXBasePartPass::InitPass(_data);
}

DXRiftPartPass::~DXRiftPartPass()
{
}

RenderItems DXRiftPartPass::Render(RenderData * _data)
{
	if (!m_RPD->m_sInitRift || !g_session) return RenderItems::RI_NONE;

	DXRenderData* DXRD = (DXRenderData*)_data;

	ovrSessionStatus sessionStatus;
	ovr_GetSessionStatus(g_session, &sessionStatus);

	if (sessionStatus.IsVisible)
	{
		auto RTV = m_RPD->m_pEyeRenderTexture[m_eye]->GetRTV();
		DXRD->m_pImmediateContext->OMSetRenderTargets(1, &RTV, nullptr);// pEyeRenderTexture->GetDSV()  Depth should be this put null to get transparency right

		//set ViewPort to required values BASE_PASS actually uses it
		m_vp.Width = (float)m_RPD->m_eyeRenderViewport[m_eye].Size.w;    m_vp.Height = (float)m_RPD->m_eyeRenderViewport[m_eye].Size.h;
		m_vp.MinDepth = 0;   m_vp.MaxDepth = 1;
		m_vp.TopLeftX = (float)m_RPD->m_eyeRenderViewport[m_eye].Pos.x; m_vp.TopLeftY = (float)m_RPD->m_eyeRenderViewport[m_eye].Pos.y;
	}

	//go and do the base render stuff here
	DXBasePartPass::Render(_data);

	return RI_PARTICLES;
}

void DXRiftPartPass::SetDS(DXRenderPassData* _DXRPD)
{
}
