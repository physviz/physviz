#ifndef _DX_BASE_LINE_PASS_H_
#define _DX_BASE_LINE_PASS_H_
#include "DXBasePass.h"

class DXBaseLinePass : public DXBasePass
{
public:
	DXBaseLinePass(string _type = "BASE_LINE_PASS") :DXBasePass(_type)
	{
		m_order = 1;
	}

	void InitPass(RenderPassData* _data);

	virtual RenderItems Render(RenderData* _data);

protected:

};

#endif