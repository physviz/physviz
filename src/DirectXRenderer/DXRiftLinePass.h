#pragma once
#include "DXBaseLinePass.h"
#include "../RiftXBoxControl/RiftXBoxControl.h"

class DXRiftPassData;

class DXRiftLinePass : public DXBaseLinePass
{
public:
	DXRiftLinePass(string _type = "BASE_LINE_PASS") :DXBaseLinePass(_type)
	{
		m_order = 1;
	}
	~DXRiftLinePass();

	void InitPass(RenderPassData* _data);

	virtual RenderItems Render(RenderData* _data);

	virtual void* GetProj() override { return RiftXBoxController::Get()->GetProj(m_type); }

protected:

	DXRiftPassData* m_RPD;

	int eye = -1;
};
