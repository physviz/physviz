#ifndef _DX_BASE_PART_PASS_H_
#define _DX_BASE_PART_PASS_H_
#include "DXBasePass.h"
#include "ParticleCB.h"

class DXBasePartPass : public DXBasePass
{
public:
	DXBasePartPass(string _type = "BASE_PART_PASS") :DXBasePass(_type), m_vertsToDraw(6), m_partVB(nullptr), m_partIB(nullptr)
	{
		m_order = 2;
	}

	virtual ~DXBasePartPass();

	virtual void InitPass(RenderPassData* _data);

	virtual RenderItems Render(RenderData* _data);

	virtual int SetUpFromRenderable(void* _renderable, void* _data);

protected:

	virtual void SetDS(DXRenderPassData* _DXRPD);

	int m_displayType = 0;
	int m_vertsToDraw = 0;

	ParticleCB m_particleCB;

	ID3D11Buffer* m_partVB;
	UINT m_partVBstride, m_partVBoffset;
	ID3D11Buffer* m_partIB;
};

#endif