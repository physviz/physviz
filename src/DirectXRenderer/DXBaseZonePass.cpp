#include "DXBaseZonePass.h"
#include "DXRenderPassData.h"
#include "simpleVertexh.h"
#include "DXRenderData.h"
#include "../Storage/particleh.h"

DXBaseZonePass::~DXBaseZonePass()
{
}

void DXBaseZonePass::InitPass(RenderPassData* _data)
{
	DXRenderPassData* DXRPD = (DXRenderPassData*)_data;

	MakeVP(DXRPD);

	ID3DBlob* pBlob = nullptr;
	//compile vertex Shader
	CompileShaderFromFile(L"shaders/physvis_zones.hlsl", "VS", "vs_4_0", &pBlob);

	// Create the vertex shader
	HRESULT hr = DXRPD->m_pd3dDevice->CreateVertexShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &m_pVertexShader);

	// Define the vertex layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = DXRPD->m_pd3dDevice->CreateInputLayout(layout, numElements, pBlob->GetBufferPointer(),
		pBlob->GetBufferSize(), &m_pVertexLayout);

	//compile pixel shader
	CompileShaderFromFile(L"shaders/physvis_zones.hlsl", "PS", "ps_4_0", &pBlob);

	// Create the pixel shader
	hr = DXRPD->m_pd3dDevice->CreatePixelShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &m_pPixelShader);
	pBlob->Release();

	m_topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	//Setup Raster State
	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_NONE;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer state from the description we just filled out.
	hr = DXRPD->m_pd3dDevice->CreateRasterizerState(&rasterDesc, &m_pRasterState);

	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(blendDesc));

	D3D11_RENDER_TARGET_BLEND_DESC rtbd;
	ZeroMemory(&rtbd, sizeof(rtbd));

	// create an additive blend state.
	rtbd.BlendEnable = TRUE;
	rtbd.SrcBlend = D3D11_BLEND_SRC_ALPHA;
	rtbd.DestBlend = D3D11_BLEND_DEST_ALPHA;
	rtbd.BlendOp = D3D11_BLEND_OP_ADD;
	rtbd.SrcBlendAlpha = D3D11_BLEND_ONE;
	rtbd.DestBlendAlpha = D3D11_BLEND_ONE;
	rtbd.BlendOpAlpha = D3D11_BLEND_OP_ADD;
	rtbd.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.IndependentBlendEnable = false;
	blendDesc.RenderTarget[0] = rtbd;

	hr = DXRPD->m_pd3dDevice->CreateBlendState(&blendDesc, &m_pBlendState);

	//Render trans parent objects//
	m_blendFactor = new float[4];
	m_blendFactor[0] = m_blendFactor[1] = m_blendFactor[2] = m_blendFactor[3] = 0xff;

	//create m_partVB
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleVertex) * 24;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;

	// Grabbed from here
	//https://docs.microsoft.com/en-us/windows/uwp/gaming/applying-textures-to-primitives

	SimpleVertex verts[] =
	{
		{ DirectX::XMFLOAT3(-0.5f, 0.5f, -0.5f),  DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f) }, // +Y (top face)
		{ DirectX::XMFLOAT3(0.5f, 0.5f, -0.5f),  DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 0.0f) },
		{ DirectX::XMFLOAT3(0.5f, 0.5f,  0.5f),  DirectX::XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f) },
		{ DirectX::XMFLOAT3(-0.5f, 0.5f,  0.5f),  DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f) },

		{ DirectX::XMFLOAT3(-0.5f, -0.5f,  0.5f),  DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f) }, // -Y (bottom face)
		{ DirectX::XMFLOAT3(0.5f, -0.5f,  0.5f),  DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 0.0f) },
		{ DirectX::XMFLOAT3(0.5f, -0.5f, -0.5f),  DirectX::XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f) },
		{ DirectX::XMFLOAT3(-0.5f, -0.5f, -0.5f),  DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f) },

		{ DirectX::XMFLOAT3(0.5f,  0.5f,  0.5f),  DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f) }, // +X (right face)
		{ DirectX::XMFLOAT3(0.5f,  0.5f, -0.5f),  DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 0.0f) },
		{ DirectX::XMFLOAT3(0.5f, -0.5f, -0.5f),  DirectX::XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f) },
		{ DirectX::XMFLOAT3(0.5f, -0.5f,  0.5f),  DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f) },

		{ DirectX::XMFLOAT3(-0.5f,  0.5f, -0.5f),  DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f) }, // -X (left face)
		{ DirectX::XMFLOAT3(-0.5f,  0.5f,  0.5f),  DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 0.0f) },
		{ DirectX::XMFLOAT3(-0.5f, -0.5f,  0.5f),  DirectX::XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f) },
		{ DirectX::XMFLOAT3(-0.5f, -0.5f, -0.5f),  DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f) },

		{ DirectX::XMFLOAT3(-0.5f,  0.5f, 0.5f),  DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f) }, // +Z (front face)
		{ DirectX::XMFLOAT3(0.5f,  0.5f, 0.5f),  DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 0.0f) },
		{ DirectX::XMFLOAT3(0.5f, -0.5f, 0.5f),  DirectX::XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f) },
		{ DirectX::XMFLOAT3(-0.5f, -0.5f, 0.5f), DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f) },

		{ DirectX::XMFLOAT3(0.5f,  0.5f, -0.5f),  DirectX::XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f) }, // -Z (back face)
		{ DirectX::XMFLOAT3(-0.5f,  0.5f, -0.5f),  DirectX::XMFLOAT4(1.0f, 0.0f, 0.0f, 0.0f) },
		{ DirectX::XMFLOAT3(-0.5f, -0.5f, -0.5f),  DirectX::XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f) },
		{ DirectX::XMFLOAT3(0.5f, -0.5f, -0.5f),  DirectX::XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f) },
	};
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = verts;
	hr = DXRPD->m_pd3dDevice->CreateBuffer(&bd, &InitData, &m_partVB);
	m_partVBstride = sizeof(SimpleVertex);
	m_partVBoffset = 0;

	WORD indices[] =
	{
		0, 1, 2,
		0, 2, 3,

		4, 5, 6,
		4, 6, 7,

		8, 9, 10,
		8, 10, 11,

		12, 13, 14,
		12, 14, 15,

		16, 17, 18,
		16, 18, 19,

		20, 21, 22,
		20, 22, 23
	};

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(WORD) * ARRAYSIZE(indices);
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	InitData.pSysMem = indices;
	hr = DXRPD->m_pd3dDevice->CreateBuffer(&bd, &InitData, &m_partIB);

	SetDS(DXRPD);

	DXBasePass::InitPass(_data);
}


int DXBaseZonePass::SetUpFromRenderable(void * _renderable, void* _data)
{
	Particle* part = (Particle*)_renderable;
	if ((int)*(float*)(part->Get(PDN_DISPLAY_TYPE)) != m_displayType) return 0;

	DXRenderData* DXRD = (DXRenderData*)_data;

	float* pos = (float*)part->Get(PDN_POSITION);
	float* size = (float*)part->Get(PDN_SIZE);
	float* rot = (float*)part->Get(PDN_ROTATION);
	float* colour = (float*)part->Get(PDN_COLOUR);

	//m_particleCB.Particle = XMFLOAT4(size[0], size[1], *rot, 1.0f);
	m_particleCB.vMeshColor = XMFLOAT4(colour);

	XMFLOAT4X4 trans, scale;
	XMStoreFloat4x4(&trans, XMMatrixTranslation(pos[0], pos[1], pos[2]));
	XMStoreFloat4x4(&scale, XMMatrixScaling(size[0], size[1], size[2]));
	XMStoreFloat4x4(&m_particleCB.mWorld, XMMatrixTranspose(XMMatrixMultiply(XMLoadFloat4x4(&scale), XMLoadFloat4x4(&trans))));

	ID3D11ShaderResourceView* tex = (ID3D11ShaderResourceView*)part->Get(PDN_TEXTURE);
	DXRD->m_pImmediateContext->PSSetShaderResources(0, 1, &tex);
	DXRD->m_pImmediateContext->UpdateSubresource(DXRD->m_pCBChangesEveryFrame, 0, nullptr, &m_particleCB, 0, 0);
	return m_vertsToDraw;
}
