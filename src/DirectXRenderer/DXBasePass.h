#ifndef _DX_BASE_PASS_H_
#define _DX_BASE_PASS_H_
#include <d3d11_1.h>
#include "../DefaultRenderer/RenderPass.h"
#include "../DefaultRenderer/RenderItems.h"
#include "..\DirectXTK\Inc\SimpleMath.h"

using namespace std;
using namespace DirectX;
using namespace SimpleMath;

struct DXRenderPassData;

class DXBasePass : public RenderPass
{
public:
	DXBasePass(string _type);
	virtual ~DXBasePass();

	virtual void InitPass(RenderPassData* _data);

	virtual RenderItems Render(RenderData* _data);

	virtual int SetUpFromRenderable(void* _renderable, void* _data) { return 1; }

	virtual void* GetProj() override {	return &m_proj; }

protected:

	void MakeVP(DXRenderPassData* _DXRPD);
	virtual void SetDS(DXRenderPassData* _DXRPD) {};

	//g_pRasterState
	ID3D11RasterizerState*		m_pRasterState = nullptr;
	//g_pVertexShader2
	ID3D11VertexShader*			m_pVertexShader = nullptr;
	//g_pVertexLayout
	ID3D11InputLayout*			m_pVertexLayout = nullptr;
	//g_pPixelShader2
	ID3D11PixelShader*			m_pPixelShader = nullptr;
	//viewport
	D3D11_VIEWPORT m_vp;

	//type of primatives used
	D3D11_PRIMITIVE_TOPOLOGY m_topology;

	ID3D11BlendState*			m_pBlendState = nullptr;
	float*						m_blendFactor = nullptr;

	ID3D11DepthStencilState *	m_pDSState = nullptr;


	HRESULT DXBasePass::CompileShaderFromFile(WCHAR* _szFileName, LPCSTR _szEntryPoint, LPCSTR _szShaderModel, ID3DBlob** _ppBlobOut);

	Matrix m_proj;
};

#endif