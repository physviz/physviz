#pragma once
#include "DXBasePass.h"
#include "../RiftXBoxControl/RiftXBoxControl.h"

class DXRiftPassData;

class DXRiftPostPass : public DXBasePass
{
public:
	DXRiftPostPass(string _type = "RIFT_POST_PASS") :DXBasePass(_type)
	{
		m_order = 4;
	}
	~DXRiftPostPass();

	virtual RenderItems Render(RenderData* _data);

	void InitPass(RenderPassData* _data) {};

	virtual void PostRender(RenderData* _data);

	virtual void* GetProj() override { return RiftXBoxController::Get()->GetProj(m_type); }

protected:

};

