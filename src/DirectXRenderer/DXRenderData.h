#ifndef _DX_RENDER_DATA_H_
#include "../DefaultRenderer/RenderPass.h"
#include "../DirectXTK/Inc/SpriteBatch.h"
#include "../DirectXTK/Inc/SpriteFont.h"

using namespace DirectX;

struct DXRenderData : public RenderData
{
	ID3D11DeviceContext*		m_pImmediateContext;
	ID3D11Buffer*				m_pCBChangesEveryFrame;

	unique_ptr<SpriteBatch>	m_Sprites;
	unique_ptr<SpriteFont> m_Font;

	ID3D11SamplerState*			m_pSamplerLinear;

	UINT m_height, m_width;

	ID3D11Texture2D* m_BackBuffer = nullptr;
};

#endif