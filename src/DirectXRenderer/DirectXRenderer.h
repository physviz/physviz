#ifndef _DIRECT_X_RENDERER_H_
#define _DIRECT_X_RENDERER_H_
#include <d3d11_1.h>
#include "../DefaultRenderer/DefaultRenderer.h"
#include <map>

using namespace std;
using namespace DirectX;

struct DXRendererData : public RendererData
{
	HWND m_hWnd;
};

class DirectXRenderer : public DefaultRenderer
{
public:
	DirectXRenderer();
	~DirectXRenderer();

	//DefaultRenderer Interface

	bool Init(RendererData* _data) override;
	virtual void Render(float _dt, unsigned int _poll) override;
	void Shutdown() override;

	void* GetTexture(string _filename) override;

	virtual void LoadTexture(string _filename, string _package = "", bool _bCheckCase = false) override;
	virtual void FlushSideloadedTextures() override;

	void AddScene(Scene* _realScene) override;

	virtual bool WndProc(void* _hWnd, void* _msg, void* _wParam, void* _lParam) override;

	virtual void Gui_start() override;
	void PostRender() override;
	virtual void RenderGUIOnly() override;

	bool m_OculusMode = false;


protected:

	int m_width, m_height;

	bool m_bRenderTargetActive = false;
	void CreateRenderTarget();
	void CleanupRenderTarget();

	void InitRender(float _dt, unsigned int _poll) override;
	virtual void PrePass(RenderData* _RD) override;
	void InitTextures() override;

	D3D_DRIVER_TYPE				m_driverType = D3D_DRIVER_TYPE_NULL;
	D3D_FEATURE_LEVEL			m_featureLevel = D3D_FEATURE_LEVEL_11_0;

	ID3D11Device*				m_pd3dDevice = nullptr;

	ID3D11DeviceContext*		m_pImmediateContext = nullptr;
	ID3D11Device1*				m_pd3dDevice1 = nullptr;
	ID3D11DeviceContext1*		m_pImmediateContext1 = nullptr;
	IDXGISwapChain*				m_pSwapChain = nullptr;
	IDXGISwapChain1*			m_pSwapChain1 = nullptr;
	ID3D11RenderTargetView*		m_pRenderTargetView = nullptr;
	ID3D11Texture2D*			m_pDepthStencil = nullptr;
	ID3D11DepthStencilView*		m_pDepthStencilView = nullptr;

	ID3D11Buffer*				m_pCBNeverChanges = nullptr;
	ID3D11Buffer*				m_pCBChangeOnResize = nullptr;
	ID3D11Buffer*				m_pCBChangesEveryFrame = nullptr;

	ID3D11SamplerState*			m_pSamplerLinear = nullptr;

	ID3D11Texture2D* m_pBackBuffer;

	map<string, ID3D11ShaderResourceView*> m_textures;
	map<string, ID3D11ShaderResourceView*> m_sideloadedTextures;

	float m_shade = 0.0f;

};

#endif