#include "DXBasePartPass.h"
#include "DXRenderPassData.h"
#include "DXRenderData.h"
#include "simpleVertexh.h"
#include "../Storage/particleh.h"

DXBasePartPass::~DXBasePartPass()
{
	if (m_partVB) m_partVB->Release();
	if (m_partIB) m_partIB->Release();
}

void DXBasePartPass::InitPass(RenderPassData* _data)
{
	DXRenderPassData* DXRPD = (DXRenderPassData*)_data;

	MakeVP(DXRPD);

	ID3DBlob* pBlob = nullptr;
	//compile vertex Shader
	CompileShaderFromFile(L"shaders/physvis_default.fx", "VS", "vs_4_0", &pBlob);

	// Create the vertex shader
	HRESULT hr = DXRPD->m_pd3dDevice->CreateVertexShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &m_pVertexShader);

	// Define the vertex layout
	D3D11_INPUT_ELEMENT_DESC layout[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	// Create the input layout
	hr = DXRPD->m_pd3dDevice->CreateInputLayout(layout, numElements, pBlob->GetBufferPointer(),
		pBlob->GetBufferSize(), &m_pVertexLayout);

	//compile pixel shader
	CompileShaderFromFile(L"shaders/physvis_default.fx", "PS", "ps_4_0", &pBlob);

	// Create the pixel shader
	hr = DXRPD->m_pd3dDevice->CreatePixelShader(pBlob->GetBufferPointer(), pBlob->GetBufferSize(), nullptr, &m_pPixelShader);
	pBlob->Release();

	m_topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	//Setup Raster State
	D3D11_RASTERIZER_DESC rasterDesc;
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_NONE;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	// Create the rasterizer state from the description we just filled out.
	hr = DXRPD->m_pd3dDevice->CreateRasterizerState(&rasterDesc, &m_pRasterState);

	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(blendDesc));

	D3D11_RENDER_TARGET_BLEND_DESC rtbd;
	ZeroMemory(&rtbd, sizeof(rtbd));

	// create an additive blend state.
	rtbd.BlendEnable = TRUE;
	rtbd.SrcBlend = D3D11_BLEND_SRC_ALPHA;
	rtbd.DestBlend = D3D11_BLEND_DEST_ALPHA;
	rtbd.BlendOp = D3D11_BLEND_OP_ADD;
	rtbd.SrcBlendAlpha = D3D11_BLEND_ONE;
	rtbd.DestBlendAlpha = D3D11_BLEND_ONE;
	rtbd.BlendOpAlpha = D3D11_BLEND_OP_ADD;
	rtbd.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.IndependentBlendEnable = false;
	blendDesc.RenderTarget[0] = rtbd;

	hr = DXRPD->m_pd3dDevice->CreateBlendState(&blendDesc, &m_pBlendState);

	//Render trans parent objects//
	m_blendFactor = new float[4];
	m_blendFactor[0] = m_blendFactor[1] = m_blendFactor[2] = m_blendFactor[3] = 0xff;

	//create m_partVB
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(SimpleVertex) * 4;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	D3D11_SUBRESOURCE_DATA InitData;

	SimpleVertex verts[] =
	{
		{ XMFLOAT3(-0.5f, -0.5f, 0.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(0.5f, -0.5f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(0.5, 0.5f, 0.0f), XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f) },
		{ XMFLOAT3(-0.5f, 0.5f, 0.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f) }
	};
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = verts;
	hr = DXRPD->m_pd3dDevice->CreateBuffer(&bd, &InitData, &m_partVB);
	m_partVBstride = sizeof(SimpleVertex);
	m_partVBoffset = 0;

	WORD indices[] =
	{
		3, 1, 0,
		2, 1, 3
	};

	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(WORD) * 36;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	InitData.pSysMem = indices;
	hr = DXRPD->m_pd3dDevice->CreateBuffer(&bd, &InitData, &m_partIB);

	SetDS(DXRPD);

	DXBasePass::InitPass(_data);
}


RenderItems DXBasePartPass::Render(RenderData* _data)
{
	DXBasePass::Render(_data);
	DXRenderData* DXRD = (DXRenderData*)_data;

	DXRD->m_pImmediateContext->PSSetSamplers(0, 1, &DXRD->m_pSamplerLinear);
	DXRD->m_pImmediateContext->IASetVertexBuffers(0, 1, &m_partVB, &m_partVBstride, &m_partVBoffset);
	DXRD->m_pImmediateContext->IASetIndexBuffer(m_partIB, DXGI_FORMAT_R16_UINT, 0);

	return RI_PARTICLES;
}

int DXBasePartPass::SetUpFromRenderable(void * _renderable, void* _data)
{
	Particle* part = (Particle*)_renderable;
	if ((*(float*)part->Get(PDN_DISPLAY_TYPE)) != m_displayType) return 0;

	DXRenderData* DXRD = (DXRenderData*)_data;

	float* pos = (float*)part->Get(PDN_POSITION);
	float* size = (float*)part->Get(PDN_SIZE);
	float* rot = (float*)part->Get(PDN_ROTATION);
	float* colour = (float*)part->Get(PDN_COLOUR);

	m_particleCB.Particle = XMFLOAT4(size[0], size[1], *rot, 1.0f);
	m_particleCB.vMeshColor = XMFLOAT4(colour);

	XMStoreFloat4x4(&m_particleCB.mWorld, XMMatrixTranspose(XMMatrixTranslation(pos[0], pos[1], pos[2])));

	ID3D11ShaderResourceView* tex = (ID3D11ShaderResourceView*)part->Get(PDN_TEXTURE);
	DXRD->m_pImmediateContext->PSSetShaderResources(0, 1, &tex);
	DXRD->m_pImmediateContext->UpdateSubresource(DXRD->m_pCBChangesEveryFrame, 0, nullptr, &m_particleCB, 0, 0);
	return m_vertsToDraw;
}

void DXBasePartPass::SetDS(DXRenderPassData * _DXRPD)
{
	D3D11_DEPTH_STENCIL_DESC dsDesc;
	// Depth test parameters
	dsDesc.DepthEnable = true;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

	// Stencil test parameters
	dsDesc.StencilEnable = true;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;

	// Stencil operations if pixel is front-facing
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Stencil operations if pixel is back-facing
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	// Create depth stencil state
	HRESULT hr = _DXRPD->m_pd3dDevice->CreateDepthStencilState(&dsDesc, &m_pDSState);
}