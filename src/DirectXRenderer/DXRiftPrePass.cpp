#include "DXRiftPrePass.h"
#include "DXRiftPassData.h"
#include "DXRenderPassData.h"
#include "DXRenderData.h"

//used by rift controller
extern EyeHack g_hack;
extern ovrSession g_session;

DXRiftPrePass::~DXRiftPrePass()
{
}

void DXRiftPrePass::InitPass(RenderPassData* _data)
{
	DXRenderPassData* DXRPD = (DXRenderPassData*)_data;

	m_RPD = DXRiftPassData::Get();

	g_hack.set = true;
	g_hack.EyeRenderPose = &m_RPD->m_EyeRenderPose[0];
	g_hack.eyeRenderDesc = &m_RPD->m_eyeRenderDesc[0];

	if (!m_RPD->m_sInitRift)
	{
		m_RPD->m_sInitRift = true;

		m_RPD->m_hmdDesc = ovr_GetHmdDesc(g_session);
	}

	if (m_RPD->m_sInitRift)
	{
		for (int eye = 0; eye < 2; ++eye)
		{
			ovrSizei idealSize = ovr_GetFovTextureSize(g_session, (ovrEyeType)eye, m_RPD->m_hmdDesc.DefaultEyeFov[eye], 1.0f);
			m_RPD->m_pEyeRenderTexture[eye] = new OculusTexture();
			m_RPD->m_pEyeRenderTexture[eye]->Init(g_session, idealSize.w, idealSize.h, m_RPD->m_msaaRate, true, DXRPD->m_pd3dDevice);

			m_RPD->m_eyeRenderViewport[eye].Pos.x = 0;
			m_RPD->m_eyeRenderViewport[eye].Pos.y = 0;
			m_RPD->m_eyeRenderViewport[eye].Size = idealSize;
		}

		// Create a mirror to see on the monitor.
		m_RPD->m_mirrorTexture = nullptr;
		m_RPD->m_mirrorDesc = {};
		m_RPD->m_mirrorDesc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
		m_RPD->m_mirrorDesc.Width = DXRPD->m_width;
		m_RPD->m_mirrorDesc.Height = DXRPD->m_height;
		m_RPD->m_mirrorDesc.MirrorOptions = ovrMirrorOption_Default;
		ovrResult result = ovr_CreateMirrorTextureWithOptionsDX(g_session, DXRPD->m_pd3dDevice, &m_RPD->m_mirrorDesc, &m_RPD->m_mirrorTexture);

		// FloorLevel will give tracking poses where the floor height is 0
		ovr_SetTrackingOriginType(g_session, ovrTrackingOrigin_FloorLevel);
	}
}

RenderItems DXRiftPrePass::Render(RenderData* _data)
{
	if (!m_RPD->m_sInitRift || !g_session) return RenderItems::RI_NONE;

	DXRenderData* DXRD = (DXRenderData*)_data;

	ovrSessionStatus sessionStatus;
	ovr_GetSessionStatus(g_session, &sessionStatus);

	//should handle in controls?
	if (sessionStatus.ShouldRecenter)
		ovr_RecenterTrackingOrigin(g_session);

	if (sessionStatus.IsVisible)
	{
		// Call ovr_GetRenderDesc each frame to get the ovrEyeRenderDesc, 
		// as the returned values (e.g. HmdToEyePose) may change at runtime.		
		m_RPD->m_eyeRenderDesc[0] = ovr_GetRenderDesc(g_session, ovrEye_Left, m_RPD->m_hmdDesc.DefaultEyeFov[0]);
		m_RPD->m_eyeRenderDesc[1] = ovr_GetRenderDesc(g_session, ovrEye_Right, m_RPD->m_hmdDesc.DefaultEyeFov[1]);

		// Get both eye poses simultaneously, with IPD offset already included. 
		ovrPosef HmdToEyePose[2] = { m_RPD->m_eyeRenderDesc[0].HmdToEyePose,	m_RPD->m_eyeRenderDesc[1].HmdToEyePose };
		ovr_GetEyePoses(g_session, m_RPD->m_frameIndex, ovrTrue, HmdToEyePose, m_RPD->m_EyeRenderPose, &m_RPD->m_sensorSampleTime);

		// sensorSampleTime is fed into the layer later
		for (int eye = 0; eye < 2; ++eye)
		{
			
				float BG[4] = { _data->m_shade, _data->m_shade,_data->m_shade,1.0f }; // Important that alpha=0, if want pixels to be transparent, for manual layers
				auto RTV = m_RPD->m_pEyeRenderTexture[eye]->GetRTV();
				DXRD->m_pImmediateContext->ClearRenderTargetView(m_RPD->m_pEyeRenderTexture[eye]->GetRTV(), BG);
				if (m_RPD->m_pEyeRenderTexture[eye]->GetDSV())
					DXRD->m_pImmediateContext->ClearDepthStencilView(m_RPD->m_pEyeRenderTexture[eye]->GetDSV(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1, 0);
			
		}
	}

	for (int eye = 0; eye < 2; ++eye)
	{
		ovrMatrix4f p = ovrMatrix4f_Projection(m_RPD->m_eyeRenderDesc[eye].Fov, 0.2f, 1000.0f, ovrProjection_None);
		m_RPD->m_posTimewarpProjectionDesc[eye] = ovrTimewarpProjectionDesc_FromProjection(p, ovrProjection_None);
	}

	return RI_NONE;
}
