#pragma once
#include "DXBaseLabelPass.h"
#include "../RiftXBoxControl/RiftXBoxControl.h"

class DXRiftPassData;

class DXRiftLabelPass : public DXBaseLabelPass
{
public:
	DXRiftLabelPass(string _type = "BASE_LABEL_PASS") :DXBaseLabelPass(_type)
	{
		m_order = 3;
	}
	~DXRiftLabelPass();

	void InitPass(RenderPassData* _data);

	virtual RenderItems Render(RenderData* _data);

	 virtual void* GetProj() override { return RiftXBoxController::Get()->GetProj(m_type); }

protected:

	DXRiftPassData* m_RPD;

	int eye = -1;
};
