#pragma once
#include "DXBasePass.h"
#include "../RiftXBoxControl/RiftXBoxControl.h"

class DXRiftPassData;

class DXRiftPrePass : public DXBasePass
{
public:
	DXRiftPrePass(string _type = "RIFT_PRE_PASS") :DXBasePass(_type)
	{
		m_order = 0;
	}
	~DXRiftPrePass();

	void InitPass(RenderPassData* _data);

	virtual RenderItems Render(RenderData* _data);

	virtual void* GetProj() override { return RiftXBoxController::Get()->GetProj(m_type); }

protected:

	DXRiftPassData* m_RPD;
};

