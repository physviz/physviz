#include "DXRiftPostPass.h"
#include "DXRiftPassData.h"
#include "DXRenderPassData.h"
#include "DXRenderData.h"

extern ovrSession g_session;

DXRiftPostPass::~DXRiftPostPass()
{
}

RenderItems DXRiftPostPass::Render(RenderData* _data)
{
	return RenderItems::RI_NONE;
}



void DXRiftPostPass::PostRender(RenderData* _data)
{
	DXRiftPassData* m_RPD = DXRiftPassData::Get();
	if (!m_RPD->m_sInitRift || !g_session) return;

	DXRenderData* DXRD = (DXRenderData*)_data;

	ovrSessionStatus sessionStatus;
	ovr_GetSessionStatus(g_session, &sessionStatus);

	if (sessionStatus.DisplayLost)return;


	if (m_RPD->m_sInitRift)
	{
		for (int eye = 0; eye < 2; ++eye)
		{
			m_RPD->m_pEyeRenderTexture[eye]->Commit();

			// Initialize our single full screen Fov layer.
			if (eye == 0)
			{
				m_RPD->m_ld = {};
				m_RPD->m_ld.Header.Type = ovrLayerType_EyeFovDepth;
				m_RPD->m_ld.Header.Flags = 0;
			}

			m_RPD->m_ld.ColorTexture[eye] = m_RPD->m_pEyeRenderTexture[eye]->TextureChain;
			m_RPD->m_ld.DepthTexture[eye] = m_RPD->m_pEyeRenderTexture[eye]->DepthTextureChain;
			m_RPD->m_ld.Viewport[eye] = m_RPD->m_eyeRenderViewport[eye];
			m_RPD->m_ld.Fov[eye] = m_RPD->m_hmdDesc.DefaultEyeFov[eye];
			m_RPD->m_ld.RenderPose[eye] = m_RPD->m_EyeRenderPose[eye];
			m_RPD->m_ld.SensorSampleTime = m_RPD->m_sensorSampleTime;
			m_RPD->m_ld.ProjectionDesc = m_RPD->m_posTimewarpProjectionDesc[eye];
		}

		ovrLayerHeader* layers = &m_RPD->m_ld.Header;
		auto result = ovr_SubmitFrame(g_session, m_RPD->m_frameIndex, nullptr, &layers, 1);
		m_RPD->m_frameIndex++;

		// Render mirror
		ID3D11Texture2D* tex = nullptr;
		ovr_GetMirrorTextureBufferDX(g_session, m_RPD->m_mirrorTexture, IID_PPV_ARGS(&tex));

		DXRD->m_pImmediateContext->CopyResource(DXRD->m_BackBuffer, tex);
		tex->Release();
	}
}
