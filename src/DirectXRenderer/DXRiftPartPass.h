#pragma once
#include "DXBasePartPass.h"
#include "../LibOVR/Include/OVR_CAPI.h"
#include "../LibOVR/Include/OVR_CAPI_D3D.h"
#include "../Public/oculus_structs.h"
#include "../RiftXBoxControl/RiftXBoxControl.h"

struct OculusTexture;
class DXRiftPassData;

class DXRiftPartPass :
	public DXBasePartPass
{
public:

	DXRiftPartPass(string _type = "RIFT_PART_PASS") :DXBasePartPass(_type)
	{
		m_order = 2;
	}

	void InitPass(RenderPassData* _data);
	~DXRiftPartPass();

	virtual RenderItems Render(RenderData* _data);

	virtual void* GetProj() override { return RiftXBoxController::Get()->GetProj(m_type); }

protected:

	virtual void SetDS(DXRenderPassData* _DXRPD);

	DXRiftPassData* m_RPD;

	int m_eye = -1;
};

