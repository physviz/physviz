#ifndef _DX_BASE_ZONE_PASS_H_
#define _DX_BASE_ZONE_PASS_H_
#include "DXBasePartPass.h"

class DXBaseZonePass :	public DXBasePartPass
{
public:
	DXBaseZonePass(string _type = "BASE_ZONE_PASS") :DXBasePartPass(_type)
	{
		m_vertsToDraw = 36;
		m_order = 2;
		m_displayType = 1;
	}

	virtual ~DXBaseZonePass();

	virtual void InitPass(RenderPassData* _data);

	virtual int SetUpFromRenderable(void* _renderable, void* _data);
};
#endif

