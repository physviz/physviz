#include "DXScene.h"
#include "../Storage/scene.h"
#include "../Storage/particleh.h"
#include "../Storage/line.h"
#include "simpleVertexh.h"
#include "DXRenderData.h"
#include "../DefaultControl/controllers.h"
#include "../DirectXTK/Inc/SpriteBatch.h"
#include "../DirectXTK/Inc/SpriteFont.h"
#include <iostream>
#include <math.h>

DXScene::DXScene(Scene* _RS, ID3D11Device* _device) :DefaultRenderableScene(_RS), m_lineVB(nullptr), m_lineData(nullptr)
{
	m_lineData = new SimpleVertex[2 * _RS->GetLines()->size()];

	for (UINT i = 0; i < _RS->GetLines()->size(); i++)
	{
		(*_RS->GetLines())[i]->Render(&m_lineData[i * 2]);
	}

	m_lineVBstride = sizeof(SimpleVertex);
	m_lineVBoffset = 0;

	//create m_lineVB
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(SimpleVertex) * 2 * (UINT)_RS->GetLines()->size();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bd.MiscFlags = 0;
	bd.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&InitData, sizeof(InitData));
	InitData.pSysMem = m_lineData;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;
	HRESULT hr = _device->CreateBuffer(&bd, &InitData, &m_lineVB);

	m_scene = _RS;
}

DXScene::~DXScene()
{
	//clean up DirectX stuff
	if (m_lineVB) m_lineVB->Release();
}

void DXScene::DrawParticle(Particle* _part, void* _data)
{
	if (*(bool*)_part->Get(PDN_HIDE))
	{
		return;
	}
	else if (DrawBecauseofSubScreens(_part, _data))
	{
		DXRenderData* DXRD = (DXRenderData*)_data;

		if (int verts = DXRD->m_pass->SetUpFromRenderable(_part, _data))
		{
			DXRD->m_pImmediateContext->VSSetConstantBuffers(2, 1, &DXRD->m_pCBChangesEveryFrame);
			DXRD->m_pImmediateContext->PSSetConstantBuffers(2, 1, &DXRD->m_pCBChangesEveryFrame);
			DXRD->m_pImmediateContext->DrawIndexed(verts, 0, 0);
		}
	}
}

bool DXScene::DrawBecauseofSubScreens(Particle* _part, void* _data)
{
	DXRenderData* DXRD = (DXRenderData*)_data;

	int* partsubscreen = (int*)_part->Get(PDN_SUBSCREEN);
	if (*partsubscreen >= 0) //this particle cares about subscreens and whe're on an XBox subscreen
	{

		//if (!Controllers::Get()->ShowControlParticles()) return false;//we aren't displaying control Particles

		int currentsubscreen = CurrentSubScreen(DXRD->m_pass->GetType());

		if (currentsubscreen == -1) return false;
		//if this is my home screen don't draw me or not a subscreen pass don't draw me

		//this pass actually has subscreens
		// we aren't on this particles screen
		// and its one of the subscreens we are currently drawing
		return (currentsubscreen != -1) && (*partsubscreen != currentsubscreen) && (*partsubscreen <= (NumSubScreens(DXRD->m_pass->GetType()) - 1));
	}
	else
	{
		return true;
	}
}

void DXScene::DrawLine(Line* _line, void* _data)
{
	//Grab from line stuff
}


void DXScene::DrawParticles(RenderData* _data)
{
	DXRenderData* DXRD = (DXRenderData*)_data;

	vector<Particle*>* parts = m_scene->GetParticles();
	int size = parts->size();

	for (int i = 0; i < size; i++)
	{
		DrawParticle((*parts)[i], DXRD);
	}

}

void DXScene::DrawLines(RenderData* _data)
{
	DXRenderData* DXRD = (DXRenderData*)_data;
	vector<Line*>* lines = m_scene->GetLines();
	int size = lines->size();

	int numDraw = 0;
	for (int i = 0; i < size; i++)
	{
		if ((*lines)[i]->CanDraw())
		{
			(*lines)[i]->Render(&m_lineData[numDraw * 2]);
			numDraw++;
		}
	}

	if (numDraw == 0)
	{
		//nothing to see here go home
		return;
	}

	D3D11_MAPPED_SUBRESOURCE mappedResource;
	ZeroMemory(&mappedResource, sizeof(D3D11_MAPPED_SUBRESOURCE));
	//	Disable GPU access to the vertex buffer data.
	DXRD->m_pImmediateContext->Map(m_lineVB, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	//	Update the vertex buffer here.
	memcpy(mappedResource.pData, m_lineData, sizeof(SimpleVertex) * 2 * numDraw);
	//	Reenable GPU access to the vertex buffer data.
	DXRD->m_pImmediateContext->Unmap(m_lineVB, 0);

	DXRD->m_pImmediateContext->IASetVertexBuffers(0, 1, &m_lineVB, &m_lineVBstride, &m_lineVBoffset);

	DXRD->m_pImmediateContext->Draw(2 * numDraw, 0);
}

static wchar_t* charToWChar(const char* _text)
{
	size_t size = strlen(_text) + 1;
	size_t convertedChars = 0;

	static wchar_t* wa = NULL;
	if (wa)
	{
		delete[] wa;
		wa = NULL;
	}
	wa = new wchar_t[size];
	mbstowcs_s(&convertedChars, wa, size, _text, _TRUNCATE);
	return wa;
}

void DXScene::DrawLabels(RenderData* _data)
{
	DXRenderData* DXRD = (DXRenderData*)_data;

	vector<Particle*>* parts = m_scene->GetParticles();
	int size = parts->size();

	Matrix viewProj = Controllers::Get()->GetView(DXRD->m_pass->GetType()) * *(Matrix*)DXRD->m_pass->GetProj();

	ID3D11BlendState* pBlendState;
	DXRD->m_pImmediateContext->OMGetBlendState(&pBlendState, nullptr, nullptr);
	//if I use that WIl have to release it afterwards or get a memory leak
	D3D11_VIEWPORT VP;
	UINT num = 1;
	DXRD->m_pImmediateContext->RSGetViewports(&num, &VP);

	DXRD->m_Sprites->Begin(SpriteSortMode_Immediate, pBlendState);//blendstate used so can fade out the text
	int drawn = 0;
	int subscreen = CurrentSubScreen(DXRD->m_pass->GetType()) + 1;
	int uLabelDistance = m_labelDistance[subscreen];
	for (int i = 0; i < size; i++)
	{
		Vector3 pos;
		if (!(*(bool*)(*parts)[i]->Get(PDN_HIDE)) && (*parts)[i]->GetText(Controllers::Get()->GetPos(DXRD->m_pass->GetType()), pos, uLabelDistance))
		{
			if (isnan(pos.x) || isnan(pos.y) || isnan(pos.z))
			{
				cout << "INVALID POS : " << (*parts)[i]->GetName() << endl;;
				continue;
			}

			drawn++;
			Vector3 viewPos = Vector3::Transform(pos, Controllers::Get()->GetView(DXRD->m_pass->GetType()));
			if (viewPos.z < 0.0f) //only want to show what's in front of me!
			{
				Vector3 screenPos = Vector3::Transform(pos, viewProj);
				Color Show = Colors::Yellow;
				float distance = Vector3::Distance(Controllers::Get()->GetPos(DXRD->m_pass->GetType()), pos);
				if (distance > 0.75f * uLabelDistance) //add a nice fade out as they get further away (NO POPPING!)
				{
					Show = Color::Lerp(Color(1.0f, 1.0f, 0.0f, 1.0f), Color(1.0f, 1.0f, 1.0f, 0.0f), (distance - 0.75f * uLabelDistance) / (0.25f * uLabelDistance));
				}
				if (DrawBecauseofSubScreens((*parts)[i], _data))
				{
					DXRD->m_Font->DrawString(DXRD->m_Sprites.get(), charToWChar((*parts)[i]->GetName().c_str()), XMFLOAT2(0.5f * VP.Width * (1.0f + screenPos.x), 0.5f * VP.Height * (1.0f - screenPos.y)), Show, 0.0f, XMFLOAT2(0.0f, 0.0f), 0.25f);
				}
			}

			if (drawn > m_maxLabels)
			{
				exit;
			}
		}
	}
	if (Controllers::GetTarget())
	{
		string label = "Focus : " + m_scene->GetFocusLabel();
		DXRD->m_Font->DrawString(DXRD->m_Sprites.get(), charToWChar(label.c_str()), XMFLOAT2(0.0f, 20.0f), Color(1.0f, 1.0f, 1.0f, 1.0f), 0.0f, XMFLOAT2(0.0f, 0.0f), 0.4f);
	}

	DXRD->m_Sprites->End();


	if (drawn < m_maxLabels)
	{
		m_labelDistance[subscreen] *= 1.01f;
	}

	if (drawn > m_maxLabels)
	{
		m_labelDistance[subscreen] *= 0.99f;
	}
}
