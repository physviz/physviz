#include "imgui.h"
#include "imgui_impl_dx11.h"

#include <Windows.h>
#include <d3d11_1.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <directxcolors.h>
#include <tchar.h>
#include <strsafe.h>
#include "DDSTextureLoader.h"
#include "DirectXRenderer.h"
#include "../DefaultRenderer/RenderPass.h"
#include "../DefaultControl/controllers.h"
#include "../DefaultControl/pollreturn.h"
#include "../Storage/scene.h"
#include "DXScene.h"

#include "DXRenderData.h"
#include "DXBasePartPass.h"
#include "DXBaseZonePass.h"
#include "DXBaseLinePass.h"
#include "DXBaseLabelPass.h"
#include "DXRenderPassData.h"

#ifndef NO_RIFT
#include "DXRiftPrePass.h"
#include "DXRiftPartPass.h"
#include "DXRiftZonePass.h"
#include "DXRiftLinePass.h"
#include "DXRiftLabelPass.h"
#include "DXRiftPostPass.h"
#endif

#define STB_IMAGE_IMPLEMENTATION
#include "../public/stb_image.h"
#include "../public/dirent.h"
#include "physfs.h"

#include <iostream>
using namespace std;

#define BUFSIZE MAX_PATH

#define DX_RELEASE(_ptr, _expected) _DX_RELEASE(_ptr, _expected)
#define DX_RELEASE_WARNONLY(_ptr, _expected) _DX_RELEASE(_ptr, _expected)
#define _DX_RELEASE(_ptr, _expected) \
				if (NULL != (_ptr) ) \
				{ \
					ULONG count = (_ptr)->Release(); \
					_ptr = NULL; \
				} 

//--------------------------------------------------------------------------------------
// Shader Structures
//--------------------------------------------------------------------------------------
#include "simpleVertexh.h"

struct CBNeverChanges
{
	XMMATRIX m_View;
};

struct CBChangeOnResize
{
	XMMATRIX m_Projection;
};

struct CBChangesEveryFrame
{
	XMMATRIX m_World;
	XMFLOAT4 m_vMeshColor;
	XMFLOAT3 m_Particle;
};

DirectXRenderer::DirectXRenderer() {

}

DirectXRenderer::~DirectXRenderer()
{
	if (m_pRenderData)
	{
		delete m_pRenderData;
		m_pRenderData = nullptr;
	}
}

// clean up member COM pointers
template<typename T> void Release(T*& obj)
{
	if (!obj) return;
	obj->Release();
	obj = nullptr;
}

#ifndef NO_RIFT
extern ovrGraphicsLuid g_gluid;
#endif

bool DirectXRenderer::Init(RendererData *_data)
{
	DXRendererData* DXdata = (DXRendererData*)_data;
	HRESULT hr = S_OK;

	RECT rc;
	GetClientRect(DXdata->m_hWnd, &rc);
	UINT width = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	m_width = width;
	m_height = height;

	IDXGIFactory* DXGIFactory = nullptr;
	hr = CreateDXGIFactory1(__uuidof(IDXGIFactory), (void**)(&DXGIFactory));
	//VALIDATE((hr == ERROR_SUCCESS), "CreateDXGIFactory1 failed");

#ifndef NO_RIFT
	const LUID* pLuid = reinterpret_cast<LUID*>(&g_gluid);
#else
	const LUID* pLuid = nullptr;
#endif

	IDXGIAdapter* Adapter = nullptr;
	for (UINT iAdapter = 0; DXGIFactory->EnumAdapters(iAdapter, &Adapter) != DXGI_ERROR_NOT_FOUND; ++iAdapter)
	{
		DXGI_ADAPTER_DESC adapterDesc;
		Adapter->GetDesc(&adapterDesc);
		if ((pLuid == nullptr) || memcmp(&adapterDesc.AdapterLuid, pLuid, sizeof(LUID)) == 0)
			break;
		Release(Adapter);
	}
	
	UINT createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	
	auto DriverType = Adapter ? D3D_DRIVER_TYPE_UNKNOWN : D3D_DRIVER_TYPE_HARDWARE;
	hr = D3D11CreateDevice(Adapter, DriverType, 0, createDeviceFlags, 0, 0, D3D11_SDK_VERSION, &m_pd3dDevice, 0, &m_pImmediateContext);

	Release(Adapter);

	/*D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
		D3D_DRIVER_TYPE_SOFTWARE
	};
	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_12_1,
		D3D_FEATURE_LEVEL_12_0,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		m_driverType = driverTypes[driverTypeIndex];
		hr = D3D11CreateDevice(nullptr, m_driverType, nullptr, createDeviceFlags, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, &m_pd3dDevice, &m_featureLevel, &m_pImmediateContext);

		if (hr == E_INVALIDARG)
		{
			// DirectX 11.0 platforms will not recognize D3D_FEATURE_LEVEL_11_1 so we need to retry without it
			hr = D3D11CreateDevice(nullptr, m_driverType, nullptr, createDeviceFlags, &featureLevels[1], numFeatureLevels - 1,
				D3D11_SDK_VERSION, &m_pd3dDevice, &m_featureLevel, &m_pImmediateContext);
		}

		if (SUCCEEDED(hr))
			break;
	}

	if (FAILED(hr))
	{
		return false;
	}*/

	// Obtain DXGI factory from device (since we used nullptr for pAdapter above)
	IDXGIFactory1* dxgiFactory = nullptr;
	{
		IDXGIDevice* dxgiDevice = nullptr;
		hr = m_pd3dDevice->QueryInterface(__uuidof(IDXGIDevice), reinterpret_cast<void**>(&dxgiDevice));
		if (SUCCEEDED(hr))
		{
			IDXGIAdapter* adapter = nullptr;
			hr = dxgiDevice->GetAdapter(&adapter);
			if (SUCCEEDED(hr))
			{
				hr = adapter->GetParent(__uuidof(IDXGIFactory1), reinterpret_cast<void**>(&dxgiFactory));
				adapter->Release();
			}
			dxgiDevice->Release();
		}
	}
	if (FAILED(hr))
	{
		return false;
	}

	// Create swap chain
	IDXGIFactory2* dxgiFactory2 = nullptr;
	hr = dxgiFactory->QueryInterface(__uuidof(IDXGIFactory2), reinterpret_cast<void**>(&dxgiFactory2));
	if (dxgiFactory2)
	{
		// DirectX 11.1 or later
		hr = m_pd3dDevice->QueryInterface(__uuidof(ID3D11Device1), reinterpret_cast<void**>(&m_pd3dDevice1));
		if (SUCCEEDED(hr))
		{
			(void)m_pImmediateContext->QueryInterface(__uuidof(ID3D11DeviceContext1), reinterpret_cast<void**>(&m_pImmediateContext1));
		}

		DXGI_SWAP_CHAIN_DESC1 sd;
		ZeroMemory(&sd, sizeof(sd));
		sd.Width = width;
		sd.Height = height;
		sd.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.BufferCount = 1;

		hr = dxgiFactory2->CreateSwapChainForHwnd(m_pd3dDevice, DXdata->m_hWnd, &sd, nullptr, nullptr, &m_pSwapChain1);
		if (SUCCEEDED(hr))
		{
			hr = m_pSwapChain1->QueryInterface(__uuidof(IDXGISwapChain), reinterpret_cast<void**>(&m_pSwapChain));
		}

		dxgiFactory2->Release();
	}
	else
	{
		// DirectX 11.0 systems
		DXGI_SWAP_CHAIN_DESC sd;
		ZeroMemory(&sd, sizeof(sd));
		sd.BufferCount = 1;
		sd.BufferDesc.Width = width;
		sd.BufferDesc.Height = height;
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = DXdata->m_hWnd;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Windowed = TRUE;

		hr = dxgiFactory->CreateSwapChain(m_pd3dDevice, &sd, &m_pSwapChain);
	}

	dxgiFactory->Release();

	if (FAILED(hr))
	{
		return false;
	}

	CreateRenderTarget();

	//read in textures available
	//TODO: proper access system for these
	InitTextures();

	m_pRenderData = new DXRenderData();

	((DXRenderData*)m_pRenderData)->m_pImmediateContext = m_pImmediateContext;
	((DXRenderData*)m_pRenderData)->m_pCBChangesEveryFrame = m_pCBChangesEveryFrame;

	((DXRenderData*)m_pRenderData)->m_Sprites.reset(new SpriteBatch(m_pImmediateContext));

	((DXRenderData*)m_pRenderData)->m_Font.reset(new SpriteFont(m_pd3dDevice, L"fonts/italic.spritefont"));

	((DXRenderData*)m_pRenderData)->m_pSamplerLinear = m_pSamplerLinear;

	((DXRenderData*)m_pRenderData)->m_height = height;
	((DXRenderData*)m_pRenderData)->m_width = width;

	((DXRenderData*)m_pRenderData)->m_BackBuffer = m_pBackBuffer;

	DXRenderPassData* passData = new DXRenderPassData();
	ZeroMemory(passData, sizeof(DXRenderPassData));
	passData->m_height = height;
	passData->m_width = width;
	passData->m_pd3dDevice = m_pd3dDevice;

	for (int i = 0; i <= 4; i += 2)
	{
		int j = 0;
		do
		{
			passData->m_this_screen = j;
			passData->m_total_screens = i;
			DXBasePass* pass = new DXBasePartPass();
			pass->InitPass(passData);
			AddPass(pass);

			passData->m_this_screen = j;
			passData->m_total_screens = i;
			pass = new DXBaseZonePass();
			pass->InitPass(passData);
			AddPass(pass);

			pass = new DXBaseLinePass();
			pass->InitPass(passData);
			AddPass(pass);

			pass = new DXBaseLabelPass();
			pass->InitPass(passData);
			AddPass(pass);

			j++;
		} while (j < i);
	}

#ifndef NO_RIFT

	passData->m_this_screen = 0;
	passData->m_total_screens = 1;
	DXRiftPrePass* prePass = new DXRiftPrePass("RIFT_PRE_PASS");
	prePass->InitPass(passData);
	AddPass(prePass);
	DXRiftPostPass* postPass = new DXRiftPostPass("RIFT_POST_PASS");
	AddPass(postPass);



	int j = 0;
	do
	{
		passData->m_this_screen = j;
		passData->m_total_screens = 2;
		DXRiftPartPass* pPass = new DXRiftPartPass("RIFT_PART_PASS");
		pPass->InitPass(passData);
		AddPass(pPass);

		DXRiftLinePass* lPass = new DXRiftLinePass("RIFT_LINE_PASS");
		lPass->InitPass(passData);
		AddPass(lPass);

		DXRiftLabelPass* laPass = new DXRiftLabelPass("RIFT_LABEL_PASS");
		laPass->InitPass(passData);
		AddPass(laPass);

		DXRiftZonePass* zPass = new DXRiftZonePass("RIFT_ZONE_PASS");
		zPass->InitPass(passData);
		AddPass(zPass);

		j++;
	} while (j < 2);

#endif // !NO_RIFT

	AddActivePass("BASE_PART_PASS");
	AddActivePass("BASE_ZONE_PASS");
	AddActivePass("BASE_LINE_PASS");
	AddActivePass("BASE_LABEL_PASS");

	if (!ImGui_ImplDX11_Init(DXdata->m_hWnd, m_pd3dDevice, m_pImmediateContext1)) {
		return false;
	}

	return true;
}

void DirectXRenderer::Shutdown()
{
	ImGui_ImplDX11_Shutdown();

	if (m_pImmediateContext)	m_pImmediateContext->ClearState();

	for (map<string, ID3D11ShaderResourceView*>::iterator it = m_textures.begin(); it != m_textures.end(); ++it)
	{
		if (it->second)
		{
			it->second->Release();
		}
	}
	m_textures.clear();

	FlushSideloadedTextures();

	if (m_pSamplerLinear)		m_pSamplerLinear->Release();
	if (m_pCBNeverChanges)		m_pCBNeverChanges->Release();
	if (m_pCBChangeOnResize)	m_pCBChangeOnResize->Release();
	if (m_pCBChangesEveryFrame) m_pCBChangesEveryFrame->Release();
	if (m_pDepthStencil)		m_pDepthStencil->Release();
	if (m_pDepthStencilView)	m_pDepthStencilView->Release();
	if (m_pRenderTargetView)	m_pRenderTargetView->Release();


	if (m_pSwapChain1) {
		m_pSwapChain1->Release();
	}

	if (m_pSwapChain) {
		m_pSwapChain->Release();
	}
	if (m_pImmediateContext1)	m_pImmediateContext1->Release();
	if (m_pImmediateContext)	m_pImmediateContext->Release();
	if (m_pd3dDevice1)			m_pd3dDevice1->Release();
	if (m_pd3dDevice)			m_pd3dDevice->Release();

}

void DirectXRenderer::PrePass(RenderData* _RD)
{
	CBNeverChanges cbNeverChanges;
	cbNeverChanges.m_View = XMMatrixTranspose(Controllers::Get()->GetView(_RD->m_pass->GetType()));
	m_pImmediateContext->UpdateSubresource(m_pCBNeverChanges, 0, nullptr, &cbNeverChanges, 0, 0);

	CBChangeOnResize cbChangesOnResize;
	cbChangesOnResize.m_Projection = XMMatrixTranspose(*(Matrix*)_RD->m_pass->GetProj());
	m_pImmediateContext->UpdateSubresource(m_pCBChangeOnResize, 0, nullptr, &cbChangesOnResize, 0, 0);

	m_pImmediateContext->VSSetConstantBuffers(0, 1, &m_pCBNeverChanges);
	m_pImmediateContext->VSSetConstantBuffers(1, 1, &m_pCBChangeOnResize);
}

void DirectXRenderer::CreateRenderTarget() {
	if (!m_bRenderTargetActive) {
		DXGI_SWAP_CHAIN_DESC sd;
		m_pSwapChain->GetDesc(&sd);

		// Create the render target
		//ID3D11Texture2D* pBackBuffer;
		D3D11_RENDER_TARGET_VIEW_DESC render_target_view_desc;
		ZeroMemory(&render_target_view_desc, sizeof(render_target_view_desc));
		render_target_view_desc.Format = sd.BufferDesc.Format;
		render_target_view_desc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&m_pBackBuffer);
		m_pd3dDevice->CreateRenderTargetView(m_pBackBuffer, &render_target_view_desc, &m_pRenderTargetView);
		//pBackBuffer->Release();

		HRESULT hr;

		// Create depth stencil texture
		D3D11_TEXTURE2D_DESC descDepth;
		ZeroMemory(&descDepth, sizeof(descDepth));
		descDepth.Width = sd.BufferDesc.Width;
		descDepth.Height = sd.BufferDesc.Height;
		descDepth.MipLevels = 1;
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;
		hr = m_pd3dDevice->CreateTexture2D(&descDepth, nullptr, &m_pDepthStencil);

		if (FAILED(hr))
		{
			return;
		}

		// Create the depth stencil view
		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
		ZeroMemory(&descDSV, sizeof(descDSV));
		descDSV.Format = descDepth.Format;
		descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		descDSV.Texture2D.MipSlice = 0;
		hr = m_pd3dDevice->CreateDepthStencilView(m_pDepthStencil, &descDSV, &m_pDepthStencilView);

		if (FAILED(hr))
		{
			return;
		}

		m_pImmediateContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);

		// Create the constant buffers
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(CBNeverChanges);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		hr = m_pd3dDevice->CreateBuffer(&bd, nullptr, &m_pCBNeverChanges);
		if (FAILED(hr))
		{
			return;
		}

		bd.ByteWidth = sizeof(CBChangeOnResize);
		hr = m_pd3dDevice->CreateBuffer(&bd, nullptr, &m_pCBChangeOnResize);
		if (FAILED(hr))
		{
			return;
		}

		bd.ByteWidth = sizeof(CBChangesEveryFrame);
		hr = m_pd3dDevice->CreateBuffer(&bd, nullptr, &m_pCBChangesEveryFrame);
		if (FAILED(hr))
		{
			return;
		}

		// Create the sample state
		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory(&sampDesc, sizeof(sampDesc));
		sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
		hr = m_pd3dDevice->CreateSamplerState(&sampDesc, &m_pSamplerLinear);
		if (FAILED(hr))
		{
			return;
		}

		m_bRenderTargetActive = true;
	}
}

void DirectXRenderer::CleanupRenderTarget() {
	if (m_bRenderTargetActive) {
		if (m_pCBChangeOnResize) {
			m_pCBChangeOnResize->Release();
			m_pCBChangeOnResize = NULL;
		}
		if (m_pDepthStencil) {
			m_pDepthStencil->Release();
			m_pDepthStencil = NULL;
		}
		if (m_pDepthStencilView) {
			m_pDepthStencilView->Release();
			m_pDepthStencilView = NULL;
		}
		if (m_pRenderTargetView) {
			m_pRenderTargetView->Release();
			m_pRenderTargetView = NULL;
		}

		m_bRenderTargetActive = false;
	}
}

void DirectXRenderer::InitRender(float _dt, unsigned int _poll)
{
	//
	// Clear the back buffer
	//
	if (_poll& POLL_BG_DOWN)
	{
		m_shade -= _dt;
		if (m_shade < 0.0f)
		{
			m_shade = 0.0f;
		}
	}
	if (_poll&POLL_BG_UP)
	{
		m_shade += _dt;
		if (m_shade > 1.0f)
		{
			m_shade = 1.0f;
		}
	}
	m_pRenderData->m_shade = m_shade;
	float colour[4] = { m_shade, m_shade, m_shade, 1.0f };
	m_pImmediateContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);
	m_pImmediateContext->ClearRenderTargetView(m_pRenderTargetView, colour);

	//
	// Clear the depth buffer to 1.0 (max depth)
	//
	m_pImmediateContext->ClearDepthStencilView(m_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

	m_pImmediateContext->VSSetConstantBuffers(2, 1, &m_pCBChangesEveryFrame);
	m_pImmediateContext->PSSetConstantBuffers(2, 1, &m_pCBChangesEveryFrame);
}

void DirectXRenderer::PostRender()
{
	ImGui::Render();

	//
	// Present our back buffer to our front buffer
	//
	m_pSwapChain->Present(0, 0);
}

void DirectXRenderer::Render(float _dt, unsigned int _poll)
{
	DefaultRenderer::Render(_dt, _poll);
}

void DirectXRenderer::RenderGUIOnly() {
	float colour[4] = { m_shade, m_shade, m_shade, 1.0f };
	m_pImmediateContext->ClearRenderTargetView(m_pRenderTargetView, colour);
	m_pImmediateContext->ClearDepthStencilView(m_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	m_pImmediateContext->VSSetConstantBuffers(2, 1, &m_pCBChangesEveryFrame);
	m_pImmediateContext->PSSetConstantBuffers(2, 1, &m_pCBChangesEveryFrame);

	ImGui::Render();
	m_pSwapChain->Present(0, 0);
}

void DirectXRenderer::LoadTexture(string _filename, string _package /* = "" */, bool _bCheckCase /*= false*/) {
	static const char* s_SupportedImageTypes[] = {
		".jpg", ".jpeg", ".png", ".tga", ".bmp", ".psd", ".gif", ".hdr", ".pic"
	};


	int length = _filename.length();
	string lc_name = "";
	for (int i = 0; i < length; i++) {
		lc_name += _filename[i];
	}
	bool bSupported_stbi = false;

	bool bIsInsidePackage = false;
	PHYSFS_File* packageFileHandle = NULL;


	for (int i = 0; i < ARRAYSIZE(s_SupportedImageTypes); i++) {
		int extlen = strlen(s_SupportedImageTypes[i]);
		if (strncmp(lc_name.c_str() + length - extlen, s_SupportedImageTypes[i], extlen) == 0) {
			bSupported_stbi = true;
		}
	}

	if (_package != "") {
		PHYSFS_mount(_package.c_str(), "", 0); // just to make sure! TODO: replace with proper system for managing this on our end

		if (_bCheckCase) {
			// PhysFS is case sensitive so we have to do this nonsense if we aren't certain of case
			char** szFiles = NULL;
			// string cs_filename = "";
			if (PHYSFS_isDirectory("textures")) {
				szFiles = PHYSFS_enumerateFiles("textures");
				char **i;
				for (i = szFiles; *i != NULL; i++) {
					string str = *i;
					for (int i = 0; i < str.length(); i++) {
						str[i] = tolower(str[i]);
					}

					if (str == lc_name) {
						//cs_filename = *i;
						string fullpath = "textures/";
						fullpath += *i;
						packageFileHandle = PHYSFS_openRead(fullpath.c_str());
						bIsInsidePackage = true;
						break; // We've found the file we want
					}
				}
			}

			if (szFiles != NULL) {
				PHYSFS_freeList(szFiles);
				szFiles = NULL;
			}

			if (packageFileHandle == NULL) {
				// Check in the root (some packages don't support directories)
				szFiles = PHYSFS_enumerateFiles("/");
				char **i;
				for (i = szFiles; *i != NULL; i++) {
					string str = *i;
					for (int i = 0; i < str.length(); i++) {
						str[i] = tolower(str[i]);
					}

					if (str == lc_name) {
						// cs_filename = *i;
						packageFileHandle = PHYSFS_openRead(*i);
						bIsInsidePackage = true;
						break; // We've found the file we want
					}
				}
			}

			if (szFiles != NULL) {
				PHYSFS_freeList(szFiles);
			}
		}
		else {
			// If we're certain our case is correct, just go ahead and open the thing
			packageFileHandle = PHYSFS_openRead(_filename.c_str());
			bIsInsidePackage = packageFileHandle != NULL;
		}

		if (packageFileHandle == NULL) {
			return; // FAILED
		}
	}

	// directx loader
	int extlen = strlen(".dds");
	if (strncmp(lc_name.c_str() + length - extlen, ".dds", extlen) == 0) {
		string filname = "./textures/";
		filname += _filename;
		ID3D11ShaderResourceView* newTextureRV;
		HRESULT hr;
		if (bIsInsidePackage) {
			if (packageFileHandle == NULL) {
				return; // FAILED
			}

			size_t filelen = PHYSFS_fileLength(packageFileHandle);
			unsigned char* data = new unsigned char[filelen];
			PHYSFS_readBytes(packageFileHandle, data, filelen);
			hr = CreateDDSTextureFromMemory(m_pd3dDevice, data, filelen, nullptr, &newTextureRV);
			PHYSFS_close(packageFileHandle);
			delete data;
		}
		else {
			hr = CreateDDSTextureFromFile(m_pd3dDevice, wstring(filname.begin(), filname.end()).c_str(), nullptr, &newTextureRV);
		}
		if (!FAILED(hr) && newTextureRV != NULL) {
			int nLastDot = string(_filename).find_last_of('.');
			string strNoExt = string(_filename).substr(0, nLastDot);
			for (int i = 0; i < strNoExt.length(); i++) {
				strNoExt[i] = tolower(strNoExt[i]);
			}

			if (bIsInsidePackage) {
				m_sideloadedTextures.insert(pair<string, ID3D11ShaderResourceView*>(strNoExt, newTextureRV));
			}
			else {
				m_textures.insert(pair<string, ID3D11ShaderResourceView*>(strNoExt, newTextureRV));
			}
		}
		return; // Loaded!
	}

	// stb_image loader
	if (bSupported_stbi) {
		string filname = "./textures/";
		filname += _filename;

		int x, y, comp;
		unsigned char* szImageData = NULL;

		if (bIsInsidePackage) {
			if (packageFileHandle == NULL) {
				return; // FAILED
			}

			size_t filelen = PHYSFS_fileLength(packageFileHandle);
			unsigned char* data = new unsigned char[filelen];
			PHYSFS_readBytes(packageFileHandle, data, filelen); // load the file then pass it to stbi
			szImageData = stbi_load_from_memory(data, filelen, &x, &y, &comp, STBI_default);
			PHYSFS_close(packageFileHandle);
			delete data;
		}
		else {
			szImageData = stbi_load(filname.c_str(), &x, &y, &comp, STBI_default);
		}

		ID3D11Texture2D *texture2D = nullptr;
		ID3D11ShaderResourceView* pTex = nullptr;
		if (!szImageData) {
			return;
		}

		D3D11_TEXTURE2D_DESC desc;
		ZeroMemory(&desc, sizeof(desc));
		desc.Width = x;
		desc.Height = y;
		desc.MipLevels = 1;
		desc.ArraySize = 1;
		desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		desc.SampleDesc.Count = 1;
		desc.SampleDesc.Quality = 0;
		desc.Usage = D3D11_USAGE_DEFAULT;
		desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
		desc.CPUAccessFlags = 0;
		desc.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA subres;
		subres.pSysMem = szImageData;
		subres.SysMemPitch = x * comp;
		subres.SysMemSlicePitch = 0;


		HRESULT result = m_pd3dDevice1->CreateTexture2D(&desc, &subres, &texture2D);
		if (FAILED(result)) {
			return;
		}

		result = m_pd3dDevice1->CreateShaderResourceView(texture2D, NULL, &pTex);
		if (FAILED(result)) {
			return;
		}

		int nLastDot = string(_filename).find_last_of('.');
		string strNoExt = string(_filename).substr(0, nLastDot);
		for (int i = 0; i < strNoExt.length(); i++) {
			strNoExt[i] = tolower(strNoExt[i]);
		}

		if (bIsInsidePackage) {
			m_sideloadedTextures.insert(pair<string, ID3D11ShaderResourceView*>(strNoExt, pTex));
		}
		else {
			m_textures.insert(pair<string, ID3D11ShaderResourceView*>(strNoExt, pTex)); // TODO: Replace with proper texture ref system?
		}

		stbi_image_free(szImageData);
	}
}

void DirectXRenderer::FlushSideloadedTextures() {
	for (map<string, ID3D11ShaderResourceView*>::iterator it = m_sideloadedTextures.begin(); it != m_sideloadedTextures.end(); ++it)
	{
		if (it->second)
		{
			it->second->Release();
		}
	}
	m_sideloadedTextures.clear();
}

void DirectXRenderer::InitTextures() {
	DIR *p;
	struct dirent *pp;
	p = opendir("./textures/");
	if (p != NULL)
	{
		while ((pp = readdir(p)) != NULL) {
			LoadTexture(pp->d_name);
		}

		(void)closedir(p);
	}

	return;// dwError;
}

void* DirectXRenderer::GetTexture(string _filename) {
	string lc_filename = _filename;
	for (int i = 0; i < _filename.length(); i++) {
		lc_filename[i] = tolower(_filename[i]);
	}

	// Sideloaded textures are ALWAYS the priority, since they're from the currently loaded package
	if (m_sideloadedTextures.find(lc_filename) != m_sideloadedTextures.end()) {
		return m_sideloadedTextures[lc_filename];
	}

	if (m_textures.find(lc_filename) != m_textures.end()) {
		return m_textures[lc_filename];
	}

	return NULL;
}

void DirectXRenderer::AddScene(Scene* _realScene)
{
	m_pRenderableScene = new DXScene(_realScene, m_pd3dDevice);
}

bool DirectXRenderer::WndProc(void* _hWnd, void* _msg, void* _wParam, void* _lParam)
{
	HWND hWnd = *(HWND*)_hWnd;
	UINT msg = *(UINT*)_msg;
	WPARAM wParam = *(WPARAM*)_wParam;
	LPARAM lParam = *(LPARAM*)_lParam;

	ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam);

	if (msg == WM_SIZE) {
		ImGui_ImplDX11_InvalidateDeviceObjects();
		CleanupRenderTarget();
		m_pSwapChain->ResizeBuffers(0, (UINT)LOWORD(lParam), (UINT)HIWORD(lParam), DXGI_FORMAT_UNKNOWN, 0);
		m_pSwapChain1->ResizeBuffers(0, (UINT)LOWORD(lParam), (UINT)HIWORD(lParam), DXGI_FORMAT_UNKNOWN, 0);
		CreateRenderTarget();
		ImGui_ImplDX11_CreateDeviceObjects();
	}

	return true;
}

void DirectXRenderer::Gui_start()
{
	ImGui_ImplDX11_NewFrame();
}
