#include "DXRiftLabelPass.h"
#include "DXRiftPassData.h"
#include "DXRenderPassData.h"
#include "DXRenderData.h"

extern ovrSession g_session;

DXRiftLabelPass::~DXRiftLabelPass()
{
}

void DXRiftLabelPass::InitPass(RenderPassData * _data)
{
	DXBaseLabelPass::InitPass(_data);
	DXRenderPassData* DXRPD = (DXRenderPassData*)_data;
	eye = DXRPD->m_this_screen;

	m_RPD = DXRiftPassData::Get();

}

RenderItems DXRiftLabelPass::Render(RenderData * _data)
{
	if (!m_RPD->m_sInitRift || !g_session) return RenderItems::RI_NONE;

	DXRenderData* DXRD = (DXRenderData*)_data;

	ovrSessionStatus sessionStatus;
	ovr_GetSessionStatus(g_session, &sessionStatus);

	if (sessionStatus.IsVisible)
	{
		auto RTV = m_RPD->m_pEyeRenderTexture[eye]->GetRTV();
		DXRD->m_pImmediateContext->OMSetRenderTargets(1, &RTV, nullptr);// pEyeRenderTexture->GetDSV()  Depth should be this put null to get transparency right

																		//set ViewPort to required values BASE_PASS actually uses it
		m_vp.Width = (float)m_RPD->m_eyeRenderViewport[eye].Size.w;    m_vp.Height = (float)m_RPD->m_eyeRenderViewport[eye].Size.h;
		m_vp.MinDepth = 0;   m_vp.MaxDepth = 1;
		m_vp.TopLeftX = (float)m_RPD->m_eyeRenderViewport[eye].Pos.x; m_vp.TopLeftY = (float)m_RPD->m_eyeRenderViewport[eye].Pos.y;
	}

	//go and do the base render stuff here
	DXBaseLabelPass::Render(_data);

	return RI_LABELS;
}