#include "DXRiftPassData.h"

DXRiftPassData* DXRiftPassData::s_RPD = nullptr;

DXRiftPassData* DXRiftPassData::Get()
{
	if (!s_RPD)
	{
		s_RPD = new DXRiftPassData();
	}

	return s_RPD;
}
