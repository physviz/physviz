#ifndef _SIMPLE_VERTEX_H_
#define _SIMPLE_VERTEX_H_
#include <DirectXMath.h>

using namespace DirectX;

struct SimpleVertex
{
	XMFLOAT3 m_Pos;
	XMFLOAT4 m_Tex;
};

#endif