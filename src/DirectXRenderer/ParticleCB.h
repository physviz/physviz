#ifndef _PARTICLE_CB_H_
#define _PARTICLE_CB_H_

#include <directxmath.h>
using namespace DirectX;


struct ViewCB
{
	XMMATRIX mView;
};

struct ProjCB
{
	XMMATRIX mProjection;
};

struct ParticleCB
{
	XMFLOAT4X4 mWorld;
	XMFLOAT4 vMeshColor;
	XMFLOAT4 Particle;
};
#endif