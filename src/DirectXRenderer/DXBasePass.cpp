#include "DXBasePass.h"
#include "DXRenderData.h"
#include <d3dcompiler.h>
#include "DXRenderPassData.h"

DXBasePass::DXBasePass(string _type) :RenderPass(_type)
{

}

DXBasePass::~DXBasePass()
{
	if (m_pDSState)			m_pDSState->Release();
	if (m_pBlendState)		m_pBlendState->Release();
	if (m_blendFactor)		delete[] m_blendFactor;
	if (m_pRasterState)		m_pRasterState->Release();
	if (m_pVertexShader)	m_pVertexShader->Release();
	if (m_pVertexLayout)	m_pVertexLayout->Release();
	if (m_pPixelShader)		m_pPixelShader->Release();
}

void DXBasePass::InitPass(RenderPassData* _data)
{
	DXRenderPassData* DXRPD = (DXRenderPassData*)_data;
	float useWidth = DXRPD->m_width;
	float useRatio = XM_PIDIV2;
	if (DXRPD->m_total_screens == 2)
	{
		useRatio *= 0.5;
		useWidth *= 0.5;
	}
	m_proj = Matrix::CreatePerspectiveFieldOfView(useRatio, (float)useWidth / (float)DXRPD->m_height, 0.01f, 10000.0f);
}

RenderItems DXBasePass::Render(RenderData* _data)
{
	DXRenderData* DXRD = (DXRenderData*)_data;

	//set up viewport
	DXRD->m_pImmediateContext->RSSetViewports(1, &m_vp);

	//set up raster state
	DXRD->m_pImmediateContext->RSSetState(m_pRasterState);


	//Set the blend state for transparent objects
	DXRD->m_pImmediateContext->OMSetBlendState(m_pBlendState, m_blendFactor, 0xffffffff);
	DXRD->m_pImmediateContext->OMSetDepthStencilState(m_pDSState, 0);

	// Set the input layout
	DXRD->m_pImmediateContext->IASetInputLayout(m_pVertexLayout);

	//set vertex and pixel shaders
	DXRD->m_pImmediateContext->VSSetShader(m_pVertexShader, nullptr, 0);
	DXRD->m_pImmediateContext->PSSetShader(m_pPixelShader, nullptr, 0);

	// Set primitive topology
	DXRD->m_pImmediateContext->IASetPrimitiveTopology(m_topology);

	return RI_NONE;
}

//--------------------------------------------------------------------------------------
// Helper for compiling shaders with D3DCompile
//
// With VS 11, we could load up prebuilt .cso files instead...
//--------------------------------------------------------------------------------------
HRESULT DXBasePass::CompileShaderFromFile(WCHAR* _szFileName, LPCSTR _szEntryPoint, LPCSTR _szShaderModel, ID3DBlob** _ppBlobOut)
{
	HRESULT hr = S_OK;

	DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifdef _DEBUG
	// Set the D3DCOMPILE_DEBUG flag to embed debug information in the shaders.
	// Setting this flag improves the shader debugging experience, but still allows 
	// the shaders to be optimized and to run exactly the way they will run in 
	// the release configuration of this program.
	dwShaderFlags |= D3DCOMPILE_DEBUG;

	// Disable optimizations to further improve shader debugging
	dwShaderFlags |= D3DCOMPILE_SKIP_OPTIMIZATION;
#endif

	ID3DBlob* pErrorBlob = nullptr;
	hr = D3DCompileFromFile(_szFileName, nullptr, nullptr, _szEntryPoint, _szShaderModel,
		dwShaderFlags, 0, _ppBlobOut, &pErrorBlob);
	if (FAILED(hr))
	{
		if (pErrorBlob)
		{
			OutputDebugStringA(reinterpret_cast<const char*>(pErrorBlob->GetBufferPointer()));
			pErrorBlob->Release();
		}
		return hr;
	}
	if (pErrorBlob) pErrorBlob->Release();

	if (FAILED(hr))
	{
		MessageBox(nullptr,
			L"The FX file cannot be compiled.  Please run this executable from the directory that contains the FX file.", L"Error", MB_OK);
		return hr;
	}

	return S_OK;
}

string nums[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

void DXBasePass::MakeVP(DXRenderPassData* _DXRPD)
{
	m_vp.MinDepth = 0.0f;
	m_vp.MaxDepth = 1.0f;
	if (_DXRPD->m_this_screen == 0 && _DXRPD->m_total_screens == 0)
	{
		m_vp.Width = (FLOAT)_DXRPD->m_width;
		m_vp.Height = (FLOAT)_DXRPD->m_height;
		m_vp.TopLeftX = 0;
		m_vp.TopLeftY = 0;
	}
	else
	{
		switch (_DXRPD->m_total_screens)
		{
		case 2:
			m_vp.Width = 0.5f * (FLOAT)_DXRPD->m_width;
			m_vp.Height = (FLOAT)_DXRPD->m_height;
			m_vp.TopLeftX = _DXRPD->m_this_screen == 0 ? 0 : m_vp.Width;
			m_vp.TopLeftY = 0;
			break;
		case 3:
		case 4:
			m_vp.Width = 0.5f * (FLOAT)_DXRPD->m_width;
			m_vp.Height = 0.5f *(FLOAT)_DXRPD->m_height;
			m_vp.TopLeftX = _DXRPD->m_this_screen % 2 ? m_vp.Width : 0;
			m_vp.TopLeftY = _DXRPD->m_this_screen < 2 ? 0 : m_vp.Height;
			break;
		}
		m_type = m_type + "_" + nums[_DXRPD->m_total_screens] + nums[_DXRPD->m_this_screen];
	}
}
