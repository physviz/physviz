#ifndef _DX_SCENE_H_
#define _DX_SCENE_H_
#include <d3d11_1.h>
#include <string>
#include "../DefaultRenderer/DefaultRenderableScene.h"

struct SimpleVertex;

class DXScene : public DefaultRenderableScene
{
public:
	DXScene(Scene* _realScene, ID3D11Device* _device);
	~DXScene();


protected:

	virtual void DrawOther(RenderData* _data) override {};
	virtual void DrawParticles(RenderData* _data) override;
	virtual void DrawLines(RenderData* _data) override;
	virtual void DrawLabels(RenderData* _data) override;

	ID3D11Buffer* m_lineVB;
	UINT m_lineVBstride, m_lineVBoffset;

	SimpleVertex* m_lineData;

	void DrawParticle(Particle* _part, void* _data = nullptr);
	void DrawLine(Line* _line, void* _data = nullptr);

	bool DrawBecauseofSubScreens(Particle* _part, void* _data);
};
#endif