#ifndef _DX_RENDER_PASS_DATA_H_
#include "../DefaultRenderer/RenderPass.h"

struct DXRenderPassData : public RenderPassData
{
	ID3D11Device*	m_pd3dDevice;
	UINT			m_width;
	UINT			m_height;
	UINT			m_total_screens;
	UINT			m_this_screen;
};
#endif