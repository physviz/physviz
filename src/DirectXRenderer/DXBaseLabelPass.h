#ifndef _DX_BASE_LABEL_PASS_H_
#define _DX_BASE_LABEL_PASS_H_
#include "DXBasePass.h"

class DXBaseLabelPass : public DXBasePass
{
public:
	DXBaseLabelPass(string _type = "BASE_LABEL_PASS") :DXBasePass(_type)
	{
		m_order = 3;
	}

	void InitPass(RenderPassData* _data);

	virtual RenderItems Render(RenderData* _data);

protected:

};


#endif