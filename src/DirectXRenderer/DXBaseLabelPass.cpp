#include "DXBaseLabelPass.h"
#include "DXRenderPassData.h"
#include "DXRenderData.h"


void DXBaseLabelPass::InitPass(RenderPassData* _data)
{
	DXRenderPassData* DXRPD = (DXRenderPassData*)_data;

	MakeVP(DXRPD);

	m_topology = D3D_PRIMITIVE_TOPOLOGY_UNDEFINED;
	D3D11_BLEND_DESC blendDesc;
	ZeroMemory(&blendDesc, sizeof(blendDesc));

	D3D11_RENDER_TARGET_BLEND_DESC rtbd;
	ZeroMemory(&rtbd, sizeof(rtbd));

	// create an additive blend state.
	rtbd.BlendEnable = TRUE;
	rtbd.SrcBlend = D3D11_BLEND_SRC_ALPHA;
	rtbd.DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	rtbd.BlendOp = D3D11_BLEND_OP_ADD;
	rtbd.SrcBlendAlpha = D3D11_BLEND_ZERO;
	rtbd.DestBlendAlpha = D3D11_BLEND_ZERO;
	rtbd.BlendOpAlpha = D3D11_BLEND_OP_ADD;
	rtbd.RenderTargetWriteMask = 0x0fL;
	// create a alpha blend state.
	/*rtbd.BlendEnable = TRUE;
	rtbd.SrcBlend = D3D11_BLEND_SRC_ALPHA;
	rtbd.DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	rtbd.BlendOp = D3D11_BLEND_OP_ADD;
	rtbd.SrcBlendAlpha = D3D11_BLEND_ONE;
	rtbd.DestBlendAlpha = D3D11_BLEND_ZERO;
	rtbd.BlendOpAlpha = D3D11_BLEND_OP_ADD;
	rtbd.RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;*/ //0x0f;
	blendDesc.AlphaToCoverageEnable = false;
	blendDesc.RenderTarget[0] = rtbd;

	HRESULT hr = DXRPD->m_pd3dDevice->CreateBlendState(&blendDesc, &m_pBlendState);

	//Set the default blend state (no blending) for opaque objects
	//m_pImmediateContext->OMSetBlendState(0, 0, 0xffffffff);

	//Render trans parent objects//
	m_blendFactor = new float[4];
	ZeroMemory(m_blendFactor, sizeof(float) * 4);

	DXBasePass::InitPass(_data);
}

RenderItems DXBaseLabelPass::Render(RenderData* _data)
{
	DXBasePass::Render(_data);
	DXRenderData* DXRD = (DXRenderData*)_data;

	return RI_LABELS;
}