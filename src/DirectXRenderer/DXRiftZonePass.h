#pragma once
#include "DXBaseZonePass.h"
#include "../LibOVR/Include/OVR_CAPI.h"
#include "../LibOVR/Include/OVR_CAPI_D3D.h"
#include "../Public/oculus_structs.h"

struct OculusTexture;
class DXRiftPassData;
#include "../RiftXBoxControl/RiftXBoxControl.h"

class DXRiftZonePass :
	public DXBaseZonePass
{
public:

	DXRiftZonePass(string _type = "RIFT_ZONE_PASS") :DXBaseZonePass(_type)
	{
		m_order = 2;
	}

	void InitPass(RenderPassData* _data);
	~DXRiftZonePass();

	virtual RenderItems Render(RenderData* _data);

	virtual void* GetProj() override { return RiftXBoxController::Get()->GetProj(m_type); }

protected:

	virtual void SetDS(DXRenderPassData* _DXRPD);

	DXRiftPassData* m_RPD;

	int m_eye = -1;
};

