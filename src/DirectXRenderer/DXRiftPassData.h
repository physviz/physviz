#ifndef _DX_RIFT_RENDER_PASS_DATA_H_
#include "occulus_texture.h"

struct DXRiftPassData 
{
	virtual ~DXRiftPassData() {};
	static DXRiftPassData* Get();


	bool m_sInitRift = false;

	ovrMirrorTexture m_mirrorTexture = nullptr;
	OculusTexture  *m_pEyeRenderTexture[2] = { nullptr,nullptr };
	ovrMirrorTextureDesc m_mirrorDesc = {};
	long long m_frameIndex =0 ;
	int m_msaaRate = 4;

	ovrHmdDesc m_hmdDesc;

	ovrRecti m_eyeRenderViewport[2];

	ovrLayerEyeFovDepth m_ld;
	ovrPosef m_EyeRenderPose[2];
	ovrEyeRenderDesc m_eyeRenderDesc[2];
	double m_sensorSampleTime = 0.0;
	ovrTimewarpProjectionDesc m_posTimewarpProjectionDesc[ovrEye_Count];

private:
	DXRiftPassData() {};
	static DXRiftPassData* s_RPD;
};
#endif
