#ifndef _CMDLINE_PARSER_H_
#define _CMDLINE_PARSER_H_

#include <vector>
#include <list>
#include <string>

enum cmdLineArgType_e {
	CMDARGTYPE_ERROR,
	CMDARGTYPE_NUMERICAL,
	CMDARGTYPE_BOOL,
	CMDARGTYPE_STRING
};

struct cmdLineArg_s {
public:
	// If there's no arg, just set this to a true bool
	cmdLineArg_s();
	cmdLineArg_s(const char* _szArg, bool _bBool);
	cmdLineArg_s(const char* _szArg, const char* _szVal);
	cmdLineArg_s(std::string _szArg, std::string _szVal);
	~cmdLineArg_s() { }

	int GetInt();
	float GetFloat();
	bool GetBool();
	const char* GetString();
	cmdLineArgType_e GetType() {
		return m_eArgType;
	}

	char m_szArgument[256];
private:
	cmdLineArgType_e m_eArgType = CMDARGTYPE_ERROR;
	int m_nVal;
	bool m_bVal;
	char m_szVal[256];
	float m_fVal;
};

class CCmdLine {
public:
	static CCmdLine* Instance() {
		if (s_pInstance == NULL) {
			s_pInstance = new CCmdLine();
		}

		return s_pInstance;
	}
	void ParseCmdLine(const char* _szCmdLine);
	void Shutdown() {
		for (std::list<cmdLineArg_s*>::const_iterator iterator = m_llArgs.begin(), end = m_llArgs.end(); iterator != end; ++iterator) {
			cmdLineArg_s* del = *iterator;
			delete del;
		}
		m_llArgs.clear();

		delete s_pInstance;
	}

	bool CheckParam(const char* _szParam, bool _bCheckBool = false);
	cmdLineArg_s* GetParam(const char* _szParam);
	void AddParam(const char* _szParam);
	void AddParam(const char* _szParam, const char* _szVar);
	void RemoveParam(const char* _szParam);

	std::list<cmdLineArg_s*> m_llArgs;

protected:
	~CCmdLine() {

	}
	void AddToList(cmdLineArg_s* _pArg);
	char m_szCmdLine[4096];

	static CCmdLine* s_pInstance;
};

#endif