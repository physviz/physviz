#include "PhysVis_CMDLine.h"
#include "UTIL_String.h"


CCmdLine* CCmdLine::s_pInstance = NULL;

void CCmdLine::ParseCmdLine(const char* _szCmdLine) {
	char cProcessChar = 0;
	int nStrLen = strlen(_szCmdLine);
	int i = 0;

	strcpy(m_szCmdLine, _szCmdLine); // Copy the whole cmd string, just in case we need it

	// Loop through whole cmd string, splitting it into chunks
	std::vector<std::string> vCmdStrings;

	while (i < nStrLen) {
		std::string strBuffer = "";
		// If first char is a quote mark, we're in a quote pair and must loop until another quote mark is found
		bool bInsideQuotes = false;
		if (_szCmdLine[i] == '\"') {
			bInsideQuotes = true;
			i++; // move on a quote
		}


		// We found a string, so process
		while (i < nStrLen) {
			cProcessChar = _szCmdLine[i];

			// If we've found a space, our string is done UNLESS we're inside a quote!
			if (cProcessChar == ' ' && !bInsideQuotes) {
				i++;
				break;
			}
			else if (cProcessChar == '\"' && bInsideQuotes) {
				i++;
				i++; // may as well skip the next space while we're here
				bInsideQuotes = false;
				break;
			}
			else {
				// Add to buffer
				strBuffer += _szCmdLine[i];
			}

			i++;
		}
		cProcessChar = 0;
		// Push to string vector
		vCmdStrings.push_back(strBuffer);
		//strBuffer = "";
	}

	// Now loop through and actually process the strings
	bool bSecondHalf = false; // Is the next-to-read var the second half of an arg?
	int nCmdStringsSize = vCmdStrings.size();
	for (int j = 0; j < nCmdStringsSize; j++) {
		std::string strCurrentString = vCmdStrings[j];

		// If this is the very first string, it's the filename
		// Add a special case "-path" var for this
		if (j == 0) {
			AddParam("-path", const_cast<char*>(strCurrentString.c_str()));
			continue;
		}

		// If first char is a dash (-), we're saving as a CMD LINE ATTRIBUTE
		if (strCurrentString[0] == '-') {
			// Does the next var start with a - or a +? if it does, it's a new attribute
			if (nCmdStringsSize >= j + 1 && j + 1 < vCmdStrings.size() && vCmdStrings[j + 1][0] != '-' && vCmdStrings[j + 1][0] != '+') {
				bSecondHalf = true;
				continue;
			}
			else {
				bSecondHalf = false;
			}
		}

		// This is a 2nd half of an arg!
		if (bSecondHalf) {
			bSecondHalf = false; // Reset the "half state"

			// strip quotes just in case
			strCurrentString = UTIL_STR_StripWrapperChars(strCurrentString, '\"');

			// Don't use the standard AddParam call, since we're in special circumstances. 
			AddParam(const_cast<char*>(vCmdStrings[j - 1].c_str()), const_cast<char*>(strCurrentString.c_str()));
			continue;
		}

		// Standard, non-paired var. Add to cmdlineargs storage
		AddParam(strCurrentString.c_str());
	}
}


// Does this param exist? (or if bCheckBool, if it does exist and is a bool type, is it true?)
bool CCmdLine::CheckParam(const char* _szParam, bool _bCheckBool /*= false*/) {
	for (std::list<cmdLineArg_s*>::const_iterator iterator = m_llArgs.begin(), end = m_llArgs.end(); iterator != end; ++iterator) {
		cmdLineArg_s* pCurrent = *iterator;
		if (strcmp(pCurrent->m_szArgument, _szParam) == 0) {
			if (_bCheckBool) {
				return pCurrent->GetBool();
			}
			else {
				return true;
			}
		}
	}

	return false;
}

cmdLineArg_s* CCmdLine::GetParam(const char* _szParam) {
	for (std::list<cmdLineArg_s*>::const_iterator iterator = m_llArgs.begin(), end = m_llArgs.end(); iterator != end; ++iterator) {
		cmdLineArg_s* pCurrent = *iterator;
		if (strcmp(pCurrent->m_szArgument, _szParam) == 0) {
			return pCurrent;
		}
	}

	return NULL;
}

void CCmdLine::AddParam(const char* _szParam) {
	cmdLineArg_s* pArg = new cmdLineArg_s(_szParam, true);
	AddToList(pArg);
}

void CCmdLine::AddParam(const char* _szParam, const char* _szVar) {
	cmdLineArg_s* pArg = new cmdLineArg_s(_szParam, _szVar);
	AddToList(pArg);
}


void CCmdLine::RemoveParam(const char* _szParam) {
	for (std::list<cmdLineArg_s*>::const_iterator iterator = m_llArgs.begin(), end = m_llArgs.end(); iterator != end; ++iterator) {
		cmdLineArg_s* pCurrent = *iterator;
		if (strcmp(pCurrent->m_szArgument, _szParam) == 0) {
			m_llArgs.remove(*iterator);
			delete pCurrent;
			return;
		}
	}
}

void CCmdLine::AddToList(cmdLineArg_s* _pArg) {
	m_llArgs.push_back(_pArg);
}

cmdLineArg_s::cmdLineArg_s() {
	m_eArgType = CMDARGTYPE_BOOL;
	m_szVal[0] = 0;
	m_bVal = true;
}

cmdLineArg_s::cmdLineArg_s(const char* _szArg, const char* _szVal) {
	strcpy(m_szArgument, _szArg);

	if (_szVal) {
		if (!UTIL_STR_IsNumerical(_szVal)) {
			// We're not a number! Set the string var
			strcpy(m_szVal, _szVal);
			m_eArgType = CMDARGTYPE_STRING;
		}
		else {
			// We're a number! Set the numbers.
			float fVal = (float)atof(_szVal);
			m_fVal = fVal;
			m_nVal = int(fVal + 0.5); // round up
			m_bVal = fVal <= 0; // if the value is 0 or below, set as false
			strcpy(m_szVal, _szVal);
			m_eArgType = CMDARGTYPE_NUMERICAL;
		}
	}
	else {
		m_szVal[0] = 0;
		m_eArgType = CMDARGTYPE_BOOL;
		m_bVal = true;
	}
}

cmdLineArg_s::cmdLineArg_s(std::string _szArg, std::string _szVal) {
	strcpy(m_szArgument, _szArg.c_str());

	if (!UTIL_STR_IsNumerical(_szVal)) {
		// We're not a number! Set the string 
		strcpy(m_szVal, _szVal.c_str());
		m_eArgType = CMDARGTYPE_STRING;
	}
	else {
		// We're a number! Set the numbers.
		float fVal = (float)atof((char*)_szVal.c_str());
		m_fVal = fVal;
		m_nVal = int(fVal + 0.5); // round up
		m_bVal = fVal <= 0; // if the value is 0 or below, set as false
		strcpy(m_szVal, _szVal.c_str());
		m_eArgType = CMDARGTYPE_NUMERICAL;
	}
}

cmdLineArg_s::cmdLineArg_s(const char* _szArg, bool _bBool) {
	strcpy(m_szArgument, _szArg);
	m_bVal = _bBool;
	m_nVal = _bBool;
	m_fVal = _bBool;
	m_eArgType = CMDARGTYPE_NUMERICAL;
}

int cmdLineArg_s::GetInt() {
	if (m_eArgType == CMDARGTYPE_NUMERICAL) {
		return m_nVal;
	}
	else {
		// AssertMsg(0, "Attempted GetInt of CMDLineArg of non-numerical type! Will return -INT_MAX!");
		return -INT_MAX;
	}
}

float cmdLineArg_s::GetFloat() {
	if (m_eArgType == CMDARGTYPE_NUMERICAL) {
		return m_fVal;
	}
	else {
		// AssertMsg(0, "Attempted GetFloat of CMDLineArg of non-numerical type! Will return -FLT_MAX!");
		return -FLT_MAX;
	}
}

bool cmdLineArg_s::GetBool() {
	if (m_eArgType == CMDARGTYPE_BOOL || m_eArgType == CMDARGTYPE_NUMERICAL) {
		return m_bVal;
	}
	else {
		return true;
	}
}

const char* cmdLineArg_s::GetString() {
	return m_szVal;
}