#include <windows.h>
#include "resource.h"
#include "../JSON/reader.h"
#include "../JSON/filereadstream.h"
#include "../JSON/document.h"
#include "../Storage/graph.h"
#include "../Storage/VEEHandler.h"
#include "../Storage/XYZHandler.h"
#include "../Storage/scene.h"
#include "ControllersAdded.h"

#include <iostream>

#include "../DefaultRenderer/DefaultRenderer.h"
#include "../DefaultRenderer/renderpass.h"

//#ifdef _USE_DIRECTX_RENDERER_
#pragma comment(lib, "DefaultRenderer.lib")
#pragma comment(lib, "DirectXRenderer.lib")
#include "../DirectXRenderer/DirectXRenderer.h"

#include "physfs-3.0.1/src/physfs.h"
#include "physfs-3.0.1/src/streams/physfs_file_system.hpp"

#include "MemoryModule/MemoryModule.h"

#include "UTIL_String.h"

using namespace DirectX;
//#endif

#include "../Public/ImportModuleFactory.h"
#include "dirent.h"

#include "../dearimgui/imgui.h"

#include "PhysVis_CMDLine.h"

#include <time.h>
#include <algorithm>


#define TINYFILES_IMPL
#include "tinyfiles.h"

#define UT_ARRAYSIZE(_ARR)  ((int)(sizeof(_ARR)/sizeof(*_ARR)))

using namespace rapidjson;
using namespace std;

// TODO: (Updated 19/03/2018)
// - Implement modular import system (DONE - 18/01/2018)
// - Clean up modular import system, test for memory leaks (DONE)
// - Create and decouple new import modules from Storage.lib
// - Improve commandline parsing (DONE)
// - Add stbimage texture system (ATTEMPTED - IT DOESNT WORK? - LOOK AT THIS AGAIN!)
// - Add DearImgui (DONE - 05/02/2018)
// - Rework application flow to use DearImgui interface for selecting data (DONE)
// - Add window resizing support (DONE - sort of?)
// - Add 'open recent' support (DONE)
// - Update OculusSDK
//    - Integrate new HMD rotation/position code (DONE)
//    - Integrate new renderer


//--------------------------------------------------------------------------------------
// Global Variables
//--------------------------------------------------------------------------------------
HINSTANCE                           g_hInst = nullptr;
HWND                                g_hWnd = nullptr;

//--------------------------------------------------------------------------------------
// Forward declarations
//--------------------------------------------------------------------------------------
HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
MSG ApplicationLoop();
void GUI_DrawDebugWindow();
bool GUI_DrawSceneMenuBar();
void GUI_DrawFileLoader();
void GUI_OpenLoadDialog();
void GUI_CloseLoadDialog();
void UnloadScene();

void GUI_LoadRecents();
void GUI_AddRecent(string filename);
#define MAX_RECENT_FILES 5
static std::vector<string> s_RecentFiles;

#define SUPPORTED_PACKAGES(vec) \
vec.push_back("pvis"); \
vec.push_back("zip"); \
vec.push_back("7z"); \
vec.push_back("pk3"); \
vec.push_back("wad"); \
vec.push_back("grp"); \

std::wstring s2ws(const std::string& _str)
{
	int size_needed = MultiByteToWideChar(CP_UTF8, 0, &_str[0], (int)_str.size(), NULL, 0);
	std::wstring wstrTo(size_needed, 0);
	MultiByteToWideChar(CP_UTF8, 0, &_str[0], (int)_str.size(), &wstrTo[0], size_needed);
	return wstrTo;
}

//--------------------------------------------------------------------------------------
// Modular Import System
//--------------------------------------------------------------------------------------
static std::vector<CInterfaceFactory*> g_Importers;
static std::vector<HINSTANCE> g_ImporterInstances;
typedef CInterfaceFactory**(*GetInterfacesFromDLL_t)(int& nImporterCount);

void LoadImporterLibraries() {
	DIR *p;
	struct dirent *pp;
	p = opendir("./importers/");

	if (p != NULL)
	{
		while ((pp = readdir(p)) != NULL) {
			int length = strlen(pp->d_name);
			if (strncmp(pp->d_name + length - 4, ".dll", 4) == 0) {
				//puts(pp->d_name);
				wstring filname = L"./importers/" + s2ws(pp->d_name);

				HINSTANCE instance = LoadLibraryEx(filname.c_str(), NULL, LOAD_WITH_ALTERED_SEARCH_PATH);
				GetInterfacesFromDLL_t getter = (GetInterfacesFromDLL_t)GetProcAddress(instance, "__GetImportModuleList");
				if (getter == NULL) {
					continue;
				}

				int nImporters = 0;
				CInterfaceFactory** importModules = getter(nImporters);
				for (int i = 0; i < nImporters; i++) {
					CInterfaceFactory* module = importModules[i];
					g_Importers.push_back(module);
				}
			}
		}

		(void)closedir(p);
	}
}

void ShutdownImporterLibraries() {
	for (int i = 0; i < g_Importers.size(); i++) {
		g_Importers[i]->Shutdown();
		g_Importers[i] = NULL;
	}
	g_Importers.clear();

	for (int i = 0; i < g_ImporterInstances.size(); i++) {
		if (!FreeLibrary(g_ImporterInstances[i])) {
			if (!UnmapViewOfFile(g_ImporterInstances[i])) {
				continue; // Sod it. Just carry on.
			}
		}
	}
	g_ImporterInstances.clear();
}

IImportModule* GetSupportedModule(const char* _szExt) {
	for (int i = 0; i < g_Importers.size(); i++) {
		if (g_Importers[i]->SupportsExtension(_szExt)) {
			return g_Importers[i]->GetImportModule();
		}
	}

	return NULL;
}

vector<string> GetSupportedFiletypeList() {
	vector<string> types;
	for (int i = 0; i < g_Importers.size(); i++) {
		for (int j = 0; j < g_Importers[i]->m_vFileExtensions.size(); j++) {
			types.push_back(g_Importers[i]->m_vFileExtensions[j]);
		}
	}

	// Package formats (just a selection)
	SUPPORTED_PACKAGES(types);

	return types;
}

DefaultRenderer* g_pRenderer = nullptr;
enum physVisAppState_e {
	NONE,
	SCENE_VIEW,
	QUIT_WAITING
};
physVisAppState_e
g_eAppState = NONE;

Scene* g_pScene = nullptr;
Graph* g_pGraph = nullptr;

void LoadPackage(const char* _szFilename, bool _bAddParam) {
	UnloadScene();

	if (_bAddParam) {
		// TODO: Edit existing param instead of this!!
		if (CCmdLine::Instance()->GetParam("-f")) {
			CCmdLine::Instance()->RemoveParam("-f");
		}

		CCmdLine::Instance()->AddParam("-f", _szFilename);
	}

	if (!PHYSFS_isInit()) {
		PHYSFS_init(0);
	}
	PHYSFS_mount(_szFilename, "", 0);

	// Find our data type
	string ext = "";
	string filename = "";
	{
		char** szFiles = PHYSFS_enumerateFiles("/");
		char **i;
		for (i = szFiles; *i != NULL; i++) {
			string str = *i;
			int nLastDot = str.find_last_of('.');
			string strNoExt = str.substr(0, nLastDot);
			for (int i = 0; i < strNoExt.length(); i++) {
				strNoExt[i] = tolower(strNoExt[i]);
			}

			if (strNoExt == "d") {
				filename = *i;
				ext = str.substr(nLastDot + 1);
				for (int i = 0; i < ext.length(); i++) {
					ext[i] = tolower(ext[i]);
				}
				continue;
			}
		}
		PHYSFS_freeList(szFiles);
	}

	// Look for an adhoc importer in the package
	{
		char** szFiles = PHYSFS_enumerateFiles("/");
		char **i;
		for (i = szFiles; *i != NULL; i++) {
			string str_lc = *i;
			for (int i = 0; i < str_lc.length(); i++) {
				str_lc[i] = tolower(str_lc[i]);
			}

			if (str_lc == "importer.dll") {
				int result = MessageBox(0, L"Bundled importer found inside package!\nShould PhysVis use this importer?\n\n(Only use with packages from trusted sources)",
					L"PhysVis Loader", MB_YESNOCANCEL);
				switch (result) {
				case IDCANCEL:
					g_eAppState = NONE;
					return;
				case IDNO:
					i = NULL;
					break; // Don't use the package importer
				case IDYES:
					// Let's continue!
					break;
				}

				if (i == NULL) {
					break;
				}

				PHYSFS_File* fil = PHYSFS_openRead(*i);
				size_t libSize = PHYSFS_fileLength(fil);
				unsigned char* szPackageDLL = new unsigned char[libSize];
				PHYSFS_readBytes(fil, szPackageDLL, libSize);
				PHYSFS_close(fil);
				HMEMORYMODULE handle = MemoryLoadLibrary(szPackageDLL, libSize);
				if (handle == NULL) {
					MessageBox(0, L"Failed to load bundled importer. Will attempt to use built-in importer instead.", L"PhysVis Loader Error", MB_OK);
					delete szPackageDLL;
					break;
				}

				GetInterfacesFromDLL_t getter = (GetInterfacesFromDLL_t)MemoryGetProcAddress(handle, "__GetImportModuleList");
				if (getter == NULL) {
					MessageBox(0, L"Failed to load bundled importer. Will attempt to use built-in importer instead.", L"PhysVis Loader Error", MB_OK);
					MemoryFreeLibrary(handle);
					delete szPackageDLL;
					break;
				}

				// Find our supported import module
				int nImporters = 0;
				CInterfaceFactory** importModules = getter(nImporters);
				for (int i = 0; i < nImporters; i++) {
					CInterfaceFactory* factory = importModules[i];
					if (factory->SupportsExtension(ext.c_str())) {
						IImportModule* imp = factory->GetImportModule();
						if (imp != NULL) {
							imp->ImportFromPackage(filename.c_str(), _szFilename);
							g_pGraph = imp->GetGraph();
						}
					}

					factory->Shutdown(); // Shut it down, we don't need it again
				}

				MemoryFreeLibrary(handle);
				delete szPackageDLL;

				if (g_pGraph == NULL) {
					MessageBox(0, L"Failed to load bundled importer. Will attempt to use built-in importer instead.", L"PhysVis Loader Error", MB_OK);
				}

				break;
			}
		}
		PHYSFS_freeList(szFiles);
	}


	// If the bundled library didn't work, or if we didn't find one
	if (g_pGraph == NULL) {
		IImportModule* matchingImporter = GetSupportedModule(ext.c_str());
		if (matchingImporter != NULL) {
			// Found a supported module.
			matchingImporter->ImportFromPackage(filename.c_str(), _szFilename);
			g_pGraph = matchingImporter->GetGraph();
		}
		else {
			MessageBox(0, (L"Could not find supported importer for filetype \"" + wstring(ext.begin(), ext.end()) + L"\"").c_str(), L"PhysVis Import Error", MB_OK);
			g_eAppState = NONE;
			return;
		}
	}

	if (CCmdLine::Instance()->CheckParam("-r"))
	{
		// Scan though and output all properties and their ranges
		system("cls"); // HACKHACK - change this, it's bad
		g_pGraph->DisplayRanges(_szFilename);
	}

	// Our graph was properly loaded, so now go ahead and pull out textures from the package...
	// (just throw everything at the renderer and see what sticks)
	{
		char** szFiles_texture = PHYSFS_enumerateFiles("textures");
		char **i;
		for (i = szFiles_texture; *i != NULL; i++) {
			g_pRenderer->LoadTexture(*i, _szFilename);
		}
		PHYSFS_freeList(szFiles_texture);
	}

	// Also check the root directory; not all package file formats (such as GRP) support directories
	{
		char** szFiles_texture_root = PHYSFS_enumerateFiles("/");
		char **i;
		for (i = szFiles_texture_root; *i != NULL; i++) {
			g_pRenderer->LoadTexture(*i, _szFilename);
		}
		PHYSFS_freeList(szFiles_texture_root);
	}


	// Set up the scene
	g_pScene = new Scene(g_pGraph, g_pRenderer, "d.disp.json", _szFilename);
	g_pRenderer->AddScene(g_pScene);
	g_eAppState = SCENE_VIEW;
	delete g_pGraph;
	g_pGraph = nullptr;

	GUI_AddRecent(_szFilename);

	PHYSFS_unmount(_szFilename);
}

void LoadFile(const char* _szFilename, bool _bSetParam = true) {
	UnloadScene(); // Unload if we need to

	if (_bSetParam) {
		// TODO: Edit existing param instead of this!!
		if (CCmdLine::Instance()->GetParam("-f")) {
			CCmdLine::Instance()->RemoveParam("-f");
		}

		CCmdLine::Instance()->AddParam("-f", _szFilename);
	}

	vector<string> fileNameBits;
	string full_filename = _szFilename;
	int prevDot = 0;
	for (int i = 0; i < full_filename.length(); i++) {
		if (full_filename[i] == '.')
		{
			fileNameBits.push_back(full_filename.substr(prevDot+((prevDot==0)?0:1), i - prevDot + ((prevDot == 0) ? 0 : -1)));
			prevDot=i;
		}
	}
	fileNameBits.push_back(full_filename.substr(prevDot + 1, full_filename.length() - prevDot - 1));

	string file_ext = "";
	string fileload = "";
	string filedispload = "";
	if (fileNameBits[fileNameBits.size() - 1] == "json" && fileNameBits[fileNameBits.size() - 2] == "disp")
	{
		//we are loading from a .disp.json file
		file_ext = fileNameBits[fileNameBits.size() - 3];
		fileload = fileNameBits[0]+"."+file_ext;
		filedispload = full_filename;
	}
	else 
	{
		//loading from a direct file so load the default .disp.json file
		file_ext = fileNameBits[fileNameBits.size() - 1];
		fileload = full_filename;
		filedispload = fileload + ".disp.json";
	}

	IImportModule* matchingImporter = GetSupportedModule(file_ext.c_str());
	if (matchingImporter != NULL) {
		// Found a supported module.
		matchingImporter->ImportFile(fileload.c_str());
		g_pGraph = matchingImporter->GetGraph();
	}
	else {
		MessageBox(0, L"Could not find supported importer for this filetype.", L"PhysVis Import Error", MB_OK);
		g_eAppState = NONE;
		return;
	}

	if (CCmdLine::Instance()->CheckParam("-r"))
	{
		// Scan though and output all properties and their ranges
		system("cls"); // HACKHACK - change this, it's bad
		g_pGraph->DisplayRanges(full_filename.substr(0, full_filename.length()));
	}

	// Set up the scene
	g_pScene = new Scene(g_pGraph, g_pRenderer, filedispload);
	g_pRenderer->AddScene(g_pScene);
	g_eAppState = SCENE_VIEW;
	delete g_pGraph;
	g_pGraph = nullptr;

	GUI_AddRecent(_szFilename);
}

void UnloadScene() {
	// If a scene is currently loaded, unload it
	if (g_pGraph != nullptr) {
		delete g_pGraph;
		g_pGraph = nullptr;
	}

	if (g_pScene != nullptr) {
		delete g_pScene;
		g_pScene = nullptr;
	}

	if (g_pRenderer != nullptr) {
		g_pRenderer->ShutdownScene();
	}
}

void SetBaseDirectory(HINSTANCE _hInstance) {
	WCHAR moduleName[MAX_PATH];
	if (!GetModuleFileName(_hInstance, moduleName, MAX_PATH)) {
		return;
	}

	wstring dummy(moduleName);
	string moduleNameChar(dummy.begin(), dummy.end());

	static char	basedir[MAX_PATH];
	char szBuffer[MAX_PATH];
	int j;
	char *pBuffer = NULL;

	strcpy(szBuffer, moduleNameChar.c_str());

	pBuffer = strrchr(szBuffer, '\\');
	if (pBuffer) {
		*(pBuffer + 1) = '\0';
	}

	strcpy(basedir, szBuffer);

	j = strlen(basedir);
	if (j > 0) {
		if ((basedir[j - 1] == '\\') ||
			(basedir[j - 1] == '/')) {
			basedir[j - 1] = 0;
		}
	}

	CCmdLine::Instance()->AddParam("-root", basedir);
}

bool ExtMatches(string _filename, vector<string> _ext) {
	//string ext = _filename.substr(_filename.find_last_of('.') + 1);
	for (int i = 0; i < _ext.size(); i++) {
		if (_filename.find(_ext[i].c_str(), _filename.length() - _ext[i].length()) != string::npos)
		{
			return true;
		}

	}

	return false;
}

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int WINAPI wWinMain(_In_ HINSTANCE _hInstance, _In_opt_ HINSTANCE _hPrevInstance, _In_ LPWSTR _lpCmdLine, _In_ int _nCmdShow) {

	// Parse command line
	wstring dummy(GetCommandLine());
	string cmdLine(dummy.begin(), dummy.end());
	CCmdLine::Instance()->ParseCmdLine(cmdLine.c_str());
	SetBaseDirectory(_hInstance);

	PHYSFS_init(0);

	// Load importer modules
	LoadImporterLibraries();
	if (g_Importers.size() == 0) {
		MessageBox(0, L"Could not find any importer modules! Please check the ./Importers/ directory for valid module files.", L"PhysVis Startup Error", MB_OK);
		return 0; // If there are no importers, quit the program.
	}

	if (CCmdLine::Instance()->CheckParam("-r")) {
		// Create console window if we are going to write range data out
		// NOOOOO! OUTPUT BEFORE THIS ELSE NOTHING WILL BE DISPLAYED!
		if (AllocConsole()) {
			AttachConsole(GetCurrentProcessId());
			freopen("CONOUT$", "w", stdout);
			SetConsoleTitle(L"Range Values");
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_RED);
			cout.sync_with_stdio();
		}
	}

	UNREFERENCED_PARAMETER(_hPrevInstance);
	UNREFERENCED_PARAMETER(_lpCmdLine);

	if (FAILED(InitWindow(_hInstance, _nCmdShow))) {
		return 0;
	}

	// TODO: Decouple renderer
	g_pRenderer = new DirectXRenderer();
	RenderData renderData;
	RendererData* rendererData = new DXRendererData();
	((DXRendererData*)rendererData)->m_hWnd = g_hWnd;

	if (g_pRenderer) {
		if (!g_pRenderer->Init(rendererData)) {
			g_pRenderer->Shutdown();
			return 0;
		}
	}
	else {
		return 0;
	}

	IControllerData ICD;
	ICD.m_hInst = g_hInst;
	ICD.m_hWnd = g_hWnd;
	Controllers::Get()->Init(&ICD);

	// Setup Oculus render stuff.
	IController* hmd = Controllers::Get()->GetHMD();
	if (hmd != NULL) {
		// found a hmd, so set it up
		void* renderData = hmd->GetDataObject("renderdata", NULL);
		//g_pRenderer->OVR_init(renderData);
	}

	GUI_LoadRecents();

	// If we've got this defined, we're loading a file immediately. Don't display the file menu.
	if (CCmdLine::Instance()->CheckParam("-f")) {
		vector<string> pkgs;
		SUPPORTED_PACKAGES(pkgs);
		if (ExtMatches(CCmdLine::Instance()->GetParam("-f")->GetString(), pkgs)) {
			LoadPackage(CCmdLine::Instance()->GetParam("-f")->GetString(), false);
		}
		else {
			LoadFile(CCmdLine::Instance()->GetParam("-f")->GetString(), false);
		}
	}
	else {
		GUI_OpenLoadDialog();
	}

	// Perform application loop
	MSG msg = ApplicationLoop();

	UnloadScene();
	delete rendererData;
	g_pRenderer->Shutdown();
	ShutdownImporterLibraries();
	CCmdLine::Instance()->Shutdown();
	PHYSFS_deinit();
	return (int)msg.wParam;
}

MSG ApplicationLoop() {
	// Main message loop
	MSG msg = { 0 };
	while (WM_QUIT != msg.message)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else {
			g_pRenderer->Gui_start();
			GUI_DrawDebugWindow();

			switch (g_eAppState) {
			case NONE:
			default:
				GUI_DrawSceneMenuBar();
				GUI_DrawFileLoader();
				g_pRenderer->RenderGUIOnly();
				break;
			case SCENE_VIEW:
				if (!GUI_DrawSceneMenuBar()) {
					g_pRenderer->RenderGUIOnly();
					break;
				}

				if (g_pScene)
				{
					//calculate delta-time between frames
					static DWORD playTime = 0.0f;
					DWORD currentTime = GetTickCount();
					float dt = (float)(currentTime - playTime) / 1000.0f;
					//float dt = min((float)(currentTime - playTime) / 1000.0f, 0.1f);
					unsigned int poll = Controllers::Get()->Poll(dt, g_pRenderer);
					if (poll & POLL_RETURN_OFF) {
						g_eAppState = QUIT_WAITING; // quit program
					}
					playTime = currentTime;

					g_pScene->Update(dt, poll);
					g_pRenderer->Render(dt, poll);
				}
				break;
			case QUIT_WAITING:
				msg.message = WM_QUIT;
				break;
			}
		}
	}

	return msg;
}

HKEY Registry_OpenKey(HKEY _hRootKey, wchar_t* _strKey)
{
	HKEY hKey;
	LONG nError = RegOpenKeyEx(_hRootKey, _strKey, NULL, KEY_ALL_ACCESS, &hKey);

	if (nError == ERROR_FILE_NOT_FOUND)
	{
		cout << "Creating registry key: " << _strKey << endl;
		nError = RegCreateKeyEx(_hRootKey, _strKey, NULL, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
	}

	if (nError)
		cout << "Error: " << nError << " Could not find or create " << _strKey << endl;

	return hKey;
}

void Registry_SetVal(HKEY _hKey, LPCTSTR _lpValue, string _data)
{
	const char *x = _data.c_str();
	LONG nError = RegSetValueEx(_hKey, _lpValue, NULL, REG_SZ, (LPBYTE)_data.c_str(), _data.size());

	if (nError)
		cout << "Error: " << nError << " Could not set registry value: " << (char*)_lpValue << endl;
}

string Registry_GetVal(HKEY _hKey, LPCTSTR _lpValue)
{
	string data;
#define MAXLENGTH MAX_PATH

	char buffer[MAXLENGTH];
	DWORD size = sizeof(buffer);
	DWORD type = REG_SZ;

	LONG nError = RegQueryValueEx(_hKey, _lpValue, NULL, &type, (LPBYTE)buffer, &size);

	if (nError == ERROR_FILE_NOT_FOUND)
	{
		data = ""; // The value will be created and set to data next time SetVal() is called.
		return data;
	}
	else if (nError) {
		cout << "Error: " << nError << " Could not get registry value " << (char*)_lpValue << endl;
		data = "";
		return data;
	}

	data = buffer;
	return data;
}

void GUI_LoadRecents() {
	HKEY key = Registry_OpenKey(HKEY_CURRENT_USER, L"SOFTWARE\\PhysVis");

	for (int i = 0; i < MAX_RECENT_FILES; i++) {
		wstring name = L"RecentFile" + to_wstring(i);
		s_RecentFiles.push_back(Registry_GetVal(key, name.c_str()));
	}
}

void GUI_SaveRecents() {
	HKEY key = Registry_OpenKey(HKEY_CURRENT_USER, L"SOFTWARE\\PhysVis");

	for (int i = 0; i < MAX_RECENT_FILES; i++) {
		wstring name = L"RecentFile" + to_wstring(i);
		if (s_RecentFiles[i] != "") {
			Registry_SetVal(key, name.c_str(), s_RecentFiles[i]);
		}
	}
}

void GUI_AddRecent(string _filename) {
	int nAlreadyPlaced = -1;
	for (int i = 0; i < MAX_RECENT_FILES; i++) {
		if (_filename == s_RecentFiles[i]) {
			nAlreadyPlaced = i;
			break;
		}
	}

	if (nAlreadyPlaced != -1) {
		s_RecentFiles.erase(s_RecentFiles.begin() + nAlreadyPlaced);
	}

	s_RecentFiles.insert(s_RecentFiles.begin(), _filename);

	while (s_RecentFiles.size() > MAX_RECENT_FILES) {
		s_RecentFiles.erase(s_RecentFiles.begin() + MAX_RECENT_FILES);
	}

	GUI_SaveRecents();
}

static bool g_bDebugWindow = false;
void GUI_DrawDebugWindow() {
	if (g_bDebugWindow) {
		ImGui::Begin("Debug Information", &g_bDebugWindow);
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::End();
	}
}

typedef void(*openFileDlg_callback_pfn)(const char* szPath, FILE* pFil);
void FileOpenCallback(const char* _szPath, FILE* _pFil) {
	vector<string> pkgs;
	SUPPORTED_PACKAGES(pkgs);
	if (ExtMatches(_szPath, pkgs)) {
		LoadPackage(_szPath, true);
	}
	else {
		LoadFile(_szPath, true);
	}

	// LoadFile(szPath);
	fclose(_pFil);
}

bool m_bFileDialogOpen = false;
bool m_bFileDialogClosed = true;
tfDIR* dir = NULL;
char m_szCurPath[MAX_PATH];
char m_szCurShortened[MAX_PATH];
char m_szFileOpenFlags[4];
FILE* m_pSelectedFile = NULL;
openFileDlg_callback_pfn pfnOpenFileCallback;
bool m_bFilterExtensions = true;
enum extensionsFilter_e {
	EXT_FILTER_NOFILTER = 0,
	EXT_FILTER_ALLTYPES = 1
};

static vector<string> s_SupportedTypes = { "disp.json" };
void GUI_OpenLoadDialog() {
	if (!m_bFileDialogOpen) {
		m_pSelectedFile = NULL;
		strcpy(m_szFileOpenFlags, "r");
		memset(m_szCurPath, 0, sizeof(m_szCurPath));
		memset(m_szCurShortened, 0, sizeof(m_szCurShortened));
		delete dir;
		m_bFileDialogOpen = true;
		m_bFileDialogClosed = false;

		pfnOpenFileCallback = FileOpenCallback;

		s_SupportedTypes = GetSupportedFiletypeList();
	}
}

void GUI_CloseLoadDialog() {
	if (!m_bFileDialogClosed) {
		m_bFileDialogClosed = true;
		if (dir) {
			tfDirClose(dir);
			delete dir;
			dir = NULL;
		}

		s_SupportedTypes.clear();
	}
}

bool GUI_DrawSceneMenuBar() {
	bool bRet = true;
	if (ImGui::BeginMainMenuBar())
	{
		if (ImGui::BeginMenu("File")) {
			if (ImGui::MenuItem("Load Dataset"/*, "Ctrl+O"*/)) {
				// Unload the current scene
				UnloadScene();
				g_eAppState = NONE;
				GUI_OpenLoadDialog();
				bRet = false;
			}
			if (ImGui::BeginMenu("Load Recent")) {
				for (int i = 0; i < MAX_RECENT_FILES; i++) {
					if (s_RecentFiles[i] != "") {
						if (ImGui::MenuItem(s_RecentFiles[i].c_str())) {
							vector<string> pkgs;
							SUPPORTED_PACKAGES(pkgs);
							if (ExtMatches(s_RecentFiles[i].c_str(), pkgs)) {
								LoadPackage(s_RecentFiles[i].c_str(), false);
							}
							else {
								LoadFile(s_RecentFiles[i].c_str(), false);
							}
						}
					}
				}
				ImGui::EndMenu();
			}
			if (ImGui::MenuItem("Quit"/*, "Ctrl+Q"*/)) {
				g_eAppState = QUIT_WAITING;
				bRet = false;
			}
			ImGui::EndMenu();
		}
		if (ImGui::BeginMenu("About")) {
			ImGui::MenuItem("Debug", "", &g_bDebugWindow);
			ImGui::MenuItem("PhysVis 2018");
			ImGui::EndMenu();
		}
		ImGui::EndMainMenuBar();
	}

	return bRet;
}

void PathCanonicalize(char* _path) {
	size_t i;
	size_t j;
	size_t k;

	//Move to the beginning of the string
	i = 0;
	k = 0;

	//Replace backslashes with forward slashes
	while (_path[i] != '\0') {
		//Forward slash or backslash separator found?
		if (_path[i] == '/' || _path[i] == '\\') {
			_path[k++] = '/';
			while (_path[i] == '/' || _path[i] == '\\')
				i++;
		}
		else {
			_path[k++] = _path[i++];
		}
	}

	//Properly terminate the string with a NULL character
	_path[k] = '\0';

	//Move back to the beginning of the string
	i = 0;
	j = 0;
	k = 0;

	//Parse the entire string
	do {
		//Forward slash separator found?
		if (_path[i] == '/' || _path[i] == '\0') {
			//"." element found?
			if ((i - j) == 1 && !strncmp(_path + j, ".", 1)) {
				//Check whether the pathname is empty?
				if (k == 0) {
					if (_path[i] == '\0') {
						_path[k++] = '.';
					}
					else if (_path[i] == '/' && _path[i + 1] == '\0') {
						_path[k++] = '.';
						_path[k++] = '/';
					}
				}
				else if (k > 1) {
					//Remove the final slash if necessary
					if (_path[i] == '\0')
						k--;
				}
			}
			//".." element found?
			else if ((i - j) == 2 && !strncmp(_path + j, "..", 2)) {
				//Check whether the pathname is empty?
				if (k == 0) {
					_path[k++] = '.';
					_path[k++] = '.';

					//Append a slash if necessary
					if (_path[i] == '/')
						_path[k++] = '/';
				}
				else if (k > 1) {
					//Search the path for the previous slash
					for (j = 1; j < k; j++) {
						if (_path[k - j - 1] == '/')
							break;
					}

					//Slash separator found?
					if (j < k) {
						if (!strncmp(_path + k - j, "..", 2)) {
							_path[k++] = '.';
							_path[k++] = '.';
						}
						else {
							k = k - j - 1;
						}

						//Append a slash if necessary
						if (k == 0 && _path[0] == '/')
							_path[k++] = '/';
						else if (_path[i] == '/')
							_path[k++] = '/';
					}
					//No slash separator found?
					else {
						if (k == 3 && !strncmp(_path, "..", 2)) {
							_path[k++] = '.';
							_path[k++] = '.';

							//Append a slash if necessary
							if (_path[i] == '/')
								_path[k++] = '/';
						}
						else if (_path[i] == '\0') {
							k = 0;
							_path[k++] = '.';
						}
						else if (_path[i] == '/' && _path[i + 1] == '\0') {
							k = 0;
							_path[k++] = '.';
							_path[k++] = '/';
						}
						else {
							k = 0;
						}
					}
				}
			}
			else {
				//Copy directory name
				memmove(_path + k, _path + j, i - j);
				//Advance write pointer
				k += i - j;

				//Append a slash if necessary
				if (_path[i] == '/')
					_path[k++] = '/';
			}

			//Move to the next token
			while (_path[i] == '/')
				i++;
			j = i;
		}
		else if (k == 0) {
			while (_path[i] == '.' || _path[i] == '/') {
				j++, i++;
			}
		}
	} while (_path[i++] != '\0');

	//Properly terminate the string with a NULL character
	_path[k] = '\0';
}

void GUI_DrawFileLoader() {
	if (ImGui::Begin("Load Data...", &m_bFileDialogOpen, ImGuiWindowFlags_NoResize)) {
		ImVec2 curSize = ImGui::GetWindowSize();
		ImGui::SetWindowSize(ImVec2(curSize.x < 500 ? 500 : curSize.x, curSize.y < 375 ? 375 : curSize.y));
		ImGui::BeginChild("TremorModuleMount_ChildFrame", ImVec2(0, -ImGui::GetFrameHeightWithSpacing() * 2), false, ImGuiWindowFlags_HorizontalScrollbar);

		int nCount = 0;
		static int nSelected = -1;
		static int nExtensionsSelected = EXT_FILTER_ALLTYPES;

		if (dir == NULL) {
			dir = new tfDIR();
			tfDirOpen(dir, CCmdLine::Instance()->GetParam("-root")->GetString());
			PathCanonicalize(dir->path);
			strcpy(m_szCurPath, dir->path);
		}

		bool bOpen = false;

		ImGui::Text(dir->path);
		ImGui::Separator();
		ImGui::Columns(3, "MountModuleColumns", true);
		ImGui::Text("File Name");
		ImGui::NextColumn();
		ImGui::Text("Last Modified");
		ImGui::NextColumn();
		ImGui::Text("Size");
		ImGui::Separator();
		ImGui::NextColumn();

		if (dir->handle != INVALID_HANDLE_VALUE) {
			tfDirClose(dir);
		}
		tfDirOpen(dir, m_szCurPath);

		while (dir->has_next) {
			tfFILE file;
			tfReadFile(dir, &file);

			if (strcmp(file.name, ".") == 0) {
				nCount++;
				tfDirNext(dir);
				continue;
			}

			if (m_bFilterExtensions && !file.is_dir && nExtensionsSelected != EXT_FILTER_NOFILTER) {
				for (int i = 0; i < s_SupportedTypes.size(); i++) {
					string filename = file.name;
					if (filename.find(s_SupportedTypes[i].c_str(),filename.length()-1- s_SupportedTypes[i].length()) != string::npos) {
						// We've found a matching ext
						if (nExtensionsSelected == EXT_FILTER_ALLTYPES ||
							nExtensionsSelected - 2 == i) {
							goto extension_matched;
						}
					}
				}

				// We've not got a matching ext!
				nCount++;
				tfDirNext(dir);
				continue;
			}

		extension_matched:
			if (ImGui::Selectable(file.name, nSelected == nCount, ImGuiSelectableFlags_SpanAllColumns | ImGuiSelectableFlags_AllowDoubleClick)) {
				nSelected = nCount;
				strcpy(m_szCurShortened, file.name);
				if (ImGui::IsMouseDoubleClicked(0)) {
					if (file.is_dir) {
						PathCanonicalize(file.path);
						strcpy(m_szCurPath, file.path);
						nSelected = -1;
					}
					else {
						bOpen = true;
					}
				}
			}

			struct stat file_stats;
			if (stat(file.path, &file_stats) == 0) {
				ImGui::NextColumn();

				time_t t = file_stats.st_mtime;
				struct tm* lt;
				lt = localtime(&t);
				char timbuf[80];
				strftime(timbuf, sizeof(timbuf), "%c", lt);
				ImGui::Text(timbuf);

				ImGui::NextColumn();
				ImGui::Text("%i KiB", file_stats.st_size / 1024);
				ImGui::NextColumn();
			}


			nCount++;
			tfDirNext(dir);
		}

		ImGui::Columns(1);
		ImGui::Separator();

		ImGui::EndChild();

		ImGui::BeginChild("##FileControls", ImVec2(0, 0), false, ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoScrollbar);

		ImGui::InputText("##PATH", m_szCurShortened, sizeof(m_szCurShortened));
		if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Enter))) {
			bOpen = true;
		}

		if (ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Escape))) {
			m_bFileDialogOpen = false;
		}

		ImGui::SameLine();
		if (ImGui::Button("Open")) {
			// Open the selected file
			bOpen = true;
		}
		ImGui::SameLine();
		if (ImGui::Button("Cancel")) {
			// Close the dialog
			m_bFileDialogOpen = false;
		}
		if (m_bFilterExtensions) {
			char** szArray = new char*[s_SupportedTypes.size() + 2];
			szArray[0] = "All files...";
			szArray[1] = "All allowed files...";

			for (int i = 0; i < s_SupportedTypes.size(); i++) {
				std::string str = "*.";
				str += s_SupportedTypes[i];
				szArray[i + 2] = new char[str.length() + 1];
				strcpy(szArray[i + 2], str.c_str());
				szArray[i + 2][str.length()] = NULL;
			}

			ImGui::Combo("Extensions", &nExtensionsSelected, const_cast<const char**>(szArray), s_SupportedTypes.size() + 2);

			for (int i = 0; i < s_SupportedTypes.size(); i++) {
				delete[] szArray[i + 2];
			}

			delete szArray;
		}

		if (bOpen) {
			std::string pathToOpen = dir->path;
			pathToOpen += '/';
			pathToOpen += m_szCurShortened;

			char szbuf[MAX_PATH];
			memset(szbuf, 0, MAX_PATH);
			strcpy(szbuf, pathToOpen.c_str());
			PathCanonicalize(szbuf);

			tfDIR* newDir = new tfDIR();
			tfDirOpen(newDir, szbuf);
			if (newDir->handle != INVALID_HANDLE_VALUE) {
				// It's a directory. Switch to it!
				tfDirClose(dir);
				delete dir;
				dir = newDir;
				bOpen = false;

				strcpy(m_szCurPath, szbuf);
			}
			else {
				tfDirClose(newDir);
				delete newDir;
				// It's not a directory, must be a file or not exist
				FILE* fil = fopen(szbuf, m_szFileOpenFlags);
				if (fil) {
					// Yep, it's a file
					m_pSelectedFile = fil;
					bOpen = false;
					m_bFileDialogOpen = false;

					if (pfnOpenFileCallback != NULL) {
						pfnOpenFileCallback(pathToOpen.c_str(), fil);
					}
					tfDirClose(dir);
					delete dir;
					dir = NULL;
				}
				else {
					// File doesn't exist!
					nSelected = -1;
				}
			}
		}

		//ImGui::Separator();
		ImGui::EndChild();
		ImGui::End();
	}

}

//--------------------------------------------------------------------------------------
// Register class and create window
//--------------------------------------------------------------------------------------
HRESULT InitWindow(HINSTANCE _hInstance, int _nCmdShow)
{
	// Register class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = _hInstance;
	wcex.hIcon = LoadIcon(_hInstance, (LPCTSTR)IDI_TUTORIAL1);
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = nullptr;
	wcex.lpszClassName = L"PhysVisWindowClass";
	wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_TUTORIAL1);
	if (!RegisterClassEx(&wcex))
		return E_FAIL;

	// Create window
	g_hInst = _hInstance;
	RECT rc = { 0, 0, 1600, 900 };
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
	g_hWnd = CreateWindow(L"PhysVisWindowClass", L"PhysViz", WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, nullptr, nullptr, _hInstance,
		nullptr);
	if (!g_hWnd)
		return E_FAIL;

	ShowWindow(g_hWnd, _nCmdShow);

	return S_OK;
}


//--------------------------------------------------------------------------------------
// Called every time the application receives a message
//--------------------------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND _hWnd, UINT _message, WPARAM _wParam, LPARAM _lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	if (g_pRenderer != nullptr) {
		g_pRenderer->WndProc(((void*)&_hWnd), ((void*)&_message), ((void*)&_wParam), ((void*)&_lParam));
	}

	switch (_message)
	{
	case WM_PAINT:
		hdc = BeginPaint(_hWnd, &ps);
		EndPaint(_hWnd, &ps);
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefWindowProc(_hWnd, _message, _wParam, _lParam);
	}

	return 0;
}