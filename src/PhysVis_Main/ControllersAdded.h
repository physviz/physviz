#ifndef _CONTROLLERS_ADDED_H_
#define _CONTROLLERS_ADDED_H_

#include "../DefaultControl/controllers.h"
#include "../DefaultControl/IControllerData.h"
#include "../DefaultControl/adddefaultcontrol.h"
#include "../KeyMouseControl/addKeyMousecontrol.h"
#include "../XBoxControl/AddXboxController.h"
#ifndef NO_RIFT
#include "../RiftXBoxControl/addRiftXboxControl.h"
#endif
// #include "../LeapControl/addLeapControl.h"

#endif