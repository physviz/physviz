// KeyMouseControl.cpp 

#include "keyMouseControl.h"
#include "../DefaultControl/controllers.h"
#include "../DefaultControl/IControllerData.h"

#include <windows.h>

KeyMouseController* KeyMouseController::s_controller = nullptr;


KeyMouseController::KeyMouseController(string _type) :IController(_type)
{
	m_rotation = Vector2(0.0f, 0.0f);
	m_distance = 20.0f;
	m_drag = 0.9f;
	m_showControlParticles = false;
}

KeyMouseController::~KeyMouseController()
{
	//tidy away Direct Input Stuff
	if (m_pMouse)
	{
		m_pMouse->Unacquire();
		m_pMouse->Release();
	}
	if (m_pKeyboard)
	{
		m_pKeyboard->Unacquire();
		m_pKeyboard->Release();
	}
	if (m_pDirectInput) m_pDirectInput->Release();
}

KeyMouseController* KeyMouseController::Get()
{
	if (!s_controller)
	{
		s_controller = new KeyMouseController("KeyMouse");
	}

	return s_controller;
}

void KeyMouseController::Init(void* _data)
{
	IControllerData* IDC = (IControllerData*)_data;
	//Direct Input Stuff
	HRESULT hr = DirectInput8Create(IDC->m_hInst, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_pDirectInput, NULL);
	// initialize the keyboard
	hr = m_pDirectInput->CreateDevice(GUID_SysKeyboard, &m_pKeyboard, NULL);
	hr = m_pKeyboard->SetDataFormat(&c_dfDIKeyboard);
	hr = m_pKeyboard->SetCooperativeLevel(IDC->m_hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

	// initialize the mouse
	hr = m_pDirectInput->CreateDevice(GUID_SysMouse, &m_pMouse, NULL);
	hr = m_pMouse->SetCooperativeLevel(IDC->m_hWnd, DISCL_BACKGROUND | DISCL_NONEXCLUSIVE);
	hr = m_pMouse->SetDataFormat(&c_dfDIMouse);

	RECT rc;
	GetClientRect(IDC->m_hWnd, &rc);
	m_width = rc.right - rc.left;
	m_height = rc.bottom - rc.top;

}

Poll_Return KeyMouseController::Poll(float _dt, bool _isMain)
{
	if ((!_isMain && !m_alwaysTick) || (m_alwaysTick && _isMain))	return 	POLL_RETURN_CONT;	//just continue 

	//copy over old keyboard state
	memcpy(m_prevKeyboardState, m_keyboardState, sizeof(m_keyboardState));

	//clear out previous state
	ZeroMemory(&m_keyboardState, sizeof(m_keyboardState));

	// Read the keyboard device.
	HRESULT hr = m_pKeyboard->GetDeviceState(sizeof(m_keyboardState), (LPVOID)&m_keyboardState);
	if (FAILED(hr))
	{
		// If the keyboard lost focus or was not acquired then try to get control back.
		if ((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
		{
			m_pKeyboard->Acquire();
		}
		else
		{
			return POLL_RETURN_CONT;
		}
	}

	//clear out previous state
	ZeroMemory(&m_mouse_state, sizeof(m_mouse_state));

	// Read the Mouse device.
	hr = m_pMouse->GetDeviceState(sizeof(m_mouse_state), (LPVOID)&m_mouse_state);
	if (FAILED(hr))
	{
		// If the keyboard lost focus or was not acquired then try to get control back.
		if ((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
		{
			m_pMouse->Acquire();
		}
		else
		{
			return POLL_RETURN_CONT;
		}
	}

	Vector3 forward = Vector3(-1.0f, 0.0f, 0.0f);
	Vector3 right = Vector3(0.0f, 0.0f, 1.0f);
	Vector3 up = Vector3(0.0f, 1.0f, 0.0f);

	float speed = 20.0f;
	if (m_keyboardState[DIK_LCONTROL] & 0x80)
	{
		speed *= 2.0f;
	}

	//rotation from mouse
	m_rotation.x -= 0.01f * m_mouse_state.lX;
	m_rotation.y += 0.01f * m_mouse_state.lY;

	if (_isMain && (Controllers::GetTarget()))
	{
		m_distance += 0.01f *  m_mouse_state.lZ;
	}

	//aim camera where mouse says
	m_aim = Vector3::Transform(forward, Matrix::CreateFromYawPitchRoll(m_rotation.x, 0.0f, m_rotation.y));

	Matrix rot = Matrix::CreateRotationY(m_rotation.x);

	if (!(Controllers::GetTarget()))
	{
		bool keyPressed = false;
		//forward n back in the plane
		if (m_keyboardState[DIK_W] & 0x80)
		{
			m_acc += speed * Vector3::Transform(forward, rot);
			keyPressed = true;
		}
		if (m_keyboardState[DIK_S] & 0x80)
		{
			m_acc -= speed * Vector3::Transform(forward, rot);;
			keyPressed = true;
		}

		//strafe in the plane
		if (m_keyboardState[DIK_A] & 0x80)
		{
			m_acc += speed * Vector3::Transform(right, rot);;
			keyPressed = true;
		}
		if (m_keyboardState[DIK_D] & 0x80)
		{
			m_acc -= speed * Vector3::Transform(right, rot);;
			keyPressed = true;
		}

		//up n down
		if (m_keyboardState[DIK_SPACE] & 0x80)
		{
			m_acc.y += speed;;
			keyPressed = true;
		}
		if (m_keyboardState[DIK_LSHIFT] & 0x80)
		{
			m_acc.y -= speed;;
			keyPressed = true;
		}

		if (!keyPressed && (m_keyboardState[DIK_LCONTROL] & 0x80))
		{
			m_acc -= m_drag * m_vel;
			//get a double dose of drag to apply brakes if no keys pressed
		}
	}
	if (m_rotation.y > XM_PIDIV2 - 0.1f)
	{
		m_rotation.y = XM_PIDIV2 - 0.1f;
	}
	if (m_rotation.y < 0.1f - XM_PIDIV2)
	{
		m_rotation.y = 0.1f - XM_PIDIV2;
	}

	UpdatePos(_dt);

	if (m_vel.Length() < 0.1f)
	{
		m_vel = Vector3(0.0f);
	}

	if (Controllers::GetTarget())
	{
		Vector3 pos = (Vector3)*Controllers::GetTarget();
		m_view = Matrix::CreateLookAt(pos + m_distance * m_aim, pos, up);
	}
	else
	{
		Vector3 look = m_pos + m_aim;
		m_view = Matrix::CreateLookAt(m_pos, look, up);
	}

	return POLL_RETURN_CONT;
}
