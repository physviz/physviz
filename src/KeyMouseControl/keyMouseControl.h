#ifndef _KEY_MOUSE_CONTROL_H_
#define _KEY_MOUSE_CONTROL_H_
#include "..\DefaultControl\IController.h"
#include <dinput.h>
#include "..\DirectXTK\Inc\SimpleMath.h"

using namespace DirectX;
using namespace SimpleMath;

class KeyMouseController : public IController
{
public:
	virtual ~KeyMouseController();

	static KeyMouseController* Get();

	//stuff from IController
	virtual void Init(void* _data) override;
	virtual Poll_Return Poll(float _dt, bool _isMain) override;

private:
	KeyMouseController(string _type);//private and empty constructor
	static KeyMouseController* s_controller;

	// Dont forget to declare these two. You want to make sure they
	// are unaccessable otherwise you may accidently get copies of
	// your singleton appearing.
	KeyMouseController(KeyMouseController const&);              // Don't Implement as want it to be illegal to do this.
	void operator=(KeyMouseController const&);			// ditto

	IDirectInput8*			m_pDirectInput;
	IDirectInputDevice8*	m_pKeyboard;
	unsigned char			m_keyboardState[256];
	unsigned char			m_prevKeyboardState[256];
	IDirectInputDevice8*	m_pMouse;
	DIMOUSESTATE			m_mouse_state;

	Vector2 m_rotation;
};
#endif 