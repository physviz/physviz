#ifndef _PHYSVIS_IIMPORTMODULE_H_
#define _PHYSVIS_IIMPORTMODULE_H_

class Graph;
class IImportModule {
public:
	virtual ~IImportModule() {}

	virtual bool ImportFile(const char* _szPath) = 0;
	virtual bool ImportData(const char* _szData) = 0;
	virtual bool ImportFromPackage(const char* _szPath, const char* _szPackage) = 0;
	virtual Graph* GetGraph() = 0;
};

#endif