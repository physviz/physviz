#ifndef _IMPORTMODULEFACTORY_H_
#define _IMPORTMODULEFACTORY_H_

#include <string>
#include <vector>
#include "../Public/IImportModule.h"

#pragma optimize( "", off )
#if PHYSVIS_MAIN
#define IMPORTMODULE_INTERFACE __declspec( dllimport )
#else
#define IMPORTMODULE_INTERFACE __declspec( dllexport )
#endif

enum interfaceResult_e {
	INTERFACE_SUCCESS = 0,
	INTERFACE_FAIL
};

// Creation function typedefs
typedef void* (*InstantiateInterfaceFunc)();
typedef void* (*CreateInterfaceFunc)(const char *pName, interfaceResult_e* eResult);

// Interface factory
class CInterfaceFactory {
public:
	CInterfaceFactory() {};
	CInterfaceFactory(InstantiateInterfaceFunc pFunc, const char* pFileExtensions, const char* szDescription) {
		m_szDescription = szDescription;

		// Parse file extensions
		int nStrlen = strlen(pFileExtensions);
		std::string curExt = "";
		for (int i = 0; i < nStrlen; i++) {
			if (pFileExtensions[i] == '|') {
				m_vFileExtensions.push_back(curExt);
				curExt = "";
			}
			else {
				curExt += pFileExtensions[i];
			}
		}
		if (curExt.length() > 0) {
			m_vFileExtensions.push_back(curExt);
		}

		m_CreateFunc = pFunc;
	}
	~CInterfaceFactory() {
		if (m_pImportModule != NULL) {
			delete m_pImportModule;
		}
	};

	InstantiateInterfaceFunc	m_CreateFunc;
	std::vector<std::string> m_vFileExtensions;
	const char* m_szDescription;

	bool SupportsExtension(const char* szExt) {
		for (int i = 0; i < m_vFileExtensions.size(); i++) {
			if (szExt == m_vFileExtensions[i]) {
				return true;
			}
		}

		return false;
	}

	IImportModule* GetImportModule() {
		if (m_pImportModule == NULL) {
			m_pImportModule = (IImportModule*)m_CreateFunc();
		}

		return m_pImportModule;
	}

	void Shutdown() {
		delete this;
	}

private:
	IImportModule* m_pImportModule = NULL;
};


#define BEGIN_IMPORTMODULES() \
	extern "C" __declspec(dllexport) CInterfaceFactory** __GetImportModuleList(int& nImporterCount) { \
		CInterfaceFactory* __factories[] = {

#define EXPOSE_IMPORTMODULE(className, fileExtensions, descinfo) \
	new CInterfaceFactory(&##className##::___New, fileExtensions, descinfo)

#define END_IMPORTMODULES() \
			}; \
		nImporterCount = ((int)(sizeof(__factories)/sizeof(*__factories))); \
		return __factories; \
	}

#define DECLARE_IMPORTMODULE(className) \
	typedef className ThisClass; \
	typedef IImportModule BaseClass; \
	public: \
	static void* ___New() { return new className(); }

#pragma optimize( "", on ) 

#endif
