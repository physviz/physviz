#ifndef _STRING_UTILS_H_
#define _STRING_UTILS_H_

#include <string>
#include <vector>

// Purpose: Will strip chars strapping around a string, such as quotes or brackets
std::string UTIL_STR_StripWrapperChars(std::string strIn, char stripChar = '\"');
char* UTIL_STR_StripWrapperChars(char* strIn, char stripChar = '\"');

std::vector<char*>* UTIL_STR_BreakStringIntoSections(const char* strIn, char cDivider, bool bWithQuotes = false);

void UTIL_STR_MakeLowerCase(char* strIn);
void UTIL_STR_MakeUpperCase(char* strIn);

void UTIL_STR_ReplaceChars(char* strIn, char find, char replace);

// Is the string a valid integer? (func from StackOverflow)
bool UTIL_STR_IsInteger(const std::string & s);
// Is the string a valid float? (func from StackOverflow... again)
bool UTIL_STR_IsFloat(std::string myString);
bool UTIL_STR_IsNumerical(std::string stri);

bool UTIL_STR_IsInteger(const char* pszString);
bool UTIL_STR_IsFloat(const char* pszString);
bool UTIL_STR_IsNumerical(const char* pszString);

// Basic hashing algo that works by a pow2 and some multiplication
unsigned int UTIL_STR_BasicHash(const char* szString);

// Custom format tokens
char* UTIL_STR_ReplaceTokensInString(const char* szFormat, char** aTokens, char** aValues);

#endif // _TREMOR_STRING_UTILS_H_