#ifndef _OVR_STRUCTS_H_
#define _OVR_STRUCTS_H_

struct ovrRendererDetails_s
{
	void* m_pSession;
	int m_eyeHeight[2];
	int m_eyeWidth[2];
};

#endif