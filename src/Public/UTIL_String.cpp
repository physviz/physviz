#include "util_string.h"

std::string UTIL_STR_StripWrapperChars(std::string _strIn, char _stripChar) {
	std::string newStr = "";
	int i = 0;
	int end = _strIn.length();

	if (_strIn[0] == _stripChar) {
		i = 1;
	}

	if (_strIn[end] == _stripChar) {
		end--;
	}

	for (; i < end; i++) {
		newStr += _strIn[i];
	}

	return newStr;
}

char* UTIL_STR_StripWrapperChars(char* _strIn, char _stripChar) {
	int nStrlen = strlen(_strIn);
	if (_strIn[0] != _stripChar || _strIn[nStrlen - 1] != _stripChar) {
		// Just make a copy
		char* newStr = new char[nStrlen + 1];
		memcpy(newStr, _strIn, sizeof(char) * nStrlen);
		newStr[nStrlen] = 0;
		return newStr;
	}

	char* newStr = new char[nStrlen - 1];
	newStr[nStrlen - 2] = 0;

	int i = 1;
	int end = nStrlen - 1;

	int j = 0;
	for (; i < end; i++) {
		newStr[j] = _strIn[i];
		j++;
	}

	return newStr;
}

std::vector<char*>* UTIL_STR_BreakStringIntoSections(const char* _strIn, char _cDivider, bool _bWithQuotes /*= false*/) {
	// Copy the string
	// Set our divider chars to null
	// Set up pointers

	std::vector<char*>* pResultVec = new std::vector<char*>();
	int strLen = strlen(_strIn);
	char* copiedStr = new char[strLen + 1];
	strcpy(copiedStr, _strIn);
	copiedStr[strLen] = 0;

	char* currentPtr = copiedStr;
	bool bWithinQuotes = false;
	for (int i = 0; i < strLen; i++) {
		if (_bWithQuotes && copiedStr[i] == '"') {
			bWithinQuotes = !bWithinQuotes;
			continue;
		}

		if (copiedStr[i] == _cDivider && !bWithinQuotes) {
			copiedStr[i] = 0;
			pResultVec->push_back(currentPtr);

			if (strLen == i) {
				currentPtr = nullptr;
				break;
			}

			currentPtr = &copiedStr[i + 1];
		}
	}

	if (currentPtr != nullptr) {
		pResultVec->push_back(currentPtr);
	}

	if (_bWithQuotes) {
		// Strip all our quotes 
		for (int i = 0; i < pResultVec->size(); i++) {
			(*pResultVec)[i] = UTIL_STR_StripWrapperChars((*pResultVec)[i], '\"');
		}

		delete copiedStr;
	}

	return pResultVec;
}

void UTIL_STR_MakeLowerCase(char* _strIn)
{
	int nStrlen = strlen(_strIn);
	//char* lcStr = new char[nStrlen]{0};
	for (int i = 0; i < nStrlen; i++) {
		_strIn[i] = tolower(_strIn[i]);
	}

	//strIn[nStrlen] = 0;

	return;
}

void UTIL_STR_MakeUpperCase(char* _strIn)
{
	int nStrlen = strlen(_strIn);
	//char* ucStr = new char[nStrlen]{0};
	for (int i = 0; i < nStrlen; i++) {
		_strIn[i] = toupper(_strIn[i]);
	}

	//strIn[nStrlen] = 0;

	return;
}

void UTIL_STR_ReplaceChars(char* _strIn, char _find, char _replace)
{
	int nStrlen = strlen(_strIn);
	/*char* newStr = new char[nStrlen]{0};*/
	for (int i = 0; i < nStrlen; i++) {
		if (_strIn[i] == _find) {
			_strIn[i] = _replace;
		}
	}

	//strIn[nStrlen] = 0;

	return;
}

bool UTIL_STR_IsInteger(const std::string & _s) {
	if (_s.length() > 0 || ((!isdigit(_s[0])) && (_s[0] != '-') && (_s[0] != '+'))) return false;

	char * p;
	strtol(_s.c_str(), &p, 10);

	return (*p == 0);
}

bool UTIL_STR_IsFloat(std::string _myString) {
	return UTIL_STR_IsFloat(_myString.c_str());
}

bool UTIL_STR_IsNumerical(std::string _stri) {
	return UTIL_STR_IsFloat(_stri) || UTIL_STR_IsInteger(_stri);
}

unsigned int UTIL_STR_BasicHash(const char* _szString) {
	// Really basic hasing algorhthm
	unsigned int nNewHash = 0;
	unsigned int nCurrentPow2 = 256; // Start at 256, so that there can't be collisions (char is 0-255)
	const char* szCur;
	for (szCur = _szString; szCur[0] != NULL; ++szCur) {
		// Convert our current char to int
		unsigned int nCurChar = szCur[0];
		// Mult us by the current pow2
		nCurChar *= nCurrentPow2;
		// Add to the current hash value
		nNewHash += nCurChar;
		// Move to next pow2
		nCurrentPow2 *= 2;
	}

	return nNewHash;
}

#define REPLACETOKEN_BUFFERLENGTH 4096
char* UTIL_STR_ReplaceTokensInString(const char* _szFormat, char** _aTokens, char** _aValues) {
	char szBuffer[REPLACETOKEN_BUFFERLENGTH];
	memset(szBuffer, 0, sizeof(szBuffer));
	int nBufferPos = 0;
	int nFormatStrlen = strlen(_szFormat);
	for (int i = 0; i < nFormatStrlen; i++) {
		char** pCur;
		char** pCurVal = _aValues;
		bool bMatched = false;
		for (pCur = _aTokens; *pCur != 0; ++pCur, ++pCurVal) {
			if (strncmp(&_szFormat[i], *pCur, strlen(*pCur)) == 0) {
				// we've matched a token!
				bMatched = true;
				i += strlen(*pCur) - 1;
				memcpy(&szBuffer[nBufferPos], *pCurVal, strlen(*pCurVal));
				nBufferPos += strlen(*pCurVal);
			}
		}
		if (!bMatched) {
			szBuffer[nBufferPos++] = _szFormat[i];
		}
	}

	szBuffer[sizeof(szBuffer) - 1] = 0;
	return szBuffer;
}

bool UTIL_STR_IsInteger(const char* _pszString) {
	if (_pszString || strlen(_pszString) > 0 || _pszString[0] != 0 || ((!isdigit(_pszString[0])) && (_pszString[0] != '-') && (_pszString[0] != '+'))) {
		return false;
	}

	char * p;
	strtol(_pszString, &p, 10);

	return (*p == 0);
}

bool UTIL_STR_IsFloat(const char* _pszString) {
	/*std::istringstream iss(pszString);
	float f;
	iss >> std::noskipws >> f; // noskipws considers leading whitespace invalid
	// Check the entire string was consumed and if either failbit or badbit is set
	return iss.eof() && !iss.fail();*/

	// let's make a quick and dirty non-stl version here...
	int nStrlen = strlen(_pszString);
	for (int i = 0; i < nStrlen; i++) {
		if (!isdigit(_pszString[i]) && _pszString[i] != '.' && _pszString[i] != '-' && _pszString[i] != '+') {
			return false; // Found a non-numeric character - so return false
		}
	}

	// Only numeric (or + - .) chars found! Return true.
	return true;
}

bool UTIL_STR_IsNumerical(const char* _pszString) {
	return UTIL_STR_IsFloat(_pszString) || UTIL_STR_IsInteger(_pszString);
}