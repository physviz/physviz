#include "DefaultRenderer.h"
#include "RenderPass.h"
#include "DefaultRenderableScene.h"
#include "RenderItems.h"
#include <iostream>

DefaultRenderer::DefaultRenderer()
{
}


DefaultRenderer::~DefaultRenderer()
{
}

//make sure they are in order, but if have the same order then go by memory location (probably going to be in order that created?
bool PassOrder(RenderPass* _one, RenderPass* _two)
{
	if (_one->MyOrder() == _two->MyOrder())
	{
		return (_one < _two);
	}

	return (_one->MyOrder() < _two->MyOrder());
}

void DefaultRenderer::Render(float _dt, unsigned int _poll)
{
	InitRender(_dt, _poll);
	std::list<RenderPass*>::iterator it;
	for (it = m_activePasses.begin(); it != m_activePasses.end(); ++it)
	{
		if ((*it)->MyOrder() != -1)
		{
			m_pRenderData->m_pass = (*it);
			PrePass(m_pRenderData);
			RenderItems RI = (*it)->Render(m_pRenderData);
			m_pRenderableScene->Render(RI, m_pRenderData, _dt, _poll);
			(*it)->PostRender(m_pRenderData);
		}

	}
	PostRender();
}

void DefaultRenderer::AddPass(RenderPass* _pass)
{
	m_passes.insert(pair<string, RenderPass*>(_pass->GetType(), _pass));
}

void DefaultRenderer::AddActivePass(string _pass)
{
	RenderPass* newPass = m_passes[_pass];
	if (newPass)
	{
		m_activePasses.push_back(newPass);
		m_activePasses.sort(PassOrder);
	}
	else
	{
		cout << "Pass does not exist:" << _pass << endl;
	}
}


void DefaultRenderer::ClearActivePasses()
{
	m_activePasses.clear();
}