#ifndef _RENDER_PASS_H_
#define _RENDER_PASS_H_
#include <string>
#include "RenderItems.h"

using namespace std;

struct RenderPassData
{

};

struct RenderData;

class RenderPass
{
public:
	RenderPass(string _type = "INVALID_PASS") :m_order(-1), m_type(_type)
	{}

	virtual ~RenderPass() {};

	virtual void InitPass(RenderPassData* _data) = 0;

	virtual RenderItems Render(RenderData* _data) = 0;

	virtual int SetUpFromRenderable(void* _renderable, void* _data) = 0;

	virtual void PostRender(RenderData* _data) {};

	const int MyOrder() { return m_order; }

	string GetType() { return m_type; }

	virtual void* GetProj() = 0;

protected:
	int m_order;
	std::string m_type;

};

struct RenderData
{
	RenderPass* m_pass;
	float m_shade = 1.0;
};
#endif