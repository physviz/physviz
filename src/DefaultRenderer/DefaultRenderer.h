#ifndef _DEFAULT_RENDERER_H_
#define _DEFAULT_RENDERER_H_
#include <list>
#include <map>
#include <string>

using namespace std;

class RenderPass;
struct RenderData;
class Scene;
class DefaultRenderableScene;

struct RendererData
{
};

class DefaultRenderer
{
public:
	DefaultRenderer();
	virtual ~DefaultRenderer();

	virtual bool Init(RendererData* _inData) = 0;
	virtual void Render(float _dt, unsigned int _poll);
	virtual void Shutdown() = 0;

	void AddPass(RenderPass* _pass);
	void AddActivePass(string _pass);
	void ClearActivePasses();

	virtual void* GetTexture(string _filename) = 0;

	virtual void LoadTexture(string _filename, string _package = "", bool _bCheckCase = false) = 0;
	virtual void FlushSideloadedTextures() = 0;

	virtual void AddScene(Scene* _realScene) = 0;

	virtual bool WndProc(void* _hWnd, void* _msg, void* _wParam, void* _lParam) { return true; };

	virtual void Gui_start() {};
	virtual void PostRender() = 0;
	virtual void RenderGUIOnly() = 0;

	virtual void ShutdownScene() {
		FlushSideloadedTextures(); // Flush any scene-specific textures

		if (m_pRenderableScene != nullptr) {
			delete m_pRenderableScene;
			m_pRenderableScene = nullptr;
		}
	}

protected:
	list<RenderPass*> m_activePasses;
	map<std::string, RenderPass*> m_passes;

	virtual void InitRender(float _dt, unsigned int _poll) = 0;
	virtual void PrePass(RenderData* _RD) {};
	virtual void InitTextures() = 0;

	DefaultRenderableScene* m_pRenderableScene = nullptr;

	RenderData* m_pRenderData = nullptr;
};

#endif