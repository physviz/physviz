#ifndef _RENDER_ITEMS_H_
#define _RENDER_ITEMS_H_

enum RenderItems
{
	RI_NONE = 0,
	RI_OTHER = 1,
	RI_PARTICLES = 2,
	RI_LINES = 4,
	RI_LABELS = 8
};

#endif