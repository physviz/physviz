#ifndef _DEFAULT_RENDERABLE_SCENE_H_
#define _DEFAULT_RENDERABLE_SCENE_H_
#include "RenderItems.h"
#include <string>
class Scene;
struct RenderData;
class Particle;
class Line;

class DefaultRenderableScene
{
public:
	DefaultRenderableScene(Scene* _realScene);
	virtual ~DefaultRenderableScene() {};

	virtual void Render(RenderItems _items, RenderData* _data, float _dt, unsigned int _poll);

protected:

	virtual void DrawOther(RenderData* _data) = 0;
	virtual void DrawParticles(RenderData* _data) = 0;
	virtual void DrawLines(RenderData* _data) = 0;
	virtual void DrawLabels(RenderData* _data) = 0;

	int m_maxLabels;
	float m_labelDistance[10];
	Scene* m_scene;

	bool m_drawLabels = true;

	int CurrentSubScreen(std::string _pass);
	int NumSubScreens(std::string _pass);
};

#endif