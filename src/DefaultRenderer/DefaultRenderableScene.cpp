#include "DefaultRenderableScene.h"
#include "../DefaultControl//pollreturn.h"
#include <string>

DefaultRenderableScene::DefaultRenderableScene(Scene* _realScene)
{
	m_maxLabels = 50;
	for (int i = 0; i < 10; i++)
	{
		m_labelDistance[i] = 100.0f;
	}
}

int DefaultRenderableScene::NumSubScreens(std::string _pass)
{
	int val = (int)(_pass.c_str()[_pass.length() - 2] - '0');
	if (val < 1 || val > 4) { val = -1; }
	return val;
}

int DefaultRenderableScene::CurrentSubScreen(std::string _pass)
{
	int val = (int)(_pass.c_str()[_pass.length() - 1] - '0');
	if (val < 0 || val > 3) { val = -1; }
	//REALLLY HACKY way of stopping the XBOX particles to appear in the rift view
	std::string rift = "RIFT";
	if (strncmp(_pass.c_str(), rift.c_str(), strlen(rift.c_str())) == 0) return -1;
	return val;
}

void DefaultRenderableScene::Render(RenderItems _items, RenderData* _data, float _dt, unsigned int _poll)
{
	if (_poll & POLL_TOGGLE_LABELS)
	{
		m_drawLabels = !m_drawLabels;
	}
	if (_items & RI_PARTICLES)
	{
		DrawParticles(_data);
	}
	if (_items & RI_LINES)
	{
		DrawLines(_data);
	}
	if (_items & RI_OTHER)
	{
		DrawOther(_data);
	}
	if (_items & RI_LABELS && m_drawLabels)
	{
		DrawLabels(_data);
	}
}