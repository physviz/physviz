#include "PhysVis_JSON.h"
#include "VEEHandler.h"
#include "../JSON/reader.h"
#include "../JSON/filereadstream.h"
#include "../JSON/document.h"
#include "../JSON/encodedstream.h"
#include "physfs-3.0.1/src/physfs.h"
#include "physfs-3.0.1/src/streams/physfs_file_system.hpp"
#include "physfs-3.0.1/src/streams/ifile_stream.hpp"
#include "../JSON/istreamwrapper.h"

using namespace rapidjson;
using namespace std;

bool CJSONImporter::ImportFile(const char* _szPath) {
	try {
		string fetch = _szPath;
		FILE* fp = fopen(fetch.c_str(), "rb"); // non-Windows use "r"
		char readBuffer[65536];
		VEEHandler veeHandler;
		FileReadStream is(fp, readBuffer, sizeof(readBuffer));

		Reader reader;
		reader.Parse(is, veeHandler);

		fclose(fp);

		m_pGraph = veeHandler.myGraph();
		return true;
	}
	catch (std::exception* e) {
		return false;
	}
}

Graph* CJSONImporter::GetGraph() {
	return m_pGraph;
}

bool CJSONImporter::ImportData(const char* _szData) {
	try {
		StringStream ss(_szData);
		VEEHandler veeHandler;
		Reader reader;
		Document doc;
		reader.Parse(ss, veeHandler);
		m_pGraph = veeHandler.myGraph();
		return true;
	}
	catch (std::exception* e) {
		return false;
	}
}

bool CJSONImporter::ImportFromPackage(const char* _szPath, const char* _szPackage) {
	try {
		if (!PHYSFS_isInit()) {
			PHYSFS_init(0);
		}
		PHYSFS_mount(_szPackage, "", 0);

		auto_ptr<std::istream> stream = PhysFSFileSystem::open_file(_szPath);
		IStreamWrapper wrappedStream(*stream.get()); // TWO MORE EGGS
		VEEHandler veeHandler;
		Reader reader;
		reader.Parse(wrappedStream, veeHandler);
		m_pGraph = veeHandler.myGraph();

		PHYSFS_unmount(_szPackage);
		return true;
	}
	catch (std::exception* e) {
		return false;
	}
}



BEGIN_IMPORTMODULES()
EXPOSE_IMPORTMODULE(CJSONImporter, "json|jso", "JSON Files")
END_IMPORTMODULES()
