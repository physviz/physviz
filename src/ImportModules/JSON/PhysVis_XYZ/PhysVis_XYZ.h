#ifndef _PHYSVIS_JSON_IMPORTER_H_
#define _PHYSVIS_JSON_IMPORTER_H_

#include "ImportModuleFactory.h"

class CXYZImporter : public IImportModule {
	DECLARE_IMPORTMODULE(CXYZImporter)

public:
	virtual bool ImportFile(const char* _szPath);
	virtual Graph* GetGraph();
	virtual void Shutdown() {
		delete this;
	}
	virtual bool ImportData(const char* _szData);
	virtual bool ImportFromPackage(const char* _szPath, const char* _szPackage);

private:
	Graph* m_pGraph;
};


#endif