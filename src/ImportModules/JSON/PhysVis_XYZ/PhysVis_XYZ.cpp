#include "PhysVis_XYZ.h"
#include "XYZHandler.h"

#include "physfs-3.0.1/src/physfs.h"
#include "physfs-3.0.1/src/streams/physfs_file_system.hpp"

using namespace std;

bool CXYZImporter::ImportFile(const char* _szPath) {
	try {
		XYZHandler xyzHandler(_szPath);
		xyzHandler.parse();
		m_pGraph = xyzHandler.myGraph();
		return true;
	}
	catch (std::exception* e) {
		return false;
	}
}

Graph* CXYZImporter::GetGraph() {
	return m_pGraph;
}

bool CXYZImporter::ImportData(const char* _szData) {
	return false; // TODO: Implement me!
}

bool CXYZImporter::ImportFromPackage(const char* _szPath, const char* _szPackage) {
	return false; // Implementation was attempted but it broke XYZ support entirely due to api incompatibilities between fstream and istream :c
}


BEGIN_IMPORTMODULES()
EXPOSE_IMPORTMODULE(CXYZImporter, "xyz", "XYZ Files")
END_IMPORTMODULES()
