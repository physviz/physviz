#include "controllers.h"
#include "defaultcontrol.h"
#include "..\Storage\particledata.h"

Controllers* Controllers::s_controllers = nullptr;

Controllers::Controllers() : m_current(0), m_target(nullptr), m_focus(false)
{

}

Controllers::~Controllers()
{
	for (int i = 0; i < m_controllers.size(); i++)
	{
		delete m_controllers[i];
	}
	m_controllers.clear();
}

Controllers* Controllers::Get()
{
	if (!s_controllers)
	{
		s_controllers = new Controllers();
	}

	return s_controllers;
}

void Controllers::Init(void* _data)
{
	for (int i = 0; i < m_controllers.size(); i++)
	{
		m_controllers[i]->Init(_data);
	}
}

unsigned int Controllers::Poll(float _dt, DefaultRenderer* _renderer)
{
	int o_current = m_current;
	if (m_controllers.size() == 0) return POLL_RETURN_CONT;

	unsigned int  outcome = POLL_RETURN_CONT;

	for (int i = 0; i < m_controllers.size(); i++)
	{
		outcome |= m_controllers[i]->Poll(_dt);
	}

	outcome |= m_controllers[m_current]->Poll(_dt, true);//calling the current control system

	if (outcome & POLL_TOGGLE_FOCUS)
	{
		m_focus = !m_focus;
	}

	if (outcome & POLL_RETURN_DOWN)
	{
		m_current--;
		if (m_current < 0) m_current += m_controllers.size();
	}

	if (outcome & POLL_RETURN_UP)
	{
		m_current++;
		if (m_current >= m_controllers.size()) m_current -= m_controllers.size();
	}

	if (_renderer && o_current != m_current)
	{
		m_controllers[m_current]->UpdateRenderPasses(_renderer);
	}

	return 	outcome;
}

IController* Controllers::GetHMD() {
	for (int i = 0; i < m_controllers.size(); i++) {
		if (m_controllers[i]->IsHMD()) {
			return m_controllers[i];
		}
	}

	return NULL;
}

int Controllers::AddController(IController* _controller)
{
	m_controllers.push_back(_controller);
	return 1;
}

Matrix& Controllers::GetView(string _pass)
{
	return m_controllers[m_current]->GetView(_pass);
}

Vector3 Controllers::GetPos(string _pass)
{
	return m_controllers[m_current]->GetPos(_pass);
}


ParticleData** Controllers::GetParticles(int& _num_parts, vector<string>& _names)
{
	if (s_controllers)
	{
		_num_parts = 0;
		for (int i = 0; i < m_controllers.size(); i++)
		{
			_num_parts += m_controllers[i]->GetNumParts();
		}
		ParticleData** parts = new ParticleData*[_num_parts];
		int j = 0;
		for (int i = 0; i < m_controllers.size(); i++)
		{
			if (m_controllers[i]->GetNumParts() > 0)
			{
				for (int k = 0; k < m_controllers[i]->GetNumParts(); k++)
				{
					parts[j++] = (m_controllers[i]->GetParticle()) + k;//YEA, pointer math!
					_names.push_back(Get()->GetControllerName(i));
				}
			}
		}
		return parts;
	}

	return nullptr;
}


string Controllers::GetControllerName(int _i)
{
	if (s_controllers)
	{
		return m_controllers[_i]->Type();
	}

	return "";
}