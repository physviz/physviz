#ifndef _DEFAULT_CONTROL_H_
#define _DEFAULT_CONTROL_H_
#include "IController.h"
#include <dinput.h>

class Controller : public IController
{
public:
	virtual ~Controller();

	static Controller* Get();

	//stuff from IController
	virtual void Init(void* _data) override;
	virtual Poll_Return Poll(float _dt, bool _isMain) override;

private:
	Controller(string _type);//private and empty constructor
	static Controller* s_controller;

	// Dont forget to declare these two. You want to make sure they
	// are unaccessable otherwise you may accidently get copies of
	// your singleton appearing.
	Controller(Controller const&);              // Don't Implement as want it to be illegale to do this.
	void operator=(Controller const&);			// ditto

	IDirectInput8*			m_pDirectInput;
	IDirectInputDevice8*	m_pKeyboard;
	unsigned char			m_keyboardState[256];
	unsigned char			m_prevKeyboardState[256];
	float m_t = 0.0f;
	int m_CentreX, m_CentreY;

};
#endif 