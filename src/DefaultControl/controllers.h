#ifndef _CONTROLLERS_H_
#define _CONTROLLERS_H_
#include <vector>
#include <string>
#include "pollreturn.h"
#include <d3d11.h>
#include "../DirectXTK/Inc/SimpleMath.h"
#include "IController.h"

using namespace std;
using namespace DirectX;
using namespace SimpleMath;

//class IController;
class DefaultRenderer;
struct ParticleData;

class Controllers
{

public:
	~Controllers();

	static Controllers* Get();

	//stuff from IController
	virtual void Init(void* _data);
	virtual unsigned int Poll(float _dt, DefaultRenderer* _renderer = nullptr);

	virtual IController* GetHMD();

	int AddController(IController* _controller);

	Matrix& GetView(string _pass = "NO_SUBSCREENS");
	Vector3 GetPos(string _pass = "NO_SUBSCREENS");

	static void SetTarget(Vector3* _inTarget) { s_controllers->m_target = _inTarget; }
	static Vector3* GetTarget() { if (s_controllers->m_focus) { return s_controllers->m_target; } else return nullptr; }

	ParticleData** GetParticles(int& _num_parts, vector<string>& _names);

	string GetControllerName(int _i);

	string GetControllerName() { return GetControllerName(m_current); }

	bool ShowControlParticles() { return m_controllers[m_current]->m_showControlParticles; }

private:
	Controllers();//private and empty constructor
	static Controllers* s_controllers;

	// Dont forget to declare these two. You want to make sure they
	// are unaccessable otherwise you may accidently get copies of
	// your singleton appearing.
	Controllers(Controllers const&);            // Don't Implement as want it to be illegal to do this.
	void operator=(Controllers const&);			// ditto

	vector<IController*> m_controllers;

	int m_current = 0;
	Vector3* m_target;
	bool m_focus;
};

#endif