#ifndef _POLL_RETURN_H_
#define _POLL_RETURN_H_

enum Poll_Return
{
	POLL_RETURN_CONT = 0,	//just continue
	POLL_RETURN_OFF = 1,	//quit signal
	POLL_RETURN_UP = 2,		//move up the list of controllers
	POLL_RETURN_DOWN = 4,	//move down the list of controllers 	
	POLL_TOGGLE_FOCUS = 8,	//toggle follow cam
	POLL_FOCUS_UP = 16,		//move up of interest list
	POLL_FOCUS_DOWN  =32,	//move down of interest list
	POLL_TOGGLE_PHYSICS = 64,    // pause physics on and off
	POLL_TOGGLE_LABELS = 128,	//toggle display all text
	POLL_BG_UP = 256,  //shift grey of background up
	POLL_BG_DOWN = 512 //shift grey of background down
};

#endif