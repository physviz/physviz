#include "IController.h"
#include "../DefaultControl/controllers.h"
#include "../DefaultRenderer/DefaultRenderer.h"

IController::IController(string _type) :m_type(_type), m_alwaysTick(false)
{
	m_view = Matrix::Identity;
	m_aim = Vector3(1.0, 0.0, 0.0);
	m_pos = Vector3(10.0f, 0.0f, 0.0f);
	m_vel = Vector3(0.0f, 0.0f, 0.0f);
	m_acc = Vector3(0.0f, 0.0f, 0.0f);
	m_particle = nullptr;
	m_drag = 0.9f;
}

Vector3 IController::GetPos(string _pass)
{
	if (Controllers::GetTarget())
	{
		return *Controllers::GetTarget() + m_distance * m_aim;
	}
	else
	{ 
		return m_pos;
	}
}


void IController::UpdateRenderPasses(DefaultRenderer* _renderer)
{
	_renderer->ClearActivePasses();
	_renderer->AddActivePass("BASE_PART_PASS");
	_renderer->AddActivePass("BASE_ZONE_PASS");
	_renderer->AddActivePass("BASE_LINE_PASS");
	_renderer->AddActivePass("BASE_LABEL_PASS");
}



void IController::UpdatePos(float _dt)
{
	Vector3 newPos = m_pos + _dt * m_vel;
	Vector3 newVel = m_vel + _dt * (m_acc - m_drag * m_vel);
	m_pos = newPos;
	m_vel = newVel;
	m_acc = Vector3(0.0f);
}

int IController::Subscreen(string _pass)
{
	char c_subscreen = _pass.substr(_pass.length() - 1, 1).c_str()[0];
	return (int)(c_subscreen - '0');
}