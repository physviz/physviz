#ifndef _ICONTROLLER_H_
#define _ICONTROLLER_H_
#include <string>
#include <d3d11.h>
#include "..\DirectXTK\Inc\SimpleMath.h"
#include "pollreturn.h"
//Interface class for the Controllers allows us to chuck them in the same data structure
//but we'll have the singlton stuff laters

using namespace std;
using namespace DirectX;
using namespace SimpleMath;

class DefaultRenderer;
struct ParticleData;

class IController
{
public:
	IController(string _type);
	~IController() {
		if (m_particle != nullptr) {
			delete m_particle;
		}
	}

	virtual void Init(void* _data) = 0;
	virtual Poll_Return Poll(float _dt = 0.1f, bool _isMain = 0) = 0; //quit on returns false

	string Type() { return m_type; }

	virtual Matrix& GetView(string _pass) { return m_view; }
	virtual Vector3 GetPos(string _pass);

	virtual void UpdateRenderPasses(DefaultRenderer * _renderer);

	ParticleData* GetParticle() { return m_particle; }

	virtual void* GetDataObject(const char* _desc, void* _extraData) { return NULL; }
	virtual void  SendDataObject(const char* _desc, void* _data) {}

	virtual bool IsHMD() { return false; }

	bool m_showControlParticles = true;

	int GetNumParts() { return m_numParticles; }

protected:
	void UpdatePos(float _dt);

	string m_type;
	Matrix m_view;
	bool m_alwaysTick;
	unsigned int m_width, m_height;
	Vector3 m_pos, m_aim;
	Vector3 m_vel;
	Vector3 m_acc;
	float m_drag;

	float m_distance; //for focus camera

	ParticleData* m_particle = nullptr;
	int m_numParticles = 0;

	int Subscreen(string _pass);

};
#endif