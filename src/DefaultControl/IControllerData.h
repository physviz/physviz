#ifndef _I_CONTROLLER_DATA_H_
#define _I_CONTROLLER_DATA_H_
#include <Windows.h>

struct IControllerData
{
	HINSTANCE m_hInst;
	HWND m_hWnd;
};

#endif