#include "defaultcontrol.h"
#include "controllers.h"
#include "IControllerData.h"

#include <windows.h>

Controller* Controller::s_controller = nullptr;


Controller::Controller(string _type) :IController(_type), m_t(0.0f)
{
	m_alwaysTick = true;
	m_distance = 200.0f;
	m_pos = Vector3(10.0f, 0.0f, 0.0f);
	m_vel = Vector3(0.0f, 0.0f, 0.0f);
	m_acc = Vector3(0.0f, 0.0f, 0.0f);
}

Controller::~Controller()
{
	//tidy away Direct Input Stuff
	if (m_pKeyboard)
	{
		m_pKeyboard->Unacquire();
		m_pKeyboard->Release();
	}
	if (m_pDirectInput) m_pDirectInput->Release();
}

Controller* Controller::Get()
{
	if (!s_controller)
	{
		s_controller = new Controller("default");
	}

	return s_controller;
}

void Controller::Init(void* _data)
{
	IControllerData* IDC = (IControllerData*)_data;
	//Direct Input Stuff
	HRESULT hr = DirectInput8Create(IDC->m_hInst, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_pDirectInput, NULL);
	hr = m_pDirectInput->CreateDevice(GUID_SysKeyboard, &m_pKeyboard, NULL);
	hr = m_pKeyboard->SetDataFormat(&c_dfDIKeyboard);
	hr = m_pKeyboard->SetCooperativeLevel(IDC->m_hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);

	RECT rc;
	GetClientRect(IDC->m_hWnd, &rc);
	m_width = rc.right - rc.left;
	m_height = rc.bottom - rc.top;

	//used to lock cursor
	GetWindowRect(IDC->m_hWnd, &rc);
	m_CentreX = (rc.right + rc.left) / 2;
	m_CentreY = (rc.bottom + rc.top) / 2;
}

Poll_Return Controller::Poll(float _dt, bool _isMain)
{
	if (_isMain && (Controllers::GetTarget()))
	{
		if ((m_keyboardState[DIK_Q] & 0x80))
		{
			m_distance += 100.0f * _dt;
		}
		if ((m_keyboardState[DIK_W] & 0x80))
		{
			m_distance -= 100.0f * _dt;
		}
	}

	if (!m_alwaysTick || (m_alwaysTick && _isMain))	return 	POLL_RETURN_CONT;	//just continue 


	//copy over old keyboard state
	memcpy(m_prevKeyboardState, m_keyboardState, sizeof(m_keyboardState));

	//clear out previous state
	ZeroMemory(&m_keyboardState, sizeof(m_keyboardState));

	// Read the keyboard device.
	HRESULT hr = m_pKeyboard->GetDeviceState(sizeof(m_keyboardState), (LPVOID)&m_keyboardState);
	if (FAILED(hr))
	{
		// If the keyboard lost focus or was not acquired then try to get control back.
		if ((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
		{
			m_pKeyboard->Acquire();
		}
		else
		{
			return POLL_RETURN_CONT;
		}
	}

	//escape to quit
	if (m_keyboardState[DIK_ESCAPE] & 0x80)
	{
		return POLL_RETURN_OFF;
	}

	unsigned int outcome = POLL_RETURN_CONT;

	//+ on numpad or keybpard  to move up controllers
	if ((m_keyboardState[DIK_NUMPADPLUS] & 0x80) && !(m_prevKeyboardState[DIK_NUMPADPLUS] & 0x80)
		||
		(m_keyboardState[DIK_EQUALS] & 0x80) && !(m_prevKeyboardState[DIK_EQUALS] & 0x80))
	{
		outcome |= POLL_RETURN_UP;
	}

	//- on main keyboard or number pad move down controllers
	if ((m_keyboardState[DIK_MINUS] & 0x80) && !(m_prevKeyboardState[DIK_MINUS] & 0x80)
		||
		(m_keyboardState[DIK_NUMPADMINUS] & 0x80) && !(m_prevKeyboardState[DIK_NUMPADMINUS] & 0x80))
	{
		outcome |= POLL_RETURN_DOWN;
	}

	//toggle to on focus cam
	if ((m_keyboardState[DIK_TAB] & 0x80) && !(m_prevKeyboardState[DIK_TAB] & 0x80))
	{
		outcome |= POLL_TOGGLE_FOCUS;
	}
	//move focus cam up and down
	if ((m_keyboardState[DIK_1] & 0x80) && !(m_prevKeyboardState[DIK_1] & 0x80))
	{
		outcome |= POLL_FOCUS_UP;
	}
	if ((m_keyboardState[DIK_2] & 0x80) && !(m_prevKeyboardState[DIK_2] & 0x80))
	{
		outcome |= POLL_FOCUS_DOWN;
	}
	if ((m_keyboardState[DIK_P] & 0x80) && !(m_prevKeyboardState[DIK_P] & 0x80))
	{
		outcome |= POLL_TOGGLE_PHYSICS;
	}
	if ((m_keyboardState[DIK_L] & 0x80) && !(m_prevKeyboardState[DIK_L] & 0x80))
	{
		outcome |= POLL_TOGGLE_LABELS;
	}
	if ((m_keyboardState[DIK_LBRACKET] & 0x80))
	{
		outcome |= POLL_BG_DOWN;
	}
	if ((m_keyboardState[DIK_RBRACKET] & 0x80))
	{
		outcome |= POLL_BG_UP;
	}

	m_t += _dt * 0.1f;
	if (m_t > XM_2PI) m_t -= XM_2PI;
	Vector3 at = (Controllers::GetTarget()) ? *Controllers::GetTarget() : Vector3(0.0f, 0.0f, 0.0f);
	Vector3 eye = at + m_distance * Vector3(cos(m_t), 0.0f, sin(m_t));
	Vector3 up = Vector3(0.0f, 1.0f, 0.0f);

	m_view = Matrix::CreateLookAt(eye, at, up);

	m_pos = eye;
	//SetCursorPos(CentreX, CentreY);

	return (Poll_Return)outcome;
}