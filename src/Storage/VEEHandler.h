#ifndef _VEEHANDLER_H_
#define _VEEHANDLER_H_
#include "attrib.h"
#include <stdint.h>
#include <iostream>

// #include "../JSON/reader.h"

//#define BUILDING

class Graph;
class Link;
class Node;
class AttribArray;

//VEE Handler State
enum VHS;

using namespace std;

class VEEHandler /*: BaseReaderHandler<>*/ {

public:

	VEEHandler();
	~VEEHandler();

	Graph* myGraph(){ return m_graph; }

	//functions required by rapidJSON
#ifdef BUILDING
	bool Null() { cout << "Null()" << endl; return readValue( nullptr, ATTRIB_NULL); }
	bool Bool(bool b) { cout << "Bool(" << boolalpha << b << ")" << endl; return readValue(&b,ATTRIB_BOOL); }
	bool Int(int i) { cout << "Int(" << i << ")" << endl; return readValue(&i, ATTRIB_INT); }
	bool Int64(int64_t i) { cout << "Int64(" << i << ")" << endl; return readValue(&i, ATTRIB_INT64); }
	bool Double(double d){ cout << "Double(" << d << ")" << endl; return readValue(&d, ATTRIB_DOUBLE); }
#else
	bool Null() { return readValue(nullptr, ATTRIB_NULL); }
	bool Bool(bool b) { return readValue(&b, ATTRIB_BOOL); }
	bool Int(int i) { return readValue(&i, ATTRIB_INT); }
	bool Int64(int64_t i) { return readValue(&i, ATTRIB_INT64); }
	bool Double(double d){ return readValue(&d, ATTRIB_DOUBLE); }
#endif
	bool Uint(unsigned u);
	bool Uint64(uint64_t u);
	bool RawNumber(const char* str, size_t len, bool copy) { return String(str, len, copy); } // NEW
	bool String(const char* str, unsigned length, bool copy);
	bool StartObject();
	bool Key(const char* str, size_t len, bool copy) { return String(str, len, copy); } // NEW
	bool EndObject(unsigned memberCount);
	bool StartArray();
	bool EndArray(unsigned elementCount);

protected:

	bool readValue(void* _val, AttribType _type);
	Graph* m_graph;
	Link* m_currLink;
	Node* m_currNode;
	AttribArray* m_currAA;
	Attrib* m_currAttrib;
	VHS m_state;
};

#endif