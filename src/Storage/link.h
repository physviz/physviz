#ifndef _LINK_H_
#define _LINK_H_
#include "node.h"

class Particle;


class Link : public Node
{
public:
	Link();
	~Link();

	bool Recast(void *_nodes);

	void SetFrom(Node* _from) { m_from = _from; }
	void SetTo(Node* _to) { m_to = _to; }

	Node* GetFrom() { return m_from; }
	Node* GetTo() { return m_to; }


	bool IsRenderable();

	void GetParticles(Particle** _from, Particle** _to);

	void SetRecast(bool _recast) { m_recast = _recast; }

protected:
	Node* m_from;
	Node* m_to;

	bool m_recast;
};

#endif