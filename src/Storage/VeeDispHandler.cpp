#include "VEEDispHandler.h"
#include "NodeDispParam.h"
#include "node.h"
#include "particleh.h"
#include "line.h"
#include "link.h"
#include "../DefaultRenderer/DefaultRenderer.h"
#include "graph.h"

//this thing doesn't give nice null terminated arrays let's fix that
extern char* terminate(const char* _str, unsigned _length);
extern PDN GetPDN(string _string);
extern LDN GetLDN(string _string);

//VEE Disply Handler State
enum VDHS
{
	VDHS_BEFORE,
	VDHS_START_NODES,
	VDHS_READING_NODES,
	VDHS_READ_NODE_NAME_START,
	VDHS_READ_NODE_NAME,
	VDHS_READ_NODE_BY_NAME,
	VDHS_READ_NODE_TEX_START,
	VDHS_READ_NODE_TEX,
	VDHS_READ_NODE_ATTRIBS_START,
	VDHS_READ_NODE_ATTRIBS_ARRAY_1,
	VDHS_READ_NODE_ATTRIBS_ARRAY_2,
	VDHS_READ_NODE_ATTRIB_START,
	VDHS_READ_NODE_ATTRIBS_EXIT,
	VDHS_READ_NODE_EXIT,
	VDHS_READ_NODE_ATTRIB_NAME,
	VDHS_READ_NODE_ATTRIB_VALUE,
	VDHS_START_LINKS,
	VDHS_READ_LINK_START,
	VDHS_READ_LINK_NAME,
	VDHS_READ_LINK_TYPE,
	VDHS_READ_LINK_TYPE_VAL,
	VDHS_READ_LINK_ATTRIB_START,
	VDHS_READ_LINK_ATTRIB_NAME,
	VDHS_READ_LINK_ATTRIB_VALUE,
	VDHS_START_OF_INTEREST,
	VDHS_READ_NODE_OF_INTEREST,
	VDHS_READ_TYPE_OF_INTEREST,
	VDHS_START_FIXED,
	VDHS_READ_NODE_FIXED,
	VDHS_READ_TYPE_FIXED,
	VDHS_START_HIDE,
	VDHS_READ_NODE_HIDE,
	VDHS_READ_TYPE_HIDE,
	VDHS_COUNT
};

VEEDispHandler::VEEDispHandler() :m_state(VDHS_BEFORE), m_currNode(nullptr), m_currParam(nullptr), m_currLink(nullptr)
{

}

VEEDispHandler::~VEEDispHandler()
{
	// Serious memory leak (100+ hits on small files)!
	/*for (int i = 0; i < m_currNode->size(); i++) {
		delete (*m_currNode)[i];
	}*/
}


bool VEEDispHandler::StartObject()
{
#ifdef  BUILDING
	cout << "StartObject()" << endl;
#endif

	switch (m_state)
	{
	case VDHS_BEFORE:
		m_state = VDHS_START_NODES;
		return true;
	case VDHS_START_NODES:
		m_state = VDHS_READ_NODE_NAME_START;
		return true;
	case VDHS_READ_NODE_NAME_START:
		if (!m_currNode)
		{
			m_currNode = new vector<NodeDispParam*>();
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_READ_NODE_NAME:
		return true;
	case VDHS_READ_NODE_BY_NAME:
		return true;
	case VDHS_READ_NODE_ATTRIB_START:
		m_state = VDHS_READ_NODE_ATTRIB_NAME;
	case VDHS_READ_NODE_ATTRIB_NAME:
		if (!m_currParam)
		{
			m_currParam = new NodeDispParam();
			m_currNode->push_back(m_currParam);
		}
		return true;
	case VDHS_READ_LINK_START:
		if (!m_currLink)
		{
			m_currLink = new vector<NodeDispParam*>;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_READ_LINK_ATTRIB_NAME:
		if (!m_currParam)
		{
			m_currParam = new NodeDispParam();
			m_currLink->push_back(m_currParam);
		}
		return true;
	case VDHS_START_OF_INTEREST:
	case VDHS_START_FIXED:
	case VDHS_START_HIDE:
		return true;
	default:
		return false;
	}
}

bool VEEDispHandler::String(const char* _str, unsigned _length, bool _copy)
{

#ifdef  BUILDING
	cout << "String(" << _str << ", " << _length << ", " << boolalpha << _copy << ")" << endl;
#endif

	switch (m_state)
	{
	case VDHS_START_NODES:
		//if we are staring to load the nodes and this string is "nodes" we are doing okay
		return (strcmp(_str, "nodes") == 0);
	case VDHS_READ_NODE_NAME_START:
		if (strcmp(_str, "type") == 0)
		{
			m_state = VDHS_READ_NODE_NAME;
			return true;
		}
		else if (strcmp(_str, "node") == 0)
		{
			m_state = VDHS_READ_NODE_BY_NAME;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_READ_NODE_NAME:
	{
		string node = terminate(_str, _length);
		m_nodeParams.insert(pair<string, vector<NodeDispParam*>*>(node, m_currNode));
		m_state = VDHS_READ_NODE_TEX_START;
		return true;
	}
	case VDHS_READ_NODE_BY_NAME:
	{
		string node = terminate(_str, _length);
		m_nodeNameParams.insert(pair<string, vector<NodeDispParam*>*>(node, m_currNode));
		m_state = VDHS_READ_NODE_TEX_START;
		return true;
	}
	case VDHS_READ_NODE_TEX_START:
	{
		if (strcmp(_str, "PDN_TEXTURE") == 0 && !m_currParam)
		{
			m_currParam = new NodeDispParam();
			m_currParam->SetType("PDN_TEXTURE");
			m_currNode->push_back(m_currParam);
			m_state = VDHS_READ_NODE_TEX;
			return true;
		}
		else
		{
			return false;
		}
	}
	case VDHS_READ_NODE_TEX:
		if (strcmp(m_currParam->GetType().c_str(), "PDN_TEXTURE") == 0)
		{
			m_currParam->SetBase(terminate(_str, _length));
			m_currParam = nullptr;
			m_state = VDHS_READ_NODE_ATTRIBS_START;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_READ_NODE_ATTRIBS_START:
		if (strcmp(_str, "attributes") == 0)
		{
			m_state = VDHS_READ_NODE_ATTRIBS_ARRAY_1;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_READ_NODE_ATTRIB_NAME:
		if (m_currParam)
		{
			char* szDummy = terminate(_str, _length);
			m_attribName = szDummy;
			delete szDummy;
			m_state = VDHS_READ_NODE_ATTRIB_VALUE;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_READ_NODE_ATTRIB_VALUE:
		if (m_currParam)
		{
			char* szDummy = terminate(_str, _length);
			m_attribValS = szDummy;
			delete szDummy;
			m_currParam->SetValue(m_attribName, m_attribValF, m_attribValS);
			m_state = VDHS_READ_NODE_ATTRIB_NAME;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_START_LINKS:
		return (strcmp(_str, "relations") == 0);
	case VDHS_READ_LINK_START:
		if (strcmp(_str, "type") == 0)
		{
			m_state = VDHS_READ_LINK_NAME;
			return true;
		}
		else if (strcmp(_str, "ofinterest") == 0)
		{
			m_state = VDHS_START_OF_INTEREST;
			return true;
		}
		else if (strcmp(_str, "fixed") == 0)
		{
			m_state = VDHS_START_FIXED;
			return true;
		}
		else if (strcmp(_str, "hide") == 0)
		{
			m_state = VDHS_START_HIDE;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_READ_LINK_NAME:
	{
		string link = terminate(_str, _length);
		m_linkParams.insert(pair<string, vector<NodeDispParam*>*>(link, m_currLink));
		m_state = VDHS_READ_LINK_ATTRIB_START;
		return true;
	}
	case VDHS_READ_LINK_TYPE_VAL:
		m_currParam->SetType(terminate(_str, _length));
		return true;
	case VDHS_READ_LINK_ATTRIB_START:
		return (strcmp(_str, "attributes") == 0);
	case VDHS_READ_LINK_ATTRIB_NAME:
		if (m_currParam)
		{
			m_currParam->SetType(terminate(_str, _length));
			m_state = VDHS_READ_LINK_ATTRIB_VALUE;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_READ_LINK_ATTRIB_VALUE:
		if (m_currParam)
		{
			char* szDummy = terminate(_str, _length);
			m_attribValS = szDummy;
			delete szDummy;
			m_currParam->SetValue("base", m_attribValF, m_attribValS);
			m_state = VDHS_READ_LINK_ATTRIB_NAME;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_START_OF_INTEREST:
		if (strcmp(_str, "node") == 0)
		{
			m_state = VDHS_READ_NODE_OF_INTEREST;
			return true;
		}
		else if (strcmp(_str, "type") == 0)
		{
			m_state = VDHS_READ_TYPE_OF_INTEREST;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_READ_NODE_OF_INTEREST:
		m_state = VDHS_START_OF_INTEREST;
		m_nodesOfInterest.push_back(_str);
		return true;
	case VDHS_READ_TYPE_OF_INTEREST:
		m_state = VDHS_START_OF_INTEREST;
		m_typesOfInterest.push_back(_str);
		return true;
	case VDHS_START_FIXED:
		if (strcmp(_str, "node") == 0)
		{
			m_state = VDHS_READ_NODE_FIXED;
			return true;
		}
		else if (strcmp(_str, "type") == 0)
		{
			m_state = VDHS_READ_TYPE_FIXED;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_READ_NODE_FIXED:
		m_state = VDHS_START_FIXED;
		m_nodesFixed.push_back(_str);
		return true;
	case VDHS_READ_TYPE_FIXED:
		m_state = VDHS_START_FIXED;
		m_typesHide.push_back(_str);
		return true;
	case VDHS_START_HIDE:
		if (strcmp(_str, "node") == 0)
		{
			m_state = VDHS_READ_NODE_HIDE;
			return true;
		}
		else if (strcmp(_str, "type") == 0)
		{
			m_state = VDHS_READ_TYPE_HIDE;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_READ_NODE_HIDE:
		m_state = VDHS_START_HIDE;
		m_nodesHide.push_back(_str);
		return true;
	case VDHS_READ_TYPE_HIDE:
		m_state = VDHS_START_FIXED;
		m_typesHide.push_back(_str);
		return true;
	default:
		return false;
	}
}


bool VEEDispHandler::StartArray()
{
#ifdef  BUILDING
	cout << "StartArray()" << endl;
#endif

	switch (m_state)
	{
	case VDHS_START_NODES:
		m_state = VDHS_READ_NODE_NAME_START;
		return true;
	case VDHS_READ_NODE_ATTRIBS_ARRAY_1:
		m_state = VDHS_READ_NODE_ATTRIBS_ARRAY_2;
		return true;
	case VDHS_READ_NODE_ATTRIBS_ARRAY_2:
		m_state = VDHS_READ_NODE_ATTRIB_START;
		return true;
	case VDHS_START_LINKS:
		m_state = VDHS_READ_LINK_START;
		return true;
	case VDHS_READ_LINK_ATTRIB_START:
		m_state = VDHS_READ_LINK_ATTRIB_NAME;
		return true;
	case VDHS_READ_LINK_ATTRIB_NAME:
		return true;
	case VDHS_START_OF_INTEREST:
	case VDHS_START_FIXED:
	case VDHS_START_HIDE:
		return true;
	default:
		return false;

	}
}


bool VEEDispHandler::EndArray(unsigned _elementCount)
{
#ifdef BUILDING
	cout << "EndArray(" << _elementCount << ")" << endl;
#endif

	switch (m_state)
	{
	case VDHS_READ_NODE_ATTRIBS_EXIT:
	case VDHS_READ_NODE_ATTRIBS_ARRAY_2:
		m_currNode = nullptr;
		return true;
	case VDHS_READ_NODE_ATTRIB_START:
		m_state = VDHS_READ_NODE_ATTRIBS_EXIT;
		return true;
	case VDHS_READ_NODE_NAME_START:
		//I've reached the end of the Nodes on to the Links
		m_state = VDHS_START_LINKS;
		return true;
	case VDHS_READ_NODE_ATTRIB_NAME:
		m_state = VDHS_READ_NODE_ATTRIBS_EXIT;
		return true;
	case VDHS_READ_LINK_ATTRIB_NAME:
	case VDHS_READ_LINK_START:
	case VDHS_START_OF_INTEREST:
	case VDHS_START_FIXED:
	case VDHS_START_HIDE:
		m_state = VDHS_READ_LINK_START;
		return true;
	default:
		return false;
	}
}

bool VEEDispHandler::Double(double _d)
{
#ifdef BUILDING
	cout << "Double(" << _d << ")" << endl;
#endif

	switch (m_state)
	{
	case VDHS_READ_NODE_ATTRIB_VALUE:
		if (m_currParam)
		{
			m_attribValF = (float)_d;
			m_currParam->SetValue(m_attribName, m_attribValF, m_attribValS);
			m_state = VDHS_READ_NODE_ATTRIB_NAME;
			return true;
		}
		else
		{
			return false;
		}
	case VDHS_READ_LINK_ATTRIB_VALUE:
		if (m_currParam)
		{
			m_attribValF = (float)_d;
			m_currParam->SetValue("default", m_attribValF, m_attribValS);
			m_state = VDHS_READ_LINK_ATTRIB_NAME;
			return true;
		}
		else
		{
			return false;
		}
	default:
		return false;

	}
}

bool VEEDispHandler::EndObject(unsigned _memberCount)
{

#ifdef BUILDING
	cout << "EndObject(" << _memberCount << ")" << endl;
#endif

	switch (m_state)
	{
	case VDHS_READ_NODE_ATTRIB_NAME:
		m_currParam = nullptr;
		return true;
	case VDHS_READ_NODE_ATTRIBS_EXIT:
		m_state = VDHS_READ_NODE_NAME_START;
		return true;
	case VDHS_READ_LINK_ATTRIB_NAME:
		m_currParam = nullptr;
		return true;
	case VDHS_READ_LINK_START:
		m_currLink = nullptr;
		return true;
	case VDHS_START_OF_INTEREST:
	case VDHS_START_FIXED:
	case VDHS_START_HIDE:
		return true;
	default:
		return false;
	}
}


float VEEDispHandler::PseudofetchAttribShiftScaleClamp(float _inVal, float _delta, float _factor, float _max, float _min)
{
	float outVal = _factor * (_inVal + _delta);
	if (outVal < _min)
	{
		outVal = _min;
	}
	if (outVal > _max)
	{
		outVal = _max;
	}
	return outVal;
}

bool VEEDispHandler::SetParticle(Particle* _part, Node* _node, int _place, DefaultRenderer* _renderer, int _grid)
{
	string type = _node->GetType();
	map<string, vector<NodeDispParam*>* >::iterator it;
	it = m_nodeParams.find(type);
	if (it != m_nodeParams.end())
	{
		vector<NodeDispParam*>* params = it->second;
		SetParticleBody(_part, _node, _place, _renderer, _grid, params);

		float makeTrue = 1.0f;
		if (find(m_nodesOfInterest.begin(), m_nodesOfInterest.end(), _node->GetName()) != m_nodesOfInterest.end()
			||
			find(m_typesOfInterest.begin(), m_typesOfInterest.end(), _node->GetType()) != m_typesOfInterest.end())
		{
			_part->Set(PDN_OF_INTEREST, &makeTrue);
		}

		if (find(m_nodesFixed.begin(), m_nodesFixed.end(), _node->GetName()) != m_nodesFixed.end()
			||
			find(m_typesFixed.begin(), m_typesFixed.end(), _node->GetType()) != m_typesFixed.end())
		{
			_part->Set(PDN_FIXED, &makeTrue);
		}

		if (find(m_nodesHide.begin(), m_nodesHide.end(), _node->GetName()) != m_nodesHide.end()
			||
			find(m_typesHide.begin(), m_typesHide.end(), _node->GetType()) != m_typesHide.end())
		{
			_part->Set(PDN_HIDE, &makeTrue);
		}

		string name = _node->GetName();
		it = m_nodeNameParams.find(name);
		if (it != m_nodeNameParams.end())
		{
			vector<NodeDispParam*>* params = it->second;
			SetParticleBody(_part, _node, _place, _renderer, _grid, params, true);
		}

		return true;
	}
	else
	{
		return false;
	}
}

void VEEDispHandler::SetParticleBody(Particle* _part, Node* _node, int _place, DefaultRenderer* _renderer, int _grid, vector<NodeDispParam*>* _params, bool byName)
{
	if (!byName)
	{
		float setFalse = 0.0f;
		//let's assume if we got here we want to draw this particle (unless told otherwise)
		_part->Set(PDN_HIDE, &setFalse);
	}
	_node->SetHasParticle(_part);
	for (int i = 0; i < _params->size(); i++)
	{
		NodeDispParam* loop = (*_params)[i];
		PDN data_type = GetPDN(loop->GetType());
		if (data_type == PDN_TEXTURE)
		{
			if (!byName || (byName && loop->GetBase() != ""))
			{
				_part->Set(PDN_TEXTURE, _renderer->GetTexture(loop->GetBase()/*+".dds"*/)); // no need to specify dds now
			}
			continue;
		}

		if (data_type == PDN_CONTAINS_LINK)
		{
			_part->Set(PDN_CONTAINS_LINK, (void*)loop->GetBase().c_str());
			continue;
		}

		float value = 0.0f;
		//special case of converting the string into one of the particle data values
		if ((data_type >= PDN_OSC_VAL_1 && data_type <= PDN_OSC_VAL_3)
			||
			(data_type >= PDN_JIG_VAL_1 && data_type <= PDN_JIG_VAL_3))
		{
			value = (float)GetPDN(loop->GetBase());
		}
		else if (loop->GetBase() == "i")
		{
			value = PseudofetchAttribShiftScaleClamp(
				(float)_place,
				loop->GetValue(NDPC_DELTA),
				loop->GetValue(NDPC_SCALE),
				loop->GetValue(NDPC_MAX),
				loop->GetValue(NDPC_MIN));

		}
		else if (loop->GetBase() == "grid_x")
		{
			value = PseudofetchAttribShiftScaleClamp(
				(float)(_place % _grid),
				loop->GetValue(NDPC_DELTA),
				loop->GetValue(NDPC_SCALE),
				loop->GetValue(NDPC_MAX),
				loop->GetValue(NDPC_MIN));
		}
		else if (loop->GetBase() == "grid_y")
		{
			value = PseudofetchAttribShiftScaleClamp(
				(float)(_place / _grid),
				loop->GetValue(NDPC_DELTA),
				loop->GetValue(NDPC_SCALE),
				loop->GetValue(NDPC_MAX),
				loop->GetValue(NDPC_MIN));
		}
		else if (loop->GetBase() == "")
		{
			value = PseudofetchAttribShiftScaleClamp(
				loop->GetValue(NDPC_DEFAULT),
				loop->GetValue(NDPC_DELTA),
				loop->GetValue(NDPC_SCALE),
				loop->GetValue(NDPC_MAX),
				loop->GetValue(NDPC_MIN));
		}
		else
		{
			value = _node->FetchAttribShiftScaleClamp(
				loop->GetBase().c_str(),
				loop->GetValue(NDPC_DEFAULT),
				loop->GetValue(NDPC_DELTA),
				loop->GetValue(NDPC_SCALE),
				loop->GetValue(NDPC_MAX),
				loop->GetValue(NDPC_MIN));
		}

		_part->Set(data_type, &value);
	}
}

bool VEEDispHandler::SetLine(Line* _line, Link* _link)
{
	string type = _link->GetType();
	_line->SetType(type);
	map<string, vector<NodeDispParam*>* >::iterator it;
	it = m_linkParams.find(type);
	if (it != m_linkParams.end())
	{
		vector<NodeDispParam*>* params = it->second;
		for (int i = 0; i < params->size(); i++)
		{
			NodeDispParam* loop = (*params)[i];
			LDN data_type = GetLDN(loop->GetType());
			float _val = loop->GetValue(NDPC_DEFAULT);
			_line->Set(data_type, &_val);
		}

		return true;
	}
	else
	{
		return false;
	}
}

void VEEDispHandler::AddGeneralLinks(Graph* _graph)
{

	map<string, vector<NodeDispParam*>* >::iterator it;
	for (it = m_linkParams.begin(); it != m_linkParams.end(); it++)
	{
		string lineName = it->first;
		vector<NodeDispParam*>* params = it->second;
		string from = "";
		string to = "";
		for (int i = 0; i < params->size(); i++)
		{
			NodeDispParam* loop = (*params)[i];
			if (loop->GetType() == "FROM")
			{
				from = loop->GetBase();
			}
			if (loop->GetType() == "TO")
			{
				to = loop->GetBase();
			}
		}
		if (from != "" && to != "")//this line type has a from and to so is a universal one lets add it to the system
		{
			for (int i = 0; i < _graph->NumNodes(); i++)
			{
				if (_graph->GetNode(i)->GetName() == from || _graph->GetNode(i)->GetType() == from)
				{
					for (int j = i + 1; j < _graph->NumNodes(); j++)
					{
						if (_graph->GetNode(j)->GetType() == to || _graph->GetNode(j)->GetName() == to)
						{
							Link* newLink = new Link();
							newLink->SetType(terminate(lineName.c_str(), (UINT)lineName.size()));
							newLink->SetFrom(_graph->GetNode(i));
							newLink->SetTo(_graph->GetNode(j));
							newLink->SetRecast(true);
							_graph->AddLink(newLink);
						}
					}
				}
				if (to != from && (_graph->GetNode(i)->GetType() == to || _graph->GetNode(i)->GetName() == to))
				{
					for (int j = i + 1; j < _graph->NumNodes(); j++)
					{
						if (_graph->GetNode(j)->GetName() == from || _graph->GetNode(j)->GetType() == from)
						{
							Link* newLink = new Link();
							newLink->SetType(terminate(lineName.c_str(), (UINT)lineName.size()));
							newLink->SetFrom(_graph->GetNode(j));
							newLink->SetTo(_graph->GetNode(i));
							newLink->SetRecast(true);
							_graph->AddLink(newLink);
						}
					}
				}
			}
		}
	}
}
