#ifndef _PARTICLEDATA_H_
#define _PARTICLEDATA_H_
#include "PDN.h"

struct ParticleData
{
	float m_pos[3];
	float m_vel[3];
	float m_acc[3];
	float m_grav[3];
	float m_rot;
	float m_rot_v;
	float m_rot_a;
	float m_colour[4];
	float m_size[3];
	float m_mass;
	float m_drag;
	char* m_textureName;
	void* m_pTexture;
	bool m_ofInterest;
	bool m_fixed;
	bool m_hide;
	int m_subscreen;

	float m_oscAmp[3];
	float m_oscW[3];
	float m_oscPhase[3];
	PDN   m_oscVal[3];

	float m_jigMag[3];
	PDN   m_jigVal[3];

	bool m_copy; //if this data is a copy I don't need to delete the texture name or contains links as that holds it

	// External ownership - prevent deletion of this when unloading a scene
	bool m_externalOwnership = false;

	float m_displayType; //0 = billboard 1 = Box
	char* m_containsLink; //for zones this are the things that are held inside via links of this sort.
	int m_containDirection;//0 = from locked inside to 1 = to locked inside from

	ParticleData();
	~ParticleData();
	void Set(PDN _type, void* _val);
	void* Get(PDN _type);
};

#endif