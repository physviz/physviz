#ifndef _VEEDISPHANDLER_H_
#define _VEEDISPHANDLER_H_
#include "attrib.h"
#include <stdint.h>
#include <iostream>
#include <map>

//#define BUILDING

//VEE Handler State
enum VDHS;

using namespace std;

class NodeDispParam;
class Particle;
class Node;
class DefaultRenderer;
class Line;
class Link;
class Graph;

//This class is used by the rapid json library to load the display mapping 
class VEEDispHandler {

public:

	VEEDispHandler();
	~VEEDispHandler();

	//functions required by rapidJSON
#ifdef BUILDING
	bool Null() { cout << "Null()" << endl; return ReadValue(nullptr, ATTRIB_NULL); }
	bool Bool(bool _b) { cout << "Bool(" << boolalpha << _b << ")" << endl; return ReadValue(&_b, ATTRIB_BOOL); }
	bool Int(int _i) { cout << "Int(" << _i << ")" << endl; return ReadValue(&_i, ATTRIB_INT); }
	bool Int64(int64_t _i) { cout << "Int64(" << _i << ")" << endl; return ReadValue(&_i, ATTRIB_INT64); }
#else
	bool Null() { return ReadValue(nullptr, ATTRIB_NULL); }
	bool Bool(bool _b) { return ReadValue(&_b, ATTRIB_BOOL); }
	bool Int(int _i) { return ReadValue(&_i, ATTRIB_INT); }
	bool Int64(int64_t _i) { return ReadValue(&_i, ATTRIB_INT64); }
#endif
	bool Double(double _d);
	bool Uint(unsigned _u) { return false; };
	bool Uint64(uint64_t _u) { return false; };
	bool RawNumber(const char* _str, size_t _len, bool _copy) { return String(_str, _len, _copy); }
	bool String(const char* _str, unsigned _length, bool _copy);
	bool StartObject();
	bool Key(const char* _str, size_t _len, bool _copy) { return String(_str, _len, _copy); }
	bool EndObject(unsigned _memberCount);
	bool StartArray();
	bool EndArray(unsigned _elementCount);

	//Set a Particle to appropriate parameters based on the data read in from the .disp.json file
	bool SetParticle(Particle* _part, Node* _node, int _place, DefaultRenderer* _renderer, int _grid);
	//Main work function for the above function to avoid repeated code
	void SetParticleBody(Particle* _part, Node* _node, int _place, DefaultRenderer* _renderer, int _grid, vector<NodeDispParam*>* _params, bool byName = false);
	//ditto but for the lines
	bool SetLine(Line* _line, Link* _link);

	//add general links between named or types of nodes based trailing sections of the .disp.json file
	void AddGeneralLinks(Graph* _graph);

protected:

	bool ReadValue(void* _val, AttribType _type) { cout << "ReadValue" << endl; return false; }
	map<string, vector<NodeDispParam*> * > m_nodeParams;
	map<string, vector<NodeDispParam*> * > m_nodeNameParams;
	map<string, vector<NodeDispParam*> * > m_linkParams;

	//a rather nasty and over complex state machine is used to step through the .disp.json file
	//this is the enum for that
	VDHS m_state;

	//current attributes needed to build up a display parameter
	vector<NodeDispParam*>* m_currNode;
	vector<NodeDispParam*>* m_currLink;
	NodeDispParam* m_currParam;
	string m_attribName;
	string m_attribValS;
	float m_attribValF;

	//node names and types for the "of interest" system
	vector<string> m_nodesOfInterest;
	vector<string> m_typesOfInterest;

	//node names and types to be fixed in place (overriding base parameters)
	vector<string> m_nodesFixed;
	vector<string> m_typesFixed;

	//node names and types to be hidden (overriding base parameters)
	vector<string> m_nodesHide;
	vector<string> m_typesHide;

	//a dummy fetch of data for a parameter
	float PseudofetchAttribShiftScaleClamp(float _inVal, float _delta, float _factor, float _max, float _min);
};

#endif