#ifndef _LDN_H_
#define _LDN_H_


//any additions / changes to this list must also be made to the list in the editor in Document_Data.cs
enum LDN //Line Data Names
{
	LDN_NULL = 0,
	LDN_COLOUR,
	LDN_R,
	LDN_G,
	LDN_B,
	LDN_A,
	LDN_DRAW,
	LDN_FORCE,
	LDN_STRENGTH,
	LDN_POWER,
	LDN_BASE_LENGTH,
	LDN_CUT_OFF,
	LDN_NEAR_CUT_OFF,
	LDN_FORCE_TYPE,
	LDN_TBF_ANGLE,
	LDN_TBF_STRENGTH,
	LDN_TBF_SYMM,
	LDN_COUNT
};

#endif