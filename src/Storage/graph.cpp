#include "graph.h"
#include "node.h"
#include "link.h"
#include "HowLink.h"
#include <assert.h>
#include <map>
#include "AttribArray.h"
#include "attrib.h"
#include <limits>  
#include <fstream>
#include <iostream>
#include <string>

//this thing doesn't give nice null terminated arrays let's fix that
extern char* terminate(const char* _str, unsigned _length);

Graph::Graph()
{

}

Graph::~Graph()
{
	if (m_Nodes.size())
	{
		for (unsigned int i = 0; i < m_Nodes.size(); i++)
		{
			delete m_Nodes[i];
		}
		m_Nodes.clear();
	}

	if (m_Links.size())
	{
		for (unsigned int i = 0; i < m_Links.size(); i++)
		{
			delete m_Links[i];
		}
		m_Links.clear();
	}
#ifdef STRING_LINK
	if (m_NodeMap.size())
	{
		//this is just a list of strings and pointers elsewhere so can just be cleared
		m_NodeMap.clear();
	}
#endif
}

Node* Graph::GetNode(int _i)
{
	//TODO: error checks
	return m_Nodes[_i];
}

int Graph::NumNodes()
{
	return m_Nodes.size();
}

Link* Graph::GetLink(int _i)
{
	//TODO: error checks
	return m_Links[_i];
}

int Graph::numLinks()
{
	return m_Links.size();
}

void Graph::AddNode(Node* _node)
{
	_node->SetNum(m_Nodes.size());
	m_Nodes.push_back(_node);
}


//if the link is not valid it will not be added to the graph
bool Graph::AddLink(Link* _link, bool _recast)
{
	bool added = true;

	if (_recast)
	{
#ifdef STRING_LINK
		added = _link->Recast(&m_NodeMap);
#endif
#ifdef INT_LINK
		added = _link->recast(&m_Nodes);
#endif
	}

	if (added)
	{
		m_Links.push_back(_link);
	}

	return added;
}


void Graph::TidyLinks()
{
#ifdef STRING_LINK
	if (m_NodeMap.size())
	{
		//no longer needed, but this is just a list of pointers elsewhere so can just be cleared
		m_NodeMap.clear();
	}
#endif
	m_Links.shrink_to_fit();
}


#ifdef STRING_LINK
void Graph::AddNodeToMap(Node* _node)
{
	m_NodeMap.insert(pair<char*, Node*>(_node->GetName(), _node));
}
#endif

struct range
{
	double min;
	double max;
	double total;
	double total2;
	int number;
};

void Graph::DisplayRanges(string _filename)
{
	ofstream rangeFile;
	_filename = _filename + ".ranges";
	cout << "Ranges filename" << _filename << endl;
	rangeFile.open(_filename);
	typedef map<string, range*> AttribDescrips;
	map<string, AttribDescrips*> ranges;
	//collect possible ranges for those types
	for (unsigned int i = 0; i < m_Nodes.size(); i++)
	{
		//if type doesn't exist add it to the map
		string type = m_Nodes[i]->GetType();
		if (ranges.find(type) == ranges.end())
		{
			ranges.insert(pair<string, AttribDescrips*>(type, new AttribDescrips()));
		}

		AttribDescrips* AD = ranges.find(type)->second;
		//collecting all names of attributes for this type of node
		AttribArray* AA = m_Nodes[i]->GetAttribs();
		if (AA)
		{
			for (unsigned int j = 0; j < AA->GetLength(); j++)
			{
				bool isNew = false;
				string AType = (*AA)[j]->GetName();
				if (AD->find(AType) == AD->end())
				{
					AD->insert(pair<string, range*>(AType, new range()));
					isNew = true;
				}
				range* R = AD->find(AType)->second;

				char* atSTRING;
				float atFLOAT;
				AttribArray* atARRAY;

				double value;
				switch ((*AA)[j]->MyType())
				{
				case ATTRIB_BOOL:
					value = *(bool*)(*AA)[j]->GetValue();
					break;
				case ATTRIB_INT:
					value = *(int*)(*AA)[j]->GetValue();
					break;
				case ATTRIB_UINT:
					value = *(unsigned*)(*AA)[j]->GetValue();
					break;
				case ATTRIB_INT64:
					value = *(int64_t*)(*AA)[j]->GetValue();
					break;
				case ATTRIB_UINT64:
					value = *(uint64_t*)(*AA)[j]->GetValue();
					break;
				case ATTRIB_DOUBLE:
					value = *(double*)(*AA)[j]->GetValue();
					break;
				case ATTRIB_FLOAT:
					value = *(float*)(*AA)[j]->GetValue();
					break;
				case ATTRIB_STRING:
				case ATTRIB_ARRAY:
					//these two don't realy make sense but let's count them any how
					value = 0;
					break;
				}

				if (isNew)
				{
					R->max = value;
					R->min = value;
					R->total = value;
					R->total2 = value * value;
					R->number = 1;
				}
				else
				{
					R->total += value;
					R->total2 += value * value;
					R->number++;
					if (value < R->min)
					{
						R->min = value;
					}
					if (value > R->max)
					{
						R->max = value;
					}
				}
			}
		}
	}

	//repeat for the Links
	map<string, AttribDescrips*> rangesL;
	//collect possible ranges for those types
	for (unsigned int i = 0; i < m_Links.size(); i++)
	{
		//if type doesn't exist add it to the map
		string type = m_Links[i]->GetType();
		if (rangesL.find(type) == rangesL.end())
		{
			rangesL.insert(pair<string, AttribDescrips*>(type, new AttribDescrips()));
		}

		AttribDescrips* AD = rangesL.find(type)->second;
		//collecting all names of attributes for this type of Link
		AttribArray* AA = m_Links[i]->GetAttribs();
		if (AA)
		{
			for (unsigned int j = 0; j < AA->GetLength(); j++)
			{
				bool isNew = false;
				string AType = (*AA)[j]->GetName();
				if (AD->find(AType) == AD->end())
				{
					AD->insert(pair<string, range*>(AType, new range()));
					isNew = true;
				}
				range* R = AD->find(AType)->second;
				double* value = (double*)(*AA)[j]->GetValue();//assuming these are all doubles
				if (isNew)
				{
					R->max = *value;
					R->min = *value;
				}
				else
				{
					if (*value < R->min)
					{
						R->min = *value;
					}
					if (*value > R->max)
					{
						R->max = *value;
					}
				}
			}
		}
	}

	//display all nodes stuff collected
	cout << "Nodes: \n";
	rangeFile << "Nodes: \n";
	cout << "Types: " << ranges.size() << "\n \n";
	rangeFile << "Types: " << ranges.size() << "\n \n";
	for (map<string, AttribDescrips*>::iterator it = ranges.begin(); it != ranges.end(); it++)
	{
		cout << it->first << "\n";
		rangeFile << it->first << "\n";
		AttribDescrips* AD = it->second;
		if (AD->size() > 0)
		{
			cout << "\n";
			rangeFile << "\n";
			for (AttribDescrips::iterator it2 = AD->begin(); it2 != AD->end(); it2++)
			{
				double mean = it2->second->total / (double)it2->second->number;
				double std = sqrt((it2->second->total2 / (double)it2->second->number) - mean * mean);
				cout << it2->first << " " << it2->second->min << " " << it2->second->max << " " << mean << " " << std << "\n";
				rangeFile << it2->first << " " << it2->second->min << " " << it2->second->max << " " << mean << " " << std << "\n";
			}
			cout << "\n";
			rangeFile << "\n";
		}
	}

	//repeat for links
	cout << "\n\nLinks: \n";
	rangeFile << "\n\nLinks: \n";
	cout << "Types: " << rangesL.size() << "\n \n";
	rangeFile << "Types: " << rangesL.size() << "\n \n";
	for (map<string, AttribDescrips*>::iterator it = rangesL.begin(); it != rangesL.end(); it++)
	{
		cout << it->first << "\n";
		rangeFile << it->first << "\n";
		AttribDescrips* AD = it->second;
		if (AD->size() > 0)
		{
			cout << "\n";
			rangeFile << "\n";
			for (AttribDescrips::iterator it2 = AD->begin(); it2 != AD->end(); it2++)
			{
				cout << it2->first << " " << it2->second->min << " " << it2->second->max << "\n";
				rangeFile << it2->first << " " << it2->second->min << " " << it2->second->max << "\n";

			}
			cout << "\n";
			rangeFile << "\n";
		}
	}

	cout.flush();
	rangeFile.flush();
	rangeFile.close();

	return;
}

void Graph::WriteNMap(string _filename)
{
	//THIS PURELY WRITES OUT CONNECTIONS
	//NO OTHER INFORMATION
	fstream nmap;
	nmap.open(_filename, fstream::out);
	nmap << m_Links.size() << endl;
	for (int i = 0; i < m_Links.size(); i++)
	{
		nmap << m_Links[i]->GetFrom()->GetNum() << " " << m_Links[i]->GetTo()->GetNum() << endl;
	}
	nmap.close();
}

void Graph::ReadNMap(fstream& _nmap, string _type)
{
	int num;
	_nmap >> num;
	for (int i = 0; i < num; i++)
	{
		int from, to;
		_nmap >> from >> to;
		Link* link = new Link();
		int test = _type.length();
		link->SetType(terminate(_type.c_str(), _type.size()));
		link->SetFrom(GetNode(from));
		link->SetTo(GetNode(to));
		link->SetRecast(true);

		AddLink(link);
	}
}
