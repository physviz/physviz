#ifndef _ATTRIB_H_
#define _ATTRIB_H_

#include <stdint.h>
#include "AttribArray.h"

enum AttribType
{
	ATTRIB_NULL,
	ATTRIB_BOOL,
	ATTRIB_INT,
	ATTRIB_UINT,
	ATTRIB_INT64,
	ATTRIB_UINT64,
	ATTRIB_DOUBLE,
	ATTRIB_STRING,
	ATTRIB_ARRAY,
	ATTRIB_FLOAT,
	ATTRIB_COUNT
};

class Attrib
{
public:
	Attrib();
	~Attrib();

	AttribType MyType() { return m_type; }

	bool IsType(AttribType _type) { return (_type == m_type); }


	void SetName(char* _name);
	char* GetName() { return m_name; };

	void SetValue(void* _val, AttribType _type);
	void* GetValue() { return &m_value; }; //assume you know what your doing with as you've asked me what it is

protected:

	char* m_name;
	AttribType m_type;
	union
	{
		int atINT;
		unsigned atUINT;
		double atDOUBLE;
		int64_t atINT64;
		uint64_t atUINT64;
		char* atSTRING;
		float atFLOAT;
		AttribArray* atARRAY;
	} m_value;

};

#endif