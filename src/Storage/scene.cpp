#include "scene.h"
#include "graph.h"
#include "node.h"
#include "particleh.h"
#include "link.h"
#include "line.h"
#include <DirectXMath.h>
#include "..\DirectXTK\Inc\SimpleMath.h"
#include <windows.h> 
#include <stdio.h>
#include "../DefaultControl/controllers.h"
#include "../DefaultRenderer/DefaultRenderer.h"
#include "VEEDispHandler.h"
#include "../JSON/reader.h"
#include "../JSON/filereadstream.h"

#include "../Public/physfs-3.0.1/src/physfs.h"
#include "../Public/physfs-3.0.1/src/streams/physfs_file_system.hpp"
#include "../Public/physfs-3.0.1/src/streams/ifile_stream.hpp"
#include "../JSON/istreamwrapper.h"

using namespace DirectX;
using namespace SimpleMath;
using namespace rapidjson;

Scene::Scene(Graph* _myGraph, DefaultRenderer* _renderer, string _filename) :m_focus(-1), m_PhysicsOn(true)
{
	VEEDispHandler VDHandler;
	Reader reader;
	char* readBuffer = new char[65536];
	FILE* fp = nullptr;
	fopen_s(&fp, _filename.c_str(), "rb");// non-Windows use "r"
	FileReadStream is(fp, readBuffer, sizeof(readBuffer));
	reader.Parse(is, VDHandler);
	fclose(fp);

	//add particles based on Node translation system
	int numNodes = _myGraph->NumNodes();
	int rNN = (int)sqrt(numNodes);
	for (int i = 0; i < numNodes; i++)
	{
		Node* node = _myGraph->GetNode(i);
		Particle* newPart = new Particle(nullptr, node->GetName());
		if (VDHandler.SetParticle(newPart, node, i, _renderer, rNN))
		{
			newPart->SetInitData();
			m_Particles.push_back(newPart);
		}
		else
		{
			delete newPart;
		}
	}

	//add particles from the controllers
	int numControlParts;
	vector<string> names;
	ParticleData** controllerParts = Controllers::Get()->GetParticles(numControlParts, names);
	for (int i = 0; i < numControlParts; i++)
	{
		if (controllerParts[i])
		{
			Node* dummyNode = new Node;
			char* dummyType = new char[11];
			strcpy(dummyType, &"controller"[0]);//yuch! but bizarely this works!
			dummyNode->SetType(dummyType);
			char* dummyName = new char[names[i].length() + 1];
			strcpy(dummyName, names[i].c_str());
			dummyNode->SetName(dummyName);
			_myGraph->AddNode(dummyNode);
			Particle* newPart = new Particle(controllerParts[i], names[i]);
			VDHandler.SetParticle(newPart, dummyNode, numNodes + i, _renderer, rNN);
			m_Particles.push_back(newPart);
		}
	}

	//andd general links before we start to add the appropriate lines
	VDHandler.AddGeneralLinks(_myGraph);

	//add lines based on Link translation system, but only on ones conecting visible nodes
	//i.e. ones with particles
	for (int i = 0; i < _myGraph->numLinks(); i++)
	{
		Link* link = _myGraph->GetLink(i);
		if (link->IsRenderable())
		{
			Line* newLine = new Line(link);
			m_Lines.push_back(newLine);
			VDHandler.SetLine(newLine, link);
			//some set up for the Three Body Forces
			if (*(float*)newLine->Get(LDN_TBF_STRENGTH))
			{
				newLine->GetFrom()->AddTBFLine(newLine);
				if (*(bool*)newLine->Get(LDN_TBF_SYMM))
				{
					//if this is symmetric add another bending only force
					Line* newLine2 = new Line(link);
					VDHandler.SetLine(newLine2, link);
					bool setFalse = false;
					newLine2->Set(LDN_FORCE, &setFalse);
					newLine2->Set(LDN_DRAW, &setFalse);
					newLine2->Invert();
					m_Lines.push_back(newLine2);
					newLine2->GetFrom()->AddTBFLine(newLine2);
				}
			}
		}

	}

	delete[] readBuffer;

	SetFocus(POLL_FOCUS_UP);
}

Scene::Scene(Graph* _myGraph, DefaultRenderer* _renderer, const char* _szFilename, const char* _szPackageName) :m_focus(-1), m_PhysicsOn(true) {

	VEEDispHandler VDHandler;
	Reader reader;
	/*
		char* readBuffer = new char[65536];
		FILE* fp = nullptr;
		fopen_s(&fp, fetch.c_str(), "rb");// non-Windows use "r"
		FileReadStream is(fp, readBuffer, sizeof(readBuffer));*/
		//StringStream ss(szData);
	if (!PHYSFS_isInit()) {
		PHYSFS_init(0);
	}
	PHYSFS_mount(_szPackageName, "", 0);

	// doing this because physfs is case sensitive, but our other code isn't!
	string ext = "";
	string filename = "";
	char** szFiles = PHYSFS_enumerateFiles("/");
	char** i;
	for (i = szFiles; *i != NULL; i++) {
		string str = *i;
		int nLastDot = str.find_last_of('.');
		string strNoExt = str.substr(0, nLastDot);
		for (int i = 0; i < strNoExt.length(); i++) {
			strNoExt[i] = tolower(strNoExt[i]);
		}

		if (strNoExt == "d.disp") {
			filename = *i;
			ext = str.substr(nLastDot + 1);
			for (int i = 0; i < ext.length(); i++) {
				ext[i] = tolower(ext[i]);
			}
			continue;
		}
	}

	PHYSFS_freeList(szFiles);

	auto_ptr<std::istream> stream = PhysFSFileSystem::open_file(filename);
	IStreamWrapper wrappedStream(*stream.get()); // TWO MORE EGGS
	reader.Parse(wrappedStream, VDHandler);
	PHYSFS_unmount(_szPackageName);

	//fclose(fp);

	//add particles based on Node translation system
	int numNodes = _myGraph->NumNodes();
	int rNN = (int)sqrt(numNodes);
	for (int i = 0; i < numNodes; i++)
	{
		Node* node = _myGraph->GetNode(i);
		Particle* newPart = new Particle(nullptr, node->GetName());
		if (VDHandler.SetParticle(newPart, node, i, _renderer, rNN))
		{
			m_Particles.push_back(newPart);
		}
		else
		{
			delete newPart;
		}
	}

	//add particles from the controllers
	int numControlParts = 0;
	vector<string> names;
	ParticleData** controllerParts = Controllers::Get()->GetParticles(numControlParts, names);
	Node dummyNode;
	char* dummyType = new char[11];
	strcpy(dummyType, &"controller"[0]);//yuch! but bizarely this works!
	dummyNode.SetType(dummyType);
	for (int i = 0; i < numControlParts; i++)
	{
		if (controllerParts[i])
		{
			char* dummyName = new char[names[i].length() + 1];
			strcpy(dummyName, names[i].c_str());
			dummyNode.SetName(dummyName);
			Particle* newPart = new Particle(controllerParts[i], names[i]);
			VDHandler.SetParticle(newPart, &dummyNode, numNodes + i, _renderer, rNN);
			m_Particles.push_back(newPart);
		}
	}

	//add lines based on Link translation system, but only on ones conecting visible nodes
	//i.e. ones with particles
	for (int i = 0; i < _myGraph->numLinks(); i++)
	{
		Link* link = _myGraph->GetLink(i);
		if (link->IsRenderable())
		{
			Line* newLine = new Line(link);
			m_Lines.push_back(newLine);
			VDHandler.SetLine(newLine, link);
		}

	}

	//delete[] readBuffer;

	SetFocus(POLL_FOCUS_UP);

	PHYSFS_unmount(_szFilename);
}

Scene::~Scene()
{
	//delete Particles

	if (m_Particles.size())
	{
		for (unsigned int i = 0; i < m_Particles.size(); i++)
		{
			delete m_Particles[i];
		}
		m_Particles.clear();
	}

	//delete Lines
	if (m_Lines.size())
	{
		for (unsigned int i = 0; i < m_Lines.size(); i++)
		{
			delete m_Lines[i];
		}
		m_Lines.clear();
	}

}

void Scene::SetFocus(unsigned int _poll)
{

	if (m_focus != -2 && (_poll & (POLL_FOCUS_UP | POLL_FOCUS_DOWN)))
	{
		int step = 0;
		if (_poll & POLL_FOCUS_UP)
		{
			step = 1;
		}

		if (_poll & POLL_FOCUS_DOWN)
		{
			step = -1;
		}

		int newFocus = -2;

		for (int i = m_focus + step; i != (m_focus + step * (m_Particles.size() + 1)); i += step)
		{
			int useI = i;
			if (useI < 0)
			{
				useI += m_Particles.size();
			}
			if (useI >= m_Particles.size())
			{
				useI -= m_Particles.size();
			}
			if (*(bool*)(m_Particles[useI]->Get(PDN_OF_INTEREST)))
			{
				newFocus = useI;
				break;
			}
		}
		//if got here I've spun through all of them so don't bother again
		m_focus = newFocus;

		if (m_focus > 0 && m_focus < m_Particles.size())
		{
			Controllers::SetTarget((Vector3*)m_Particles[m_focus]->Get(PDN_POSITION));
		}
	}

}

void Scene::Update(float _dt, unsigned int _poll)
{
	if (_poll & POLL_TOGGLE_PHYSICS)
	{
		m_PhysicsOn = !m_PhysicsOn;
	}

	if (m_PhysicsOn)
	{
		//pre-tick apply forces from lines
		for (unsigned int i = 0; i < m_Lines.size(); i++)
		{
			m_Lines[i]->Tick(_dt);
		}
		//apply TBF
		for (unsigned int i = 0; i < m_Particles.size(); i++)
		{
			m_Particles[i]->PreTick(_dt);
		}

		//tick particles
		for (unsigned int i = 0; i < m_Particles.size(); i++)
		{
			m_Particles[i]->Tick(_dt);
		}

		//post tick (set nodes inside zones they "belong" to) and fixed line length stuff
		for (unsigned int i = 0; i < m_Lines.size(); i++)
		{
			m_Lines[i]->PostTick(_dt);
		}

		for (unsigned int i = 0; i < m_Particles.size(); i++)
		{
			m_Particles[i]->PostTick(_dt);
		}
	}

	SetFocus(_poll);
}

void Scene::Reset()
{
	for (unsigned int i = 0; i < m_Particles.size(); i++)
	{
		m_Particles[i]->Reset();
	}
}


string Scene::GetFocusLabel()
{
	if (m_focus >= 0)
	{
		return m_Particles[m_focus]->GetName();
	}
	else
	{
		return "";
	}
}