#ifndef _LINE_H_
#define _LINE_H_
#include <d3d11_1.h>
#include <DirectXColors.h>
#include "..\DirectXTK\Inc\SimpleMath.h"
#include "LDN.h"
#include <string>

class Particle;
class Link;
struct SimpleVertex;
using namespace DirectX;
using namespace SimpleMath;

class Line
{
public:
	Line(Link* _link = nullptr);
	~Line();

	void Render(SimpleVertex* _VBLoc);
	void Tick(float _dt);

	void PostTick(float _dt);

	void Set(LDN _type, void* _val);
	void* Get(LDN _type);

	void SetType(std::string _type) { m_type = _type; }
	std::string GetType() { return m_type; }

	bool CanDraw();

	Particle* GetFrom() { return m_from; }
	Particle* GetTo() { return m_to; }

	void Invert();

protected:
	Particle* m_from;
	Particle* m_to;

	std::string m_type;

	bool m_draw;
	bool m_force;

	float m_strength;
	float m_power;
	float m_baseLength;
	float m_cutOff = FLT_MAX;
	float m_nearCutOff = 0.0f;
	float m_forceType = 0.0;
	Color m_colour;

	float m_postTickPush[3];

	float m_TBF_angle;
	float m_TBF_strength = 0.0f;
	bool m_TBF_symm = false;
};

#endif