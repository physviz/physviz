#ifndef _GRAPH_H_
#define _GRAPH_H_
#include <vector>
#include "HowLink.h"

using namespace std;

class Node;
class Link;

class Graph
{
public:
	Graph();
	~Graph();

	void AddNode(Node* _node);
	bool AddLink(Link* _link, bool _recast = true);
#ifdef STRING_LINK
	void AddNodeToMap(Node* _node);
#endif

	void TidyNodes() { m_Nodes.shrink_to_fit(); }
	void TidyLinks();

	Node* GetNode(int _i);
	int NumNodes();

	Link* GetLink(int i);
	int numLinks();

	void DisplayRanges(string _filename);

	void WriteNMap(string _filename);
	void ReadNMap(fstream& _nmap, string _type = "neigh");

protected:
	vector<Node*> m_Nodes;
	vector<Link*> m_Links;
#ifdef STRING_LINK
	NodeMap m_NodeMap;
#endif
};

#endif