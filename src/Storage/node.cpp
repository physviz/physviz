#include "node.h"
#include "AttribArray.h"
#include "attrib.h"
#include <string>

Node::Node() :m_name(nullptr), m_type(nullptr), m_attribs(nullptr), m_hasParticle(nullptr)
{
}

Node::~Node()
{
	if (m_name)
	{
		delete[] m_name;
		m_name = nullptr;
	}

	if (m_type)
	{
		delete[] m_type;
		m_type = nullptr;
	}

	if (m_attribs)
	{
		delete m_attribs;
		m_attribs = nullptr;
	}
}


void Node::SetName(char* _name)
{
	if (m_name)
	{
		delete[] m_name;
		m_name = nullptr;
	}

	m_name = _name;
}


void Node::SetType(char* _type)
{
	if (m_type)
	{
		delete[] m_type;
		m_type = nullptr;
	}

	m_type = _type;
}

void Node::SetAttribs(AttribArray* _attribs)
{
	if (m_attribs)
	{
		delete m_attribs;
		m_attribs = nullptr;
	}

	m_attribs = _attribs;
}


float Node::FetchAttrib(const char* _attrib, float _def)
{
	return FetchAttribShiftScaleClamp(_attrib, _def, 0.0f, 1.0f, CLAMP_MAX, CLAMP_MIN);
}


float Node::FetchAttribClamp(const char* _attrib, float _def, float _max, float _min)
{
	return FetchAttribShiftScaleClamp(_attrib, _def, 0.0f, 1.0f, _max, _min);
}

float Node::FetchAttribScale(const char* _attrib, float _def, float _factor)
{
	return FetchAttribShiftScaleClamp(_attrib, _def, 0.0f, _factor, CLAMP_MAX, CLAMP_MIN);
}

float Node::FetchAttribShift(const char* _attrib, float _def, float _delta)
{
	return FetchAttribShiftScaleClamp(_attrib, _def, _delta, 1.0f, CLAMP_MAX, CLAMP_MIN);
}

float Node::FetchAttribShiftScaleClamp(const char* _attrib, float _def, float _delta, float _factor, float _max, float _min)
{
	Attrib* nodeAttrib = nullptr;
	if (m_attribs && (nodeAttrib = m_attribs->FetchAttrib(_attrib)))
	{
		float value = _def;
		switch (nodeAttrib->MyType())
		{
		case ATTRIB_BOOL:
			value = *(bool*)nodeAttrib->GetValue();
			break;
		case ATTRIB_INT:
			value = *(int*)nodeAttrib->GetValue();
			break;
		case ATTRIB_UINT:
			value = *(unsigned*)nodeAttrib->GetValue();
			break;
		case ATTRIB_INT64:
			value = *(int64_t*)nodeAttrib->GetValue();
			break;
		case ATTRIB_UINT64:
			value = *(uint64_t*)nodeAttrib->GetValue();
			break;
		case ATTRIB_DOUBLE:
			value = *(double*)nodeAttrib->GetValue();
			break;
		case ATTRIB_FLOAT:
			value = *(float*)nodeAttrib->GetValue();
			break;
		case ATTRIB_STRING:
		case ATTRIB_ARRAY:
			//these two don't realy make sense but let's count them any how
			return _def;
		}

		float retValue = _factor * (value + _delta);
		if (retValue < _min)
		{
			retValue = _min;
		}
		if (retValue > _max)
		{
			retValue = _max;
		}
		return retValue;
	}
	else
	{
		return _def;
	}
};