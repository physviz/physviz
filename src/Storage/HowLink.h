#ifndef _HOW_LINK_
#define _HOW_LINK_

//How are we giving the links information
//name of the node
#define STRING_LINK
//integer to place in graph
//#define INT_LINK

#ifdef STRING_LINK
#include <string>
#include <unordered_map>
class Node;
typedef std::unordered_map<std::string, Node*> NodeMap;
#ifdef INT_LINK
#error MUST ONLY DEFINE STRING OR INT LINKS
#endif
#endif

#ifdef INT_LINK
#ifdef STRING_LINK
#error MUST ONLY DEFINE STRING OR INT LINKS
#endif
#endif


#endif