#ifndef _NODE_H_
#define _NODE_H_

#define CLAMP_MAX 9.9e10
#define CLAMP_MIN -9.9e10

class AttribArray;

class Node
{
public:
	Node();
	~Node();

	void SetName(char* _name);
	void SetType(char* _type);
	void SetAttribs(AttribArray* _attribs);
	void SetNum(int _num) { m_num = _num; }

	char* GetName() { return m_name; }
	char* GetType() { return m_type; }
	AttribArray* GetAttribs() { return m_attribs; }
	int GetNum() { return m_num; }

	//need to do different versions of this
	float FetchAttrib(const char* _attrib, float _def);
	float FetchAttribClamp(const char* _attrib, float _def, float _max, float _min);
	float FetchAttribScale(const char* _attrib, float _def, float _factor);
	float FetchAttribShift(const char* _attrib, float _def, float _delta);
	float FetchAttribShiftScaleClamp(const char* _attrib, float _def, float _delta, float _factor, float _max, float _min);

	void SetHasParticle(void* _part) { m_hasParticle = _part; }
	void* HasParticle() { return m_hasParticle; }

protected:
	void* m_hasParticle;//has a particle been created for this node

	char* m_name;
	char* m_type;

	AttribArray* m_attribs;

	int m_num = -1;//This nodes position in the list of nodes
};

#endif