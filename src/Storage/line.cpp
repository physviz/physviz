#include "line.h"
#include "link.h"
//#include "ParticleCB.h"
#include "../DirectXRenderer/simpleVertexh.h"
#include "particleh.h"
#include <string>
#include <iostream>

//any additions / changes to this list must also be made to the list in the editor in Document_Data.cs
std::string LDN_Names[] =
{
	"LDN_NULL",
	"COLOUR",
	"R",
	"G",
	"B",
	"A",
	"DRAW",
	"FORCE",
	"STRENGTH",
	"POWER",
	"BASE_LENGTH",
	"CUT_OFF",
	"NEAR_CUT_OFF",
	"FORCE_TYPE",
	"TBF_ANGLE",
	"TBF_STRENGTH",
	"TBF_SYMM",
	"LDN_COUNT"
};

LDN GetLDN(std::string _name)
{

	for (int i = 0; i < LDN_COUNT; i++)
	{
		if (LDN_Names[i] == _name)
		{
			return (LDN)i;
		}
	}

	return LDN_NULL;
}

Line::Line(Link* _link) :m_from(nullptr), m_to(nullptr), m_draw(false), m_force(false), m_power(1.0f), m_baseLength(0.0f), m_strength(0.0f)
{
	if (_link && _link->IsRenderable())
	{
		_link->GetParticles(&m_from, &m_to);
		m_draw = true;
	}
	memset(m_postTickPush, 0, 3 * sizeof(float));
}

Line::~Line()
{
}

//need to move this out to DXRenderer
void Line::Render(SimpleVertex* _VBLoc)
{
	_VBLoc[0].m_Pos = XMFLOAT3((float*)m_from->Get(PDN_POSITION));
	_VBLoc[0].m_Tex = XMFLOAT4(m_colour);
	_VBLoc[0].m_Tex.w = 0.3f;
	_VBLoc[1].m_Pos = XMFLOAT3((float*)m_to->Get(PDN_POSITION));
	_VBLoc[1].m_Tex = XMFLOAT4(1.0f, 1.0f, 1.0f, 0.1f);
}

void Line::Tick(float _dt)
{
	memset(m_postTickPush, 0, sizeof(float) * 3);

	if (!m_force) return;

	float* fP = (float*)m_from->Get(PDN_POSITION);//from Pos
	float* tP = (float*)m_to->Get(PDN_POSITION);//to Pos

	float d[3] = { fP[0] - tP[0], fP[1] - tP[1], fP[2] - tP[2] };
	float length = sqrtf(d[0] * d[0] + d[1] * d[1] + d[2] * d[2]);

	if (length > m_cutOff || length < m_nearCutOff || (length < 0.000001f) || (m_power < 1.0f && abs(length - m_baseLength) < 0.000001f))
	{
		return;
	}

	float fac = 0.0f;
	float* fA = (float*)m_from->Get(PDN_ACC); //from Acc
	float* tA = (float*)m_to->Get(PDN_ACC); //to Acc

	switch ((int)m_forceType)
	{
	case 1://Morse Potential
	{
		float bit = exp(m_power * (m_baseLength - length));
		fac = 2.0f * m_strength * bit * (1 - bit);
		break;
	}
	case 2://Fixed Length (apply this on top of forces) https://www.gamasutra.com/blogs/ScottLembcke/20180404/316046/Improved_Lerp_Smoothing.php
	{
		float newLength = m_baseLength + exp2f(-m_strength * _dt) * (length - m_baseLength);
		fac = 0.5f * (length - newLength);
		break;
	}
	case 0://Hooke Spring / other base forces
	default:
		fac = m_strength * pow(length - m_baseLength, m_power);
		break;
	}
	// 
	//4.0f * m_strength *(12.0f * pow(m_baseLength, 12.0f) / pow(length, 13.0f) - 6.0f * pow(m_baseLength, 6.0f) / pow(length,7.0f));

	for (int i = 0; i < 3; i++)
	{
		float change = fac * d[i] / length;
		if (!isnan(change))//if anything screwy happens don't apply
		{
			if (m_forceType != 2)
			{
				fA[i] -= change / (*(float*)m_from->Get(PDN_MASS));
				tA[i] += change / (*(float*)m_to->Get(PDN_MASS));
			}
			else
			{
				m_postTickPush[i] = -change;//calculate now but apply in the post tick phase
			}
		}
	}
}

void Line::PostTick(float _dt)
{
	//"lerp" to fix length nodes
	if (m_force && m_forceType == 2)
	{
		float* fA = (float*)m_from->Get(PDN_ACC);//from Pos
		float* tA = (float*)m_to->Get(PDN_ACC);//to Pos

		for (int i = 0; i < 3; i++)
		{
			switch ((int)m_power)
			{
			case -1:
				if (!*(bool*)m_to->Get(PDN_FIXED)) tA[i] -= 2.0f * m_postTickPush[i];
				break;
			case 0:
				if (!*(bool*)m_from->Get(PDN_FIXED)) fA[i] += m_postTickPush[i];
				if (!*(bool*)m_to->Get(PDN_FIXED)) tA[i] -= m_postTickPush[i];
				break;
			case 1:
				if (!*(bool*)m_from->Get(PDN_FIXED)) fA[i] += 2.0f * m_postTickPush[i];
				break;
			}
		}
	}

	if (*(float*)m_from->Get(PDN_DISPLAY_TYPE) == 1)
	{
		char* linkInside = (char*)m_from->Get(PDN_CONTAINS_LINK);
		if (linkInside && (*(int*)m_from->Get(PDN_CONTAINS_DIRECTION)) == 0 && strcmp(linkInside, m_type.c_str()) == 0)
		{
			m_from->FixInside(m_to);
		}
	}
	if (*(float*)m_to->Get(PDN_DISPLAY_TYPE) == 1)
	{
		char* linkInside = (char*)m_to->Get(PDN_CONTAINS_LINK);
		if (linkInside && (*(int*)m_to->Get(PDN_CONTAINS_DIRECTION)) == 1 && strcmp(linkInside, m_type.c_str()) == 0)
		{
			m_to->FixInside(m_from);
		}
	}
}



void Line::Set(LDN _type, void* _val)
{
	switch (_type)
	{

	case LDN_COLOUR:
		memcpy(&this->m_colour, _val, sizeof(float) * 4);
		break;
	case LDN_R:
		m_colour.x = *(float*)_val;
		break;
	case LDN_G:
		m_colour.y = *(float*)_val;
		break;
	case LDN_B:
		m_colour.z = *(float*)_val;
		break;
	case LDN_A:
		m_colour.w = *(float*)_val;
		break;
	case LDN_DRAW:
		m_draw = (bool)*(float*)_val;
		break;
	case LDN_FORCE:
		m_force = (bool)*(float*)_val;
		break;
	case LDN_STRENGTH:
		m_strength = *(float*)_val;
		break;
	case LDN_POWER:
		m_power = *(float*)_val;
		break;
	case LDN_BASE_LENGTH:
		m_baseLength = *(float*)_val;
		break;
	case LDN_CUT_OFF:
		m_cutOff = *(float*)_val;
		break;
	case LDN_NEAR_CUT_OFF:
		m_nearCutOff = *(float*)_val;
		break;
	case LDN_FORCE_TYPE:
		m_forceType = *(float*)_val;
		break;
	case LDN_TBF_ANGLE:
		m_TBF_angle = *(float*)_val;
		break;
	case LDN_TBF_STRENGTH:
		m_TBF_strength = *(float*)_val;
		break;
	case LDN_TBF_SYMM:
		m_TBF_symm = (bool)*(float*)_val;
		break;
	}
}

void* Line::Get(LDN _type)
{
	switch (_type)
	{

	case LDN_COLOUR:
		return &m_colour;
	case LDN_R:
		return &m_colour;
	case LDN_G:
		return &m_colour.y;
	case LDN_B:
		return &m_colour.z;
	case LDN_A:
		return &m_colour.w;
	case LDN_DRAW:
		return &m_draw;
	case LDN_FORCE:
		return &m_force;
	case LDN_STRENGTH:
		return &m_strength;
	case LDN_POWER:
		return &m_power;
	case LDN_BASE_LENGTH:
		return &m_baseLength;
	case LDN_CUT_OFF:
		return &m_cutOff;
	case LDN_NEAR_CUT_OFF:
		return &m_nearCutOff;
	case LDN_FORCE_TYPE:
		return &m_forceType;
	case LDN_TBF_ANGLE:
		return &m_TBF_angle;
	case LDN_TBF_STRENGTH:
		return &m_TBF_strength;
	case LDN_TBF_SYMM:
		return &m_TBF_symm;
	default:
		return nullptr;
	}
}

bool Line::CanDraw()
{
	return (m_draw && (!*(bool*)m_from->Get(PDN_HIDE)) && (!*(bool*)m_to->Get(PDN_HIDE)));
}

void Line::Invert()
{
	Particle* dummy = m_from;
	m_from = m_to;
	m_to = dummy;
}
