#include "AttribArray.h"
#include "attrib.h"

AttribArray::AttribArray()
{
}


AttribArray::~AttribArray()
{
	for (int i = 0; i < m_attribs.size(); i++)
	{
		delete m_attribs[i];
	}

	m_attribs.clear();
}

void AttribArray::AddAttrib(Attrib* _attrib)
{
	_ASSERT(_attrib);
	m_attribs.push_back(_attrib);
}

void AttribArray::Shrink()
{
	m_attribs.shrink_to_fit();
}

Attrib* AttribArray::FetchAttrib(const char* _attrib)
{
	Attrib* attribVal = nullptr;
	for (int i = 0; i < m_attribs.size(); i++)
	{
		if (strcmp(m_attribs[i]->GetName(), _attrib) == 0)
		{
			attribVal = m_attribs[i];
			break;
		}
	}

	return attribVal;
}