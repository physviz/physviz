#include "attrib.h"

Attrib::Attrib() :m_name(nullptr), m_type(ATTRIB_NULL)
{
}

Attrib::~Attrib()
{
	if (m_name)
	{
		delete[] m_name;
		m_name = nullptr;
	}

	switch (m_type)
	{
	case ATTRIB_STRING:
		if (m_value.atSTRING)
		{
			delete[] m_value.atSTRING;
		}
		m_value.atSTRING = nullptr;
		break;
	case ATTRIB_ARRAY:
		if (m_value.atARRAY)
		{
			delete m_value.atARRAY;
			m_value.atARRAY = nullptr;
		}
		break;
	}
}


void Attrib::SetName(char* _name)
{
	if (m_name)
	{
		delete[] m_name;
		m_name = nullptr;
	}

	m_name = _name;
}

void Attrib::SetValue(void* _val, AttribType _type)
{
	m_type = _type;

	switch (_type)
	{
	case ATTRIB_NULL:
		//nothing should need to be done here
		return;
	case ATTRIB_STRING:
	case ATTRIB_ARRAY:
		//need to copy in the pointer for these two types
		//note attrib takes responsibility for delteing these
		m_value.atSTRING = (char*)_val;
		return;
	default:
		//in general need to copy in all the data
		memcpy(&m_value, _val, sizeof(m_value));
	}
}