#include "particleh.h"
#include "particledata.h"
#include "line.h"
#include <string>

#include <iostream>

using namespace std;

Particle::Particle(ParticleData* _inData, string _name) :m_piData(nullptr), m_pcData(_inData), m_name(_name)
{
	if (_inData)
	{
		m_piData = new ParticleData();
		memcpy(m_piData, m_pcData, sizeof(ParticleData));
		Reset();
	}
	else
	{
		m_piData = new ParticleData();
		m_pcData = new ParticleData();
	}
}

Particle::~Particle()
{
	if (m_piData != nullptr && !m_pcData->m_externalOwnership)
	{
		delete m_piData;
	}
	if (m_pcData != nullptr && !m_pcData->m_externalOwnership)
	{
		delete m_pcData;
	}

	m_ThreeBodyLines.clear();
}

void Particle::Tick(float _dt)
{
	float newRot = m_pcData->m_rot + _dt * m_pcData->m_rot_v;
	float newRot_v = m_pcData->m_rot_v + _dt * m_pcData->m_rot_a - _dt * m_pcData->m_drag * m_pcData->m_rot_v;

	m_pcData->m_rot = newRot;
	m_pcData->m_rot_v = newRot_v;
	m_pcData->m_rot_a = 0.0f;

	DWORD currentTime = GetTickCount();
	float t = currentTime / 1000.0f;

	for (int i = 0; i < 3; i++)
	{
		if (m_pcData->m_oscVal[i])
		{
			float val = (*(float*)m_piData->Get(m_pcData->m_oscVal[i])) + m_pcData->m_oscAmp[i] * cos(m_pcData->m_oscW[i] * t + m_pcData->m_oscPhase[i]);
			m_pcData->Set(m_pcData->m_oscVal[i], &val);
		}
		if (m_pcData->m_jigVal[i])
		{
			float rnd = (float)rand() / (float)RAND_MAX;
			float val = _dt * m_pcData->m_jigMag[i] * (2.0f * rnd - 1.0f) + (*(float*)m_pcData->Get(m_pcData->m_jigVal[i]));
			m_pcData->Set(m_pcData->m_jigVal[i], &val);
		}
	}

	if (m_pcData->m_fixed)
	{
		memset(m_pcData->m_acc, 0, sizeof(float) * 3);
		return;
	}

	float newPos[3];
	float newVel[3];

	for (int i = 0; i < 3; i++)
	{
		newPos[i] = m_pcData->m_pos[i] + _dt * m_pcData->m_vel[i];
		newVel[i] = m_pcData->m_vel[i] + _dt * (m_pcData->m_acc[i] - m_pcData->m_drag * m_pcData->m_vel[i] / m_pcData->m_mass + m_pcData->m_grav[i]);

		if (abs(newVel[i]) > PART_MAX_V)
		{
			newVel[i] = (signbit(newVel[i]) ? -1.0f : 1.0f) * PART_MAX_V;
		}

	}

	memcpy(m_pcData->m_pos, newPos, sizeof(float) * 3);
	memcpy(m_pcData->m_vel, newVel, sizeof(float) * 3);
	memset(m_pcData->m_acc, 0, sizeof(float) * 3);
}

void Particle::PreTick(float _dt)
{
	int numTBF = m_ThreeBodyLines.size();
	if (numTBF != 0)
	{
		if (numTBF == 1)
		{
			m_ThreeBodyLines.clear();
		}
		else
		{
			for (int i = 0; i < numTBF; i++)
			{
				for (int j = i + 1; j < numTBF; j++)
				{
					ApplyTBF(m_ThreeBodyLines[i], m_ThreeBodyLines[j]);
				}
			}
		}
	}
}

void Particle::PostTick(float _dt)
{
	float maxV = PART_MAX_V * _dt; //effectively this is what the Euler solver clamp is doing
	//YES! I know double use of the m_acc as now using it as an impulse!
	for (int i = 0; i < 3; i++)
	{
		float dP = m_pcData->m_acc[i];
		if (abs(dP) > maxV)
		{
			dP = (signbit(dP) ? -1.0f : 1.0f) * maxV;
		}
		m_pcData->m_pos[i] += dP;
		m_pcData->m_acc[i] = 0.0f;
	}
}

void Particle::Reset()
{
	memcpy(m_pcData, m_piData, sizeof(ParticleData));
	m_piData->m_copy = false;
	m_pcData->m_copy = true;
}

void Particle::Set(PDN _type, void* _val)
{
	m_pcData->Set(_type, _val);
}

void* Particle::Get(PDN _type)
{
	return m_pcData->Get(_type);
}

void Particle::AddTBFLine(Line* _line)
{
	m_ThreeBodyLines.push_back(_line);
}

void Particle::ApplyTBF(Line* _one, Line* _two)
{
	if (_one->GetType() == _two->GetType())
	{
		Particle* one = _one->GetTo();
		Particle* two = _two->GetTo();
		float strength = *(float*)_one->Get(LDN_TBF_STRENGTH);
		float angleE = cosf(*(float*)_one->Get(LDN_TBF_ANGLE));
		Vector3 pos = Vector3(m_pcData->m_pos);
		Vector3 oneP = Vector3(one->m_pcData->m_pos);
		Vector3 twoP = Vector3(two->m_pcData->m_pos);
		Vector3 vec1 = oneP - pos;
		Vector3 vec2 = twoP - pos;
		vec1.Normalize();
		vec2.Normalize();
		float angle = vec1.Dot(vec2);
		float force = strength * (angleE - angle);
		Vector3 dP = twoP - oneP;
		dP.Normalize();
		Vector3* acc1 = (Vector3*)one->m_pcData->m_acc;
		Vector3* acc2 = (Vector3*)two->m_pcData->m_acc;
		(*acc1) += (force / one->m_pcData->m_mass) * dP;
		(*acc2) -= (force / two->m_pcData->m_mass) * dP;
	}
}

bool Particle::GetText(Vector3 _camPos, Vector3& _pos, float _maxDistance)
{
	XMVECTOR toTarget = XMLoadFloat3(&_camPos) - XMLoadFloat3(&XMFLOAT3(&m_pcData->m_pos[0]));
	float distance = XMVectorGetX(XMVector3Length(toTarget));
	if (distance > _maxDistance) return false;

	_pos = Vector3(&m_pcData->m_pos[0]);

	return true;
}
//set the Initial Data to be the current data
void Particle::SetInitData()
{
	memcpy(m_piData, m_pcData, sizeof(ParticleData));
	m_piData->m_copy = false;
	m_pcData->m_copy = true;
}

void Particle::FixInside(Particle* _in)
{
	float* pos = m_pcData->m_pos;
	float* size = m_pcData->m_size;

	float* inPos = (float*)_in->Get(PDN_POSITION);
	for (int i = 0; i < 3; i++)
	{
		//remember that the box is centred at 0 so extends to half the size in each direction
		inPos[i] = max(inPos[i], (pos[i] - 0.5f * size[i]));
		inPos[i] = min(inPos[i], (pos[i] + 0.5f * size[i]));
	}
}
