#include "link.h"
#include <vector>
#include <map>
#include "HowLink.h"
#include "node.h"
#include <iostream>

using namespace std;

Link::Link() :m_from(nullptr), m_to(nullptr), m_recast(false)
{
}

Link::~Link()
{
#ifdef STRING_LINK
	if (!m_recast)
	{
		//if we have not re-cast and these have been set then these probably point to strings
		if (m_from)
		{
			delete[](char*)m_from;
		}
		if (m_to)
		{
			delete[](char*)m_to;
		}
	}
#endif
}

bool Link::Recast(void* _nodes)
{
	if (m_recast)
	{
		return true;
	}

#ifdef STRING_LINK
	NodeMap* n = (NodeMap*)_nodes;
	char* c_from = (char*)m_from;
	char* c_to = (char*)m_to;
	string from = (char*)m_from;
	string to = (char*)m_to;

	NodeMap::iterator it_from, it_to;

	if (((it_from = n->find(from)) != n->end()) && ((it_to = n->find(to)) != n->end()))
	{
		delete[] c_from;//still need to delete these.
		delete[] c_to;
		m_from = it_from->second;
		m_to = it_to->second;

		m_recast = true;

		return true;
	}
	else
	{
		cout << "INVALID LINK" << endl;
		cout << "TYPE: " << m_type << endl;
		cout << "FROM" << ((it_from == n->end()) ? " INVALID: " : " VALID: ") << c_from << endl;
		cout << "TO  " << ((it_to == n->end()) ? " INVALID: " : " VALID: ") << c_to << endl;
		return false;
	}


#endif

#ifdef INT_LINK
	vector<Node*> *n = (vector<Node*> *) _nodes;
	int from = (int)m_from;//recast position ints to the actual pointers
	int to = (int)m_to;

	if (from < n->size() && to < n->size())
	{
		m_from = (*n)[from];
		m_to = (*n)[to];
		m_recast = true;
		return true;
	}
	else
	{
		return false;
	}
#endif
}

bool Link::IsRenderable()
{
	return (m_to->HasParticle() && m_from->HasParticle());
}

void Link::GetParticles(Particle** _from, Particle** _to)
{
	*_from = (Particle*)m_from->HasParticle();
	*_to = (Particle*)m_to->HasParticle();
}