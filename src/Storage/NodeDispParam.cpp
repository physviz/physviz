#include "NodeDispParam.h"


NodeDispParam::NodeDispParam() :m_type(""), m_base(""), m_max(), m_min(), m_scale(1.0f), m_default(0.0f), m_contains(NDPC_NONE), m_delta(0.0f)
{

}

NodeDispParam::~NodeDispParam()
{

}

bool NodeDispParam::SetValue(string _attrib, float _valF, string _valS)
{
	NDPC _attribEnum;

	if (_attrib == "type")
	{
		_attribEnum = NDPC_TYPE;
	}
	else if (_attrib == "base")
	{
		_attribEnum = NDPC_BASE;
	}
	else if (_attrib == "max")
	{
		_attribEnum = NDPC_MAX;
	}
	else if (_attrib == "min")
	{
		_attribEnum = NDPC_MIN;
	}
	else if (_attrib == "default")
	{
		_attribEnum = NDPC_DEFAULT;
	}
	else if (_attrib == "scale")
	{
		_attribEnum = NDPC_SCALE;
	}
	else if (_attrib == "delta")
	{
		_attribEnum = NDPC_DELTA;
	}
	else
	{
		return false;
	}

	SetValue(_attribEnum, _valF, _valS);

	return true;
}

void NodeDispParam::SetValue(NDPC _attrib, float _valF, string _valS)
{
	switch (_attrib)
	{
	case NDPC_MAX:
		m_max = _valF;
		m_contains = (NDPC)(m_contains | NDPC_MAX);
		return;
	case NDPC_MIN:
		m_min = _valF;
		m_contains = (NDPC)(m_contains | NDPC_MIN);
		return;
	case NDPC_SCALE:
		m_scale = _valF;
		m_contains = (NDPC)(m_contains | NDPC_SCALE);
		return;
	case NDPC_DEFAULT:
		m_default = _valF;
		m_contains = (NDPC)(m_contains | NDPC_DEFAULT);
		return;
	case NDPC_BASE:
		m_base = _valS;
		m_contains = (NDPC)(m_contains | NDPC_BASE);
		return;
	case NDPC_TYPE:
		m_type = _valS;
		m_contains = (NDPC)(m_contains | NDPC_TYPE);
		return;
	case NDPC_DELTA:
		m_delta = _valF;
		m_contains = (NDPC)(m_contains | NDPC_DELTA);
		return;
	default:
		//possibly some error state
		return;
	}
}

float NodeDispParam::GetValue(NDPC _val)
{
	if (m_contains & _val)
	{
		switch (_val)
		{
		case NDPC_MAX:
			return m_max;
		case NDPC_MIN:
			return m_min;
		case NDPC_SCALE:
			return m_scale;
		case NDPC_DEFAULT:
			return m_default;
		case NDPC_DELTA:
			return m_delta;
		default:
			return 0.0f;//probably need a fail state
		}
	}
	else
	{
		switch (_val)
		{
		case NDPC_MAX:
			return 9.9e10;
		case NDPC_MIN:
			return -9.9e10;
		case NDPC_SCALE:
			return 1.0f;
		case NDPC_DEFAULT:
			return 0.0f;
		case NDPC_DELTA:
			return 0.0f;
		default:
			return 0.0f;//probably need a fail state
		}
	}
}