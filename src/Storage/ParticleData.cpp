#include "particledata.h"
#include <string>

//any additions / changes to this list must also be made to the list in the editor in Document_Data.cs
std::string PDN_Names[] = {
	"PDN_NULL",
	"PDN_POSITION",
	"PDN_POSITION_X",
	"PDN_POSITION_Y",
	"PDN_POSITION_Z",
	"PDN_ROTATION",
	"PDN_ROT_VEL",
	"PDN_ROT_ACC",
	"PDN_SIZE",
	"PDN_SIZE_X",
	"PDN_SIZE_Y",
	"PDN_SIZE_Z",
	"PDN_MASS",
	"PDN_COLOUR",
	"PDN_COLOUR_R",
	"PDN_COLOUR_G",
	"PDN_COLOUR_B",
	"PDN_COLOUR_A",
	"PDN_TEXTURE_NAME",
	"PDN_TEXTURE",
	"PDN_VELOCITY",
	"PDN_VELOCITY_X",
	"PDN_VELOCITY_Y",
	"PDN_VELOCITY_Z",
	"PDN_ACC",
	"PDN_ACC_X",
	"PDN_ACC_Y",
	"PDN_ACC_Z",
	"PDN_DRAG",
	"PDN_GRAV",
	"PDN_GRAV_X",
	"PDN_GRAV_Y",
	"PDN_GRAV_Z",
	"PDN_OF_INTEREST",
	"PDN_FIXED",
	"PDN_HIDE",
	"PDN_SUBSCREEN",
	"PDN_OSC_AMP_1",
	"PDN_OSC_AMP_2",
	"PDN_OSC_AMP_3",
	"PDN_OSC_W_1",
	"PDN_OSC_W_2",
	"PDN_OSC_W_3",
	"PDN_OSC_P_1",
	"PDN_OSC_P_2",
	"PDN_OSC_P_3",
	"PDN_OSC_VAL_1",
	"PDN_OSC_VAL_2",
	"PDN_OSC_VAL_3",
	"PDN_JIG_MAG_1",
	"PDN_JIG_MAG_2",
	"PDN_JIG_MAG_3",
	"PDN_JIG_VAL_1",
	"PDN_JIG_VAL_2",
	"PDN_JIG_VAL_3",
	"PDN_DISPLAY_TYPE",
	"PDN_CONTAINS_LINK",
	"PDN_CONTAINS_DIRECTION",
	"PDN_COUNT"
};


PDN GetPDN(string _string)
{
	for (int i = 0; i < PDN_COUNT; i++)
	{
		if (PDN_Names[i] == _string)
		{
			return (PDN)i;
		}
	}

	return PDN_NULL;
}

ParticleData::ParticleData()
{
	//zero everything and then set stuff that shouldn't be to 1.0f or equivalent
	memset(this, 0, sizeof(ParticleData));
	m_colour[0] = m_colour[1] = m_colour[2] = m_colour[3] = 1.0f;
	m_size[0] = m_size[1] = m_size[2] = 1.0f;
	m_mass = 1.0f;
	m_drag = 1.0f;
	m_hide = true; //hide unless otherwise
	m_subscreen = -1; //ALWAYS DRAW ME
};

ParticleData::~ParticleData()
{
	if (m_textureName && !m_copy)
	{
		delete[] m_textureName;
	}
	if (m_containsLink && !m_copy)
	{
		delete[] m_containsLink;
	}
}


void ParticleData::Set(PDN _type, void* _val)
{
	switch (_type)
	{
	case PDN_POSITION:
		memcpy(this->m_pos, _val, sizeof(float) * 3);
		return;
	case PDN_POSITION_X:
		m_pos[0] = *(float*)_val;
		return;
	case PDN_POSITION_Y:
		m_pos[1] = *(float*)_val;
		return;
	case PDN_POSITION_Z:
		m_pos[2] = *(float*)_val;
		return;
	case PDN_ROTATION:
		m_rot = *(float*)_val;
		return;
	case PDN_ROT_VEL:
		m_rot_v = *(float*)_val;
		return;
	case PDN_ROT_ACC:
		m_rot_a = *(float*)_val;
		return;
	case PDN_SIZE:
		memcpy(this->m_size, _val, sizeof(float) * 2);
		return;
	case PDN_SIZE_X:
		m_size[0] = *(float*)_val;
		return;
	case PDN_SIZE_Y:
		m_size[1] = *(float*)_val;
		return;
	case PDN_SIZE_Z:
		m_size[2] = *(float*)_val;
		return;
	case PDN_MASS:
		m_mass = *(float*)_val;
		return;
	case PDN_COLOUR:
		memcpy(this->m_colour, _val, sizeof(float) * 4);
		return;
	case PDN_COLOUR_R:
		m_colour[0] = *(float*)_val;
		return;
	case PDN_COLOUR_G:
		m_colour[1] = *(float*)_val;
		return;
	case PDN_COLOUR_B:
		m_colour[2] = *(float*)_val;
		return;
	case PDN_COLOUR_A:
		m_colour[3] = *(float*)_val;
		return;
	case PDN_TEXTURE_NAME:
	{
		int length = 1 + strlen((const char*)_val);
		m_textureName = new char[length];
		memcpy(m_textureName, _val, length);
		return;
	}
	case PDN_TEXTURE:
		m_pTexture = _val;
		return;
	case PDN_VELOCITY:
		memcpy(this->m_vel, _val, sizeof(float) * 3);
		return;
	case PDN_VELOCITY_X:
		m_vel[0] = *(float*)_val;
		return;
	case PDN_VELOCITY_Y:
		m_vel[1] = *(float*)_val;
		return;
	case PDN_VELOCITY_Z:
		m_vel[2] = *(float*)_val;
		return;
	case PDN_ACC:
		memcpy(this->m_acc, _val, sizeof(float) * 3);
		return;
	case PDN_ACC_X:
		m_acc[0] = *(float*)_val;
		return;
	case PDN_ACC_Y:
		m_acc[1] = *(float*)_val;
		return;
	case PDN_ACC_Z:
		m_acc[2] = *(float*)_val;
		return;
	case PDN_DRAG:
		m_drag = *(float*)_val;
		return;
	case PDN_GRAV:
		memcpy(this->m_grav, _val, sizeof(float) * 3);
		return;
	case PDN_GRAV_X:
		m_grav[0] = *(float*)_val;
		return;
	case PDN_GRAV_Y:
		m_grav[1] = *(float*)_val;
		return;
	case PDN_GRAV_Z:
		m_grav[2] = *(float*)_val;
		return;
	case PDN_OF_INTEREST:
		m_ofInterest = (bool)*(float*)_val;
		return;
	case PDN_FIXED:
		m_fixed = (bool)*(float*)_val;
		return;
	case PDN_HIDE:
		m_hide = (bool)*(float*)_val;
		return;
	case PDN_SUBSCREEN:
		m_subscreen = (int)*(float*)_val;
		return;
	case PDN_OSC_AMP_1:
		m_oscAmp[0] = *(float*)_val;
		return;
	case PDN_OSC_AMP_2:
		m_oscAmp[1] = *(float*)_val;
		return;
	case PDN_OSC_AMP_3:
		m_oscAmp[2] = *(float*)_val;
		return;
	case PDN_OSC_W_1:
		m_oscW[0] = *(float*)_val;
		return;
	case PDN_OSC_W_2:
		m_oscW[1] = *(float*)_val;
		return;
	case PDN_OSC_W_3:
		m_oscW[2] = *(float*)_val;
		return;
	case PDN_OSC_P_1:
		m_oscPhase[0] = *(float*)_val;
		return;
	case PDN_OSC_P_2:
		m_oscPhase[1] = *(float*)_val;
		return;
	case PDN_OSC_P_3:
		m_oscPhase[2] = *(float*)_val;
		return;
	case PDN_OSC_VAL_1:
		m_oscVal[0] = (PDN)(int)*(float*)_val;
		return;
	case PDN_OSC_VAL_2:
		m_oscVal[1] = (PDN)(int)*(float*)_val;
		return;
	case PDN_OSC_VAL_3:
		m_oscVal[2] = (PDN)(int)*(float*)_val;
		return;
	case PDN_JIG_VAL_1:
		m_jigVal[0] = (PDN)(int)*(float*)_val;
		return;
	case PDN_JIG_VAL_2:
		m_jigVal[1] = (PDN)(int)*(float*)_val;
		return;
	case PDN_JIG_VAL_3:
		m_jigVal[2] = (PDN)(int)*(float*)_val;
		return;
	case PDN_JIG_MAG_1:
		m_jigMag[0] = *(float*)_val;
		return;
	case PDN_JIG_MAG_2:
		m_jigMag[1] = *(float*)_val;
		return;
	case PDN_JIG_MAG_3:
		m_jigMag[2] = *(float*)_val;
		return;
	case PDN_DISPLAY_TYPE:
		m_displayType = *(float*)_val;
		return;
	case PDN_CONTAINS_LINK:
	{
		int length = 1 + strlen((const char*)_val);
		m_containsLink = new char[length];
		memcpy(m_containsLink, _val, length);
		return;
	}
	case PDN_CONTAINS_DIRECTION:
		m_containDirection = (int)*(float*)_val;
		return;
	default:
		//some kind of fail state?
		return;
	}
}

void* ParticleData::Get(PDN _type)
{
	//okay it breaks encapsulation, but it works pretty cool!
	switch (_type)
	{
	case PDN_POSITION:
		return m_pos;
	case PDN_POSITION_X:
		return m_pos;
	case PDN_POSITION_Y:
		return &m_pos[1];
	case PDN_POSITION_Z:
		return &m_pos[2];
	case PDN_ROTATION:
		return &m_rot;
	case PDN_ROT_VEL:
		return &m_rot_v;
	case PDN_ROT_ACC:
		return &m_rot_a;
	case PDN_SIZE:
		return m_size;
	case PDN_SIZE_X:
		return m_size;
	case PDN_SIZE_Y:
		return &m_size[1];
	case PDN_SIZE_Z:
		return &m_size[2];
	case PDN_MASS:
		return &m_mass;
	case PDN_COLOUR:
		return m_colour;
	case PDN_COLOUR_R:
		return m_colour;
	case PDN_COLOUR_G:
		return &m_colour[1];
	case PDN_COLOUR_B:
		return &m_colour[2];
	case PDN_COLOUR_A:
		return &m_colour[3];
	case PDN_TEXTURE_NAME:
		return m_textureName;
	case PDN_TEXTURE:
		return m_pTexture;
	case PDN_VELOCITY:
		return m_vel;
	case PDN_VELOCITY_X:
		return m_vel;
	case PDN_VELOCITY_Y:
		return &m_vel[1];
	case PDN_VELOCITY_Z:
		return &m_vel[2];
	case PDN_ACC:
		return m_acc;
	case PDN_ACC_X:
		return m_acc;
	case PDN_ACC_Y:
		return &m_acc[1];
	case PDN_ACC_Z:
		return &m_acc[2];
	case PDN_DRAG:
		return &m_drag;
	case PDN_GRAV:
		return m_grav;
	case PDN_GRAV_X:
		return m_grav;
	case PDN_GRAV_Y:
		return &m_grav[1];
	case PDN_GRAV_Z:
		return &m_grav[2];
	case PDN_OF_INTEREST:
		return &m_ofInterest;
	case PDN_FIXED:
		return &m_fixed;
	case PDN_HIDE:
		return &m_hide;
	case PDN_SUBSCREEN:
		return &m_subscreen;
	case PDN_OSC_AMP_1:
		return m_oscAmp;
	case PDN_OSC_AMP_2:
		return &m_oscAmp[1];
	case PDN_OSC_AMP_3:
		return &m_oscAmp[2];
	case PDN_OSC_W_1:
		return m_oscW;
	case PDN_OSC_W_2:
		return &m_oscW[1];
	case PDN_OSC_W_3:
		return &m_oscW[2];
	case PDN_OSC_P_1:
		return m_oscPhase;
	case PDN_OSC_P_2:
		return &m_oscPhase[1];
	case PDN_OSC_P_3:
		return &m_oscPhase[2];
	case PDN_OSC_VAL_1:
		return m_oscVal;
	case PDN_OSC_VAL_2:
		return &m_oscVal[1];
	case PDN_OSC_VAL_3:
		return &m_oscVal[2];
	case PDN_JIG_VAL_1:
		return m_jigVal;
	case PDN_JIG_VAL_2:
		return &m_jigVal[1];
	case PDN_JIG_VAL_3:
		return &m_jigVal[2];
	case PDN_JIG_MAG_1:
		return m_jigMag;
	case PDN_JIG_MAG_2:
		return &m_jigMag[1];
	case PDN_JIG_MAG_3:
		return &m_jigMag[2];
	case PDN_DISPLAY_TYPE:
		return &m_displayType;
	case PDN_CONTAINS_LINK:
		return m_containsLink;
	case PDN_CONTAINS_DIRECTION:
		return &m_containDirection;
	default:
		//some kind of fail state?
		return nullptr;
	}
}