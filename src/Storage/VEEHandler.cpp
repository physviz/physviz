#include "VEEHandler.h"
#include "graph.h"
#include "node.h"
#include "AttribArray.h"
#include "attrib.h"
#include "link.h"
#include "HowLink.h"

//VEE Handler State
enum VHS
{
	VHS_BEFORE,
	VHS_START_NODES,
	VHS_READING_NODES,
	VHS_READ_NODE_NAME_START,
	VHS_READ_NODE_NAME,
	VHS_READ_NODE_TYPE_START,
	VHS_READ_NODE_TYPE,
	VHS_READ_NODE_ATTRIBS_START,
	VHS_READ_NODE_ATTRIBS_ARRAY_1,
	VHS_READ_NODE_ATTRIBS_ARRAY_2,
	VHS_READ_NODE_EXIT,
	VHS_READ_NODE_ATTRIB_START,
	VHS_READ_NODE_ATTRIB_NAME,
	VHS_READ_NODE_ATTRIB_VALUE,
	VHS_READ_NODE_ATTRIBS_EXIT,
	VHS_START_LINKS,
	VHS_READ_LINK_START,
	VHS_READ_LINK_FROM,
	VHS_READ_LINK_FROM_VALUE,
	VHS_READ_LINK_TO,
	VHS_READ_LINK_TO_VALUE,
	VHS_READ_LINK_TYPE,
	VHS_READ_LINK_TYPE_VALUE,
	VHS_COUNT
};

VEEHandler::VEEHandler() :m_state(VHS_BEFORE), m_graph(nullptr), m_currLink(nullptr), m_currNode(nullptr), m_currAA(nullptr), m_currAttrib(nullptr)
{
	m_graph = new Graph();
}

VEEHandler::~VEEHandler()
{
	//now the main code gets rid of this
}

bool VEEHandler::StartObject()
{
#ifdef  BUILDING
	cout << "StartObject()" << endl;
#endif

	switch (m_state)
	{
	case VHS_BEFORE:
		m_state = VHS_START_NODES;
		return true;
	case VHS_START_NODES:
		m_state = VHS_READ_NODE_NAME_START;
		return true;
	case VHS_READ_NODE_NAME_START:
		if (!m_currNode)
		{
			m_currNode = new Node();
			m_graph->AddNode(m_currNode);
			return true;
		}
		else
		{
			return false;
		}
	case VHS_READ_NODE_NAME:
		return true;
	case VHS_READ_NODE_TYPE_START:
		return true;
	case VHS_READ_NODE_TYPE:
		return true;
	case VHS_READ_NODE_ATTRIB_START:
		m_state = VHS_READ_NODE_ATTRIB_NAME;
		if (!m_currAttrib)
		{
			m_currAttrib = new Attrib();
			m_currAA->AddAttrib(m_currAttrib);
		}
		return true;
	case VHS_READ_LINK_START:
		if (!m_currLink)
		{
			m_currLink = new Link();
		}
		m_state = VHS_READ_LINK_FROM;
		return true;
	default:
		return false;
	}
}

//this thing doesn't give nice null terminated arrays let's fix that
char* terminate(const char* _str, unsigned _length)
{
	char* dummy = new char[_length + 1];
	memcpy(dummy, _str, _length);
	dummy[_length] = '\0';
	return dummy;
}

bool VEEHandler::String(const char* _str, unsigned _length, bool _copy)
{

#ifdef  BUILDING
	cout << "String(" << _str << ", " << _length << ", " << boolalpha << _copy << ")" << endl;
#endif

	switch (m_state)
	{
	case VHS_START_NODES:
		//if we are staring to load the nodes and this string is "nodes" we are doing okay
		return (strcmp(_str, "nodes") == 0);
	case VHS_READ_NODE_NAME_START:
		if (strcmp(_str, "name") == 0)
		{
			m_state = VHS_READ_NODE_NAME;
			return true;
		}
		else
		{
			return false;
		}
	case VHS_READ_NODE_NAME:
		m_currNode->SetName(terminate(_str, _length));
#ifdef STRING_LINK
		m_graph->AddNodeToMap(m_currNode);
#endif
		m_state = VHS_READ_NODE_TYPE_START;
		return true;
	case VHS_READ_NODE_TYPE_START:
		if (strcmp(_str, "type") == 0)
		{
			m_state = VHS_READ_NODE_TYPE;
			return true;
		}
		else
		{
			return false;
		}
	case VHS_READ_NODE_TYPE:
		m_currNode->SetType(terminate(_str, _length));
		m_state = VHS_READ_NODE_ATTRIBS_START;
		return true;
	case VHS_READ_NODE_ATTRIBS_START:
		if (strcmp(_str, "attributes") == 0)
		{
			m_state = VHS_READ_NODE_ATTRIBS_ARRAY_1;
			return true;
		}
		else
		{
			return false;
		}
	case VHS_READ_NODE_ATTRIB_NAME:
		if (m_currAttrib)
		{
			m_currAttrib->SetName(terminate(_str, _length));
			m_state = VHS_READ_NODE_ATTRIB_VALUE;
			return true;
		}
		else
		{
			return false;
		}
	case VHS_READ_NODE_ATTRIB_VALUE:
		if (m_currAttrib)
		{
			m_currAttrib->SetValue((void*)terminate(_str, _length), ATTRIB_STRING);
			return true;
		}
		else
		{
			return false;
		}
	case VHS_START_LINKS:
		//if we are staring to load the nodes and this string is "relations" we are doing okay
		return (strcmp(_str, "relations") == 0);
	case VHS_READ_LINK_FROM:
		m_state = VHS_READ_LINK_FROM_VALUE;
		return (strcmp(_str, "from") == 0);
	case VHS_READ_LINK_TO:
		m_state = VHS_READ_LINK_TO_VALUE;
		return (strcmp(_str, "to") == 0);
#ifdef STRING_LINK
	case VHS_READ_LINK_FROM_VALUE:
		if (m_currLink)
		{
			m_currLink->SetFrom((Node*)terminate(_str, _length));
			m_state = VHS_READ_LINK_TO;
			return true;
		}
		else
		{
			return false;
		}
	case VHS_READ_LINK_TO_VALUE:
		if (m_currLink)
		{
			m_currLink->SetTo((Node*)terminate(_str, _length));
			m_state = VHS_READ_LINK_TYPE;
			return true;
		}
		else
		{
			return false;
		}
#endif
	case VHS_READ_LINK_TYPE:
		m_state = VHS_READ_LINK_TYPE_VALUE;
		return (strcmp(_str, "type") == 0);
	case VHS_READ_LINK_TYPE_VALUE:
		if (m_currLink)
		{
			m_currLink->SetType(terminate(_str, _length));
			return true;
		}
		else
		{
			return false;
		}
	default:
		return false;
	}

}

bool VEEHandler::StartArray()
{
#ifdef  BUILDING
	cout << "StartArray()" << endl;
#endif

	switch (m_state)
	{
	case VHS_START_NODES:
		m_state = VHS_READ_NODE_NAME_START;
		return true;
	case VHS_READ_NODE_ATTRIBS_ARRAY_1:
		if (!m_currAA)
		{
			m_currAA = new AttribArray();
			m_state = VHS_READ_NODE_ATTRIBS_ARRAY_2;
			return true;
		}
		else
		{
			return false;
		}
	case VHS_READ_NODE_ATTRIBS_ARRAY_2:
		m_state = VHS_READ_NODE_ATTRIB_START;
		return true;

	case VHS_READ_NODE_ATTRIB_VALUE:
		cout << "Not yet dealt with Array attribues!" << endl;
		return false;
	case VHS_START_LINKS:
		m_state = VHS_READ_LINK_START;
		return true;
	default:
		return false;
	}
}


bool VEEHandler::EndArray(unsigned _elementCount)
{
#ifdef BUILDING
	cout << "EndArray(" << _elementCount << ")" << endl;
#endif

	switch (m_state)
	{
	case VHS_READ_NODE_ATTRIBS_EXIT:
	case VHS_READ_NODE_ATTRIBS_ARRAY_2:
		if (m_currAA)
		{
			//if there is nothing in the current Attrib Array delete it
			if (!m_currAA->GetLength())
			{
				delete m_currAA;
				m_currAA = nullptr;
			}
			else
			{
				m_currAA->Shrink();
			}
			//attach to the current node
			m_currNode->SetAttribs(m_currAA);
			//no longer any of my business on to the next attribute array
			m_currAA = nullptr;
			m_state = VHS_READ_NODE_EXIT;
			return true;
		}
		return false;
	case VHS_READ_NODE_ATTRIB_START:
		m_state = VHS_READ_NODE_ATTRIBS_EXIT;
		return true;
	case VHS_READ_NODE_NAME_START:
		//I've reached the end of the Nodes on to the Links
		m_graph->TidyNodes();
		m_state = VHS_START_LINKS;
		return true;
	case VHS_READ_LINK_START:
		m_graph->TidyLinks();
		return true;
	default:
		return false;
	}
}

bool VEEHandler::EndObject(unsigned _memberCount)
{
#ifdef BUILDING
	cout << "EndObject(" << _memberCount << ")" << endl;
#endif

	switch (m_state)
	{
	case VHS_READ_NODE_ATTRIBS_EXIT:
	case VHS_READ_NODE_EXIT:
		if (m_currNode)
		{
			m_currNode = nullptr;
			m_state = VHS_READ_NODE_NAME_START;
			return true;
		}
		else
		{
			return false;
		}
	case VHS_READ_NODE_ATTRIB_VALUE:
		if (m_currAttrib && m_currAA)
		{
			m_currAttrib = nullptr;
			m_state = VHS_READ_NODE_ATTRIB_START;
			return true;
		}
		else
		{
			return false;
		}
	case VHS_READ_LINK_TYPE_VALUE:
		if (m_currLink)
		{
			if (m_graph->AddLink(m_currLink))
			{
#ifdef BUILDING
				cout << "VALID LINK" << endl;
#endif
			}
			else
			{
				//link was invalid so wasn't added lets remove it
				delete m_currLink;
			}

			m_currLink = nullptr;
			m_state = VHS_READ_LINK_START;
			return true;
		}
		else
		{
			return false;
		}
	default:
		return false;
	}
}

bool VEEHandler::readValue(void* _val, AttribType _type)
{

	switch (m_state)
	{
	case VHS_READ_NODE_ATTRIB_VALUE:
		if (m_currAttrib)
		{
			m_currAttrib->SetValue(_val, _type);
			return true;
		}
		else
		{
			return false;
		}
	default:
		return false;
	}
}


bool VEEHandler::Uint(unsigned _u)
{
#ifdef BUILDING
	cout << "Uint(" << _u << ")" << endl;
#endif

	switch (m_state)
	{
#ifdef INT_LINK
	case VHS_READ_LINK_FROM_VALUE:
		if (m_currLink)
		{
			m_currLink->setFrom((Node*)_u);
			m_state = VHS_READ_LINK_TO;
			return true;
		}
		else
		{
			return false;
		}
	case VHS_READ_LINK_TO_VALUE:
		if (m_currLink)
		{
			m_currLink->setTo((Node*)_u);
			m_state = VHS_READ_LINK_TYPE;
			return true;
		}
		else
		{
			return false;
		}
#endif
	default:
		return readValue(&_u, ATTRIB_UINT);
	}

}


bool VEEHandler::Uint64(uint64_t _u)
{
#ifdef BUILDING
	cout << "Uint64(" << _u << ")" << endl;
#endif

	switch (m_state)
	{
#ifdef INT_LINK
	case VHS_READ_LINK_FROM_VALUE:
		if (m_currLink)
		{
			m_currLink->setFrom((Node*)_u);
			m_state = VHS_READ_LINK_TO;
			return true;
		}
		else
		{
			return false;
		}
	case VHS_READ_LINK_TO_VALUE:
		if (m_currLink)
		{
			m_currLink->setTo((Node*)_u);
			m_state = VHS_READ_LINK_TYPE;
			return true;
		}
		else
		{
			return false;
		}
#endif
	default:
		return readValue(&_u, ATTRIB_UINT64);
	}
}