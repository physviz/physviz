#ifndef _ATTRIB_ARRAY
#define _ATTRIB_ARRAY
#include <vector>

class Attrib;

class AttribArray
{
public:
	AttribArray();
	~AttribArray();

	Attrib* operator[](int place) { return m_attribs[place]; }

	int GetLength() { return (int)m_attribs.size(); }

	void AddAttrib(Attrib* _attrib);

	void Shrink();

	//need to do different verisons
	Attrib* FetchAttrib(const char* attrib);

protected:
	std::vector<Attrib*> m_attribs;
};

#endif