#include "XYZHandler.h"
#include "graph.h"
#include <iostream>
#include <sstream>
#include "node.h"
#include "attrib.h"
#include "AttribArray.h"
#include "link.h"
#include <vector>
#include <map>

XYZHandler::XYZHandler(string _filename) :m_graph(nullptr)
{
	m_graph = new Graph();

	m_filename = _filename;
	m_file.open(m_filename.c_str(), fstream::in);
}

XYZHandler::~XYZHandler()
{
	m_file.close();
}

char* XYZHandler::terminate(const char* _str, unsigned _length)
{
	char* dummy = new char[_length + 1];
	memcpy(dummy, _str, _length);
	dummy[_length] = '\0';
	return dummy;
}

void XYZHandler::parse()
{

	//load the colours
	map<string, XYZColour> colourMap;
	fstream colours;
	colours.open("data/rgb.txt");
	string colourLine;
	while (getline(colours, colourLine))
	{
		if (colourLine[0] != '!')
		{
			string name;
			XYZColour input;
			stringstream data(colourLine);
			data >> input.m_R >> input.m_G >> input.m_B;
			getline(data, name);
			size_t first = name.find_first_not_of(" \t");
			string trimName = name.substr(first, name.size() - first);
			colourMap[trimName] = input;
		}
	}
	colours.close();

	//load xyz.types file
	map<string, XYZType> typeMap;
	fstream types;
	types.open("data/xyz.types");
	int iTypes;
	types >> iTypes;
	types.ignore(80, '\n');
	//ofstream missing;  Quickly can add these to spot missing colours in the rgb.txt
	//missing.open("data/missing.txt");
	for (int i = 0; i < iTypes; i++)
	{
		XYZType iT;
		string typeLine;
		getline(types, typeLine);
		stringstream data(typeLine);
		data >> iT.m_baseName >> iT.m_name >> iT.m_atomicNumber >> iT.m_mass >> iT.m_bondLength >> iT.m_radius;
		string colour;
		getline(data, colour);
		size_t first = colour.find_first_not_of(" \t");
		string trimCol = colour.substr(first, colour.size() - first);
		//if (colourMap.find(trimCol) == colourMap.end())
		//{
		//	missing<< trimCol << endl;
		//}
		XYZColour fetchCol = colourMap[trimCol];
		iT.m_R = fetchCol.m_R;
		iT.m_G = fetchCol.m_G;
		iT.m_B = fetchCol.m_B;
		typeMap[iT.m_name] = iT;
	}
	types.close();
	//missing.close();

	//now load the XYZ file
	int atoms;

	m_file >> atoms;

	m_file.ignore(80, '\n');
	m_file.ignore(80, '\n');//skip comment line

	string axes = "xyz";

	//IF they do, the fourth column is the atom charge, then columns 5-7 are a vector for that atom, so generally showing the atomic vibration.
	for (int i = 0; i < atoms; i++)
	{
		string name;

		string buffer;
		getline(m_file, buffer);

		stringstream data(buffer);

		data >> name;

		float dummy;
		vector<double> values;
		while (data >> dummy)
		{
			values.push_back(dummy);
		}
		data.clear();

		Node* node = new Node;

		node->SetName(terminate(name.c_str(), name.length()));
		node->SetType(terminate("atom", 4));

		AttribArray* AA = new AttribArray();

		for (int j = 0; j < 3; j++)
		{
			Attrib* A = new Attrib;
			A->SetName(terminate(((string)"atom" + axes[j]).c_str(), 5));
			A->SetValue(&values[j], ATTRIB_DOUBLE);
			AA->AddAttrib(A);
		}

		//if we have a 4th term it'll be charge
		if (values.size() > 3)
		{
			Attrib* A = new Attrib;
			A->SetName(terminate("charge", 6));
			A->SetValue(&values[3], ATTRIB_DOUBLE);
			AA->AddAttrib(A);
		}
		//if we have further terms they'll be some form of vector assoicated with the atom
		if (values.size() == 7)
		{
			for (int j = 4; j < 7; j++)
			{
				Attrib* A = new Attrib;
				A->SetName(terminate(((string)"vec" + axes[j - 4]).c_str(), 4));
				A->SetValue(&values[j], ATTRIB_DOUBLE);
				AA->AddAttrib(A);
			}
		}
		XYZType aT = typeMap[node->GetName()];

		Attrib* atomicNumA = new Attrib;
		atomicNumA->SetName(terminate("atomicNum", 9));
		atomicNumA->SetValue(&aT.m_atomicNumber, ATTRIB_INT);
		AA->AddAttrib(atomicNumA);
		Attrib* mass = new Attrib;
		mass->SetName(terminate("mass", 4));
		mass->SetValue(&aT.m_mass, ATTRIB_FLOAT);
		AA->AddAttrib(mass);
		Attrib* BL = new Attrib;
		BL->SetName(terminate("BL", 2));
		BL->SetValue(&aT.m_bondLength, ATTRIB_FLOAT);
		AA->AddAttrib(BL);
		Attrib* radius = new Attrib;
		radius->SetName(terminate("radius", 6));
		radius->SetValue(&aT.m_radius, ATTRIB_FLOAT);
		AA->AddAttrib(radius);
		Attrib* CR = new Attrib;
		CR->SetName(terminate("CR", 2));
		CR->SetValue(&aT.m_R, ATTRIB_INT);
		AA->AddAttrib(CR);
		Attrib* CG = new Attrib;
		CG->SetName(terminate("CG", 2));
		CG->SetValue(&aT.m_G, ATTRIB_INT);
		AA->AddAttrib(CG);
		Attrib* CB = new Attrib;
		CB->SetName(terminate("CB", 2));
		CB->SetValue(&aT.m_B, ATTRIB_INT);
		AA->AddAttrib(CB);

		node->SetAttribs(AA);

		m_graph->AddNode(node);

	}
	//if nmap exists link up based on that
	fstream nmap;
	nmap.open(m_filename + ".nmap", fstream::in);
	if (nmap.good())
	{
		m_graph->ReadNMap(nmap, "bond");
	}
	else
	{
		//else link nodes up based on standard bond lengths
		for (int i = 0; i < atoms; i++)
		{
			float pos[3];
			pos[0] = m_graph->GetNode(i)->FetchAttrib("atomx", 0.0);
			pos[1] = m_graph->GetNode(i)->FetchAttrib("atomy", 0.0);
			pos[2] = m_graph->GetNode(i)->FetchAttrib("atomz", 0.0);

			float bondLength = m_graph->GetNode(i)->FetchAttrib("BL", 0.0);

			for (int j = i + 1; j < atoms; j++)
			{
				float pos2[3];
				pos2[0] = m_graph->GetNode(j)->FetchAttrib("atomx", 0.0);
				pos2[1] = m_graph->GetNode(j)->FetchAttrib("atomy", 0.0);
				pos2[2] = m_graph->GetNode(j)->FetchAttrib("atomz", 0.0);

				float bondLength2 = m_graph->GetNode(j)->FetchAttrib("BL", 0.0);

				float dist2 = pow(pos[0] - pos2[0], 2) + pow(pos[1] - pos2[1], 2) + pow(pos[2] - pos2[2], 2);

				if (fabs(dist2 - bondLength*bondLength) < 1.0 || fabs(dist2 - bondLength2*bondLength2) < 1.0)
				{
					Link* link = new Link();
					link->SetType(terminate("bond", 4));
					link->SetFrom(m_graph->GetNode(i));
					link->SetTo(m_graph->GetNode(j));
					link->SetRecast(true);

					m_graph->AddLink(link);
				}
			}
		}
		m_graph->WriteNMap(m_filename + ".nmap");
	}
}
