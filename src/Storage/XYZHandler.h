#ifndef _XYZHANDLER_H_
#define _XYZHANDLER_H_
#include <fstream>
#include <string>

//#define BUILDING

class Graph;
class Link;
class Node;

using namespace std;

class XYZHandler {

public:

	XYZHandler(string _filename);
	~XYZHandler();

	Graph* myGraph() {
		return m_graph;

	}

	void parse();
	char* terminate(const char* _str, unsigned length);

protected:

	Graph* m_graph;
	fstream m_file;
	string m_filename;
};

struct XYZType
{
	string m_baseName;
	string m_name;
	int m_atomicNumber;
	float m_mass;
	float m_bondLength;
	float m_radius;
	int m_R, m_G, m_B;
};

struct XYZColour
{
	int m_R, m_G, m_B;
};

#endif
