#ifndef _NODEDISPPARAM_H_
#define _NODEDISPPARAM_H_
#include <string>

using namespace std;

//node display parameter contains
enum NDPC
{
	NDPC_NONE = 0,
	NDPC_MAX = 1,
	NDPC_MIN = 2,
	NDPC_SCALE = 4,
	NDPC_DEFAULT = 8,
	NDPC_BASE = 16,
	NDPC_TYPE = 32,
	NDPC_DELTA = 64
};

class NodeDispParam
{
public:
	NodeDispParam();
	~NodeDispParam();

	bool SetValue(string _attrib, float _valF, string _valS);
	void SetValue(NDPC _attrib, float _valF, string _valS);

	float GetValue(NDPC _val);

	void SetType(char* _type) { m_type = _type; }
	void SetType(string _type) { m_type = _type; }
	string GetType() { return m_type; }

	void SetBase(char* _base) { m_base = _base; }
	void SetBase(string _base) { m_base = _base; }
	string GetBase() { return m_base; }

protected:

	string m_type;

	string m_base;

	float m_max, m_min, m_scale, m_default, m_delta;

	NDPC m_contains;
};

#endif