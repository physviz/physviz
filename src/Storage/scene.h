#ifndef _SCENE_H_
#define _SCENE_H_
#include <vector>
#include <string>
class Particle;
class Line;
class Graph;
class DefaultRenderer;

using namespace std;

class Scene
{
public:

	Scene(Graph* _myGraph, DefaultRenderer* _renderer, string _filename);
	Scene(Graph* _myGraph, DefaultRenderer* _renderer, const char* _szFilename, const char* _szPackageName);
	~Scene();

	void Update(float _dt, unsigned int _poll);

	void Reset();

	vector<Line*>* GetLines() { return &m_Lines; }
	vector<Particle*>* GetParticles() { return &m_Particles; }

	string GetFocusLabel();

protected:
	vector<Particle*> m_Particles;
	vector<Line*> m_Lines;

	void SetFocus(unsigned int poll);

	int m_focus;//particle we are currently looking at -1 is nothing -2 if no particles are of interest
	bool m_PhysicsOn;
};

#endif