#ifndef _PARTICLE_H_
#define _PARTICLE_H_
#include <d3d11_1.h>
#include <string>
#include <vector>
#include "PDN.h"
#include <directxmath.h>
#include "..\DirectXTK\Inc\SimpleMath.h"
#define PART_MAX_V 50.0f

struct ParticleData;

using namespace std;
using namespace DirectX;
using namespace SimpleMath;

class Line;

//Particles to be drawn as part of these scene
class Particle
{
public:
	Particle(ParticleData* _inData = nullptr, string _name = "");
	~Particle();

	//update physics of the particle based on forces from lines
	//as well as jiggles and oscilations
	void Tick(float _dt);
	void PreTick(float _dt);
	void PostTick(float _dt);

	//reset particle to initial conditions
	void Reset();

	//set the particle initial conditions to be a copy of the current state
	void SetInitData();

	//Part of the process of deciding to draw the label for this particle
	bool GetText(Vector3 _camPos, Vector3& _pos, float _maxDistance);

	//using my Size[3] and Position[3] set the incoming particle to be inside my zone
	void FixInside(Particle* _in);

	//Setters
	void Set(PDN _type, void* _val);

	//Getters
	void* Get(PDN _type);
	string GetName() { return m_name; }

	void AddTBFLine(Line* _line);
	void ApplyTBF(Line* _one, Line* _two);

protected:
	string m_name; //name of the Particle (for the label)
	ParticleData* m_pcData = nullptr; //current Data
	ParticleData* m_piData = nullptr; //initial Data

	vector<Line*> m_ThreeBodyLines;
};
#endif