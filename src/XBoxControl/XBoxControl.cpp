#include "XBoxController.h"
#include "../DefaultControl/controllers.h"
#include "../DefaultControl/IControllerData.h"
#include "../DefaultRenderer/DefaultRenderer.h"
#include "../Storage/particledata.h"
#include <iostream>
XBoxController* XBoxController::s_controller[4] = { nullptr, nullptr, nullptr, nullptr };

XBoxController::XBoxController(string _type, int _num) :IController(_type)
{
	m_num = _num;
	char dummy[4];
	m_type = m_type + itoa(_num, dummy, 10);
	m_pos = Vector3(1.0f + m_num, 0.0f, 0.0f);
	m_rotation = Vector2(0.0f, 0.0f);
	m_distance = 20.0f;
	m_drag = 0.9f;

	m_numParticles = 1;
	m_particle = new ParticleData();
	m_particle->m_externalOwnership = true; // Protect our particle from the scene unloader
	//as far as the main physics stuff is concerned this is a fixed particle as its controlled from here
	m_particle->m_fixed = true;
	m_particle->m_hide = false;
	m_particle->m_subscreen = m_num;
	m_particle->m_size[0] = 50;
	m_particle->m_size[1] = 50;
}

XBoxController::~XBoxController()
{
	//delete XInput stuff
}

XBoxController* XBoxController::Get(int _num)
{
	//assert on not in range here
	assert(_num >= 0 && _num < 4);
	if (!s_controller[_num])
	{
		s_controller[_num] = new XBoxController("XBox", _num);
	}

	return s_controller[_num];
}

void XBoxController::Init(void* _data)
{
	//init XINPUT stuff
	IControllerData* IDC = (IControllerData*)_data;

	RECT rc;
	GetClientRect(IDC->m_hWnd, &rc);
	m_width = rc.right - rc.left;
	m_height = rc.bottom - rc.top;

	if (m_num == 1)m_width /= 2;
}

Poll_Return XBoxController::Poll(float _dt, bool _isMain)
{
	if ((!_isMain && !m_alwaysTick) || (m_alwaysTick && _isMain))	return 	POLL_RETURN_CONT;	//just continue 

	memcpy(m_particle->m_pos, &m_pos, sizeof(Vector3));

	Vector3 forward = Vector3(-1.0f, 0.0f, 0.0f);
	Vector3 right = Vector3(0.0f, 0.0f, 1.0f);
	Vector3 up = Vector3(0.0f, 1.0f, 0.0f);
	Vector3 dpos = Vector3(0.0f);

	float speed = 100.0f;
	Vector3 push = Vector3(0.0f, 0.0f, 0.0f);
	DWORD dwResult;
	float triggers = 0.0f;
	//for (DWORD i = 0; i< XUSER_MAX_COUNT; i++)
	{
		XINPUT_STATE state;
		ZeroMemory(&state, sizeof(XINPUT_STATE));

		// Simply get the state of the controller from XInput.
		dwResult = XInputGetState(m_num, &state);

		if (dwResult == ERROR_SUCCESS)
		{
			float LX = state.Gamepad.sThumbLX;
			float LY = state.Gamepad.sThumbLY;
			Deadzone(LX, LY);
			float RX = state.Gamepad.sThumbRX;
			float RY = state.Gamepad.sThumbRY;
			Deadzone(RX, RY, false);
			triggers += state.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD ? state.Gamepad.bRightTrigger : 0.0f;
			triggers -= state.Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD ? state.Gamepad.bLeftTrigger : 0.0f;
			triggers *= 10.0f * speed / (float)65534;
			m_rotation.x -= 0.001f * RX;
			//m_rotation.y += 0.1f * RY;
			if (!(Controllers::GetTarget()))
			{
				push.x += speed * LY; //forward
				push.y += speed * triggers;//up/down
				push.z -= speed * LX;//strafe
			}
		}
		else
		{
			// Controller is not connected 
		}
	}

	if (m_rotation.y > XM_PIDIV2 - 0.1f)
	{
		m_rotation.y = XM_PIDIV2 - 0.1f;
	}
	if (m_rotation.y < 0.1f - XM_PIDIV2)
	{
		m_rotation.y = 0.1f - XM_PIDIV2;
	}
	//aim camera 
	m_aim = Vector3::Transform(forward, Matrix::CreateFromYawPitchRoll(m_rotation.x, 0.0f, m_rotation.y));
	Matrix rot = Matrix::CreateRotationY(m_rotation.x);

	m_acc += push.x * Vector3::Transform(forward, rot);//forward
	m_acc += push.y * up; //up & down
	m_acc += push.z * Vector3::Transform(right, rot); //right

	UpdatePos(_dt);

	if (_isMain && (Controllers::GetTarget()))
	{
		m_distance += triggers;
	}

	if (Controllers::GetTarget())
	{
		Vector3 pos = (Vector3)*Controllers::GetTarget();
		m_view = Matrix::CreateLookAt(pos + m_distance * m_aim, pos, up);
	}
	else
	{
		Vector3 look = m_pos + m_aim;
		m_view = Matrix::CreateLookAt(m_pos, look, up);
	}

	return POLL_RETURN_CONT;
}


void XBoxController::Deadzone(float& _LX, float& _LY, bool _isLeft)
{
	//determine how far the controller is pushed
	float magnitude = sqrt(_LX * _LX + _LY * _LY);

	if (fabs(magnitude) < 0.001f)
	{
		//weird new bug if comes in with actually zero 
		//get a division by zero error
		return;
	}
	else
	{
		//determine the direction the controller is pushed
		float normalizedLX = _LX / magnitude;
		float normalizedLY = _LY / magnitude;

		float normalizedMagnitude = 0;

		SHORT INPUT_DEADZONE = _isLeft ? XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE : XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE;

		//check if the controller is outside a circular dead zone
		if (magnitude > INPUT_DEADZONE)
		{
			//clip the magnitude at its expected maximum value
			if (magnitude > 32767) magnitude = 32767;

			//adjust magnitude relative to the end of the dead zone
			magnitude -= INPUT_DEADZONE;

			//optionally normalize the magnitude with respect to its expected range
			//giving a magnitude value of 0.0 to 1.0
			normalizedMagnitude = magnitude / (32767 - INPUT_DEADZONE);
		}
		else //if the controller is in the deadzone zero out the magnitude
		{
			magnitude = 0.0;
			normalizedMagnitude = 0.0;
		}
		_LX = normalizedLX * normalizedMagnitude;
		_LY = normalizedLY * normalizedMagnitude;
		return;
	}
}

Matrix& XBoxController::GetView(string _pass)
{
	int ss = Subscreen(_pass);
	if (ss < 0 || ss>3)
	{
		return m_view;
	}
	else
	{
		return s_controller[ss]->m_view;
	}
}

Vector3 XBoxController::GetPos(string _pass)
{
	int ss = Subscreen(_pass);
	if (ss < 0 || ss>3)
	{
		return m_pos;
	}
	else
	{
		return s_controller[ss]->m_pos;
	}

}

void XBoxController::UpdateRenderPasses(DefaultRenderer* _renderer)
{
	for (int i = 0; i < 4; i++)
	{
		//activate all previous ones
		s_controller[i]->m_alwaysTick = (i <= m_num);
	}

	//add suitable passes based on m_num
	switch (m_num)
	{
	case 0:
		IController::UpdateRenderPasses(_renderer);
		break;
	case 1:
		_renderer->ClearActivePasses();
		_renderer->AddActivePass("BASE_PART_PASS_20");
		_renderer->AddActivePass("BASE_ZONE_PASS_20");
		_renderer->AddActivePass("BASE_LINE_PASS_20");
		_renderer->AddActivePass("BASE_LABEL_PASS_20");
		_renderer->AddActivePass("BASE_PART_PASS_21");
		_renderer->AddActivePass("BASE_ZONE_PASS_21");
		_renderer->AddActivePass("BASE_LINE_PASS_21");
		_renderer->AddActivePass("BASE_LABEL_PASS_21");
		break;
	case 2:
		_renderer->ClearActivePasses();
		_renderer->AddActivePass("BASE_PART_PASS_40");
		_renderer->AddActivePass("BASE_ZONE_PASS_40");
		_renderer->AddActivePass("BASE_LINE_PASS_40");
		_renderer->AddActivePass("BASE_LABEL_PASS_40");
		_renderer->AddActivePass("BASE_PART_PASS_41");
		_renderer->AddActivePass("BASE_ZONE_PASS_41");
		_renderer->AddActivePass("BASE_LINE_PASS_41");
		_renderer->AddActivePass("BASE_LABEL_PASS_41");
		_renderer->AddActivePass("BASE_PART_PASS_42");
		_renderer->AddActivePass("BASE_ZONE_PASS_42");
		_renderer->AddActivePass("BASE_LINE_PASS_42");
		_renderer->AddActivePass("BASE_LABEL_PASS_42");
	case 3:
		_renderer->ClearActivePasses();
		_renderer->AddActivePass("BASE_PART_PASS_40");
		_renderer->AddActivePass("BASE_ZONE_PASS_40");
		_renderer->AddActivePass("BASE_LINE_PASS_40");
		_renderer->AddActivePass("BASE_LABEL_PASS_40");
		_renderer->AddActivePass("BASE_PART_PASS_41");
		_renderer->AddActivePass("BASE_ZONE_PASS_41");
		_renderer->AddActivePass("BASE_LINE_PASS_41");
		_renderer->AddActivePass("BASE_LABEL_PASS_41");
		_renderer->AddActivePass("BASE_PART_PASS_42");
		_renderer->AddActivePass("BASE_ZONE_PASS_42");
		_renderer->AddActivePass("BASE_LINE_PASS_42");
		_renderer->AddActivePass("BASE_LABEL_PASS_42");
		_renderer->AddActivePass("BASE_PART_PASS_43");
		_renderer->AddActivePass("BASE_ZONE_PASS_43");
		_renderer->AddActivePass("BASE_LINE_PASS_43");
		_renderer->AddActivePass("BASE_LABEL_PASS_43");
		break;
	}
}


