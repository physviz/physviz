#ifndef _ADD_XBOX_CONTROL_H_
#define _ADD_XBOX_CONTROL_H_
#include "../XBoxControl/XBoxController.h"

#pragma comment(lib, "xboxcontrol.lib")
#pragma comment(lib, "Xinput9_1_0.lib")

static int addXBoxControl0 = Controllers::Get()->AddController(XBoxController::Get(0));
static int addXBoxControl1 = Controllers::Get()->AddController(XBoxController::Get(1));
static int addXBoxControl2 = Controllers::Get()->AddController(XBoxController::Get(2));
static int addXBoxControl3 = Controllers::Get()->AddController(XBoxController::Get(3));
#endif