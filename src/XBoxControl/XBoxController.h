#ifndef _XBOX_CONTROL_H_
#define _XBOX_CONTROL_H_
#include "..\DefaultControl\IController.h"
#include "..\DirectXTK\Inc\SimpleMath.h"
#include <windows.h>
#include <Xinput.h>

using namespace DirectX;
using namespace SimpleMath;

class XBoxController : public IController
{
public:
	virtual ~XBoxController();

	static XBoxController* Get(int _num = 0);

	//stuff from IController
	virtual void Init(void* _data) override;
	virtual Poll_Return Poll(float _dt, bool _isMain) override;

	virtual void UpdateRenderPasses(DefaultRenderer * _renderer) override;

	virtual Matrix& GetView(string _pass) override;
	virtual Vector3 GetPos(string _pass) override;

private:

	XBoxController(string _type, int _num);//private constructor
	static XBoxController* s_controller[4];

	// Dont forget to declare these two. You want to make sure they
	// are unaccessable otherwise you may accidently get copies of
	// your singleton appearing.
	XBoxController(XBoxController const&);              // Don't Implement as want it to be illegal to do this.
	void operator=(XBoxController const&);

	//XINPUT stuff
	void Deadzone(float& _x, float& _y, bool _isLeft = true);

	Vector2 m_rotation = Vector2(0.0f, 0.0f);

	int m_num = 0;

};
#endif