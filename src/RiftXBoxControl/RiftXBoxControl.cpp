#include "RiftXBoxControl.h"
#include "../DefaultControl/controllers.h"
#include "../DefaultControl/IControllerData.h"
#include "../DefaultRenderer/DefaultRenderer.h"
#include "rift.h"
#include <iostream>
#include "../DirectXRenderer/occulus_texture.h"

#include "../Public/oculus_structs.h"
#include "..\Storage\particledata.h"

RiftXBoxController* RiftXBoxController::s_controller = nullptr;

extern EyeHack g_hack;
extern ovrSession g_session;

RiftXBoxController::RiftXBoxController(string _type) :IController(_type)
{
	m_showControlParticles = false;
	m_pos = Vector3(0.0f, 0.0f, 0.0f);
	m_rotation = Vector2(0.0f, 0.0f);
	m_distance = 20.0f;
	m_drag = 0.9f;

	//set up rift stuff here
	//init OR stuff
	m_pRift = new Rift();

	//add some particles to show where the touch controllers are
	m_numParticles = 2;
	m_particle = new ParticleData[2];
	for (int i = 0; i < 2; i++)
	{
		m_particle[i].m_externalOwnership = true; // Protect our particle from the scene unloader
		//as far as the main physics stuff is concerned this is a fixed particle as its controlled from here
		m_particle[i].m_fixed = true;
		m_particle[i].m_hide = false;
		m_particle[i].m_size[0] = 0.5;
		m_particle[i].m_size[1] = 0.5;
		memcpy(m_particle, &m_pos, sizeof(float) * 3);
	}

}

RiftXBoxController::~RiftXBoxController()
{
	//delete stuff
}



RiftXBoxController* RiftXBoxController::Get()
{
	if (!s_controller)
	{
		s_controller = new RiftXBoxController("RiftXBox");
	}

	return s_controller;
}

void RiftXBoxController::Init(void* _data)
{
	//init stuff
}



Poll_Return RiftXBoxController::Poll(float _dt, bool _isMain)
{
	if (!_isMain)
	{
		m_particle[0].m_hide = true;
		m_particle[1].m_hide = true;
	}
	else
	{
		m_particle[0].m_hide = false;
		m_particle[1].m_hide = false;
	}

	if ((!_isMain && !m_alwaysTick) || (m_alwaysTick && _isMain))	return 	POLL_RETURN_CONT;	//just continue 

	Vector3 forward = Vector3(0.0f, 0.0f, -1.0f);
	Vector3 right = Vector3(-1.0f, 0.0f, 0.0f);
	Vector3 up = Vector3(0.0f, 1.0f, 0.0f);
	Vector3 dpos = Vector3(0.0f);

	float speed = 100.0f;
	Vector3 push = Vector3(0.0f, 0.0f, 0.0f);
	DWORD dwResult;
	float triggers = 0.0f;
	//for (DWORD i = 0; i< XUSER_MAX_COUNT; i++)
	/* {
		XINPUT_STATE state;
		ZeroMemory(&state, sizeof(XINPUT_STATE));

		// Simply get the state of the controller from XInput.
		dwResult = XInputGetState(0, &state);

		if (dwResult == ERROR_SUCCESS)
		{
			float LX = state.Gamepad.sThumbLX;
			float LY = state.Gamepad.sThumbLY;
			Deadzone(LX, LY);
			float RX = state.Gamepad.sThumbRX;
			float RY = state.Gamepad.sThumbRY;
			Deadzone(RX, RY, false);
			triggers += state.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD ? state.Gamepad.bRightTrigger : 0.0f;
			triggers -= state.Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD ? state.Gamepad.bLeftTrigger : 0.0f;
			triggers *= 10.0f * speed / (float)65534;
			m_rotation.x -= 0.05f * RX;
			//m_rotation.y += 0.05f * RY;
			if (!(Controllers::GetTarget()))
			{
				push.x += speed * LY; //forward
				push.y += speed * triggers;//up/down
				push.z -= speed * LX;//strafe
			}
		}
		else
		{
			// Controller is not connected
		}
	}*/


	ovrTrackingState hmdState = ovr_GetTrackingState(g_session, 0.0f, ovrFalse);
	ovrInputState inState;

	if (OVR_SUCCESS(ovr_GetInputState(g_session, ovrControllerType_Touch, &inState)))
	{
		float LX = inState.Thumbstick[ovrHand_Left].x;
		float LY = inState.Thumbstick[ovrHand_Left].y;
		//Deadzone(LX, LY);
		float RX = inState.Thumbstick[ovrHand_Right].x;
		float RY = inState.Thumbstick[ovrHand_Right].y;
		//Deadzone(RX, RY, false);
		triggers = inState.IndexTrigger[ovrHand_Right];
		triggers -= inState.IndexTrigger[ovrHand_Left];
		//triggers += state.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD ? state.Gamepad.bRightTrigger : 0.0f;
		//triggers -= state.Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD ? state.Gamepad.bLeftTrigger : 0.0f;
		//triggers *= 10.0f * speed / (float)65534;
		m_rotation.x -= 0.05f * RX;
		//m_rotation.y += 0.05f * RY;
		if (!(Controllers::GetTarget()))
		{
			push.x += speed * LY; //forward
			push.y += speed * triggers;//up/down
			push.z -= speed * LX;//strafe
		}
	}
	else
	{
		// Controller is not connected 
	}


	//now poll orientation from the rift
	float yawOR = 0.0f, pitchOR = 0.0f, rollOR = 0.0f;
	m_pRift->PollOri(yawOR, pitchOR, rollOR);

	Matrix OROri = Matrix::CreateFromYawPitchRoll(yawOR, pitchOR, rollOR);

	if (m_rotation.y > XM_PIDIV2 - 0.1f)
	{
		m_rotation.y = XM_PIDIV2 - 0.1f;
	}
	if (m_rotation.y < 0.1f - XM_PIDIV2)
	{
		m_rotation.y = 0.1f - XM_PIDIV2;
	}

	//additional rotation from the Rift
	Matrix rot = Matrix::CreateRotationY(yawOR + m_rotation.x);

	m_acc += push.x * Vector3::Transform(forward, rot);//forward
	m_acc += push.y * up; //up & down
	m_acc += push.z * Vector3::Transform(right, rot); //right

	UpdatePos(_dt);
	if (_isMain && (Controllers::GetTarget()))
	{
		m_distance += triggers;
	}

	float x = 0.0f, y = 0.0f, z = 0.0f;
	//m_pRift->PollHeadPosition(x, y, z);
	Vector3 headPos(x, y, z);

	if (Controllers::GetTarget())
	{
		Vector3 pos = (Vector3)*Controllers::GetTarget();
		m_view = Matrix::CreateLookAt(pos + m_distance * m_aim, pos, up);
	}
	else
	{
		Vector3 look = m_pos + m_aim;
		m_view = Matrix::CreateLookAt(m_pos + headPos, look, up);
	}

	if (g_hack.set)
	{
		for (int eye = 0; eye < 2; ++eye)
		{
			//Get the pose information in XM format
			XMVECTOR eyeQuat = XMVectorSet(g_hack.EyeRenderPose[eye].Orientation.x, g_hack.EyeRenderPose[eye].Orientation.y,
				g_hack.EyeRenderPose[eye].Orientation.z, g_hack.EyeRenderPose[eye].Orientation.w);
			XMVECTOR eyePos = XMVectorSet(g_hack.EyeRenderPose[eye].Position.x, g_hack.EyeRenderPose[eye].Position.y, g_hack.EyeRenderPose[eye].Position.z, 0);

			// Get view and projection matrices for the Rift camera
			XMVECTOR CombinedPos = XMVectorAdd(m_pos, XMVector3Rotate(eyePos, XMQuaternionRotationRollPitchYaw(m_rotation.y, m_rotation.x, 0)));
			Camera finalCam(CombinedPos, XMQuaternionMultiply(eyeQuat, XMQuaternionRotationRollPitchYaw(m_rotation.y, m_rotation.x, 0)));
			m_viewEyes[eye] = finalCam.GetViewMatrix();
			ovrMatrix4f p = ovrMatrix4f_Projection(g_hack.eyeRenderDesc[eye].Fov, 0.2f, 1000.0f, ovrProjection_None);
			m_projEyes[eye] = XMMatrixSet(p.M[0][0], p.M[1][0], p.M[2][0], p.M[3][0],
				p.M[0][1], p.M[1][1], p.M[2][1], p.M[3][1],
				p.M[0][2], p.M[1][2], p.M[2][2], p.M[3][2],
				p.M[0][3], p.M[1][3], p.M[2][3], p.M[3][3]);
		}
	}

	Matrix rotX = Matrix::CreateRotationY(m_rotation.x);

	for (int i = 0; i < 2; i++)
	{
		Vector3 handPos = Vector3(hmdState.HandPoses[i].ThePose.Position.x, hmdState.HandPoses[i].ThePose.Position.y, hmdState.HandPoses[i].ThePose.Position.z);
		handPos = Vector3::Transform(handPos, rotX);
		handPos += m_pos;
		memcpy(&(m_particle[i].m_pos), &handPos, sizeof(Vector3));
	}

	XMFLOAT4 partRot = XMFLOAT4(hmdState.HandPoses[ovrHand_Left].ThePose.Orientation.x,
		hmdState.HandPoses[ovrHand_Left].ThePose.Orientation.y,
		hmdState.HandPoses[ovrHand_Left].ThePose.Orientation.z,
		hmdState.HandPoses[ovrHand_Left].ThePose.Orientation.w);

	ovrSessionStatus sessionStatus;
	ovr_GetSessionStatus(g_session, &sessionStatus);
	if (sessionStatus.ShouldQuit)
	{
		return POLL_RETURN_OFF;
	}
	else
	{

		return POLL_RETURN_CONT;
	}
}

void RiftXBoxController::Deadzone(float& _LX, float& _LY, bool _isLeft)
{
	//determine how far the controller is pushed
	float magnitude = sqrt(_LX * _LX + _LY * _LY);

	//determine the direction the controller is pushed
	float normalizedLX = _LX / magnitude;
	float normalizedLY = _LY / magnitude;

	float normalizedMagnitude = 0;

	SHORT INPUT_DEADZONE = _isLeft ? XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE : XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE;

	//check if the controller is outside a circular dead zone
	if (magnitude > INPUT_DEADZONE)
	{
		//clip the magnitude at its expected maximum value
		if (magnitude > 32767) magnitude = 32767;

		//adjust magnitude relative to the end of the dead zone
		magnitude -= INPUT_DEADZONE;

		//optionally normalize the magnitude with respect to its expected range
		//giving a magnitude value of 0.0 to 1.0
		normalizedMagnitude = magnitude / (32767 - INPUT_DEADZONE);
	}
	else //if the controller is in the Deadzone zero out the magnitude
	{
		magnitude = 0.0;
		normalizedMagnitude = 0.0;
	}
	_LX = normalizedLX * normalizedMagnitude;
	_LY = normalizedLY * normalizedMagnitude;
	return;
}

void* RiftXBoxController::GetProj(string _pass)
{
	static Matrix s_giveMatrix;
	int ss = Subscreen(_pass);

	s_giveMatrix = m_projEyes[ss];

	return  &s_giveMatrix;
}

Matrix& RiftXBoxController::GetView(string _pass)
{
	static Matrix giveView;
	giveView = s_controller->m_view;
	int ss = Subscreen(_pass);

	giveView = m_viewEyes[ss];

	return 	giveView;

	//apply shift due to SS
	m_pRift->ApplyViewShift(giveView, ss);

	return giveView;
}

bool RiftXBoxController::IsHMD() {
	return m_pRift->m_Initialized;
}

void* RiftXBoxController::GetDataObject(const char* _desc, void* _extraData) {
	string str = _desc;
	if (str == "session") {
		return m_pRift->GetSession();
	}
	if (str == "init") {
		return &m_pRift->m_Initialized;
	}

	if (str == "renderdata") {
		ovrRendererDetails_s* details = new ovrRendererDetails_s();
		details->m_pSession = m_pRift->GetSession();
		details->m_eyeHeight[0] = details->m_eyeHeight[1] = m_pRift->GetHMDDesc().Resolution.h;
		details->m_eyeWidth[0] = details->m_eyeWidth[1] = m_pRift->GetHMDDesc().Resolution.w;
		return details;
	}

	return nullptr;
}

void RiftXBoxController::SendDataObject(const char* _desc, void* _data) {

}

void RiftXBoxController::UpdateRenderPasses(DefaultRenderer* _renderer)
{
	_renderer->ClearActivePasses();
	_renderer->AddActivePass("RIFT_PRE_PASS");
	_renderer->AddActivePass("RIFT_PART_PASS_20");
	_renderer->AddActivePass("RIFT_ZONE_PASS_20");
	_renderer->AddActivePass("RIFT_LINE_PASS_20");
	_renderer->AddActivePass("RIFT_LABEL_PASS_20");
	_renderer->AddActivePass("RIFT_PART_PASS_21");
	_renderer->AddActivePass("RIFT_ZONE_PASS_21");
	_renderer->AddActivePass("RIFT_LINE_PASS_21");
	_renderer->AddActivePass("RIFT_LABEL_PASS_21");
	_renderer->AddActivePass("RIFT_POST_PASS");
}