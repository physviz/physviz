#ifndef _RIFT_XBOX_CONTROL_H_
#define _RIFT_XBOX_CONTROL_H_
#include "..\DefaultControl\IController.h"
#include "..\DirectXTK\Inc\SimpleMath.h"
#include <windows.h>
#include <Xinput.h>

using namespace DirectX;
using namespace SimpleMath;

class Rift;

//REALLY OUGHT TO INHERIT MORE FROM XBOX CONTROLLERS

struct Camera
{
	XMVECTOR Pos;
	XMVECTOR Rot;
	Camera() {};
	Camera(XMVECTOR * _pos, XMVECTOR * _rot) : Pos(*_pos), Rot(*_rot) {};
	Camera(const XMVECTOR & _pos, const XMVECTOR & _rot) : Pos(_pos), Rot(_rot) {};
	XMMATRIX GetViewMatrix()
	{
		XMVECTOR forward = XMVector3Rotate(XMVectorSet(0, 0, -1, 0), Rot);
		return(XMMatrixLookAtRH(Pos, XMVectorAdd(Pos, forward), XMVector3Rotate(XMVectorSet(0, 1, 0, 0), Rot)));
	}

	static void* operator new(std::size_t size)
	{
		UNREFERENCED_PARAMETER(size);
		return _aligned_malloc(sizeof(Camera), __alignof(Camera));
	}

	static void operator delete(void* p)
	{
		_aligned_free(p);
	}
};


class RiftXBoxController : public IController
{
public:
	virtual ~RiftXBoxController();

	static RiftXBoxController* Get();

	//stuff from IController
	virtual void Init(void* _data) override;
	virtual Poll_Return Poll(float dt, bool isMain) override;

	virtual void UpdateRenderPasses(DefaultRenderer * _renderer) override;

	virtual Matrix& GetView(string _pass) override;

	void* GetProj(string _pass);

	virtual bool IsHMD() override;


	virtual void* GetDataObject(const char* _desc, void* _extraData) override;
	virtual void  SendDataObject(const char* _desc, void* _data) override;

private:

	RiftXBoxController(string _type);//private constructor
	static RiftXBoxController* s_controller;

	// Dont forget to declare these two. You want to make sure they
	// are unaccessable otherwise you may accidently get copies of
	// your singleton appearing.
	RiftXBoxController(RiftXBoxController const&);              // Don't Implement as want it to be illegal to do this.
	void operator=(RiftXBoxController const&);

protected:
	//XINPUT stuff
	void Deadzone(float& _x, float& _y, bool _isLeft = true);

	Vector2 m_rotation;

	Rift* m_pRift;

	XMMATRIX m_projEyes[2];
	XMMATRIX m_viewEyes[2];
};
#endif