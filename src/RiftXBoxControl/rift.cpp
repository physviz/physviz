#include "rift.h"
#include "Extras/OVR_Math.h"
#include "..\DirectXTK\Inc\SimpleMath.h"
#include <iostream>

// using namespace OVR;
using namespace std;

//desperate hack to get this to render pass and here
ovrSession g_session = 0;
ovrGraphicsLuid g_gluid;


ovrSession Rift::GetSession() {
	return g_session;
}

ovrGraphicsLuid Rift::GetGLUID() {
	return g_gluid;
}

Rift::Rift() :m_prevYaw(0.0f)
{
	//ovrInitParams initParams = { ovrInit_RequestVersion | ovrInit_FocusAware, OVR_MINOR_VERSION, NULL, 0, 0 };
	ovrInitParams initParams = {
	 0,
	 0, nullptr, 0, 0, OVR_ON64("") };
	ovrResult result = ovr_Initialize(&initParams);
	if (OVR_FAILURE(result)) {
		Failure();
		return;
	}

	result = ovr_Create(&g_session, &g_gluid);
	if (OVR_FAILURE(result)) {
		ovr_Shutdown();
		Failure();
		return;
	}

	m_hmd = ovr_GetHmdDesc(g_session);
	ovrSizei resolution = m_hmd.Resolution;
	ovrFovPort fov = m_hmd.DefaultEyeFov[0];
	OVR::FovPort port = fov;

	// Compute Aspect Ratio. Stereo mode cuts width in half.
	//float aspectRatio = 0.5f * float(1600) / float(900);// float(info.HResolution) / float(info.VResolution);
	float aspectRatio = 0.5f * float(resolution.w) / float(resolution.h);// float(info.HResolution) / float(info.VResolution);
	// Compute Vertical FOV based on distance.
	//float halfScreenDistance = ( VSize / 2);
	//float yfov = 2.0f * atan(halfScreenDistance / info.EyeToScreenDistance);
	float yfov = port.GetVerticalFovDegrees();
	// Post-projection viewport coordinates range from (-1.0, 1.0), with the
	// center of the left viewport falling at (1/4) of horizontal screen size.
	// We need to shift this projection center to match with the lens center.
	// We compute this shift in physical units (meters) to correct
	// for different screen sizes and then rescale to viewport coordinates.
	//float viewCenter = info.HScreenSize * 0.25f;
	//float eyeProjectionShift = viewCenter - info.LensSeparationDistance*0.5f;

	float fudgeFac = 2.0f;

	//float projectionCenterOffset = fudgeFac * 4.0f * eyeProjectionShift / info.HScreenSize;
	float projectionCenterOffset = 0.151976421f; // Stolen this number from Unreal 4...
	// Projection matrix for the "center eye", which the left/right matrices are based on.

	DirectX::SimpleMath::Matrix proj = DirectX::SimpleMath::Matrix::CreatePerspectiveFieldOfView(yfov, aspectRatio, 0.01f, 10000.0f);
	DirectX::SimpleMath::Matrix eyeShift = DirectX::SimpleMath::Matrix::CreateTranslation(projectionCenterOffset, 0, 0);
	m_proj[0] = DirectX::SimpleMath::Matrix(proj * eyeShift);
	eyeShift = DirectX::SimpleMath::Matrix::CreateTranslation(-projectionCenterOffset, 0, 0);
	m_proj[1] = DirectX::SimpleMath::Matrix(proj * eyeShift);

	//float halfIPD = fudgeFac * info.InterpupillaryDistance * 0.5f;
	//m_viewShift[0] = DirectX::SimpleMath::Matrix::CreateTranslation( halfIPD, 0, 0);
	//m_viewShift[1] = DirectX::SimpleMath::Matrix::CreateTranslation(-halfIPD, 0, 0);
	m_viewShift[0] = m_viewShift[1] = DirectX::SimpleMath::Matrix::Identity;

	m_Initialized = true;
}

Rift::~Rift()
{
	ovr_Destroy(g_session);
	ovr_Shutdown();
}

void Rift::PollHeadPosition(float & _x, float & _y, float & _z) {
	if (m_Initialized)
	{
		ovrTrackingState state = ovr_GetTrackingState(g_session, 0.0f, ovrFalse);
		_x = state.HeadPose.ThePose.Position.x;
		_y = state.HeadPose.ThePose.Position.y;
		_z = state.HeadPose.ThePose.Position.z;
	}
	else
	{
		_x = _y = _z = 0.0f;
	}
}

void Rift::PollOri(float& _yaw, float& _pitch, float& _roll)
{
	if (m_Initialized)
	{
		ovrTrackingState state = ovr_GetTrackingState(g_session, 0.0f, ovrFalse);
		OVR::Quatf facing = state.HeadPose.ThePose.Orientation;
		facing.GetEulerAngles<OVR::Axis_Y, OVR::Axis_X, OVR::Axis_Z>(&_yaw, &_pitch, &_roll);
	}
	else
	{
		_yaw = _pitch = _roll = 0.0f;
	}
}

void Rift::Failure() {
	m_proj[0] = m_proj[1] = DirectX::SimpleMath::Matrix::Identity;//= NULL;
	m_viewShift[0] = m_viewShift[1] = DirectX::SimpleMath::Matrix::Identity;
}
