#ifndef _RIFT_H_
#define _RIFT_H_
#include "OVR_CAPI.h"
#include <d3d11.h>
#include "..\DirectXTK\Inc\SimpleMath.h"	

class Rift
{
public:
	Rift();
	~Rift();

	void PollHeadPosition(float& _x, float& _y, float& _z);
	void PollOri(float& _yaw, float& _pitch, float& _roll);
	void GetProj(DirectX::SimpleMath::Matrix& _inProj, int _left) { _inProj = m_proj[_left]; }
	void ApplyViewShift(DirectX::SimpleMath::Matrix& _inView, int _left) { _inView = _inView * m_viewShift[_left]; }

	ovrSession GetSession();

	ovrHmdDesc GetHMDDesc() { return m_hmd; }

	ovrGraphicsLuid GetGLUID();

	bool m_Initialized = false;

protected:

	void Failure();

	ovrHmdDesc m_hmd;

	float m_prevYaw;
	DirectX::SimpleMath::Matrix m_proj[2];
	DirectX::SimpleMath::Matrix m_viewShift[2];

};


#endif