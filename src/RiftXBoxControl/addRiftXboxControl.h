#ifndef _ADD_RIFT_XBOX_CONTROL_H_
#define _ADD_RIFT_XBOX_CONTROL_H_
#include "../RiftXBoxControl/RiftXBoxControl.h"

#pragma comment(lib, "Xinput9_1_0.lib")
#pragma comment(lib, "RiftXBoxControl.lib")
#if DEBUG
#pragma comment(lib, "../Debug/libovr.lib")
#else
#pragma comment(lib, "../Release/libovr.lib")
#endif

static int addRiftXBoxControl = Controllers::Get()->AddController(RiftXBoxController::Get()); //add Rift & XBox controller
#endif