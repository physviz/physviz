#ifndef _ADD_LEAP_CONTROL_H_
#define _ADD_LEAP_CONTROL_H_
#include "../LeapControl/LeapControl.h"

#pragma comment(lib, "LeapControl.lib")

static int addLeapControl = Controllers::Get()->AddController(LeapController::get()); //add the keyboard & mouse controller
#endif