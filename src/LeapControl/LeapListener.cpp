#include "LeapListener.h"

using Leap::Gesture;

void LeapListener::onConnect(const Leap::Controller& controller) 
{
	controller.enableGesture(Gesture::TYPE_CIRCLE);
	controller.enableGesture(Gesture::TYPE_KEY_TAP);
	controller.enableGesture(Gesture::TYPE_SCREEN_TAP);
	controller.enableGesture(Gesture::TYPE_SWIPE);
}

void LeapListener::onFrame(const Leap::Controller& controller)
{

}