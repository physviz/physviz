#ifndef _LEAP_LISTENER_H_
#define _LEAP_LISTENER_H_
#include "include/leap.h"

class LeapListener : public Leap::Listener 
{
public:
	virtual void onConnect(const Leap::Controller&);
	virtual void onFrame(const Leap::Controller&);
private:
};
#endif