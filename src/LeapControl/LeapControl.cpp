#include "LeapControl.h"

LeapController* LeapController::s_controller = nullptr;

LeapController::LeapController(string _type) :IController(_type)
{
	m_rotation = Vector2(0.0f, 0.0f);
	distance = 20.0f;
	m_drag = 0.9f;

//set up leap stuff
	// Have the sample listener receive events from the controller
	m_controller.addListener(m_listener);
}

LeapController::~LeapController()
{
	// Remove the leap  listener when done
	m_controller.removeListener(m_listener);
}


LeapController* LeapController::get()
{
	if (!s_controller)
	{
		s_controller = new LeapController("Leap");
	}

	return s_controller;
}

void LeapController::init(void* data)
{
	//init stuff
}

Poll_Return LeapController::poll(float dt, bool isMain)
{
	if ((!isMain && !alwaysTick) || (alwaysTick && isMain))	return 	POLL_RETURN_CONT;	//just continue 
}