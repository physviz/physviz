#ifndef _LEAP_CONTROL_H_
#define _LEAP_CONTROL_H_
#include "..\DefaultControl\IController.h"
#include "..\DirectXTK\Inc\SimpleMath.h"
#include "include\leap.h"
#include "LeapListener.h"

using namespace DirectX;
using namespace SimpleMath;

class LeapController : public IController
{
public:
	virtual ~LeapController();

	static LeapController* get();

	//stuff from IController
	virtual void init(void* data);
	virtual Poll_Return poll(float dt, bool isMain);

private:
	LeapController(string _type);//private and empty constructor
	static LeapController* s_controller;

	// Dont forget to declare these two. You want to make sure they
	// are unaccessable otherwise you may accidently get copies of
	// your singleton appearing.
	LeapController(LeapController const&);              // Don't Implement as want it to be illegale to do this.
	void operator=(LeapController const&);			// ditto

	// Create a sample listener and controller
	LeapListener m_listener;
	Leap::Controller m_controller;

	Vector2 m_rotation;
};
#endif 