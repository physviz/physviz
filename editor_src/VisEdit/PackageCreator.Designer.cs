﻿namespace VisEdit {
	partial class PackageCreator {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PackageCreator));
			this.extraAssets = new System.Windows.Forms.ListBox();
			this.textureList = new System.Windows.Forms.ListBox();
			this.exportButton = new System.Windows.Forms.Button();
			this.addExtraAsset = new System.Windows.Forms.Button();
			this.removeExtraAsset = new System.Windows.Forms.Button();
			this.removeTextureButton = new System.Windows.Forms.Button();
			this.addTextureButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.importerBox = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.useOpenProject = new System.Windows.Forms.CheckBox();
			this.datasetButton = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.datasetBox = new System.Windows.Forms.TextBox();
			this.displayButton = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.displayBox = new System.Windows.Forms.TextBox();
			this.importerButton = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.openDatasetDialog = new System.Windows.Forms.OpenFileDialog();
			this.openDispDialog = new System.Windows.Forms.OpenFileDialog();
			this.addTextureDialog = new System.Windows.Forms.OpenFileDialog();
			this.addDLLDialog = new System.Windows.Forms.OpenFileDialog();
			this.addOtherAssets = new System.Windows.Forms.OpenFileDialog();
			this.savePackageDialog = new System.Windows.Forms.SaveFileDialog();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// extraAssets
			// 
			this.extraAssets.FormattingEnabled = true;
			this.extraAssets.Location = new System.Drawing.Point(9, 324);
			this.extraAssets.Name = "extraAssets";
			this.extraAssets.Size = new System.Drawing.Size(285, 95);
			this.extraAssets.TabIndex = 0;
			// 
			// textureList
			// 
			this.textureList.FormattingEnabled = true;
			this.textureList.Location = new System.Drawing.Point(9, 210);
			this.textureList.Name = "textureList";
			this.textureList.Size = new System.Drawing.Size(285, 95);
			this.textureList.TabIndex = 1;
			// 
			// exportButton
			// 
			this.exportButton.Location = new System.Drawing.Point(264, 444);
			this.exportButton.Name = "exportButton";
			this.exportButton.Size = new System.Drawing.Size(75, 23);
			this.exportButton.TabIndex = 2;
			this.exportButton.Text = "Export...";
			this.exportButton.UseVisualStyleBackColor = true;
			this.exportButton.Click += new System.EventHandler(this.exportButton_Click);
			// 
			// addExtraAsset
			// 
			this.addExtraAsset.Location = new System.Drawing.Point(297, 323);
			this.addExtraAsset.Name = "addExtraAsset";
			this.addExtraAsset.Size = new System.Drawing.Size(23, 23);
			this.addExtraAsset.TabIndex = 3;
			this.addExtraAsset.Text = "+";
			this.addExtraAsset.UseVisualStyleBackColor = true;
			this.addExtraAsset.Click += new System.EventHandler(this.addExtraAsset_Click);
			// 
			// removeExtraAsset
			// 
			this.removeExtraAsset.Location = new System.Drawing.Point(297, 347);
			this.removeExtraAsset.Name = "removeExtraAsset";
			this.removeExtraAsset.Size = new System.Drawing.Size(23, 23);
			this.removeExtraAsset.TabIndex = 4;
			this.removeExtraAsset.Text = "-";
			this.removeExtraAsset.UseVisualStyleBackColor = true;
			this.removeExtraAsset.Click += new System.EventHandler(this.removeExtraAsset_Click);
			// 
			// removeTextureButton
			// 
			this.removeTextureButton.Location = new System.Drawing.Point(297, 233);
			this.removeTextureButton.Name = "removeTextureButton";
			this.removeTextureButton.Size = new System.Drawing.Size(23, 23);
			this.removeTextureButton.TabIndex = 6;
			this.removeTextureButton.Text = "-";
			this.removeTextureButton.UseVisualStyleBackColor = true;
			this.removeTextureButton.Click += new System.EventHandler(this.removeTextureButton_Click);
			// 
			// addTextureButton
			// 
			this.addTextureButton.Location = new System.Drawing.Point(297, 209);
			this.addTextureButton.Name = "addTextureButton";
			this.addTextureButton.Size = new System.Drawing.Size(23, 23);
			this.addTextureButton.TabIndex = 5;
			this.addTextureButton.Text = "+";
			this.addTextureButton.UseVisualStyleBackColor = true;
			this.addTextureButton.Click += new System.EventHandler(this.addTextureButton_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 308);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(90, 13);
			this.label1.TabIndex = 7;
			this.label1.Text = "Additional Assets:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 194);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(51, 13);
			this.label2.TabIndex = 8;
			this.label2.Text = "Textures:";
			// 
			// importerBox
			// 
			this.importerBox.Location = new System.Drawing.Point(9, 160);
			this.importerBox.Name = "importerBox";
			this.importerBox.Size = new System.Drawing.Size(285, 20);
			this.importerBox.TabIndex = 9;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.useOpenProject);
			this.groupBox1.Controls.Add(this.datasetButton);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.datasetBox);
			this.groupBox1.Controls.Add(this.displayButton);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.displayBox);
			this.groupBox1.Controls.Add(this.importerButton);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.importerBox);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.addTextureButton);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.extraAssets);
			this.groupBox1.Controls.Add(this.removeTextureButton);
			this.groupBox1.Controls.Add(this.textureList);
			this.groupBox1.Controls.Add(this.removeExtraAsset);
			this.groupBox1.Controls.Add(this.addExtraAsset);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(327, 426);
			this.groupBox1.TabIndex = 10;
			this.groupBox1.TabStop = false;
			// 
			// useOpenProject
			// 
			this.useOpenProject.AutoSize = true;
			this.useOpenProject.Location = new System.Drawing.Point(6, 19);
			this.useOpenProject.Name = "useOpenProject";
			this.useOpenProject.Size = new System.Drawing.Size(110, 17);
			this.useOpenProject.TabIndex = 18;
			this.useOpenProject.Text = "Use Open Project";
			this.useOpenProject.UseVisualStyleBackColor = true;
			this.useOpenProject.CheckedChanged += new System.EventHandler(this.useOpenProject_CheckedChanged);
			// 
			// datasetButton
			// 
			this.datasetButton.Location = new System.Drawing.Point(297, 69);
			this.datasetButton.Name = "datasetButton";
			this.datasetButton.Size = new System.Drawing.Size(23, 22);
			this.datasetButton.TabIndex = 17;
			this.datasetButton.Text = "...";
			this.datasetButton.UseVisualStyleBackColor = true;
			this.datasetButton.Click += new System.EventHandler(this.datasetButton_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 54);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(47, 13);
			this.label5.TabIndex = 16;
			this.label5.Text = "Dataset:";
			// 
			// datasetBox
			// 
			this.datasetBox.Location = new System.Drawing.Point(9, 70);
			this.datasetBox.Name = "datasetBox";
			this.datasetBox.Size = new System.Drawing.Size(285, 20);
			this.datasetBox.TabIndex = 15;
			// 
			// displayButton
			// 
			this.displayButton.Location = new System.Drawing.Point(297, 114);
			this.displayButton.Name = "displayButton";
			this.displayButton.Size = new System.Drawing.Size(23, 22);
			this.displayButton.TabIndex = 14;
			this.displayButton.Text = "...";
			this.displayButton.UseVisualStyleBackColor = true;
			this.displayButton.Click += new System.EventHandler(this.displayButton_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 99);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(63, 13);
			this.label4.TabIndex = 13;
			this.label4.Text = "Display File:";
			// 
			// displayBox
			// 
			this.displayBox.Location = new System.Drawing.Point(9, 115);
			this.displayBox.Name = "displayBox";
			this.displayBox.Size = new System.Drawing.Size(285, 20);
			this.displayBox.TabIndex = 12;
			// 
			// importerButton
			// 
			this.importerButton.Location = new System.Drawing.Point(297, 159);
			this.importerButton.Name = "importerButton";
			this.importerButton.Size = new System.Drawing.Size(23, 22);
			this.importerButton.TabIndex = 11;
			this.importerButton.Text = "...";
			this.importerButton.UseVisualStyleBackColor = true;
			this.importerButton.Click += new System.EventHandler(this.importerButton_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 144);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(82, 13);
			this.label3.TabIndex = 10;
			this.label3.Text = "Importer Library:";
			// 
			// openDatasetDialog
			// 
			this.openDatasetDialog.Title = "Open Dataset...";
			// 
			// openDispDialog
			// 
			this.openDispDialog.Filter = "PhysVis Display File|*.disp.json";
			this.openDispDialog.Title = "Open Display File...";
			// 
			// addTextureDialog
			// 
			this.addTextureDialog.Filter = resources.GetString("addTextureDialog.Filter");
			this.addTextureDialog.FilterIndex = 10;
			this.addTextureDialog.Multiselect = true;
			this.addTextureDialog.Title = "Add Textures...";
			// 
			// addDLLDialog
			// 
			this.addDLLDialog.Filter = "PhysVis Importer DLLs|*.dll";
			this.addDLLDialog.Title = "Add Importer Library...";
			// 
			// addOtherAssets
			// 
			this.addOtherAssets.Multiselect = true;
			this.addOtherAssets.Title = "Add Misc Assets...";
			// 
			// savePackageDialog
			// 
			this.savePackageDialog.Filter = "PhysVis Packages|*.pvis";
			// 
			// PackageCreator
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(350, 474);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.exportButton);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "PackageCreator";
			this.Text = "Package Creator";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox extraAssets;
		private System.Windows.Forms.ListBox textureList;
		private System.Windows.Forms.Button exportButton;
		private System.Windows.Forms.Button addExtraAsset;
		private System.Windows.Forms.Button removeExtraAsset;
		private System.Windows.Forms.Button removeTextureButton;
		private System.Windows.Forms.Button addTextureButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox importerBox;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox useOpenProject;
		private System.Windows.Forms.Button datasetButton;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox datasetBox;
		private System.Windows.Forms.Button displayButton;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox displayBox;
		private System.Windows.Forms.Button importerButton;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.OpenFileDialog openDatasetDialog;
		private System.Windows.Forms.OpenFileDialog openDispDialog;
		private System.Windows.Forms.OpenFileDialog addTextureDialog;
		private System.Windows.Forms.OpenFileDialog addDLLDialog;
		private System.Windows.Forms.OpenFileDialog addOtherAssets;
		private System.Windows.Forms.SaveFileDialog savePackageDialog;
	}
}