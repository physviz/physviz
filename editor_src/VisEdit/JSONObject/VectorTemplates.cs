public static partial class JSONTemplates {

	/*
	 * Vector2
	 */
	public static Vector2 ToVector2(JSONObject obj) {
		float x = obj["x"] ? obj["x"].f : 0;
		float y = obj["y"] ? obj["y"].f : 0;
		return new Vector2(x, y);
	}
	public static JSONObject FromVector2(Vector2 v) {
		JSONObject vdata = JSONObject.obj;
		if(v.x != 0)	vdata.AddField("x", v.x);
		if(v.y != 0)	vdata.AddField("y", v.y);
		return vdata;
	}
	/*
	 * Vector3
	 */
	public static JSONObject FromVector3(Vector3 v) {
		JSONObject vdata = JSONObject.obj;
		if(v.x != 0)	vdata.AddField("x", v.x);
		if(v.y != 0)	vdata.AddField("y", v.y);
		if(v.z != 0)	vdata.AddField("z", v.z);
		return vdata;
	}
	public static Vector3 ToVector3(JSONObject obj) {
		float x = obj["x"] ? obj["x"].f : 0;
		float y = obj["y"] ? obj["y"].f : 0;
		float z = obj["z"] ? obj["z"].f : 0;
		return new Vector3(x, y, z);
	}
	/*
	 * Vector4
	 */
	public static JSONObject FromVector4(Vector4 v) {
		JSONObject vdata = JSONObject.obj;
		if(v.x != 0)	vdata.AddField("x", v.x);
		if(v.y != 0)	vdata.AddField("y", v.y);
		if(v.z != 0)	vdata.AddField("z", v.z);
		if(v.w != 0)	vdata.AddField("w", v.w);
		return vdata;
	}
	public static Vector4 ToVector4(JSONObject obj) {
		float x = obj["x"] ? obj["x"].f : 0;
		float y = obj["y"] ? obj["y"].f : 0;
		float z = obj["z"] ? obj["z"].f : 0;
		float w = obj["w"] ? obj["w"].f : 0;
		return new Vector4(x, y, z, w);
	}
}
