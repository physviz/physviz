﻿namespace VisEdit {
	partial class VisEdit_Main {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
            System.Windows.Forms.Button disp_RemoveNodeAttr;
            System.Windows.Forms.Button disp_nodeTypeAddButton;
            System.Windows.Forms.Button disp_nodeTypeRemoveButton;
            System.Windows.Forms.Button disp_linkTypeRemoveButton;
            System.Windows.Forms.Button disp_linkTypeAddButton;
            System.Windows.Forms.Button removeNodeButton;
            System.Windows.Forms.Button newNodeButton;
            System.Windows.Forms.Button link_removeLinkButton;
            System.Windows.Forms.Button link_newLinkButton;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisEdit_Main));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.nodePage = new System.Windows.Forms.TabPage();
            this.newNodeBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dropdownNodeType = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.editLinkButton = new System.Windows.Forms.Button();
            this.dropdownLinkTypes = new System.Windows.Forms.ComboBox();
            this.linkToGoButton = new System.Windows.Forms.Button();
            this.linkFromGoButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.linkFromTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.linkToTextBox = new System.Windows.Forms.TextBox();
            this.linkList = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.attributeList = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.attributeValTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nodeNameTextbox = new System.Windows.Forms.TextBox();
            this.nodeList = new System.Windows.Forms.ListBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.link_toBox = new System.Windows.Forms.ComboBox();
            this.link_fromBox = new System.Windows.Forms.ComboBox();
            this.link_typeBox = new System.Windows.Forms.ComboBox();
            this.link_fromButton = new System.Windows.Forms.Button();
            this.link_toButton = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.link_linkList = new System.Windows.Forms.ListBox();
            this.dispPage = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.disp_linkTypeAddBox = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.disp_linkTypeBox = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.disp_linkForce = new System.Windows.Forms.CheckBox();
            this.disp_linkVisible = new System.Windows.Forms.CheckBox();
            this.disp_linkColourBox = new System.Windows.Forms.PictureBox();
            this.disp_linkColourButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.disp_linkStrengthBox = new System.Windows.Forms.TextBox();
            this.disp_linkBaseLengthBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.disp_linkPowerBox = new System.Windows.Forms.TextBox();
            this.disp_LinkTypeList = new System.Windows.Forms.ListBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.DisplayNameLabel = new System.Windows.Forms.Label();
            this.DisplayName = new System.Windows.Forms.TextBox();
            this.disp_nodeTypeAddBox = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.disp_NewAttrib = new System.Windows.Forms.ComboBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.disp_nodeStringPanel = new System.Windows.Forms.Panel();
            this.disp_nodeDefaultString = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.disp_nodeBoolPanel = new System.Windows.Forms.Panel();
            this.disp_nodeDefaultBool = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.disp_nodeType = new System.Windows.Forms.TextBox();
            this.disp_nodeFloatPanel = new System.Windows.Forms.Panel();
            this.disp_nodeMin = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.disp_nodeMax = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.disp_nodeDelta = new System.Windows.Forms.TextBox();
            this.disp_nodeScale = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.disp_nodeDefault = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.disp_nodeAttrib = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.disp_AddNodeAttr = new System.Windows.Forms.Button();
            this.disp_nodeAttribsList = new System.Windows.Forms.ListBox();
            this.disp_NodeTypeList = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visEdit001aToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.disp_ColourPicker = new System.Windows.Forms.ColorDialog();
            this.savePackageDialog = new System.Windows.Forms.SaveFileDialog();
            this.texturePickDialog = new System.Windows.Forms.OpenFileDialog();
            this.dllPickerDialog = new System.Windows.Forms.OpenFileDialog();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            disp_RemoveNodeAttr = new System.Windows.Forms.Button();
            disp_nodeTypeAddButton = new System.Windows.Forms.Button();
            disp_nodeTypeRemoveButton = new System.Windows.Forms.Button();
            disp_linkTypeRemoveButton = new System.Windows.Forms.Button();
            disp_linkTypeAddButton = new System.Windows.Forms.Button();
            removeNodeButton = new System.Windows.Forms.Button();
            newNodeButton = new System.Windows.Forms.Button();
            link_removeLinkButton = new System.Windows.Forms.Button();
            link_newLinkButton = new System.Windows.Forms.Button();
            this.tabControl.SuspendLayout();
            this.nodePage.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.dispPage.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.disp_linkColourBox)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.disp_nodeStringPanel.SuspendLayout();
            this.disp_nodeBoolPanel.SuspendLayout();
            this.disp_nodeFloatPanel.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // disp_RemoveNodeAttr
            // 
            disp_RemoveNodeAttr.Location = new System.Drawing.Point(250, 222);
            disp_RemoveNodeAttr.Margin = new System.Windows.Forms.Padding(4);
            disp_RemoveNodeAttr.Name = "disp_RemoveNodeAttr";
            disp_RemoveNodeAttr.Size = new System.Drawing.Size(29, 28);
            disp_RemoveNodeAttr.TabIndex = 2;
            disp_RemoveNodeAttr.Text = "-";
            disp_RemoveNodeAttr.UseVisualStyleBackColor = true;
            disp_RemoveNodeAttr.Click += new System.EventHandler(this.disp_RemoveNodeAttr_Click);
            // 
            // disp_nodeTypeAddButton
            // 
            disp_nodeTypeAddButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            disp_nodeTypeAddButton.Location = new System.Drawing.Point(221, 290);
            disp_nodeTypeAddButton.Margin = new System.Windows.Forms.Padding(4);
            disp_nodeTypeAddButton.Name = "disp_nodeTypeAddButton";
            disp_nodeTypeAddButton.Size = new System.Drawing.Size(29, 29);
            disp_nodeTypeAddButton.TabIndex = 5;
            disp_nodeTypeAddButton.Text = "+";
            disp_nodeTypeAddButton.UseVisualStyleBackColor = true;
            disp_nodeTypeAddButton.Click += new System.EventHandler(this.disp_nodeTypeAddButton_Click);
            // 
            // disp_nodeTypeRemoveButton
            // 
            disp_nodeTypeRemoveButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            disp_nodeTypeRemoveButton.Location = new System.Drawing.Point(256, 290);
            disp_nodeTypeRemoveButton.Margin = new System.Windows.Forms.Padding(4);
            disp_nodeTypeRemoveButton.Name = "disp_nodeTypeRemoveButton";
            disp_nodeTypeRemoveButton.Size = new System.Drawing.Size(29, 29);
            disp_nodeTypeRemoveButton.TabIndex = 6;
            disp_nodeTypeRemoveButton.Text = "-";
            disp_nodeTypeRemoveButton.UseVisualStyleBackColor = true;
            disp_nodeTypeRemoveButton.Click += new System.EventHandler(this.disp_nodeTypeRemoveButton_Click);
            // 
            // disp_linkTypeRemoveButton
            // 
            disp_linkTypeRemoveButton.Location = new System.Drawing.Point(256, 289);
            disp_linkTypeRemoveButton.Margin = new System.Windows.Forms.Padding(4);
            disp_linkTypeRemoveButton.Name = "disp_linkTypeRemoveButton";
            disp_linkTypeRemoveButton.Size = new System.Drawing.Size(29, 27);
            disp_linkTypeRemoveButton.TabIndex = 9;
            disp_linkTypeRemoveButton.Text = "-";
            disp_linkTypeRemoveButton.UseVisualStyleBackColor = true;
            disp_linkTypeRemoveButton.Click += new System.EventHandler(this.disp_linkTypeRemoveButton_Click);
            // 
            // disp_linkTypeAddButton
            // 
            disp_linkTypeAddButton.Location = new System.Drawing.Point(221, 289);
            disp_linkTypeAddButton.Margin = new System.Windows.Forms.Padding(4);
            disp_linkTypeAddButton.Name = "disp_linkTypeAddButton";
            disp_linkTypeAddButton.Size = new System.Drawing.Size(29, 27);
            disp_linkTypeAddButton.TabIndex = 8;
            disp_linkTypeAddButton.Text = "+";
            disp_linkTypeAddButton.UseVisualStyleBackColor = true;
            disp_linkTypeAddButton.Click += new System.EventHandler(this.disp_linkTypeAddButton_Click);
            // 
            // removeNodeButton
            // 
            removeNodeButton.Location = new System.Drawing.Point(256, 649);
            removeNodeButton.Margin = new System.Windows.Forms.Padding(4);
            removeNodeButton.Name = "removeNodeButton";
            removeNodeButton.Size = new System.Drawing.Size(29, 27);
            removeNodeButton.TabIndex = 12;
            removeNodeButton.Text = "-";
            removeNodeButton.UseVisualStyleBackColor = true;
            removeNodeButton.Click += new System.EventHandler(this.removeNodeButton_Click);
            // 
            // newNodeButton
            // 
            newNodeButton.Location = new System.Drawing.Point(221, 649);
            newNodeButton.Margin = new System.Windows.Forms.Padding(4);
            newNodeButton.Name = "newNodeButton";
            newNodeButton.Size = new System.Drawing.Size(29, 27);
            newNodeButton.TabIndex = 11;
            newNodeButton.Text = "+";
            newNodeButton.UseVisualStyleBackColor = true;
            newNodeButton.Click += new System.EventHandler(this.newNodeButton_Click);
            // 
            // link_removeLinkButton
            // 
            link_removeLinkButton.Location = new System.Drawing.Point(765, 500);
            link_removeLinkButton.Margin = new System.Windows.Forms.Padding(4);
            link_removeLinkButton.Name = "link_removeLinkButton";
            link_removeLinkButton.Size = new System.Drawing.Size(29, 27);
            link_removeLinkButton.TabIndex = 16;
            link_removeLinkButton.Text = "-";
            link_removeLinkButton.UseVisualStyleBackColor = true;
            link_removeLinkButton.Click += new System.EventHandler(this.link_removeLinkButton_Click);
            // 
            // link_newLinkButton
            // 
            link_newLinkButton.Location = new System.Drawing.Point(731, 500);
            link_newLinkButton.Margin = new System.Windows.Forms.Padding(4);
            link_newLinkButton.Name = "link_newLinkButton";
            link_newLinkButton.Size = new System.Drawing.Size(29, 27);
            link_newLinkButton.TabIndex = 15;
            link_newLinkButton.Text = "+";
            link_newLinkButton.UseVisualStyleBackColor = true;
            link_newLinkButton.Click += new System.EventHandler(this.link_newLinkButton_Click);
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.nodePage);
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Controls.Add(this.dispPage);
            this.tabControl.Location = new System.Drawing.Point(16, 33);
            this.tabControl.Margin = new System.Windows.Forms.Padding(4);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(813, 713);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // nodePage
            // 
            this.nodePage.Controls.Add(this.newNodeBox);
            this.nodePage.Controls.Add(removeNodeButton);
            this.nodePage.Controls.Add(newNodeButton);
            this.nodePage.Controls.Add(this.groupBox1);
            this.nodePage.Controls.Add(this.nodeList);
            this.nodePage.Location = new System.Drawing.Point(4, 25);
            this.nodePage.Margin = new System.Windows.Forms.Padding(4);
            this.nodePage.Name = "nodePage";
            this.nodePage.Padding = new System.Windows.Forms.Padding(4);
            this.nodePage.Size = new System.Drawing.Size(805, 684);
            this.nodePage.TabIndex = 0;
            this.nodePage.Text = "Nodes";
            this.nodePage.UseVisualStyleBackColor = true;
            // 
            // newNodeBox
            // 
            this.newNodeBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.newNodeBox.Location = new System.Drawing.Point(8, 650);
            this.newNodeBox.Margin = new System.Windows.Forms.Padding(4);
            this.newNodeBox.Name = "newNodeBox";
            this.newNodeBox.Size = new System.Drawing.Size(204, 22);
            this.newNodeBox.TabIndex = 13;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dropdownNodeType);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.nodeNameTextbox);
            this.groupBox1.Location = new System.Drawing.Point(293, 1);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(501, 676);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // dropdownNodeType
            // 
            this.dropdownNodeType.FormattingEnabled = true;
            this.dropdownNodeType.Location = new System.Drawing.Point(67, 50);
            this.dropdownNodeType.Margin = new System.Windows.Forms.Padding(4);
            this.dropdownNodeType.Name = "dropdownNodeType";
            this.dropdownNodeType.Size = new System.Drawing.Size(425, 24);
            this.dropdownNodeType.TabIndex = 11;
            this.dropdownNodeType.SelectedIndexChanged += new System.EventHandler(this.dropdownNodeType_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.editLinkButton);
            this.groupBox3.Controls.Add(this.dropdownLinkTypes);
            this.groupBox3.Controls.Add(this.linkToGoButton);
            this.groupBox3.Controls.Add(this.linkFromGoButton);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.linkFromTextBox);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.linkToTextBox);
            this.groupBox3.Controls.Add(this.linkList);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(12, 325);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(481, 343);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Links";
            // 
            // editLinkButton
            // 
            this.editLinkButton.Location = new System.Drawing.Point(353, 309);
            this.editLinkButton.Margin = new System.Windows.Forms.Padding(4);
            this.editLinkButton.Name = "editLinkButton";
            this.editLinkButton.Size = new System.Drawing.Size(121, 28);
            this.editLinkButton.TabIndex = 11;
            this.editLinkButton.Text = "Edit Link...";
            this.editLinkButton.UseVisualStyleBackColor = true;
            this.editLinkButton.Click += new System.EventHandler(this.editLinkButton_Click);
            // 
            // dropdownLinkTypes
            // 
            this.dropdownLinkTypes.FormattingEnabled = true;
            this.dropdownLinkTypes.Location = new System.Drawing.Point(67, 310);
            this.dropdownLinkTypes.Margin = new System.Windows.Forms.Padding(4);
            this.dropdownLinkTypes.Name = "dropdownLinkTypes";
            this.dropdownLinkTypes.Size = new System.Drawing.Size(248, 24);
            this.dropdownLinkTypes.TabIndex = 10;
            this.dropdownLinkTypes.SelectedIndexChanged += new System.EventHandler(this.dropdownLinkTypes_SelectedIndexChanged);
            // 
            // linkToGoButton
            // 
            this.linkToGoButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.linkToGoButton.Location = new System.Drawing.Point(444, 277);
            this.linkToGoButton.Margin = new System.Windows.Forms.Padding(4);
            this.linkToGoButton.Name = "linkToGoButton";
            this.linkToGoButton.Size = new System.Drawing.Size(31, 27);
            this.linkToGoButton.TabIndex = 9;
            this.linkToGoButton.Text = ">";
            this.linkToGoButton.UseVisualStyleBackColor = true;
            this.linkToGoButton.Click += new System.EventHandler(this.linkToGoButton_Click);
            // 
            // linkFromGoButton
            // 
            this.linkFromGoButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.linkFromGoButton.Location = new System.Drawing.Point(444, 245);
            this.linkFromGoButton.Margin = new System.Windows.Forms.Padding(4);
            this.linkFromGoButton.Name = "linkFromGoButton";
            this.linkFromGoButton.Size = new System.Drawing.Size(31, 27);
            this.linkFromGoButton.TabIndex = 8;
            this.linkFromGoButton.Text = ">";
            this.linkFromGoButton.UseVisualStyleBackColor = true;
            this.linkFromGoButton.Click += new System.EventHandler(this.linkFromGoButton_Click);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 250);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "From:";
            // 
            // linkFromTextBox
            // 
            this.linkFromTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkFromTextBox.Enabled = false;
            this.linkFromTextBox.Location = new System.Drawing.Point(67, 246);
            this.linkFromTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.linkFromTextBox.Name = "linkFromTextBox";
            this.linkFromTextBox.Size = new System.Drawing.Size(368, 22);
            this.linkFromTextBox.TabIndex = 6;
            this.linkFromTextBox.TextChanged += new System.EventHandler(this.linkFromTextBox_TextChanged);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 282);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "To:";
            // 
            // linkToTextBox
            // 
            this.linkToTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkToTextBox.Enabled = false;
            this.linkToTextBox.Location = new System.Drawing.Point(67, 278);
            this.linkToTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.linkToTextBox.Name = "linkToTextBox";
            this.linkToTextBox.Size = new System.Drawing.Size(367, 22);
            this.linkToTextBox.TabIndex = 4;
            this.linkToTextBox.TextChanged += new System.EventHandler(this.linkToTextBox_TextChanged);
            // 
            // linkList
            // 
            this.linkList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkList.FormattingEnabled = true;
            this.linkList.ItemHeight = 16;
            this.linkList.Location = new System.Drawing.Point(8, 23);
            this.linkList.Margin = new System.Windows.Forms.Padding(4);
            this.linkList.Name = "linkList";
            this.linkList.Size = new System.Drawing.Size(464, 212);
            this.linkList.TabIndex = 0;
            this.linkList.SelectedIndexChanged += new System.EventHandler(this.linkList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 314);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Type:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 55);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Type:";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.attributeList);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.attributeValTextBox);
            this.groupBox2.Location = new System.Drawing.Point(12, 84);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(481, 234);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Attributes:";
            // 
            // attributeList
            // 
            this.attributeList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.attributeList.FormattingEnabled = true;
            this.attributeList.ItemHeight = 16;
            this.attributeList.Location = new System.Drawing.Point(8, 23);
            this.attributeList.Margin = new System.Windows.Forms.Padding(4);
            this.attributeList.Name = "attributeList";
            this.attributeList.Size = new System.Drawing.Size(464, 164);
            this.attributeList.TabIndex = 0;
            this.attributeList.SelectedIndexChanged += new System.EventHandler(this.attributeList_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 204);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Value:";
            // 
            // attributeValTextBox
            // 
            this.attributeValTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.attributeValTextBox.Location = new System.Drawing.Point(67, 201);
            this.attributeValTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.attributeValTextBox.Name = "attributeValTextBox";
            this.attributeValTextBox.Size = new System.Drawing.Size(405, 22);
            this.attributeValTextBox.TabIndex = 2;
            this.attributeValTextBox.TextChanged += new System.EventHandler(this.attributeValTextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 23);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name:";
            // 
            // nodeNameTextbox
            // 
            this.nodeNameTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.nodeNameTextbox.Location = new System.Drawing.Point(67, 20);
            this.nodeNameTextbox.Margin = new System.Windows.Forms.Padding(4);
            this.nodeNameTextbox.Name = "nodeNameTextbox";
            this.nodeNameTextbox.Size = new System.Drawing.Size(425, 22);
            this.nodeNameTextbox.TabIndex = 0;
            this.nodeNameTextbox.TextChanged += new System.EventHandler(this.nodeNameTextbox_TextChanged);
            // 
            // nodeList
            // 
            this.nodeList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.nodeList.FormattingEnabled = true;
            this.nodeList.ItemHeight = 16;
            this.nodeList.Location = new System.Drawing.Point(8, 9);
            this.nodeList.Margin = new System.Windows.Forms.Padding(4);
            this.nodeList.Name = "nodeList";
            this.nodeList.Size = new System.Drawing.Size(276, 628);
            this.nodeList.TabIndex = 0;
            this.nodeList.SelectedIndexChanged += new System.EventHandler(this.nodeList_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox9);
            this.tabPage1.Controls.Add(link_removeLinkButton);
            this.tabPage1.Controls.Add(link_newLinkButton);
            this.tabPage1.Controls.Add(this.link_linkList);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(805, 684);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Links";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.link_toBox);
            this.groupBox9.Controls.Add(this.link_fromBox);
            this.groupBox9.Controls.Add(this.link_typeBox);
            this.groupBox9.Controls.Add(this.link_fromButton);
            this.groupBox9.Controls.Add(this.link_toButton);
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.label21);
            this.groupBox9.Location = new System.Drawing.Point(8, 538);
            this.groupBox9.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox9.Size = new System.Drawing.Size(787, 135);
            this.groupBox9.TabIndex = 17;
            this.groupBox9.TabStop = false;
            // 
            // link_toBox
            // 
            this.link_toBox.FormattingEnabled = true;
            this.link_toBox.Location = new System.Drawing.Point(65, 55);
            this.link_toBox.Margin = new System.Windows.Forms.Padding(4);
            this.link_toBox.Name = "link_toBox";
            this.link_toBox.Size = new System.Drawing.Size(673, 24);
            this.link_toBox.TabIndex = 12;
            this.link_toBox.SelectedIndexChanged += new System.EventHandler(this.link_toBox_SelectedIndexChanged);
            // 
            // link_fromBox
            // 
            this.link_fromBox.FormattingEnabled = true;
            this.link_fromBox.Location = new System.Drawing.Point(65, 23);
            this.link_fromBox.Margin = new System.Windows.Forms.Padding(4);
            this.link_fromBox.Name = "link_fromBox";
            this.link_fromBox.Size = new System.Drawing.Size(673, 24);
            this.link_fromBox.TabIndex = 11;
            this.link_fromBox.SelectedIndexChanged += new System.EventHandler(this.link_fromBox_SelectedIndexChanged);
            // 
            // link_typeBox
            // 
            this.link_typeBox.FormattingEnabled = true;
            this.link_typeBox.Location = new System.Drawing.Point(65, 89);
            this.link_typeBox.Margin = new System.Windows.Forms.Padding(4);
            this.link_typeBox.Name = "link_typeBox";
            this.link_typeBox.Size = new System.Drawing.Size(711, 24);
            this.link_typeBox.TabIndex = 10;
            this.link_typeBox.SelectedIndexChanged += new System.EventHandler(this.link_typeBox_SelectedIndexChanged);
            // 
            // link_fromButton
            // 
            this.link_fromButton.Location = new System.Drawing.Point(748, 23);
            this.link_fromButton.Margin = new System.Windows.Forms.Padding(4);
            this.link_fromButton.Name = "link_fromButton";
            this.link_fromButton.Size = new System.Drawing.Size(31, 27);
            this.link_fromButton.TabIndex = 8;
            this.link_fromButton.Text = ">";
            this.link_fromButton.UseVisualStyleBackColor = true;
            this.link_fromButton.Click += new System.EventHandler(this.link_fromButton_Click);
            // 
            // link_toButton
            // 
            this.link_toButton.Location = new System.Drawing.Point(748, 55);
            this.link_toButton.Margin = new System.Windows.Forms.Padding(4);
            this.link_toButton.Name = "link_toButton";
            this.link_toButton.Size = new System.Drawing.Size(31, 27);
            this.link_toButton.TabIndex = 9;
            this.link_toButton.Text = ">";
            this.link_toButton.UseVisualStyleBackColor = true;
            this.link_toButton.Click += new System.EventHandler(this.link_toButton_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 92);
            this.label23.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(44, 17);
            this.label23.TabIndex = 3;
            this.label23.Text = "Type:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 60);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(29, 17);
            this.label22.TabIndex = 5;
            this.label22.Text = "To:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(7, 28);
            this.label21.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(44, 17);
            this.label21.TabIndex = 7;
            this.label21.Text = "From:";
            // 
            // link_linkList
            // 
            this.link_linkList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.link_linkList.FormattingEnabled = true;
            this.link_linkList.ItemHeight = 16;
            this.link_linkList.Location = new System.Drawing.Point(8, 7);
            this.link_linkList.Margin = new System.Windows.Forms.Padding(4);
            this.link_linkList.Name = "link_linkList";
            this.link_linkList.Size = new System.Drawing.Size(785, 484);
            this.link_linkList.TabIndex = 14;
            this.link_linkList.SelectedIndexChanged += new System.EventHandler(this.link_linkList_SelectedIndexChanged);
            // 
            // dispPage
            // 
            this.dispPage.Controls.Add(this.groupBox5);
            this.dispPage.Controls.Add(this.groupBox4);
            this.dispPage.Location = new System.Drawing.Point(4, 25);
            this.dispPage.Margin = new System.Windows.Forms.Padding(4);
            this.dispPage.Name = "dispPage";
            this.dispPage.Padding = new System.Windows.Forms.Padding(4);
            this.dispPage.Size = new System.Drawing.Size(805, 684);
            this.dispPage.TabIndex = 1;
            this.dispPage.Text = "Display";
            this.dispPage.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.disp_linkTypeAddBox);
            this.groupBox5.Controls.Add(this.groupBox8);
            this.groupBox5.Controls.Add(disp_linkTypeRemoveButton);
            this.groupBox5.Controls.Add(disp_linkTypeAddButton);
            this.groupBox5.Controls.Add(this.disp_LinkTypeList);
            this.groupBox5.Location = new System.Drawing.Point(8, 343);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(787, 330);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Link Types";
            // 
            // disp_linkTypeAddBox
            // 
            this.disp_linkTypeAddBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.disp_linkTypeAddBox.Location = new System.Drawing.Point(8, 290);
            this.disp_linkTypeAddBox.Margin = new System.Windows.Forms.Padding(4);
            this.disp_linkTypeAddBox.Name = "disp_linkTypeAddBox";
            this.disp_linkTypeAddBox.Size = new System.Drawing.Size(204, 22);
            this.disp_linkTypeAddBox.TabIndex = 10;
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.disp_linkTypeBox);
            this.groupBox8.Controls.Add(this.label18);
            this.groupBox8.Controls.Add(this.disp_linkForce);
            this.groupBox8.Controls.Add(this.disp_linkVisible);
            this.groupBox8.Controls.Add(this.disp_linkColourBox);
            this.groupBox8.Controls.Add(this.disp_linkColourButton);
            this.groupBox8.Controls.Add(this.label7);
            this.groupBox8.Controls.Add(this.label11);
            this.groupBox8.Controls.Add(this.disp_linkStrengthBox);
            this.groupBox8.Controls.Add(this.disp_linkBaseLengthBox);
            this.groupBox8.Controls.Add(this.label9);
            this.groupBox8.Controls.Add(this.label10);
            this.groupBox8.Controls.Add(this.disp_linkPowerBox);
            this.groupBox8.Location = new System.Drawing.Point(293, 16);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox8.Size = new System.Drawing.Size(485, 300);
            this.groupBox8.TabIndex = 16;
            this.groupBox8.TabStop = false;
            // 
            // disp_linkTypeBox
            // 
            this.disp_linkTypeBox.Enabled = false;
            this.disp_linkTypeBox.Location = new System.Drawing.Point(113, 26);
            this.disp_linkTypeBox.Margin = new System.Windows.Forms.Padding(4);
            this.disp_linkTypeBox.Name = "disp_linkTypeBox";
            this.disp_linkTypeBox.Size = new System.Drawing.Size(363, 22);
            this.disp_linkTypeBox.TabIndex = 19;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 31);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 17);
            this.label18.TabIndex = 18;
            this.label18.Text = "Type:";
            // 
            // disp_linkForce
            // 
            this.disp_linkForce.AutoSize = true;
            this.disp_linkForce.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.disp_linkForce.Location = new System.Drawing.Point(295, 60);
            this.disp_linkForce.Margin = new System.Windows.Forms.Padding(4);
            this.disp_linkForce.Name = "disp_linkForce";
            this.disp_linkForce.Size = new System.Drawing.Size(74, 21);
            this.disp_linkForce.TabIndex = 17;
            this.disp_linkForce.Text = "Force?";
            this.disp_linkForce.UseVisualStyleBackColor = true;
            this.disp_linkForce.CheckedChanged += new System.EventHandler(this.disp_linkForce_CheckedChanged);
            // 
            // disp_linkVisible
            // 
            this.disp_linkVisible.AutoSize = true;
            this.disp_linkVisible.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.disp_linkVisible.Location = new System.Drawing.Point(113, 60);
            this.disp_linkVisible.Margin = new System.Windows.Forms.Padding(4);
            this.disp_linkVisible.Name = "disp_linkVisible";
            this.disp_linkVisible.Size = new System.Drawing.Size(79, 21);
            this.disp_linkVisible.TabIndex = 16;
            this.disp_linkVisible.Text = "Visible?";
            this.disp_linkVisible.UseVisualStyleBackColor = true;
            this.disp_linkVisible.CheckedChanged += new System.EventHandler(this.disp_linkVisible_CheckedChanged);
            // 
            // disp_linkColourBox
            // 
            this.disp_linkColourBox.BackColor = System.Drawing.Color.Transparent;
            this.disp_linkColourBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.disp_linkColourBox.Location = new System.Drawing.Point(113, 89);
            this.disp_linkColourBox.Margin = new System.Windows.Forms.Padding(4);
            this.disp_linkColourBox.Name = "disp_linkColourBox";
            this.disp_linkColourBox.Size = new System.Drawing.Size(274, 24);
            this.disp_linkColourBox.TabIndex = 1;
            this.disp_linkColourBox.TabStop = false;
            this.disp_linkColourBox.Click += new System.EventHandler(this.disp_linkColourBox_Click);
            // 
            // disp_linkColourButton
            // 
            this.disp_linkColourButton.Location = new System.Drawing.Point(396, 87);
            this.disp_linkColourButton.Margin = new System.Windows.Forms.Padding(4);
            this.disp_linkColourButton.Name = "disp_linkColourButton";
            this.disp_linkColourButton.Size = new System.Drawing.Size(81, 27);
            this.disp_linkColourButton.TabIndex = 15;
            this.disp_linkColourButton.Text = "Pick";
            this.disp_linkColourButton.UseVisualStyleBackColor = true;
            this.disp_linkColourButton.Click += new System.EventHandler(this.disp_linkColourBox_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 126);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "Strength:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(8, 94);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 17);
            this.label11.TabIndex = 14;
            this.label11.Text = "Colour:";
            // 
            // disp_linkStrengthBox
            // 
            this.disp_linkStrengthBox.Location = new System.Drawing.Point(113, 121);
            this.disp_linkStrengthBox.Margin = new System.Windows.Forms.Padding(4);
            this.disp_linkStrengthBox.Name = "disp_linkStrengthBox";
            this.disp_linkStrengthBox.Size = new System.Drawing.Size(363, 22);
            this.disp_linkStrengthBox.TabIndex = 9;
            this.disp_linkStrengthBox.TextChanged += new System.EventHandler(this.disp_linkStrengthBox_TextChanged);
            this.disp_linkStrengthBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateNumericalTextBox);
            // 
            // disp_linkBaseLengthBox
            // 
            this.disp_linkBaseLengthBox.Location = new System.Drawing.Point(113, 185);
            this.disp_linkBaseLengthBox.Margin = new System.Windows.Forms.Padding(4);
            this.disp_linkBaseLengthBox.Name = "disp_linkBaseLengthBox";
            this.disp_linkBaseLengthBox.Size = new System.Drawing.Size(363, 22);
            this.disp_linkBaseLengthBox.TabIndex = 13;
            this.disp_linkBaseLengthBox.TextChanged += new System.EventHandler(this.disp_linkBaseLengthBox_TextChanged);
            this.disp_linkBaseLengthBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateNumericalTextBox);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 158);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 17);
            this.label9.TabIndex = 10;
            this.label9.Text = "Power:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 190);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 17);
            this.label10.TabIndex = 12;
            this.label10.Text = "Base Length:";
            // 
            // disp_linkPowerBox
            // 
            this.disp_linkPowerBox.Location = new System.Drawing.Point(113, 153);
            this.disp_linkPowerBox.Margin = new System.Windows.Forms.Padding(4);
            this.disp_linkPowerBox.Name = "disp_linkPowerBox";
            this.disp_linkPowerBox.Size = new System.Drawing.Size(363, 22);
            this.disp_linkPowerBox.TabIndex = 11;
            this.disp_linkPowerBox.TextChanged += new System.EventHandler(this.disp_linkPowerBox_TextChanged);
            this.disp_linkPowerBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ValidateNumericalTextBox);
            // 
            // disp_LinkTypeList
            // 
            this.disp_LinkTypeList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.disp_LinkTypeList.FormattingEnabled = true;
            this.disp_LinkTypeList.ItemHeight = 16;
            this.disp_LinkTypeList.Location = new System.Drawing.Point(8, 23);
            this.disp_LinkTypeList.Margin = new System.Windows.Forms.Padding(4);
            this.disp_LinkTypeList.Name = "disp_LinkTypeList";
            this.disp_LinkTypeList.Size = new System.Drawing.Size(276, 260);
            this.disp_LinkTypeList.TabIndex = 0;
            this.disp_LinkTypeList.SelectedIndexChanged += new System.EventHandler(this.disp_LinkTypeList_SelectedIndexChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.DisplayNameLabel);
            this.groupBox4.Controls.Add(this.DisplayName);
            this.groupBox4.Controls.Add(this.disp_nodeTypeAddBox);
            this.groupBox4.Controls.Add(disp_nodeTypeRemoveButton);
            this.groupBox4.Controls.Add(disp_nodeTypeAddButton);
            this.groupBox4.Controls.Add(this.groupBox6);
            this.groupBox4.Controls.Add(this.disp_NodeTypeList);
            this.groupBox4.Location = new System.Drawing.Point(8, 4);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(787, 331);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Node Types";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // DisplayNameLabel
            // 
            this.DisplayNameLabel.AutoSize = true;
            this.DisplayNameLabel.Location = new System.Drawing.Point(298, 36);
            this.DisplayNameLabel.Name = "DisplayNameLabel";
            this.DisplayNameLabel.Size = new System.Drawing.Size(99, 17);
            this.DisplayNameLabel.TabIndex = 9;
            this.DisplayNameLabel.Text = "Display Name:";
            this.DisplayNameLabel.Click += new System.EventHandler(this.label24_Click);
            // 
            // DisplayName
            // 
            this.DisplayName.Location = new System.Drawing.Point(406, 33);
            this.DisplayName.Name = "DisplayName";
            this.DisplayName.Size = new System.Drawing.Size(371, 22);
            this.DisplayName.TabIndex = 8;
            // 
            // disp_nodeTypeAddBox
            // 
            this.disp_nodeTypeAddBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.disp_nodeTypeAddBox.Location = new System.Drawing.Point(8, 292);
            this.disp_nodeTypeAddBox.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeTypeAddBox.Name = "disp_nodeTypeAddBox";
            this.disp_nodeTypeAddBox.Size = new System.Drawing.Size(204, 22);
            this.disp_nodeTypeAddBox.TabIndex = 7;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.disp_NewAttrib);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Controls.Add(this.disp_AddNodeAttr);
            this.groupBox6.Controls.Add(disp_RemoveNodeAttr);
            this.groupBox6.Controls.Add(this.disp_nodeAttribsList);
            this.groupBox6.Location = new System.Drawing.Point(293, 70);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox6.Size = new System.Drawing.Size(485, 261);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            // 
            // disp_NewAttrib
            // 
            this.disp_NewAttrib.FormattingEnabled = true;
            this.disp_NewAttrib.Location = new System.Drawing.Point(9, 222);
            this.disp_NewAttrib.Margin = new System.Windows.Forms.Padding(4);
            this.disp_NewAttrib.Name = "disp_NewAttrib";
            this.disp_NewAttrib.Size = new System.Drawing.Size(196, 24);
            this.disp_NewAttrib.TabIndex = 4;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.disp_nodeStringPanel);
            this.groupBox7.Controls.Add(this.disp_nodeBoolPanel);
            this.groupBox7.Controls.Add(this.disp_nodeType);
            this.groupBox7.Controls.Add(this.disp_nodeFloatPanel);
            this.groupBox7.Controls.Add(this.label17);
            this.groupBox7.Controls.Add(this.disp_nodeAttrib);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Location = new System.Drawing.Point(213, 11);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox7.Size = new System.Drawing.Size(264, 203);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            // 
            // disp_nodeStringPanel
            // 
            this.disp_nodeStringPanel.Controls.Add(this.disp_nodeDefaultString);
            this.disp_nodeStringPanel.Controls.Add(this.label19);
            this.disp_nodeStringPanel.Location = new System.Drawing.Point(12, 87);
            this.disp_nodeStringPanel.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeStringPanel.Name = "disp_nodeStringPanel";
            this.disp_nodeStringPanel.Size = new System.Drawing.Size(244, 116);
            this.disp_nodeStringPanel.TabIndex = 33;
            this.disp_nodeStringPanel.Visible = false;
            // 
            // disp_nodeDefaultString
            // 
            this.disp_nodeDefaultString.Location = new System.Drawing.Point(69, 0);
            this.disp_nodeDefaultString.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeDefaultString.Name = "disp_nodeDefaultString";
            this.disp_nodeDefaultString.Size = new System.Drawing.Size(173, 22);
            this.disp_nodeDefaultString.TabIndex = 16;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(-4, 4);
            this.label19.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(57, 17);
            this.label19.TabIndex = 15;
            this.label19.Text = "Default:";
            // 
            // disp_nodeBoolPanel
            // 
            this.disp_nodeBoolPanel.Controls.Add(this.disp_nodeDefaultBool);
            this.disp_nodeBoolPanel.Controls.Add(this.label20);
            this.disp_nodeBoolPanel.Location = new System.Drawing.Point(12, 87);
            this.disp_nodeBoolPanel.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeBoolPanel.Name = "disp_nodeBoolPanel";
            this.disp_nodeBoolPanel.Size = new System.Drawing.Size(244, 153);
            this.disp_nodeBoolPanel.TabIndex = 34;
            this.disp_nodeBoolPanel.Visible = false;
            // 
            // disp_nodeDefaultBool
            // 
            this.disp_nodeDefaultBool.AutoSize = true;
            this.disp_nodeDefaultBool.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.disp_nodeDefaultBool.Enabled = false;
            this.disp_nodeDefaultBool.Location = new System.Drawing.Point(-4, 2);
            this.disp_nodeDefaultBool.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeDefaultBool.Name = "disp_nodeDefaultBool";
            this.disp_nodeDefaultBool.Size = new System.Drawing.Size(79, 21);
            this.disp_nodeDefaultBool.TabIndex = 29;
            this.disp_nodeDefaultBool.Text = "Default:";
            this.disp_nodeDefaultBool.UseVisualStyleBackColor = true;
            this.disp_nodeDefaultBool.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(-4, 4);
            this.label20.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(57, 17);
            this.label20.TabIndex = 15;
            this.label20.Text = "Default:";
            // 
            // disp_nodeType
            // 
            this.disp_nodeType.Enabled = false;
            this.disp_nodeType.Location = new System.Drawing.Point(81, 23);
            this.disp_nodeType.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeType.Name = "disp_nodeType";
            this.disp_nodeType.Size = new System.Drawing.Size(173, 22);
            this.disp_nodeType.TabIndex = 28;
            // 
            // disp_nodeFloatPanel
            // 
            this.disp_nodeFloatPanel.Controls.Add(this.disp_nodeMin);
            this.disp_nodeFloatPanel.Controls.Add(this.label8);
            this.disp_nodeFloatPanel.Controls.Add(this.label12);
            this.disp_nodeFloatPanel.Controls.Add(this.disp_nodeMax);
            this.disp_nodeFloatPanel.Controls.Add(this.label13);
            this.disp_nodeFloatPanel.Controls.Add(this.disp_nodeDelta);
            this.disp_nodeFloatPanel.Controls.Add(this.disp_nodeScale);
            this.disp_nodeFloatPanel.Controls.Add(this.label15);
            this.disp_nodeFloatPanel.Controls.Add(this.label14);
            this.disp_nodeFloatPanel.Controls.Add(this.disp_nodeDefault);
            this.disp_nodeFloatPanel.Location = new System.Drawing.Point(12, 87);
            this.disp_nodeFloatPanel.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeFloatPanel.Name = "disp_nodeFloatPanel";
            this.disp_nodeFloatPanel.Size = new System.Drawing.Size(244, 153);
            this.disp_nodeFloatPanel.TabIndex = 32;
            this.disp_nodeFloatPanel.Visible = false;
            // 
            // disp_nodeMin
            // 
            this.disp_nodeMin.Location = new System.Drawing.Point(69, 0);
            this.disp_nodeMin.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeMin.Name = "disp_nodeMin";
            this.disp_nodeMin.Size = new System.Drawing.Size(173, 22);
            this.disp_nodeMin.TabIndex = 16;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(-4, 4);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 17);
            this.label8.TabIndex = 15;
            this.label8.Text = "Min:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(-4, 36);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 17);
            this.label12.TabIndex = 17;
            this.label12.Text = "Max:";
            // 
            // disp_nodeMax
            // 
            this.disp_nodeMax.Location = new System.Drawing.Point(69, 32);
            this.disp_nodeMax.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeMax.Name = "disp_nodeMax";
            this.disp_nodeMax.Size = new System.Drawing.Size(173, 22);
            this.disp_nodeMax.TabIndex = 18;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(-4, 68);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(47, 17);
            this.label13.TabIndex = 19;
            this.label13.Text = "Scale:";
            // 
            // disp_nodeDelta
            // 
            this.disp_nodeDelta.Location = new System.Drawing.Point(69, 128);
            this.disp_nodeDelta.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeDelta.Name = "disp_nodeDelta";
            this.disp_nodeDelta.Size = new System.Drawing.Size(173, 22);
            this.disp_nodeDelta.TabIndex = 24;
            // 
            // disp_nodeScale
            // 
            this.disp_nodeScale.Location = new System.Drawing.Point(69, 64);
            this.disp_nodeScale.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeScale.Name = "disp_nodeScale";
            this.disp_nodeScale.Size = new System.Drawing.Size(173, 22);
            this.disp_nodeScale.TabIndex = 20;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(-4, 132);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 17);
            this.label15.TabIndex = 23;
            this.label15.Text = "Delta:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(-4, 100);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 17);
            this.label14.TabIndex = 21;
            this.label14.Text = "Default:";
            // 
            // disp_nodeDefault
            // 
            this.disp_nodeDefault.Location = new System.Drawing.Point(69, 96);
            this.disp_nodeDefault.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeDefault.Name = "disp_nodeDefault";
            this.disp_nodeDefault.Size = new System.Drawing.Size(173, 22);
            this.disp_nodeDefault.TabIndex = 22;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 27);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 17);
            this.label17.TabIndex = 27;
            this.label17.Text = "Type:";
            // 
            // disp_nodeAttrib
            // 
            this.disp_nodeAttrib.Enabled = false;
            this.disp_nodeAttrib.Location = new System.Drawing.Point(81, 55);
            this.disp_nodeAttrib.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeAttrib.Name = "disp_nodeAttrib";
            this.disp_nodeAttrib.Size = new System.Drawing.Size(173, 22);
            this.disp_nodeAttrib.TabIndex = 26;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(8, 59);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 17);
            this.label16.TabIndex = 25;
            this.label16.Text = "Attribute:";
            // 
            // disp_AddNodeAttr
            // 
            this.disp_AddNodeAttr.Location = new System.Drawing.Point(213, 222);
            this.disp_AddNodeAttr.Margin = new System.Windows.Forms.Padding(4);
            this.disp_AddNodeAttr.Name = "disp_AddNodeAttr";
            this.disp_AddNodeAttr.Size = new System.Drawing.Size(29, 28);
            this.disp_AddNodeAttr.TabIndex = 1;
            this.disp_AddNodeAttr.Text = "+";
            this.disp_AddNodeAttr.UseVisualStyleBackColor = true;
            this.disp_AddNodeAttr.Click += new System.EventHandler(this.disp_AddNodeAttr_Click);
            // 
            // disp_nodeAttribsList
            // 
            this.disp_nodeAttribsList.FormattingEnabled = true;
            this.disp_nodeAttribsList.ItemHeight = 16;
            this.disp_nodeAttribsList.Location = new System.Drawing.Point(9, 18);
            this.disp_nodeAttribsList.Margin = new System.Windows.Forms.Padding(4);
            this.disp_nodeAttribsList.Name = "disp_nodeAttribsList";
            this.disp_nodeAttribsList.Size = new System.Drawing.Size(195, 196);
            this.disp_nodeAttribsList.TabIndex = 0;
            this.disp_nodeAttribsList.SelectedIndexChanged += new System.EventHandler(this.disp_nodeAttribsList_SelectedIndexChanged);
            // 
            // disp_NodeTypeList
            // 
            this.disp_NodeTypeList.FormattingEnabled = true;
            this.disp_NodeTypeList.ItemHeight = 16;
            this.disp_NodeTypeList.Location = new System.Drawing.Point(8, 23);
            this.disp_NodeTypeList.Margin = new System.Windows.Forms.Padding(4);
            this.disp_NodeTypeList.Name = "disp_NodeTypeList";
            this.disp_NodeTypeList.Size = new System.Drawing.Size(276, 260);
            this.disp_NodeTypeList.TabIndex = 0;
            this.disp_NodeTypeList.SelectedIndexChanged += new System.EventHandler(this.disp_NodeTypeList_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(845, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(214, 26);
            this.openToolStripMenuItem.Text = "Open...";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(214, 26);
            this.saveAsToolStripMenuItem.Text = "Save As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(214, 26);
            this.exportToolStripMenuItem.Text = "Export As Package...";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(214, 26);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.visEdit001aToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.aboutToolStripMenuItem.Text = "Help";
            // 
            // visEdit001aToolStripMenuItem
            // 
            this.visEdit001aToolStripMenuItem.Name = "visEdit001aToolStripMenuItem";
            this.visEdit001aToolStripMenuItem.Size = new System.Drawing.Size(125, 26);
            this.visEdit001aToolStripMenuItem.Text = "About";
            this.visEdit001aToolStripMenuItem.Click += new System.EventHandler(this.visEdit001aToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "json";
            this.openFileDialog1.Filter = "JSON Dataset|*.json|XYZ Dataset|*.xyz";
            this.openFileDialog1.InitialDirectory = "./";
            this.openFileDialog1.Title = "Open dataset...";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "json";
            this.saveFileDialog1.Filter = "JSON Dataset|*.json";
            this.saveFileDialog1.InitialDirectory = "./";
            this.saveFileDialog1.Title = "Save As...";
            // 
            // disp_ColourPicker
            // 
            this.disp_ColourPicker.AnyColor = true;
            this.disp_ColourPicker.Color = System.Drawing.Color.White;
            // 
            // savePackageDialog
            // 
            this.savePackageDialog.Filter = "PhysVis Packages|*.pvis";
            // 
            // texturePickDialog
            // 
            this.texturePickDialog.Filter = resources.GetString("texturePickDialog.Filter");
            this.texturePickDialog.FilterIndex = 10;
            this.texturePickDialog.Multiselect = true;
            this.texturePickDialog.Title = "Select Textures...";
            // 
            // dllPickerDialog
            // 
            this.dllPickerDialog.Filter = "PhysVis Importer DLLs|*.dll";
            this.dllPickerDialog.Title = "Add Importer DLL...";
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.DefaultExt = "disp.json";
            this.openFileDialog2.Filter = "display JSON Dataset|*.disp.json";
            this.openFileDialog2.InitialDirectory = "./";
            this.openFileDialog2.Title = "Open display...";
            // 
            // VisEdit_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 761);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "VisEdit_Main";
            this.Text = "VisEdit";
            this.tabControl.ResumeLayout(false);
            this.nodePage.ResumeLayout(false);
            this.nodePage.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.dispPage.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.disp_linkColourBox)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.disp_nodeStringPanel.ResumeLayout(false);
            this.disp_nodeStringPanel.PerformLayout();
            this.disp_nodeBoolPanel.ResumeLayout(false);
            this.disp_nodeBoolPanel.PerformLayout();
            this.disp_nodeFloatPanel.ResumeLayout(false);
            this.disp_nodeFloatPanel.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage nodePage;
		private System.Windows.Forms.TabPage dispPage;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.ListBox nodeList;
		private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem visEdit001aToolStripMenuItem;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.ListBox attributeList;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox attributeValTextBox;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox nodeNameTextbox;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.ListBox linkList;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button linkToGoButton;
		private System.Windows.Forms.Button linkFromGoButton;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox linkFromTextBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox linkToTextBox;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.ComboBox dropdownNodeType;
		private System.Windows.Forms.ComboBox dropdownLinkTypes;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox disp_linkBaseLengthBox;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox disp_linkPowerBox;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox disp_linkStrengthBox;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.PictureBox disp_linkColourBox;
		private System.Windows.Forms.ListBox disp_LinkTypeList;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.ListBox disp_NodeTypeList;
		private System.Windows.Forms.ColorDialog disp_ColourPicker;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.Button disp_AddNodeAttr;
		private System.Windows.Forms.ListBox disp_nodeAttribsList;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.Panel disp_nodeStringPanel;
		private System.Windows.Forms.TextBox disp_nodeDefaultString;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Panel disp_nodeBoolPanel;
		private System.Windows.Forms.CheckBox disp_nodeDefaultBool;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox disp_nodeType;
		private System.Windows.Forms.Panel disp_nodeFloatPanel;
		private System.Windows.Forms.TextBox disp_nodeMin;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.TextBox disp_nodeMax;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox disp_nodeDelta;
		private System.Windows.Forms.TextBox disp_nodeScale;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.TextBox disp_nodeDefault;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.TextBox disp_nodeAttrib;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.ComboBox disp_NewAttrib;
		private System.Windows.Forms.Button disp_linkColourButton;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.TextBox disp_linkTypeBox;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.CheckBox disp_linkForce;
		private System.Windows.Forms.CheckBox disp_linkVisible;
		private System.Windows.Forms.TextBox disp_linkTypeAddBox;
		private System.Windows.Forms.TextBox disp_nodeTypeAddBox;
		private System.Windows.Forms.TextBox newNodeBox;
		private System.Windows.Forms.SaveFileDialog savePackageDialog;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.ComboBox link_toBox;
		private System.Windows.Forms.ComboBox link_fromBox;
		private System.Windows.Forms.ComboBox link_typeBox;
		private System.Windows.Forms.Button link_fromButton;
		private System.Windows.Forms.Button link_toButton;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.ListBox link_linkList;
		private System.Windows.Forms.Button editLinkButton;
		private System.Windows.Forms.OpenFileDialog texturePickDialog;
		private System.Windows.Forms.OpenFileDialog dllPickerDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.Label DisplayNameLabel;
        private System.Windows.Forms.TextBox DisplayName;
    }
}

