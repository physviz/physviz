﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class EnumHelper {
	/// <summary>
	/// Gets an attribute on an enum field value
	/// </summary>
	/// <typeparam name="T">The type of the attribute you want to retrieve</typeparam>
	/// <param name="enumVal">The enum value</param>
	/// <returns>The attribute of type T that exists on the enum value</returns>
	/// <example>string desc = myEnumVariable.GetAttributeOfType<DescriptionAttribute>().Description;</example>
	public static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute {
		var type = enumVal.GetType();
		var memInfo = type.GetMember(enumVal.ToString());
		var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
		return (attributes.Length > 0) ? (T)attributes[0] : null;
	}
}

namespace VisEdit.Document {

	public class EditableTypeAttribute : Attribute {
		public Type EditableType { get; private set; }

		public EditableTypeAttribute(Type value) {
			EditableType = value;
		}
	}

	// Reimplementing PhysVis document format in C#
	public class Graph {
		public class Node {
			public string m_name = null;
			public string m_type = null;

			public List<KeyValuePair<string, string>> m_attributes = new List<KeyValuePair<string, string>>();

			public List<Link> m_Links = new List<Link>();
		}

		public class Link {
			public Node m_from = null;
			public string m_fromStr = null;
			public Node m_to = null;
			public string m_toStr = null;
			public string m_type = null;

			public override string ToString() {
				return string.Format("{0} -> {1}", m_from == null ? "<NODE-NOT-FOUND>" : m_from.m_name, m_to == null ? "<NODE-NOT-FOUND>" : m_to.m_name);
			}
		}

		public enum PDN //ParticleDataNames
		{
			[EditableType(typeof(string))]
			PDN_NULL,
			//[EditableType(typeof(Vector3))]
			//PDN_POSITION,
			[EditableType(typeof(float))]
			PDN_POSITION_X,
			[EditableType(typeof(float))]
			PDN_POSITION_Y,
			[EditableType(typeof(float))]
			PDN_POSITION_Z,
			[EditableType(typeof(float))]
			PDN_ROTATION,
			[EditableType(typeof(float))]
			PDN_ROT_VEL,
			[EditableType(typeof(float))]
			PDN_ROT_ACC,
			//[EditableType(typeof(Vector2))]
			//PDN_SIZE,
			[EditableType(typeof(float))]
			PDN_SIZE_X,
			[EditableType(typeof(float))]
			PDN_SIZE_Y,
            [EditableType(typeof(float))]
            PDN_SIZE_Z,
            [EditableType(typeof(float))]
			PDN_MASS,
			//[EditableType(typeof(Color))]
			//PDN_COLOUR,
			[EditableType(typeof(float))]
			PDN_COLOUR_R,
			[EditableType(typeof(float))]
			PDN_COLOUR_G,
			[EditableType(typeof(float))]
			PDN_COLOUR_B,
			[EditableType(typeof(float))]
			PDN_COLOUR_A,
			//[EditableType(typeof(string))]
			//PDN_TEXTURE_NAME,
			[EditableType(typeof(string))]
			PDN_TEXTURE,
			//[EditableType(typeof(Vector3))]
			//PDN_VELOCITY,
			[EditableType(typeof(float))]
			PDN_VELOCITY_X,
			[EditableType(typeof(float))]
			PDN_VELOCITY_Y,
			[EditableType(typeof(float))]
			PDN_VELOCITY_Z,
			[EditableType(typeof(Vector3))]
			//PDN_ACC,
			//[EditableType(typeof(float))]
			PDN_ACC_X,
			[EditableType(typeof(float))]
			PDN_ACC_Y,
			[EditableType(typeof(float))]
			PDN_ACC_Z,
			[EditableType(typeof(float))]
			PDN_DRAG,
			//[EditableType(typeof(Vector3))]
			//PDN_GRAV,
			[EditableType(typeof(float))]
			PDN_GRAV_X,
			[EditableType(typeof(float))]
			PDN_GRAV_Y,
			[EditableType(typeof(float))]
			PDN_GRAV_Z,
			[EditableType(typeof(bool))]
			PDN_OF_INTEREST,
			[EditableType(typeof(bool))]
			PDN_FIXED,
			[EditableType(typeof(bool))]
			PDN_HIDE,
			[EditableType(typeof(int))]
			PDN_SUBSCREEN,
            [EditableType(typeof(float))]
            PDN_OSC_AMP_1,
            [EditableType(typeof(float))]
            PDN_OSC_AMP_2,
            [EditableType(typeof(float))]
            PDN_OSC_AMP_3,
            [EditableType(typeof(float))]
            PDN_OSC_W_1,
            [EditableType(typeof(float))]
            PDN_OSC_W_2,
            [EditableType(typeof(float))]
            PDN_OSC_W_3,
            [EditableType(typeof(float))]
            PDN_OSC_P_1,
            [EditableType(typeof(float))]
            PDN_OSC_P_2,
            [EditableType(typeof(float))]
            PDN_OSC_P_3,
            [EditableType(typeof(string))]
            PDN_OSC_VAL_1,
            [EditableType(typeof(string))]
            PDN_OSC_VAL_2,
            [EditableType(typeof(string))]
            PDN_OSC_VAL_3,
            [EditableType(typeof(float))]
            PDN_JIG_MAG_1,
            [EditableType(typeof(float))]
            PDN_JIG_MAG_2,
            [EditableType(typeof(float))]
            PDN_JIG_MAG_3,
            [EditableType(typeof(string))]
            PDN_JIG_VAL_1,
            [EditableType(typeof(string))]
            PDN_JIG_VAL_2,
            [EditableType(typeof(string))]
            PDN_JIG_VAL_3,
            [EditableType(typeof(float))]
            PDN_DISPLAY_TYPE,
            [EditableType(typeof(string))]
            PDN_CONTAINS_LINK,
            [EditableType(typeof(float))]
            PDN_CONTAINS_DIRECTION
        };

		public class VisType {
			public string m_Name = null;
			public List<AttributeMapper> m_Attributes = new List<AttributeMapper>();


			public AttributeMapper GetAttributeOfType(LDN _ldn, bool _addIfNotFound = false, float _default = float.NaN, string _defaultStr = "") {
				for (int i = 0; i < m_Attributes.Count; i++) {
					if((LDN)m_Attributes[i].m_MappedType == _ldn) {
						return m_Attributes[i];
					}
				}

				if(_addIfNotFound) {
					AttributeMapper mapper = new AttributeMapper((Enum)_ldn);
					m_Attributes.Add(mapper);
					if(!float.IsNaN(_default)) {
						mapper.m_bDefault = true;
						mapper.m_Default = _default;
						mapper.m_strDefault = _defaultStr;
					}
					return mapper;
				} else {
					return null;
				}
			}

			public class AttributeMapper {
				public AttributeMapper(Enum _mappedData) {
					m_MappedType = _mappedData;
					m_DataType = _mappedData.GetAttributeOfType<EditableTypeAttribute>().EditableType;
					m_EnumType = _mappedData.GetType();
				}

				public AttributeMapper(JSONObject _parse, Type _enumType) {
					m_MappedType = (Enum)Enum.Parse(_enumType, _parse["type"].str);
					m_DataType = m_MappedType.GetAttributeOfType<EditableTypeAttribute>().EditableType;
					m_EnumType = _enumType;

					m_AttributeName = _parse.HasField("base") ? _parse["base"].str : null;

					if (m_DataType == typeof(string)) {
						m_strDefault = _parse.HasField("default") ? _parse["default"].str : null;
					}

					if (m_DataType == typeof(float)) {
						m_bMin = _parse.HasField("min");
						m_Min = _parse.HasField("min") ? _parse["min"].f : 0.0f;

						m_bMax = _parse.HasField("max");
						m_Max = _parse.HasField("max") ? _parse["max"].f : 0.0f;

						m_bDefault = _parse.HasField("default");
						m_Default = _parse.HasField("default") ? _parse["default"].f : 0.0f;

						m_bDelta = _parse.HasField("delta");
						m_Delta = _parse.HasField("delta") ? _parse["delta"].f : 0.0f;

						m_bScale = _parse.HasField("scale");
						m_Scale = _parse.HasField("scale") ? _parse["scale"].f : 0.0f;
					}

					if (m_DataType == typeof(bool)) {
						m_bDefault = _parse.HasField("default") ? (_parse["default"].f == 1.0f) : false;
					}
				}

				public Type m_EnumType;
				public Enum m_MappedType;
				public Type m_DataType = null;
				public string m_AttributeName = null;

				public bool m_bMin = false; // (float) if box is empty
				public float m_Min = 0;
				public bool m_bMax = false; // (float) if box is empty
				public float m_Max = 0;
				public bool m_bScale = false; // (float) if box is empty
				public float m_Scale = 0;
				public bool m_bDefault = false; // Used for default checkbox
				public float m_Default = 0;
				public bool m_bDelta = false; // (float) if box is empty
				public float m_Delta = 0;

				public string m_strDefault = null; // Used for string type

				

				// public string m_Base; // Replaced by attributeName
				// public string m_Type; // Replaced by m_MappedType
			}
		}



		public enum LDN //Line Data Names
		{
			[EditableType(typeof(string))]
			NULL = 0,
			//[EditableType(typeof(Color))]
			//LDN_COLOUR, // not used, write rgba instead - float[4]
			[EditableType(typeof(float))]
			R, // r - float
			[EditableType(typeof(float))]
			G, // g - float
			[EditableType(typeof(float))]
			B, // b - float
			[EditableType(typeof(float))]
			A, // a - float
			[EditableType(typeof(bool))]
			DRAW, // should draw? - bool
			[EditableType(typeof(bool))]
			FORCE, // force - bool 
			[EditableType(typeof(float))]
			STRENGTH, // strength - float
			[EditableType(typeof(float))]
			POWER, // power - float
			[EditableType(typeof(float))]
			BASE_LENGTH, // base length - float
		};

		public List<Node> m_nodes = new List<Node>();
		public List<Link> m_links = new List<Link>();
		public List<VisType> m_nodeTypes = new List<VisType>();
		public List<VisType> m_linkTypes = new List<VisType>();
	
		public Dictionary<string, Node> m_NodeMap = new Dictionary<string, Node>();
	}
}
