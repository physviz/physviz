﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisEdit {
	public partial class PackageCreator : Form {
		public PackageCreator() {
			InitializeComponent();
		}

		void CreatePackage(string _packageName) {
			try {
				if (File.Exists(_packageName)) {
					File.Delete(_packageName); // Delete existing
				}

				ZipFile zipfile = new ZipFile(_packageName);

				string datasetPath;
				string dispPath;

				if (useOpenProject.Checked) {
					datasetPath = Path.GetTempPath() + "visedit_" + VisEdit_Main.RandomString(8) + ".json";
					dispPath = datasetPath.Substring(0, datasetPath.LastIndexOf('.')) + ".disp.json";

					VisEdit_Main.s_Instance.SaveFile(datasetPath);
					VisEdit_Main.s_Instance.SaveDispFile(dispPath);
				} else {
					datasetPath = datasetBox.Text;
					dispPath = displayBox.Text;
				}

				string datasetExtension = datasetPath.Substring(datasetPath.LastIndexOf('.'));

				zipfile.AddFile(datasetPath).FileName = "d" + datasetExtension;
				zipfile.AddFile(dispPath).FileName = "d.disp.json";

				if (!string.IsNullOrEmpty(importerBox.Text)) {
					zipfile.AddFile(importerBox.Text).FileName = "importer.dll";
				}

				for (int i = 0; i < textureList.Items.Count; i++) {
					zipfile.AddFile(textureList.Items[i].ToString(), "textures");
				}

				for (int i = 0; i < extraAssets.Items.Count; i++) {
					zipfile.AddFile(extraAssets.Items[i].ToString(), "misc");
				}

				zipfile.Save();

				if (useOpenProject.Checked) {
					// clear temp files
					File.Delete(datasetPath);
					File.Delete(dispPath);
				}

				MessageBox.Show(null, "Successfully saved PhysVis package \"" + _packageName + "\"!", "VisEdit",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
			} catch (Exception e) {
				MessageBox.Show(null, "EXCEPTION: " + e.ToString(), "VisEdit Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		private void useOpenProject_CheckedChanged(object sender, EventArgs e) {
			datasetBox.Enabled = !useOpenProject.Checked;
			datasetButton.Enabled = !useOpenProject.Checked;

			displayBox.Enabled = !useOpenProject.Checked;
			displayButton.Enabled = !useOpenProject.Checked;
		}

		private void datasetButton_Click(object sender, EventArgs e) {
			openDatasetDialog.ShowDialog();
			if (!string.IsNullOrEmpty(openDatasetDialog.FileName)) {
				datasetBox.Text = openDatasetDialog.FileName;
			}
		}

		private void displayButton_Click(object sender, EventArgs e) {
			openDispDialog.ShowDialog();
			if (!string.IsNullOrEmpty(openDispDialog.FileName)) {
				displayBox.Text = openDispDialog.FileName;
			}
		}

		private void importerButton_Click(object sender, EventArgs e) {
			addDLLDialog.ShowDialog();
			if (!string.IsNullOrEmpty(addDLLDialog.FileName)) {
				importerBox.Text = addDLLDialog.FileName;
			}
		}

		private void exportButton_Click(object sender, EventArgs e) {
			if(!useOpenProject.Checked) {
				if(!File.Exists(datasetBox.Text)) {
					MessageBox.Show("Please include a valid dataset or use the open project!", "Package Creator Error", MessageBoxButtons.OK);
					return;
				}

				if (!File.Exists(displayBox.Text)) {
					MessageBox.Show("Please include a valid display file or use the open project!", "Package Creator Error", MessageBoxButtons.OK);
					return;
				}

				if (!File.Exists(importerBox.Text)) {
					MessageBox.Show("Could not locate importer file!", "Package Creator Error", MessageBoxButtons.OK);
					return;
				}
			}

			if (savePackageDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
				CreatePackage(savePackageDialog.FileName);
			}
		}

		private void addTextureButton_Click(object sender, EventArgs e) {
			addTextureDialog.ShowDialog();
			for (int i = 0; i < addTextureDialog.FileNames.Length; i++) {
				if(!textureList.Items.Contains(addTextureDialog.FileNames[i])) {
					textureList.Items.Add(addTextureDialog.FileNames[i]);
				}
			}
		}

		private void removeTextureButton_Click(object sender, EventArgs e) {
			if(textureList.SelectedIndex == -1) {
				return;
			}

			textureList.Items.RemoveAt(textureList.SelectedIndex);
		}

		private void addExtraAsset_Click(object sender, EventArgs e) {
			addOtherAssets.ShowDialog();
			for (int i = 0; i < addOtherAssets.FileNames.Length; i++) {
				if (!extraAssets.Items.Contains(addOtherAssets.FileNames[i])) {
					extraAssets.Items.Add(addOtherAssets.FileNames[i]);
				}
			}
		}

		private void removeExtraAsset_Click(object sender, EventArgs e) {
			if (extraAssets.SelectedIndex == -1) {
				return;
			}

			extraAssets.Items.RemoveAt(extraAssets.SelectedIndex);
		}
	}
}
