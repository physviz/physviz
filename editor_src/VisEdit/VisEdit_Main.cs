﻿using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static VisEdit.Document.Graph;
using static VisEdit.Document.Graph.VisType;
using VisEdit.XYZ;

// Rough prototype of VisEdit, an editor for PhysVis v0.01

// SUPPORTS:
// JSON Datasets and disp files
// Adding and removing nodes
// Editing node attributes
// Editing disp types
// Editing links (sort of)

// DOES NOT SUPPORT (yet):
// XYZ Datasets

namespace VisEdit {
	public partial class VisEdit_Main : Form {
		public VisEdit_Main() {
			s_Instance = this;
			InitializeComponent();

			nodeList_SelectedIndexChanged(null, EventArgs.Empty);
		}

		public static VisEdit_Main s_Instance;
		Document.Graph g_CurrentGraph = new Document.Graph();

		private void openToolStripMenuItem_Click(object sender, EventArgs e) {
			// TODO: Move this somewhere sensible
			if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
				LoadFile(openFileDialog1.FileName);
			}
		}

		void LoadFile(string _filename) {
			try {
				System.IO.StreamReader sr = new
				   System.IO.StreamReader(_filename);
				string file = sr.ReadToEnd();
				sr.Close();

				Document.Graph newGraph = new Document.Graph();

                string[] parts = _filename.Split('.');

                if (parts[parts.Length - 1] == "json")
                {
                    if (parts[parts.Length - 2] == "disp")
                    {
                        //can't load a .disp.json here
                        throw new System.Exception("Can't load .disp.json files directly.");
                    }

                    JSONObject root_json = new JSONObject(file);

                    // Pull out all the nodes
                    JSONObject nodes = root_json["nodes"];
                    for (int i = 0; i < nodes.Count; i++)
                    {
                        Document.Graph.Node newNode = new Document.Graph.Node();
                        newNode.m_name = nodes[i]["name"].str;
                        newNode.m_type = nodes[i]["type"].str;
                        JSONObject attrs = nodes[i]["attributes"];
                        for (int j = 0; attrs != null && attrs.Count > 0 && j < attrs[0].Count; j++)
                        {
                            if (attrs[0].Count > 0)
                            {
                                newNode.m_attributes.Add(new KeyValuePair<string, string>(attrs[0][j].keys[0], attrs[0][j].list[0].ToString()));
                            }
                        }

                        newGraph.m_nodes.Add(newNode);
                        if (!newGraph.m_NodeMap.ContainsKey(newNode.m_name))
                        {
                            newGraph.m_NodeMap.Add(newNode.m_name, newNode); // Couldn't actually find any handling for this in PhysVis itself...
                        }
                    }

                    // Pull out all the links
                    JSONObject links = root_json["relations"];
                    for (int i = 0; i < links.Count; i++)
                    {
                        Document.Graph.Link newLink = new Document.Graph.Link();
                        newLink.m_from = newGraph.m_NodeMap[links[i]["from"].str];
                        newLink.m_fromStr = links[i]["from"].str;
                        newLink.m_from.m_Links.Add(newLink);
                        newLink.m_to = newGraph.m_NodeMap[links[i]["to"].str];
                        newLink.m_toStr = links[i]["to"].str;
                        newLink.m_to.m_Links.Add(newLink);
                        newLink.m_type = links[i]["type"].str;

                        newGraph.m_links.Add(newLink);
                    }

                }
                else if (parts[parts.Length - 1] == "xyz")
                {
                    //LOAD XYZ FILE
                    char[] seps = new char[] { ' ' };

                    //Load Colours
                    Dictionary<string,XYZColour> colourMap = new Dictionary<string, XYZColour> ();
                    string location = _filename.Substring(0,_filename.LastIndexOf('\\')+1);
                    System.IO.StreamReader cm = new System.IO.StreamReader(location+"rgb.txt");
                    string line; 
                    while ((line = cm.ReadLine())!=null)
                    {
                        if( line[0]=='!')
                        {
                            //this line is a comment onto the next line
                            continue;
                        }
                        string[] cmdata = line.Split(seps, StringSplitOptions.RemoveEmptyEntries);
                        XYZColour newColour = new XYZColour();
                        string colName = cmdata[3];
                        for( int i=4;i<cmdata.Length;i++)
                        {
                            //put multi-word names back together again
                            colName += ' ' + cmdata[i];
                        }
                        newColour.m_R = int.Parse(cmdata[0]);
                        newColour.m_G = int.Parse(cmdata[1]);
                        newColour.m_B = int.Parse(cmdata[2]);

                        colourMap[colName] = newColour;
                        Console.WriteLine(colName);
                    }
                    cm.Close();

                    //Load XYZ.types file (make static so only do the first time)

                    //now load the actual XYZ file data

                    string[] xyz = file.Split('\n');

                    int num_atoms = int.Parse(xyz[0]);

                    string axes = "xyz";
                    for(int i=0;i<num_atoms;i++)
                    {
                        string[] data = xyz[i + 2].Split(seps, StringSplitOptions.RemoveEmptyEntries);

                        Document.Graph.Node newNode = new Document.Graph.Node();
                        newNode.m_name = data[0];
                        newNode.m_type = "atom";
                    }


                }

                g_CurrentGraph = newGraph;

				PopulateNodeScreen();

                // only pull in MY disp files
                string strippedFileName = _filename.Substring(0, _filename.LastIndexOf('.'));
                strippedFileName = strippedFileName.Substring(1+strippedFileName.LastIndexOf('\\'));
                this.openFileDialog2.Filter = "display JSON Dataset|"+ strippedFileName + "*.disp.json";
                if (openFileDialog2.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    LoadDispFile(openFileDialog2.FileName);
                }

				SetupTypeBoxes();
				SetupNodeLists();
				PopulateLinkList();

			} catch (System.Exception ex){
				MessageBox.Show(ex.Message+"\n\nFailed to load file \"" + openFileDialog1.FileName + "\".", "VisEdit Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}

		void LoadDispFile(string _disp_filename) {
			try {
                //string disp_filename = openFileDialog1.FileName.Substring(0, openFileDialog1.FileName.LastIndexOf('.')) + ".disp.json";
                string[] parts = _disp_filename.Split('.');
                if(parts.Length ==5)
                {
                    DisplayName.Text = parts[1];
                }
                else
                {
                    DisplayName.Text = "";
                }
				System.IO.StreamReader disp = new
				   System.IO.StreamReader(_disp_filename);
				string disp_file = disp.ReadToEnd();
				disp.Close();

				JSONObject root_disp = new JSONObject(disp_file);

				// Pull out all the nodes
				JSONObject disp_nodes = root_disp["nodes"];
				for (int i = 0; i < disp_nodes.Count; i++) {
					Document.Graph.VisType newNodeType = new Document.Graph.VisType();
                    if(disp_nodes[i].HasField("type"))
                    {
                        newNodeType.m_Name = disp_nodes[i]["type"].str;
                    }
					else
                    {
                        newNodeType.m_Name = disp_nodes[i]["node"].str +" ---NODE---";
                    }

					// Textures are special! pull these out first
					if (disp_nodes[i].HasField("PDN_TEXTURE")) {
						AttributeMapper texturemap = new AttributeMapper(PDN.PDN_TEXTURE);
						texturemap.m_strDefault = disp_nodes[i]["PDN_TEXTURE"].str;
						newNodeType.m_Attributes.Add(texturemap);
					}

					// Build our attribute map
					JSONObject attrs = disp_nodes[i]["attributes"];
					for (int j = 0; attrs != null && attrs.Count > 0 && j < attrs[0].Count; j++) {
						if (attrs[0].Count > 0) {
							AttributeMapper newMapper = new AttributeMapper(attrs[0][j], typeof(PDN));                            
							newNodeType.m_Attributes.Add(newMapper);
						}
					}

					g_CurrentGraph.m_nodeTypes.Add(newNodeType);
				}

				// Pull out all the links
				JSONObject disp_links = root_disp["relations"];
				for (int i = 0; i < disp_links.Count; i++) {
					Document.Graph.VisType newLinkType = new Document.Graph.VisType();
					newLinkType.m_Name = disp_links[i]["type"].str;

					// Build our attribute map
					JSONObject attrs = disp_links[i]["attributes"];
					for (int j = 0; attrs != null && attrs.Count > 0 && j < attrs[0].Count; j++) {
						if (attrs[0].Count > 0) {
							// Doh! LDN entries are far simpler, so need alternate parsing...
							// (so much for writing one catch-all disp attribute parser, maybe I should have studied the format closer :s)
							AttributeMapper newMapper = new AttributeMapper((Enum)Enum.Parse(typeof(LDN), attrs[0][j].keys[0]));
							if (newMapper.m_DataType == typeof(string)) {
								newMapper.m_strDefault = attrs[0][j].list[0].str;
							}

							if (newMapper.m_DataType == typeof(float)) {
								newMapper.m_bDefault = true;
								newMapper.m_Default = attrs[0][j].list[0].f;
							}

							if (newMapper.m_DataType == typeof(bool)) {
								newMapper.m_bDefault = attrs[0][j].list[0].f == 1.0f;
							}

							newLinkType.m_Attributes.Add(newMapper);
						}
					}

					g_CurrentGraph.m_linkTypes.Add(newLinkType);
				}

				PopulateDispLists();
			} catch (Exception exception) {
				if (MessageBox.Show("Failed to load disp file!\n\nCreate defaults from dataset? (recommended!)", "VisEdit Error!",
					MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes) {
					// find default types from our data set
					List<string> _nodeTypes = new List<string>();
					for (int i = 0; i < g_CurrentGraph.m_nodes.Count; i++) {
						if (!_nodeTypes.Contains(g_CurrentGraph.m_nodes[i].m_type)) {
							_nodeTypes.Add(g_CurrentGraph.m_nodes[i].m_type);
						}
					}

					for (int i = 0; i < _nodeTypes.Count; i++) {
						VisType vType = new VisType();
						vType.m_Name = _nodeTypes[i];
						g_CurrentGraph.m_nodeTypes.Add(vType);
					}

					List<string> _linkTypes = new List<string>();
					for (int i = 0; i < g_CurrentGraph.m_links.Count; i++) {
						if (!_linkTypes.Contains(g_CurrentGraph.m_links[i].m_type)) {
							_linkTypes.Add(g_CurrentGraph.m_links[i].m_type);
						}
					}

					for (int i = 0; i < _linkTypes.Count; i++) {
						VisType vType = new VisType();
						vType.m_Name = _linkTypes[i];
						g_CurrentGraph.m_linkTypes.Add(vType);
					}

					PopulateDispLists();
				}
			}
		}

		private static Random random = new Random();
		public static string RandomString(int length) {
			const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			return new string(Enumerable.Repeat(chars, length)
			  .Select(s => s[random.Next(s.Length)]).ToArray());
		}

		void CreatePackage(string _packageName) {
			if(File.Exists(_packageName)) {
				File.Delete(_packageName); // Delete existing
			}

			ZipFile zipfile = new ZipFile(_packageName);

			string tempFile = Path.GetTempPath() + "visedit_" + RandomString(8) + ".json";
			SaveFile(tempFile);
			string tempFileDisp = tempFile.Substring(0, tempFile.LastIndexOf('.')) + ".disp.json";
			SaveDispFile(tempFileDisp);

			zipfile.AddFile(tempFile).FileName = "d.json";
			zipfile.AddFile(tempFileDisp).FileName = "d.disp.json";

			if (MessageBox.Show("Add textures to package file?", "VisEdit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
				texturePickDialog.ShowDialog();
				if (texturePickDialog.FileNames.Length > 0) {
					// zipfile.Add
					zipfile.AddFiles(texturePickDialog.FileNames, "textures");
				}
			}

			if (MessageBox.Show("Add custom importer DLL to package file?", "VisEdit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) {
				dllPickerDialog.ShowDialog();
				if (!string.IsNullOrEmpty(dllPickerDialog.FileName)) {
					// zipfile.Add
					zipfile.AddFile(dllPickerDialog.FileName).FileName = "importer.dll";
				}
			}

			zipfile.Save();

			// clear temp files
			File.Delete(tempFile);
			File.Delete(tempFileDisp);
		}

		public void SaveFile(string _filename) {
			JSONObject root = new JSONObject();
			JSONObject nodes = root.GetOrAddNode("nodes");
			for(int i = 0; i < g_CurrentGraph.m_nodes.Count; i++) {
				JSONObject newNode = new JSONObject();
				newNode.AddField("name", g_CurrentGraph.m_nodes[i].m_name);
				newNode.AddField("type", g_CurrentGraph.m_nodes[i].m_type);
				JSONObject attr_root = new JSONObject(JSONObject.Type.ARRAY);
				JSONObject attr = new JSONObject(JSONObject.Type.ARRAY);
				for(int j = 0; j < g_CurrentGraph.m_nodes[i].m_attributes.Count; j++) {
					JSONObject newAttr = new JSONObject();
					newAttr.AddField(g_CurrentGraph.m_nodes[i].m_attributes[j].Key, float.Parse(g_CurrentGraph.m_nodes[i].m_attributes[j].Value));
					attr.Add(newAttr);
				}
				if (g_CurrentGraph.m_nodes[i].m_attributes.Count > 0) {
					attr_root.Add(attr);
				}

				newNode.AddField("attributes", attr_root);
				nodes.Add(newNode);
			}

			JSONObject links = root.GetOrAddNode("relations");
			for (int i = 0; i < g_CurrentGraph.m_links.Count; i++) {
				if(string.IsNullOrEmpty(g_CurrentGraph.m_links[i].m_fromStr) ||
					string.IsNullOrEmpty(g_CurrentGraph.m_links[i].m_toStr) ||
					string.IsNullOrEmpty(g_CurrentGraph.m_links[i].m_type)) {
					continue; // SKIP IF ANY OF THESE VALUES ARE NULL TO AVOID BREAKING PHYSVIS PARSING
				}

				JSONObject newNode = new JSONObject();
				newNode.AddField("from", g_CurrentGraph.m_links[i].m_fromStr);
				newNode.AddField("to", g_CurrentGraph.m_links[i].m_toStr);
				newNode.AddField("type", g_CurrentGraph.m_links[i].m_type);
				links.Add(newNode);
			}

			using (System.IO.StreamWriter file =
			new System.IO.StreamWriter(_filename)) {
				file.Write(root.ToString(true));
			}
		}
		
		public void SaveDispFile(string _filename_disp) {
			JSONObject root = new JSONObject();
			JSONObject nodes = root.GetOrAddNode("nodes");
			for (int i = 0; i < g_CurrentGraph.m_nodeTypes.Count; i++) {
				JSONObject newNode = new JSONObject();
                //pull out if have the --NODE-- and add as a node NOT a type
                if (g_CurrentGraph.m_nodeTypes[i].m_Name.EndsWith("---NODE---"))
                {
                    string name = g_CurrentGraph.m_nodeTypes[i].m_Name.Replace("---NODE---", "");
                    newNode.AddField("node", name);
                }
                else
                {
                    newNode.AddField("type", g_CurrentGraph.m_nodeTypes[i].m_Name);
                }
				JSONObject attr_root = new JSONObject(JSONObject.Type.ARRAY);
				JSONObject attr = new JSONObject(JSONObject.Type.ARRAY);
				for (int j = 0; j < g_CurrentGraph.m_nodeTypes[i].m_Attributes.Count; j++) {
					var attribs = g_CurrentGraph.m_nodeTypes[i].m_Attributes;
					if ((PDN)attribs[j].m_MappedType == PDN.PDN_TEXTURE) {
						// This is special, it must be written differently!
						newNode.AddField("PDN_TEXTURE", attribs[j].m_strDefault);
						continue;
					}

					JSONObject newAttr = new JSONObject();
					newAttr.AddField("type", attribs[j].m_MappedType.ToString());
					if (!string.IsNullOrEmpty(attribs[j].m_AttributeName)) {
						newAttr.AddField("base", attribs[j].m_AttributeName);
					}

					if (attribs[j].m_DataType == typeof(bool)) {
						newAttr.AddField("default", attribs[j].m_bDefault ? 1.0f : 0.0f); // bools are written as floats for some reason?
					}

					if (attribs[j].m_DataType == typeof(float)) {
						if (attribs[j].m_bDefault) {
							newAttr.AddField("default", attribs[j].m_Default);
						}

						if (attribs[j].m_bMin) {
							newAttr.AddField("min", attribs[j].m_Min);
						}

						if (attribs[j].m_bMax) {
							newAttr.AddField("max", attribs[j].m_Max);
						}

						if (attribs[j].m_bDelta) {
							newAttr.AddField("delta", attribs[j].m_Delta);
						}

						if (attribs[j].m_bScale) {
							newAttr.AddField("scale", attribs[j].m_Scale);
						}
					}

					if (attribs[j].m_DataType == typeof(string)) {
						if (attribs[j].m_bDefault && !string.IsNullOrEmpty(attribs[j].m_strDefault)) {
							newAttr.AddField("default", attribs[j].m_strDefault);
						}
					}

					attr.Add(newAttr);
				}

				/*if (g_CurrentGraph.m_nodeTypes[i].m_Attributes.Count > 0)*/ {
					attr_root.Add(attr);
				}

				newNode.AddField("attributes", attr_root);
				nodes.Add(newNode);
			}

			JSONObject links = root.GetOrAddNode("relations");
			for (int i = 0; i < g_CurrentGraph.m_linkTypes.Count; i++) {
				JSONObject newNode = new JSONObject();
				newNode.AddField("type", g_CurrentGraph.m_linkTypes[i].m_Name);
				JSONObject attr_root = new JSONObject(JSONObject.Type.ARRAY);
				JSONObject attr = new JSONObject(JSONObject.Type.ARRAY);
				for (int j = 0; j < g_CurrentGraph.m_linkTypes[i].m_Attributes.Count; j++) {
					var attribs = g_CurrentGraph.m_linkTypes[i].m_Attributes;
					JSONObject newAttr = new JSONObject();
					if (attribs[j].m_DataType == typeof(bool)) {
						newAttr.AddField(attribs[j].m_MappedType.ToString(), attribs[j].m_bDefault ? 1.0f : 0.0f); // bools are floats and up is down
					}

					if (attribs[j].m_DataType == typeof(float)) {
						if (attribs[j].m_bDefault) {
							newAttr.AddField(attribs[j].m_MappedType.ToString(), attribs[j].m_Default);
						}
					}

					if (attribs[j].m_DataType == typeof(string)) {
						if (attribs[j].m_bDefault && !string.IsNullOrEmpty(attribs[j].m_strDefault)) {
							newAttr.AddField(attribs[j].m_MappedType.ToString(), attribs[j].m_strDefault);
						}
					}

					attr.Add(newAttr);
				}
				attr_root.Add(attr); // double nested array for no reason argghhhh
				newNode.AddField("attributes", attr_root);
				links.Add(newNode);
			}

			using (System.IO.StreamWriter file =
			new System.IO.StreamWriter(_filename_disp)) {
				file.Write(root.ToString(true));
			}
		}

		void PopulateNodeScreen() {
			nodeList.ClearSelected();
			nodeList.Items.Clear();

			for(int i = 0; i < g_CurrentGraph.m_nodes.Count; i++) {
				nodeList.Items.Add(g_CurrentGraph.m_nodes[i].m_name);
			}
		}

		private void saveToolStripMenuItem_Click(object sender, EventArgs e) {

		}

		private void saveAsToolStripMenuItem_Click(object sender, EventArgs e) {
			if (saveFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
				SaveFile(saveFileDialog1.FileName);
                string disp_filename = saveFileDialog1.FileName.Substring(0, saveFileDialog1.FileName.LastIndexOf('.'));
               
                if (DisplayName.Text!="")
                {
                    disp_filename += "."+DisplayName.Text;
                }

                disp_filename +=  ".json.disp.json";
                SaveDispFile(disp_filename);
			}
		}

		private void importDatasetToolStripMenuItem_Click(object sender, EventArgs e) {

		}

		private void exportToolStripMenuItem_Click(object sender, EventArgs e) {
			/*if (savePackageDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
				CreatePackage(savePackageDialog.FileName);
			}*/

			PackageCreator pkgMkr = new PackageCreator();
			pkgMkr.ShowDialog();
		}

		private void quitToolStripMenuItem_Click(object sender, EventArgs e) {
			Application.Exit();
		}

		private void visEdit001aToolStripMenuItem_Click(object sender, EventArgs e) {
			About abt = new About();
			abt.StartPosition = FormStartPosition.CenterParent;
			abt.ShowDialog();
		}

		private void nodeList_SelectedIndexChanged(object sender, EventArgs e) {
			if (nodeList.SelectedIndex == -1) {
				attributeList.SelectedIndex = -1;
				attributeList.Items.Clear();
				linkList.SelectedIndex = -1;
				linkList.Items.Clear();
				nodeNameTextbox.Text = "";
				dropdownLinkTypes.Text = "";

				attributeList.Enabled = false;
				linkList.Enabled = false;
				nodeNameTextbox.Enabled = false;
				dropdownNodeType.Enabled = false;
				dropdownLinkTypes.Enabled = false;
				linkFromTextBox.Enabled = false;
				linkToTextBox.Enabled = false;
				attributeValTextBox.Enabled = false;
				editLinkButton.Enabled = false;

				return;
			}

			Document.Graph.Node curNode = g_CurrentGraph.m_nodes[nodeList.SelectedIndex];
			nodeNameTextbox.Text = curNode.m_name;
			dropdownNodeType.SelectedItem = g_CurrentGraph.m_nodes[nodeList.SelectedIndex].m_type;
			if (string.IsNullOrEmpty(g_CurrentGraph.m_nodes[nodeList.SelectedIndex].m_type)) {
				dropdownNodeType.Text = "<MISSING_TYPE>";
			}
			dropdownLinkTypes.SelectedIndex = -1;

			attributeList.Enabled = true;
			linkList.Enabled = true;
			nodeNameTextbox.Enabled = true;
			dropdownNodeType.Enabled = true;
			//linkFromTextBox.Enabled = true;
			//linkToTextBox.Enabled = true;
			attributeValTextBox.Enabled = true;

			attributeList.SelectedIndex = -1;
			attributeList.Items.Clear();
			for (int i = 0; i < curNode.m_attributes.Count; i++) {
				attributeList.Items.Add(curNode.m_attributes[i].Key);
			}

			linkList.SelectedIndex = -1;
			linkList.Items.Clear();
			for (int i = 0; i < curNode.m_Links.Count; i++) {
				linkList.Items.Add(curNode.m_Links[i].ToString());
			}

			if(m_PendingLink != null) {
				linkList.SelectedIndex = curNode.m_Links.IndexOf(m_PendingLink);
				m_PendingLink = null;
			}
		}

		private void attributeList_SelectedIndexChanged(object sender, EventArgs e) {
			if(attributeList.SelectedIndex == -1 || nodeList.SelectedIndex == -1) {
				attributeValTextBox.Text = "";
				return;
			}

			Document.Graph.Node curNode = g_CurrentGraph.m_nodes[nodeList.SelectedIndex];
			attributeValTextBox.Text = curNode.m_attributes[attributeList.SelectedIndex].Value;
		}

		private void linkList_SelectedIndexChanged(object sender, EventArgs e) {
			editLinkButton.Enabled = false;
			dropdownLinkTypes.Enabled = false;

			if (linkList.SelectedIndex == -1 || nodeList.SelectedIndex == -1) {
				dropdownLinkTypes.Text = "";
				linkFromTextBox.Text = "";
				linkToTextBox.Text = "";
				return;
			}

			editLinkButton.Enabled = true;
			dropdownLinkTypes.Enabled = true;

			Document.Graph.Node curNode = g_CurrentGraph.m_nodes[nodeList.SelectedIndex];
			dropdownLinkTypes.Text = curNode.m_Links[linkList.SelectedIndex].m_type;
			linkFromTextBox.Text = curNode.m_Links[linkList.SelectedIndex].m_from?.m_name;
			linkToTextBox.Text = curNode.m_Links[linkList.SelectedIndex].m_to?.m_name;
		}

		private void linkFromGoButton_Click(object sender, EventArgs e) {
			if (linkList.SelectedIndex == -1 || nodeList.SelectedIndex == -1) {
				return;
			}

			Document.Graph.Node curNode = g_CurrentGraph.m_nodes[nodeList.SelectedIndex];
			int nIdx = nodeList.Items.IndexOf(curNode.m_Links[linkList.SelectedIndex].m_from.m_name);
			m_PendingLink = curNode.m_Links[linkList.SelectedIndex];
			nodeList.SelectedIndex = nIdx;
		}

		Document.Graph.Link m_PendingLink = null;
		private void linkToGoButton_Click(object sender, EventArgs e) {
			if (linkList.SelectedIndex == -1 || nodeList.SelectedIndex == -1) {
				return;
			}

			Document.Graph.Node curNode = g_CurrentGraph.m_nodes[nodeList.SelectedIndex];
			int nIdx = nodeList.Items.IndexOf(curNode.m_Links[linkList.SelectedIndex].m_to.m_name);
			m_PendingLink = curNode.m_Links[linkList.SelectedIndex];
			nodeList.SelectedIndex = nIdx;
		}

		private void nodeNameTextbox_TextChanged(object sender, EventArgs e) {
			if (nodeList.SelectedIndex == -1) {
				return;
			}

			Document.Graph.Node curNode = g_CurrentGraph.m_nodes[nodeList.SelectedIndex];
			curNode.m_name = nodeNameTextbox.Text;
		}

		private void dropdownNodeType_SelectedIndexChanged(object sender, EventArgs e) {
			if (nodeList.SelectedIndex == -1 || dropdownNodeType.SelectedIndex == -1 || dropdownNodeType.SelectedItem == null) {
				return;
			}

			Document.Graph.Node curNode = g_CurrentGraph.m_nodes[nodeList.SelectedIndex];
			curNode.m_type = dropdownNodeType.SelectedItem.ToString();
		}

		private void attributeValTextBox_TextChanged(object sender, EventArgs e) {
			if (attributeList.SelectedIndex == -1 || nodeList.SelectedIndex == -1) {
				return;
			}

			Document.Graph.Node curNode = g_CurrentGraph.m_nodes[nodeList.SelectedIndex];
			curNode.m_attributes[attributeList.SelectedIndex] = new KeyValuePair<string, string>(curNode.m_attributes[attributeList.SelectedIndex].Key, attributeValTextBox.Text);
		}

		// TODO: Update node's internal links
		void UpdateGoToLinkButtons() {
			if (linkList.SelectedIndex == -1 || nodeList.SelectedIndex == -1) {
				linkFromGoButton.Enabled = false;
				linkToGoButton.Enabled = false;
				return;
			}

			Document.Graph.Node curNode = g_CurrentGraph.m_nodes[nodeList.SelectedIndex];

			// TODO Update node's internal list of links
			linkFromGoButton.Enabled = g_CurrentGraph.m_NodeMap.ContainsKey(linkFromTextBox.Text);
			linkToGoButton.Enabled = g_CurrentGraph.m_NodeMap.ContainsKey(linkToTextBox.Text);

			int nIdx = linkList.SelectedIndex;
			linkList.SelectedIndexChanged -= linkList_SelectedIndexChanged; // HACK HACK
			linkList.Items[nIdx] = curNode.m_Links[linkList.SelectedIndex].ToString();
			linkList.SelectedIndex = nIdx;
			linkList.SelectedIndexChanged += linkList_SelectedIndexChanged;
		}

		private void linkFromTextBox_TextChanged(object sender, EventArgs e) {
			if (linkList.SelectedIndex == -1 || nodeList.SelectedIndex == -1) {
				return;
			}

			Document.Graph.Node curNode = g_CurrentGraph.m_nodes[nodeList.SelectedIndex];
			curNode.m_Links[linkList.SelectedIndex].m_fromStr = linkFromTextBox.Text;

			if(g_CurrentGraph.m_NodeMap.ContainsKey(linkFromTextBox.Text)) {
				curNode.m_Links[linkList.SelectedIndex].m_from = g_CurrentGraph.m_NodeMap[linkFromTextBox.Text];
			} else {
				curNode.m_Links[linkList.SelectedIndex].m_from = null;
			}

			UpdateGoToLinkButtons();
		}

		private void linkToTextBox_TextChanged(object sender, EventArgs e) {
			if (linkList.SelectedIndex == -1 || nodeList.SelectedIndex == -1) {
				return;
			}

			Document.Graph.Node curNode = g_CurrentGraph.m_nodes[nodeList.SelectedIndex];
			curNode.m_Links[linkList.SelectedIndex].m_toStr = linkToTextBox.Text;

			if (g_CurrentGraph.m_NodeMap.ContainsKey(linkToTextBox.Text)) {
				curNode.m_Links[linkList.SelectedIndex].m_to = g_CurrentGraph.m_NodeMap[linkToTextBox.Text];
			} else {
				curNode.m_Links[linkList.SelectedIndex].m_to = null;
			}

			UpdateGoToLinkButtons();
		}

		private void dropdownLinkTypes_SelectedIndexChanged(object sender, EventArgs e) {
			if (linkList.SelectedIndex == -1 || nodeList.SelectedIndex == -1 || dropdownLinkTypes.SelectedItem == null) {
				return;
			}

			Document.Graph.Node curNode = g_CurrentGraph.m_nodes[nodeList.SelectedIndex];
			curNode.m_Links[linkList.SelectedIndex].m_type = dropdownLinkTypes.SelectedItem.ToString();
		}

		ComboBox[] m_NodeTypeBoxes;
		ComboBox[] m_LineTypeBoxes;
		ComboBox[] m_NodeAttributeBoxes;
		ComboBox[] m_ListAttributeBoxes;
		void SetupTypeBoxes() {
			m_NodeTypeBoxes = new ComboBox[] {
				dropdownNodeType
			};

			for (int i = 0; i < m_NodeTypeBoxes.Length; i++) {
				m_NodeTypeBoxes[i].AutoCompleteMode = AutoCompleteMode.SuggestAppend;
				m_NodeTypeBoxes[i].AutoCompleteSource = AutoCompleteSource.ListItems;
				m_NodeTypeBoxes[i].Items.Clear();

				for (int j = 0; j < g_CurrentGraph.m_nodeTypes.Count; j++) {
					m_NodeTypeBoxes[i].Items.Add(g_CurrentGraph.m_nodeTypes[j].m_Name);
				}
			}

			m_LineTypeBoxes = new ComboBox[] {
				dropdownLinkTypes,
				link_typeBox
			};

			for (int i = 0; i < m_LineTypeBoxes.Length; i++) {
				m_LineTypeBoxes[i].AutoCompleteMode = AutoCompleteMode.SuggestAppend;
				m_LineTypeBoxes[i].AutoCompleteSource = AutoCompleteSource.ListItems;
				m_LineTypeBoxes[i].Items.Clear();

				for (int j = 0; j < g_CurrentGraph.m_linkTypes.Count; j++) {
					m_LineTypeBoxes[i].Items.Add(g_CurrentGraph.m_linkTypes[j].m_Name);
				}
			}

			m_NodeAttributeBoxes = new ComboBox[] {
				disp_NewAttrib
			};

			var PDNCount = Enum.GetNames(typeof(PDN)).Length;
			for (int i = 0; i < m_NodeAttributeBoxes.Length; i++) {
				m_NodeAttributeBoxes[i].Items.Clear();
				for (int j = 0; j < PDNCount; j++) {
					m_NodeAttributeBoxes[i].Items.Add(((PDN)j).ToString());
				}
			}

			m_ListAttributeBoxes = new ComboBox[] {
				
			};

			var LDNCount = Enum.GetNames(typeof(LDN)).Length;
			for (int i = 0; i < m_ListAttributeBoxes.Length; i++) {
				for (int j = 0; j < PDNCount; j++) {
					m_ListAttributeBoxes[i].Items.Add(((LDN)j).ToString());
				}
			}
		}

		private void disp_AddNodeAttr_Click(object sender, EventArgs e) {
			if(disp_NodeTypeList.SelectedIndex == -1 || disp_NewAttrib.SelectedItem == null) {
				return;
			}

			for (int i = 0; i < g_CurrentGraph.m_nodeTypes[disp_NodeTypeList.SelectedIndex].m_Attributes.Count; i++) {
				if(g_CurrentGraph.m_nodeTypes[disp_NodeTypeList.SelectedIndex].m_Attributes[i].m_MappedType.ToString() == disp_NewAttrib.SelectedItem.ToString()) {
					return; // already got this type here
				}
			}


			AttributeMapper newAttr = new AttributeMapper((Enum)Enum.Parse(typeof(PDN), disp_NewAttrib.SelectedItem.ToString()));
			g_CurrentGraph.m_nodeTypes[disp_NodeTypeList.SelectedIndex].m_Attributes.Add(newAttr);

			PopulateNodeAttrList();
		}

		private void disp_RemoveNodeAttr_Click(object sender, EventArgs e) {
			if(disp_NodeTypeList.SelectedIndex == -1 || disp_nodeAttribsList.SelectedIndex == -1) {
				return;
			}

			g_CurrentGraph.m_nodeTypes[disp_NodeTypeList.SelectedIndex].m_Attributes.RemoveAt(disp_nodeAttribsList.SelectedIndex);
			disp_nodeAttribsList.Items.RemoveAt(disp_nodeAttribsList.SelectedIndex);
		}
		
		void PopulateDispLists() {
			disp_NodeTypeList.Items.Clear();
			disp_LinkTypeList.Items.Clear();

			for (int i = 0; i < g_CurrentGraph.m_nodeTypes.Count; i++) {
				disp_NodeTypeList.Items.Add(g_CurrentGraph.m_nodeTypes[i].m_Name);
			}

			for (int i = 0; i < g_CurrentGraph.m_linkTypes.Count; i++) {
				disp_LinkTypeList.Items.Add(g_CurrentGraph.m_linkTypes[i].m_Name);
			}
		}

		void UpdateDispNodeEditPanel() {
			disp_nodeBoolPanel.Visible = false;
			disp_nodeFloatPanel.Visible = false;
			disp_nodeStringPanel.Visible = false;

			disp_nodeBoolPanel.Enabled = false;
			disp_nodeFloatPanel.Enabled = false;
			disp_nodeStringPanel.Enabled = false;

			disp_nodeAttrib.Enabled = false;

			disp_nodeAttrib.Text = "";
			disp_nodeType.Text = "";

			if (disp_NodeTypeList.SelectedIndex == -1 || disp_nodeAttribsList.SelectedIndex == -1) {
				disp_nodeAttribsList.SelectedIndex = -1; // just make sure
				return;
			}

			// bloody hell that's a long line
			AttributeMapper mapper = g_CurrentGraph.m_nodeTypes[disp_NodeTypeList.SelectedIndex].m_Attributes[disp_nodeAttribsList.SelectedIndex];

			disp_nodeAttrib.Enabled = true;
			disp_nodeType.Text = mapper.m_MappedType.ToString();
			disp_nodeAttrib.Text = mapper.m_AttributeName;

			if (mapper.m_DataType == typeof(string)) {
				disp_nodeStringPanel.Visible = true;
				disp_nodeStringPanel.Enabled = true;

				disp_nodeDefaultString.Text = mapper.m_strDefault;
			}

			if (mapper.m_DataType == typeof(float)) {
				disp_nodeFloatPanel.Visible = true;
				disp_nodeFloatPanel.Enabled = true;

				disp_nodeMin.Text = mapper.m_bMin ? mapper.m_Min.ToString() : "";
				disp_nodeMax.Text = mapper.m_bMax ? mapper.m_Max.ToString() : "";
				disp_nodeScale.Text = mapper.m_bScale ? mapper.m_Scale.ToString() : "";
				disp_nodeDefault.Text = mapper.m_bDefault ? mapper.m_Default.ToString() : "";
				disp_nodeDelta.Text = mapper.m_bDelta ? mapper.m_Delta.ToString() : "";
			}

			if (mapper.m_DataType == typeof(bool)) {
				disp_nodeBoolPanel.Visible = true;
				disp_nodeBoolPanel.Enabled = true;

				disp_nodeDefaultBool.Checked = mapper.m_bDefault;
			}
		}

		void UpdateDispLinkEditPanel() {
			if (disp_LinkTypeList.SelectedIndex == -1) {
				disp_linkTypeBox.Text = "";
				disp_linkColourBox.Enabled = false;
				disp_linkStrengthBox.Enabled = false;
				disp_linkPowerBox.Enabled = false;
				disp_linkBaseLengthBox.Enabled = false;
				disp_linkVisible.Enabled = false;
				disp_linkForce.Enabled = false;

				disp_linkColourBox.BackColor = Color.Transparent;
				disp_linkStrengthBox.Text = "";
				disp_linkPowerBox.Text = "";
				disp_linkBaseLengthBox.Text = "";
				disp_linkVisible.Checked = false;
				disp_linkForce.Checked = false;
				return;
			}

			VisType type = g_CurrentGraph.m_linkTypes[disp_LinkTypeList.SelectedIndex];
			Color newCol = Color.FromArgb(Convert.ToInt32(type.GetAttributeOfType(LDN.A, true, 0.0f).m_Default * 255),
				Convert.ToInt32(type.GetAttributeOfType(LDN.R, true, 0.0f).m_Default * 255),
				Convert.ToInt32(type.GetAttributeOfType(LDN.G, true, 0.0f).m_Default * 255),
				Convert.ToInt32(type.GetAttributeOfType(LDN.B, true, 0.0f).m_Default * 255));

			disp_linkTypeBox.Text = type.m_Name;
			disp_linkColourBox.BackColor = newCol;
			disp_linkStrengthBox.Text = type.GetAttributeOfType(LDN.STRENGTH, true, 0.0f).m_Default.ToString();
			disp_linkPowerBox.Text = type.GetAttributeOfType(LDN.POWER, true, 0.0f).m_Default.ToString();
			disp_linkBaseLengthBox.Text = type.GetAttributeOfType(LDN.BASE_LENGTH, true, 0.0f).m_Default.ToString();
			disp_linkVisible.Checked = type.GetAttributeOfType(LDN.DRAW, true, 0.0f).m_bDefault;
			disp_linkForce.Checked = type.GetAttributeOfType(LDN.FORCE, true).m_bDefault;
		}

		private void disp_NodeTypeList_SelectedIndexChanged(object sender, EventArgs e) {
			PopulateNodeAttrList();
		}

		void PopulateNodeAttrList() {
			disp_nodeAttribsList.Items.Clear();

			if (disp_NodeTypeList.SelectedIndex == -1) {
				UpdateDispNodeEditPanel();
				return;
			}

			for (int i = 0; i < g_CurrentGraph.m_nodeTypes[disp_NodeTypeList.SelectedIndex].m_Attributes.Count; i++) {
				disp_nodeAttribsList.Items.Add(g_CurrentGraph.m_nodeTypes[disp_NodeTypeList.SelectedIndex].m_Attributes[i].m_MappedType.ToString());
			}

			UpdateDispNodeEditPanel();
		}

		private void disp_nodeAttribsList_SelectedIndexChanged(object sender, EventArgs e) {
			UpdateDispNodeEditPanel();
		}

		private void disp_linkColourBox_Click(object sender, EventArgs e) {
			if(disp_LinkTypeList.SelectedIndex == -1) {
				return;
			}

			VisType type = g_CurrentGraph.m_linkTypes[disp_LinkTypeList.SelectedIndex];
			Color newCol = Color.FromArgb(Convert.ToInt32(type.GetAttributeOfType(LDN.A, true, 0.0f).m_Default) * 255,
				Convert.ToInt32(type.GetAttributeOfType(LDN.R, true, 0.0f).m_Default) * 255,
				Convert.ToInt32(type.GetAttributeOfType(LDN.G, true, 0.0f).m_Default) * 255,
				Convert.ToInt32(type.GetAttributeOfType(LDN.B, true, 0.0f).m_Default) * 255);
			disp_ColourPicker.Color = newCol;
			if (disp_ColourPicker.ShowDialog() == DialogResult.OK) {
				newCol = disp_ColourPicker.Color;
				type.GetAttributeOfType(LDN.A, true, 0.0f).m_Default = newCol.A / 255;
				type.GetAttributeOfType(LDN.R, true, 0.0f).m_Default = newCol.R / 255;
				type.GetAttributeOfType(LDN.G, true, 0.0f).m_Default = newCol.G / 255;
				type.GetAttributeOfType(LDN.B, true, 0.0f).m_Default = newCol.B / 255;
				disp_linkColourBox.BackColor = newCol;
			}
		}

		private void disp_linkVisible_CheckedChanged(object sender, EventArgs e) {
			if (disp_LinkTypeList.SelectedIndex == -1) {
				return;
			}

			g_CurrentGraph.m_linkTypes[disp_LinkTypeList.SelectedIndex].GetAttributeOfType(LDN.DRAW, true).m_bDefault = disp_linkVisible.Checked;
		}

		private void disp_linkForce_CheckedChanged(object sender, EventArgs e) {
			if (disp_LinkTypeList.SelectedIndex == -1) {
				return;
			}

			g_CurrentGraph.m_linkTypes[disp_LinkTypeList.SelectedIndex].GetAttributeOfType(LDN.FORCE, true).m_bDefault = disp_linkForce.Checked;
		}

		private void disp_linkStrengthBox_TextChanged(object sender, EventArgs e) {
			if (disp_LinkTypeList.SelectedIndex == -1) {
				return;
			}

			try {
				float f = float.Parse(disp_linkStrengthBox.Text);
				g_CurrentGraph.m_linkTypes[disp_LinkTypeList.SelectedIndex].GetAttributeOfType(LDN.STRENGTH, true).m_bDefault = true;
				g_CurrentGraph.m_linkTypes[disp_LinkTypeList.SelectedIndex].GetAttributeOfType(LDN.STRENGTH, true).m_Default = f;
			}
			catch {
				disp_linkStrengthBox.Text = "";
			}
		}

		private void disp_linkPowerBox_TextChanged(object sender, EventArgs e) {
			if (disp_LinkTypeList.SelectedIndex == -1) {
				return;
			}

			try {
				float f = float.Parse(disp_linkPowerBox.Text);
				g_CurrentGraph.m_linkTypes[disp_LinkTypeList.SelectedIndex].GetAttributeOfType(LDN.POWER, true).m_bDefault = true;
				g_CurrentGraph.m_linkTypes[disp_LinkTypeList.SelectedIndex].GetAttributeOfType(LDN.POWER, true).m_Default = f;
			} catch {
				disp_linkPowerBox.Text = "";
			}
		}

		private void disp_linkBaseLengthBox_TextChanged(object sender, EventArgs e) {
			if (disp_LinkTypeList.SelectedIndex == -1) {
				return;
			}

			try {
				float f = float.Parse(disp_linkBaseLengthBox.Text);
				g_CurrentGraph.m_linkTypes[disp_LinkTypeList.SelectedIndex].GetAttributeOfType(LDN.BASE_LENGTH, true).m_bDefault = true;
				g_CurrentGraph.m_linkTypes[disp_LinkTypeList.SelectedIndex].GetAttributeOfType(LDN.BASE_LENGTH, true).m_Default = f;
			} catch {
				disp_linkBaseLengthBox.Text = "";
			}
		}

		private void ValidateNumericalTextBox(object sender, KeyPressEventArgs e) {
			if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
				(e.KeyChar != '.')) {
				e.Handled = true;
			}
			// only allow one decimal point
			if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1)) {
				e.Handled = true;
			}

			// only allow one minus
			if ((e.KeyChar == '-') && ((sender as TextBox).Text.IndexOf('-') > -1)) {
				e.Handled = true;
			}
		}

		private void disp_LinkTypeList_SelectedIndexChanged(object sender, EventArgs e) {
			UpdateDispLinkEditPanel();
		}

		private void disp_nodeTypeAddButton_Click(object sender, EventArgs e) {
			if(string.IsNullOrEmpty(disp_nodeTypeAddBox.Text)) {
				return;
			}

			// check we don't already have one called this
			if(disp_NodeTypeList.Items.Contains(disp_nodeTypeAddBox.Text)) {
				MessageBox.Show("Node type \"" + disp_nodeTypeAddBox.Text + "\" already exists!", "VisEdit Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			VisType newVisType = new VisType();
			newVisType.m_Name = disp_nodeTypeAddBox.Text;
			g_CurrentGraph.m_nodeTypes.Add(newVisType);
			disp_NodeTypeList.Items.Add(disp_nodeTypeAddBox.Text);
			disp_nodeTypeAddBox.Clear();

			disp_NodeTypeList.SelectedIndex = disp_NodeTypeList.Items.Count - 1;

			SetupTypeBoxes();
		}

		private void disp_nodeTypeRemoveButton_Click(object sender, EventArgs e) {
			if (disp_NodeTypeList.SelectedIndex == -1) {
				return;
			}

			RemoveInstancesOfType(disp_NodeTypeList.SelectedItem.ToString(), false);

			g_CurrentGraph.m_nodeTypes.RemoveAt(disp_NodeTypeList.SelectedIndex);
			disp_NodeTypeList.Items.RemoveAt(disp_NodeTypeList.SelectedIndex);

			SetupTypeBoxes();
			nodeList_SelectedIndexChanged(null, EventArgs.Empty);
		}

		private void disp_linkTypeAddButton_Click(object sender, EventArgs e) {
			if (string.IsNullOrEmpty(disp_linkTypeAddBox.Text)) {
				return;
			}

			// check we don't already have one called this
			if (disp_LinkTypeList.Items.Contains(disp_linkTypeAddBox.Text)) {
				MessageBox.Show("Link type \"" + disp_linkTypeAddBox.Text + "\" already exists!", "VisEdit Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			VisType newVisType = new VisType();
			newVisType.m_Name = disp_linkTypeAddBox.Text;
			g_CurrentGraph.m_linkTypes.Add(newVisType);
			disp_LinkTypeList.Items.Add(disp_linkTypeAddBox.Text);
			disp_linkTypeAddBox.Clear();

			disp_LinkTypeList.SelectedIndex = disp_LinkTypeList.Items.Count - 1;

			SetupTypeBoxes();
		}

		private void disp_linkTypeRemoveButton_Click(object sender, EventArgs e) {
			if (disp_LinkTypeList.SelectedIndex == -1) {
				return;
			}

			RemoveInstancesOfType(disp_LinkTypeList.SelectedItem.ToString(), true);

			g_CurrentGraph.m_linkTypes.RemoveAt(disp_LinkTypeList.SelectedIndex);
			disp_LinkTypeList.Items.RemoveAt(disp_LinkTypeList.SelectedIndex);

			SetupTypeBoxes();
			nodeList_SelectedIndexChanged(null, EventArgs.Empty);
		}

		// Remove any reference to now non-existent types
		void RemoveInstancesOfType(string _type, bool _link) {
			if (_link) {
				for (int i = 0; i < g_CurrentGraph.m_links.Count; i++) {
					if (g_CurrentGraph.m_links[i].m_type == _type) {
						g_CurrentGraph.m_links[i].m_type = "";
					}
				}
			} else {
				for (int i = 0; i < g_CurrentGraph.m_nodes.Count; i++) {
					if (g_CurrentGraph.m_nodes[i].m_type == _type) {
						g_CurrentGraph.m_nodes[i].m_type = "";
					}
				}
			}
		}

		private void newNodeButton_Click(object sender, EventArgs e) {
			if (string.IsNullOrEmpty(newNodeBox.Text)) {
				return;
			}

			// check we don't already have one called this
			if (nodeList.Items.Contains(newNodeBox.Text)) {
				MessageBox.Show("Node named \"" + newNodeBox.Text + "\" already exists!", "VisEdit Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			Node newnode = new Node();
			newnode.m_name = newNodeBox.Text;
			g_CurrentGraph.m_nodes.Add(newnode);
			nodeList.Items.Add(newNodeBox.Text);
			nodeList.SelectedIndex = nodeList.Items.Count - 1;

			SetupNodeLists();
		}

		private void removeNodeButton_Click(object sender, EventArgs e) {
			if(nodeList.SelectedIndex == -1) {
				return;
			}

			g_CurrentGraph.m_nodes.RemoveAt(nodeList.SelectedIndex);
			nodeList.Items.RemoveAt(nodeList.SelectedIndex);
		}

		ComboBox[] m_NodeLists;
		void SetupNodeLists() {
			m_NodeLists = new ComboBox[] {
				link_fromBox,
				link_toBox
			};

			for (int i = 0; i < m_NodeLists.Length; i++) {
				m_NodeLists[i].AutoCompleteMode = AutoCompleteMode.SuggestAppend;
				m_NodeLists[i].AutoCompleteSource = AutoCompleteSource.ListItems;
				m_NodeLists[i].Items.Clear();

				for (int j = 0; j < g_CurrentGraph.m_nodes.Count; j++) {
					m_NodeLists[i].Items.Add(g_CurrentGraph.m_nodes[j].m_name);
				}
			}
		}

		private void link_newLinkButton_Click(object sender, EventArgs e) {
			Link newLink = new Link();
			g_CurrentGraph.m_links.Add(newLink);
			link_linkList.Items.Add(newLink.ToString());

			link_linkList.SelectedIndex = link_linkList.Items.Count - 1;
		}

		private void link_removeLinkButton_Click(object sender, EventArgs e) {
			if(link_linkList.SelectedIndex == -1) {
				return;
			}

			Link lnk = g_CurrentGraph.m_links[link_linkList.SelectedIndex];
			if (lnk.m_from != null) {
				lnk.m_from.m_Links.Remove(lnk);
			}
			if (lnk.m_to != null) {
				lnk.m_to.m_Links.Remove(lnk);
			}

			g_CurrentGraph.m_links.RemoveAt(link_linkList.SelectedIndex);
			link_linkList.Items.RemoveAt(link_linkList.SelectedIndex);

			nodeList_SelectedIndexChanged(null, EventArgs.Empty);
		}

		bool bSurpressBoxChanges = false;
		private void link_linkList_SelectedIndexChanged(object sender, EventArgs e) {
			link_fromBox.Enabled = false;
			link_fromBox.Text = "";
			link_toBox.Enabled = false;
			link_toBox.Text = "";
			link_typeBox.Enabled = false;
			link_typeBox.Text = "";

			link_toButton.Enabled = false;
			link_fromButton.Enabled = false;

			if (link_linkList.SelectedIndex == -1) {
				return;
			}

			bSurpressBoxChanges = true;
			Link lnk = g_CurrentGraph.m_links[link_linkList.SelectedIndex];
			link_fromBox.Enabled = true;
			link_fromBox.SelectedItem = lnk.m_fromStr;
			link_fromBox.Text = lnk.m_fromStr;
			link_toBox.Enabled = true;
			link_toBox.SelectedItem = lnk.m_toStr;
			link_toBox.Text = lnk.m_toStr;
			link_typeBox.Enabled = true;
			link_typeBox.SelectedItem = lnk.m_type;
			link_typeBox.Text = lnk.m_type;
			bSurpressBoxChanges = false;

			link_toButton.Enabled = true;
			link_fromButton.Enabled = true;
		}

		private void link_typeBox_SelectedIndexChanged(object sender, EventArgs e) {
			if (link_linkList.SelectedIndex == -1 || link_typeBox.SelectedItem == null) {
				return;
			}

			Link lnk = g_CurrentGraph.m_links[link_linkList.SelectedIndex];
			lnk.m_type = link_typeBox.SelectedItem.ToString();

			nodeList_SelectedIndexChanged(null, EventArgs.Empty);
		}

		private void link_fromBox_SelectedIndexChanged(object sender, EventArgs e) {
			if (link_linkList.SelectedIndex == -1 || link_fromBox.SelectedItem == null || bSurpressBoxChanges) {
				return;
			}

			Link lnk = g_CurrentGraph.m_links[link_linkList.SelectedIndex];
			if (lnk.m_from != null) {
				lnk.m_from.m_Links.Remove(lnk);
			}

			lnk.m_fromStr = link_fromBox.SelectedItem.ToString();
			lnk.m_from = g_CurrentGraph.m_NodeMap[lnk.m_fromStr];

			if (lnk.m_from != null) {
				lnk.m_from.m_Links.Add(lnk);
			}

			nodeList_SelectedIndexChanged(null, EventArgs.Empty);

			int nIdx = link_linkList.SelectedIndex;
			link_linkList.SelectedIndexChanged -= linkList_SelectedIndexChanged; // HACK HACK
			link_linkList.Items[nIdx] = lnk.ToString();
			link_linkList.SelectedIndex = nIdx;
			link_linkList.SelectedIndexChanged += linkList_SelectedIndexChanged;
		}

		private void link_toBox_SelectedIndexChanged(object sender, EventArgs e) {
			if (link_linkList.SelectedIndex == -1 || link_toBox.SelectedItem == null || bSurpressBoxChanges) {
				return;
			}

			Link lnk = g_CurrentGraph.m_links[link_linkList.SelectedIndex];
			if(lnk.m_to != null) {
				lnk.m_to.m_Links.Remove(lnk);
			}

			lnk.m_toStr = link_toBox.SelectedItem.ToString();
			lnk.m_to = g_CurrentGraph.m_NodeMap[lnk.m_toStr];

			if(lnk.m_to != null) {
				lnk.m_to.m_Links.Add(lnk);
			}

			nodeList_SelectedIndexChanged(null, EventArgs.Empty);

			int nIdx = link_linkList.SelectedIndex;
			link_linkList.SelectedIndexChanged -= linkList_SelectedIndexChanged; // HACK HACK
			link_linkList.Items[nIdx] = lnk.ToString();
			link_linkList.SelectedIndex = nIdx;
			link_linkList.SelectedIndexChanged += linkList_SelectedIndexChanged;
		}

		private void link_fromButton_Click(object sender, EventArgs e) {
			if (link_linkList.SelectedIndex == -1) {
				return;
			}

			nodeList.SelectedIndex = g_CurrentGraph.m_nodes.IndexOf(g_CurrentGraph.m_links[link_linkList.SelectedIndex].m_from);
			linkList.SelectedIndex = g_CurrentGraph.m_nodes[nodeList.SelectedIndex].m_Links.IndexOf(g_CurrentGraph.m_links[link_linkList.SelectedIndex]);
			tabControl.SelectedIndex = 0;
		}

		private void link_toButton_Click(object sender, EventArgs e) {
			if (link_linkList.SelectedIndex == -1) {
				return;
			}

			nodeList.SelectedIndex = g_CurrentGraph.m_nodes.IndexOf(g_CurrentGraph.m_links[link_linkList.SelectedIndex].m_to);
			linkList.SelectedIndex = g_CurrentGraph.m_nodes[nodeList.SelectedIndex].m_Links.IndexOf(g_CurrentGraph.m_links[link_linkList.SelectedIndex]);
			tabControl.SelectedIndex = 0;
		}

		private void editLinkButton_Click(object sender, EventArgs e) {
			if (nodeList.SelectedIndex == -1 || linkList.SelectedIndex == -1) {
				return;
			}

			link_linkList.SelectedIndex = g_CurrentGraph.m_links.IndexOf(g_CurrentGraph.m_nodes[nodeList.SelectedIndex].m_Links[linkList.SelectedIndex]);
			tabControl.SelectedIndex = 1;
		}

		void PopulateLinkList() {
			link_linkList.Items.Clear();
			for(int i =0; i < g_CurrentGraph.m_links.Count; i++) {
				link_linkList.Items.Add(g_CurrentGraph.m_links[i].ToString());
			}
		}

		private void tabControl_SelectedIndexChanged(object sender, EventArgs e) {
			switch(tabControl.SelectedIndex) {
				case 0: // NODES SCREEN
					break;
				case 1: // LINKS SCREEN
					break;
			}
		}

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }
    }
}
