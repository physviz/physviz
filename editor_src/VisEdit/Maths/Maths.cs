﻿using System;

// Quick maths library for the VisEdit program
// Likely isn't very fast but it works basically the same as the Unity one
//		  which will mean I can write code for this pretty quick!

public struct Vector2 {
	private float[] _store;
	public float x {
		get { return _store[0]; }
		set { _store[0] = value; }
	}

	public float y {
		get { return _store[1]; }
		set { _store[1] = value; }
	}

	public Vector2(float _x, float _y) {
		_store = new float[2] { _x, _y };
	}

	public static Vector2 operator +(Vector2 c1, Vector2 c2) {
		return new Vector2(c1.x + c2.x, c1.y + c2.y);
	}

	public static Vector2 operator -(Vector2 c1, Vector2 c2) {
		return new Vector2(c1.x - c2.x, c1.y - c2.y);
	}

	public static Vector2 operator *(Vector2 c1, Vector2 c2) {
		return new Vector2(c1.x * c2.x, c1.y * c2.y);
	}

	public static Vector2 operator /(Vector2 c1, Vector2 c2) {
		return new Vector2(c1.x / c2.x, c1.y / c2.y);
	}

	public static Vector2 operator *(Vector2 c1, float f2) {
		return new Vector2(c1.x * f2, c1.y * f2);
	}

	public static Vector2 operator /(Vector2 c1, float f2) {
		return new Vector2(c1.x / f2, c1.y / f2);
	}

	public static bool operator ==(Vector2 c1, Vector2 c2) {
		return c1.x == c2.x && c1.y == c2.y;
	}
	
	public override bool Equals(Object c2) {
		if (c2.GetType() == typeof(Vector2)) {
			return this == (Vector2)c2;
		}
	
		return false;
	}

	public override int GetHashCode() {
		int hash = 13;
		hash = (hash * 7) + x.GetHashCode();
		hash = (hash * 7) + y.GetHashCode();
		return hash;
	}

	public static bool operator !=(Vector2 c1, Vector2 c2) {
		return c1.x != c2.x || c1.y != c2.y;
	}

	public override string ToString() {
		return string.Format("{0}, {1}", x, y);
	}

	public float this[int i] {
		get { return _store[i]; }
		set { _store[i] = value; }
	}

	public static Vector2 Parse(string _str) {
		string[] strs = _str.Split(new char[] { ',', ' ' });
		Vector2 vec2 = new Vector2();
		for (int i = 0; i < 2 && i < strs.Length; i++) {
			try {
				vec2[i] = float.Parse(strs[i]);
			} catch {
				vec2[i] = float.NaN;
			}
		}

		return vec2;
	}
}

public struct Vector3 {
	private float[] _store;
	public float x {
		get { return _store[0]; }
		set { _store[0] = value; }
	}

	public float y {
		get { return _store[1]; }
		set { _store[1] = value; }
	}

	public float z {
		get { return _store[2]; }
		set { _store[2] = value; }
	}

	public Vector3(float _x, float _y, float _z) {
		_store = new float[3] { _x, _y, _z };
	}

	public static Vector3 operator +(Vector3 c1, Vector3 c2) {
		return new Vector3(c1.x + c2.x, c1.y + c2.y, c1.z + c2.z);
	}

	public static Vector3 operator -(Vector3 c1, Vector3 c2) {
		return new Vector3(c1.x - c2.x, c1.y - c2.y, c1.z - c2.z);
	}

	public static Vector3 operator *(Vector3 c1, Vector3 c2) {
		return new Vector3(c1.x * c2.x, c1.y * c2.y, c1.z * c2.z);
	}

	public static Vector3 operator /(Vector3 c1, Vector3 c2) {
		return new Vector3(c1.x / c2.x, c1.y / c2.y, c1.z / c2.z);
	}

	public static Vector3 operator *(Vector3 c1, float f2) {
		return new Vector3(c1.x * f2, c1.y * f2, c1.z * f2);
	}

	public static Vector3 operator /(Vector3 c1, float f2) {
		return new Vector3(c1.x / f2, c1.y / f2, c1.z / f2);
	}

	public static bool operator ==(Vector3 c1, Vector3 c2) {
		return c1.x == c2.x && c1.y == c2.y && c1.z == c2.z;
	}

	public override bool Equals(Object c2) {
		if (c2.GetType() == typeof(Vector3)) {
			return this == (Vector3)c2;
		}

		return false;
	}

	public override int GetHashCode() {
		int hash = 13;
		hash = (hash * 7) + x.GetHashCode();
		hash = (hash * 7) + y.GetHashCode();
		hash = (hash * 7) + z.GetHashCode();
		return hash;
	}

	public static bool operator !=(Vector3 c1, Vector3 c2) {
		return c1.x != c2.x || c1.y != c2.y || c1.z != c2.z;
	}

	public override string ToString() {
		return string.Format("{0}, {1}, {2}", x, y, z);
	}

	public float this[int i] {
		get { return _store[i]; }
		set { _store[i] = value; }
	}

	public static Vector3 Parse(string _str) {
		string[] strs = _str.Split(new char[] { ',', ' ' });
		Vector3 vec3 = new Vector3();
		for(int i = 0; i < 3 && i < strs.Length; i++) {
			try {
				vec3[i] = float.Parse(strs[i]);
			} catch {
				vec3[i] = float.NaN;
			}
		}

		return vec3;
	}
}

public struct Vector4 {
	private float[] _store;
	public float x {
		get { return _store[0]; }
		set { _store[0] = value; }
	}

	public float y {
		get { return _store[1]; }
		set { _store[1] = value; }
	}

	public float z {
		get { return _store[2]; }
		set { _store[2] = value; }
	}

	public float w {
		get { return _store[3]; }
		set { _store[3] = value; }
	}

	public Vector4(float _x, float _y, float _z, float _w) {
		_store = new float[4] { _x, _y, _z, _w };
	}

	public static Vector4 operator +(Vector4 c1, Vector4 c2) {
		return new Vector4(c1.x + c2.x, c1.y + c2.y, c1.z + c2.z, c1.w + c2.w);
	}

	public static Vector4 operator -(Vector4 c1, Vector4 c2) {
		return new Vector4(c1.x - c2.x, c1.y - c2.y, c1.z - c2.z, c1.z - c2.z);
	}

	public static Vector4 operator *(Vector4 c1, Vector4 c2) {
		return new Vector4(c1.x * c2.x, c1.y * c2.y, c1.z * c2.z, c1.z * c2.z);
	}

	public static Vector4 operator /(Vector4 c1, Vector4 c2) {
		return new Vector4(c1.x / c2.x, c1.y / c2.y, c1.z / c2.z, c1.z / c2.z);
	}

	public static Vector4 operator *(Vector4 c1, float f2) {
		return new Vector4(c1.x * f2, c1.y * f2, c1.z * f2, c1.w * f2);
	}

	public static Vector4 operator /(Vector4 c1, float f2) {
		return new Vector4(c1.x / f2, c1.y / f2, c1.z / f2, c1.w / f2);
	}

	public static bool operator ==(Vector4 c1, Vector4 c2) {
		return c1.x == c2.x && c1.y == c2.y && c1.z == c2.z && c1.w == c2.w;
	}

	public override bool Equals(Object c2) {
		if (c2.GetType() == typeof(Vector4)) {
			return this == (Vector4)c2;
		}

		return false;
	}

	public override int GetHashCode() {
		int hash = 13;
		hash = (hash * 7) + x.GetHashCode();
		hash = (hash * 7) + y.GetHashCode();
		hash = (hash * 7) + z.GetHashCode();
		hash = (hash * 7) + w.GetHashCode();
		return hash;
	}

	public static bool operator !=(Vector4 c1, Vector4 c2) {
		return c1.x != c2.x || c1.y != c2.y || c1.z != c2.z || c1.w != c2.w;
	}

	public override string ToString() {
		return string.Format("{0}, {1}, {2}, {3}", x, y, z, w);
	}

	public float this[int i] {
		get { return _store[i]; }
		set { _store[i] = value; }
	}

	public static Vector4 Parse(string _str) {
		string[] strs = _str.Split(new char[] { ',', ' ' });
		Vector4 vec4 = new Vector4();
		for (int i = 0; i < 4 && i < strs.Length; i++) {
			try {
				vec4[i] = float.Parse(strs[i]);
			} catch {
				vec4[i] = float.NaN;
			}
		}

		return vec4;
	}
}