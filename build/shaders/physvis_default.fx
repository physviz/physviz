//--------------------------------------------------------------------------------------
// File: Tutorial07.fx
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------

cbuffer cbNeverChanges : register( b0 )
{
    matrix View;
};

cbuffer cbChangeOnResize : register( b1 )
{
    matrix Projection;
};

cbuffer cbChangesEveryFrame : register( b2 )
{
    matrix World;
    float4 vMeshColor;
	float3 particle; // xy scale z rotation
};

Texture2D txDiffuse : register( t0 );
SamplerState samLinear : register( s0 );

//--------------------------------------------------------------------------------------
struct VS_INPUT
{
    float4 Pos : POSITION;
    float4 Tex : COLOR;
};

struct PS_INPUT
{
    float4 Pos : SV_POSITION;
    float2 Tex : TEXCOORD0;
};


//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PS_INPUT VS( VS_INPUT input )
{
    PS_INPUT output = (PS_INPUT)0;
	//rebuild input from CB input of rot and scale!
	// pos * scale * rot;
	float4x4 worldViewMatrix = mul(World, View);
	float angle = particle.z;
	float2 scale = particle.xy;
	float2 posVS = input.Pos.xy * scale;
	float3 positionVS = float3(posVS.x * cos(angle) + posVS.y * sin(angle), -posVS.x * sin(angle) + posVS.y *cos(angle), 0.0f);
	positionVS +=  float3(worldViewMatrix._41, worldViewMatrix._42, worldViewMatrix._43);
	output.Pos = mul(float4(positionVS, 1.0f), Projection);
	output.Tex = float2(input.Pos.x, input.Pos.y)+ 0.5f;
    
    return output;
}


//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS( PS_INPUT input) : SV_TARGET
{
	//float4 newMesh = float4(vMeshColor.xyz, 1.0f);
	float4 colour = txDiffuse.Sample(samLinear, input.Tex)* vMeshColor;
	//colour.a = 0.5f;
	return colour;
}
